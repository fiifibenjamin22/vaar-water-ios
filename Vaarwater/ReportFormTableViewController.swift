//
//  ReportFormTableViewController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 15/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import Photos
import RxSwift
import RxCocoa
import Localize_Swift
import Alamofire

class ReportFormTableViewController: UITableViewController {
  
  private let bag = DisposeBag()
    fileprivate let image = BehaviorRelay<UIImage?>(value:nil)
    fileprivate let name = BehaviorRelay<String>(value:"")
    fileprivate let email = BehaviorRelay<String>(value:"")
    fileprivate let complaint = BehaviorRelay<String>(value:"")
  
  var isValid: Observable<Bool> {
    return Observable.combineLatest(name.asObservable(), email.asObservable(), complaint.asObservable()) { name, email, complaint in
      name.count >= 1 && email.isEmail() && complaint.count >= 1 && !complaint.starts(with: self.descriptionPlaceholder)
    }
  }
  
  fileprivate let descriptionPlaceholder = "report.description.placeholder".localized()
  
  @IBOutlet weak var descriptionText: UITextView!
  @IBOutlet weak var nameText: UITextField!
  @IBOutlet weak var emailAddressText: UITextField!
  @IBOutlet weak var phoneNumberText: UITextField!
  @IBOutlet weak var backButton: UIBarButtonItem!
  @IBOutlet weak var closeButton: UIBarButtonItem!
  @IBOutlet weak var uploadPhotoButton: UIButton!
  @IBOutlet weak var editPhotoButton: UIButton!
  @IBOutlet weak var submitButton: UIButton!
  
  let picker = UIImagePickerController()
  
  var reportType: String = ""
  
  @IBAction func uploadPhoto(_ sender: Any) {
    selectSource()
  }
  @IBAction func editPhoto(_ sender: Any) {
    if let editPhotoButton = sender as? UIButton {
      if editPhotoButton.title(for: .normal) != "report.photo.remove".localized() {
        selectSource()
      } else {
        self.image.accept(nil)
      }
    } else {
      return
    }
  }
  
  @IBAction func sendReportButton(_ sender: Any) {
    performSegue(withIdentifier: "showReportFeedback", sender: sender)
  }
  
  @IBAction func handleBackTap(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
  
  @IBAction func handleCloseTap(_ sender: Any) {
    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
    NotificationCenter.default.post(name: name, object: nil)
  }
  
  //  static func
  
  func selectSource() {
    let alertSheet = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
    let libraryAction = UIAlertAction(title: "report.photo.alert.library".localized(), style: .default) { [weak self] _ in
      self?.libraryUpload()
    }
    alertSheet.addAction(libraryAction)
    
    if UIImagePickerController.isCameraDeviceAvailable(.rear) {
      let cameraAction = UIAlertAction(title:"report.photo.alert.camera".localized(), style: .default) { [weak self] _ in
        self?.cameraUpload()
      }
      alertSheet.addAction(cameraAction)
    }
    let cancelAction = UIAlertAction(title: "button.cancel".localized(), style: .cancel, handler: nil)
    alertSheet.addAction(cancelAction)
    
    present(alertSheet, animated: true, completion: nil)
  }
  
  func cameraUpload() {
    if UIImagePickerController.isCameraDeviceAvailable(.rear) {
      
      picker.delegate = self
      picker.allowsEditing = false
      picker.sourceType = .camera
      picker.showsCameraControls = false
      picker.cameraCaptureMode = .photo
      
      let overlayView = UIView(frame: view.bounds)
      overlayView.autoresizesSubviews = true
      
      let captureButton = UIButton(frame: CGRect(x: view.bounds.size.width / 2 - 28, y: view.bounds.size.height - 63, width: 56, height: 56))
      captureButton.addTarget(self, action: #selector(captureImage), for: UIControl.Event.touchUpInside)
      captureButton.setImage(UIImage(named: "Button Capture Photo"), for: .normal)
      
      let cancelButton = UIButton(frame: CGRect(x: view.bounds.size.width - 40, y: view.bounds.size.height - 55, width: 40, height: 40))
      cancelButton.addTarget(self, action: #selector(cancelImage), for: UIControl.Event.touchUpInside)
      cancelButton.setImage(UIImage(named: "Button Close White"), for: .normal)
      
      overlayView.addSubview(captureButton)
      overlayView.addSubview(cancelButton)
      
      picker.cameraOverlayView = overlayView
      picker.cameraDevice = .rear
      
      self.present(picker, animated: true, completion: nil)
    } else {
      noCamera()
    }
  }
  
  @objc func captureImage(sender: UIButton) {
    picker.takePicture()
  }
  
  @objc func cancelImage(sender: UIButton) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  func noCamera() {
    let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertVC.addAction(okAction)
    present(alertVC, animated: true, completion: nil)
  }
  
  func libraryUpload() {
    picker.delegate = self
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    submitButton.isEnabled = false
    
    _ = nameText.rx.text.map { $0 ?? "" }
    .bind(to: name)
    
    _ = emailAddressText.rx.text.map { $0 ?? "" }
      .bind(to: email)
    
    _ = descriptionText.rx.text.map { $0 ?? "" }
      .bind(to: complaint)
    
    _ = isValid.bind(to: submitButton.rx.isEnabled)
    
    _ = isValid.subscribe(onNext: { [unowned self] isValid in
      self.submitButton.isEnabled = isValid
    })
    
    
    image.asObservable()
      .subscribe(onNext: { [weak self] photo in
        if let preview = self?.uploadPhotoButton, let editPhotoButton = self?.editPhotoButton {
          if let photo = photo {
            preview.setImage(photo, for: .normal)
            editPhotoButton.setTitle("report.photo.remove".localized(), for: .normal)
            editPhotoButton.setTitleColor(Globals.redColor, for: .normal)
          } else {
            preview.setImage(UIImage(named: "Icon Foto toevoegen"), for: .normal)
            editPhotoButton.setTitle("report.photo.add".localized(), for: .normal)
            editPhotoButton.setTitleColor(Globals.blueColor, for: .normal)
          }
        }
      })
    .disposed(by: bag)
    
    self.descriptionText.text = descriptionPlaceholder
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let controller = segue.destination as? ReportFeedbackViewController {
      let parameters: [String: String] = [
        "name": self.nameText.text ?? "",
        "type": self.reportType,
        "email": self.emailAddressText.text ?? "",
        "message": self.descriptionText.text ?? "",
        "phone": self.phoneNumberText.text ?? ""
      ]
      var imageData: Data? = nil
      
      if let image = self.image.value {
        imageData = image.jpegData(compressionQuality: 80)
      }
      controller.data = imageData
      controller.parameters = parameters
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return 7
  }
}

extension ReportFormTableViewController: UITextViewDelegate {
  func textViewDidBeginEditing(_ textView: UITextView) {
    if descriptionText.text == descriptionPlaceholder {
      descriptionText.text = nil
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if descriptionText.text.isEmpty || descriptionText.text == "" {
      descriptionText.text = descriptionPlaceholder
    }
  }
}

extension ReportFormTableViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

    if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
      self.image.accept(chosenImage)
    }
    picker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
