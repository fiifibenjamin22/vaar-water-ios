//
//  LoaderView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreGraphics

class LoadingLayer: CALayer {
    
    var parentView: UIView?
    
    override init(layer: Any) {
        super.init(layer: layer)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init(attachToView view: UIView ) {
        super.init()
        self.parentView = view
        setup()
    }
    
    func setup() {
        
        if let loaderImage = UIImage(named: "Icon Tiny Engine") {
            
            self.frame = parentView?.layer.bounds ?? bounds
            self.contents = loaderImage.cgImage
            self.contentsGravity = CALayerContentsGravity.center
            
            parentView?.layer.addSublayer(self)
            
            self.startAnimating()
        }
                
    }
    
    func startAnimating() {
        
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        // Set the animation duration appropriately
        animation.duration = 0.74
        animation.fromValue = 0
        animation.toValue = Double.pi * 2
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.repeatCount = 999
        
        // Do the actual animation
        self.add(animation, forKey: "loaderAnimation")
        
    }
    
    func stopAnimating() {
        self.isHidden = true
        self.removeAllAnimations()
        self.removeFromSuperlayer()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
