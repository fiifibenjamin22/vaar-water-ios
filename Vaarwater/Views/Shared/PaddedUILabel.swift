//
//  PaddedUILabel.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

@IBDesignable

class PaddedLabel: UILabel {

    @IBInspectable var padding: CGFloat = 0 {
        didSet {
            self.textInsets = UIEdgeInsets(top: self.padding, left: self.padding, bottom: self.padding, right: self.padding)
        }
    }
    
    var textInsets = UIEdgeInsets.zero {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var insets = self.textInsets
        let insetRect = bounds.inset(by: insets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        insets = UIEdgeInsets(top: -insets.top, left: -insets.left, bottom: -insets.bottom, right: -insets.right)
        return textRect.inset(by: insets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: self.textInsets))
    }
    
}
