//
//  TouchForwardingView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 24/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class TouchForwardingView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
//    @implementation PSPDFTouchForwardingView
//    
//    - (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    UIView *hitView = [super hitTest:point withEvent:event];
//    
//    if (hitView != self) return hitView;
//    
//    for (UIView *passthroughView in self.passthroughViews) {
//    UIView *passthroughHitView = [passthroughView hitTest:[self convertPoint:point toView:passthroughView] withEvent:event];
//    if (passthroughHitView) return passthroughHitView;
//    }
//    
//    return self;
//    }
//    
//    @end
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        let hitView = super.hitTest(point, with: event)
        
        if (hitView != self) { return hitView }
        
//        for passthroughView in self.passthroughViews {
//            let passthroughHitView = passthroughView.hitTest( self.convert(point, to: passthroughView) with: event )
//        }
        
        return self
        
    }
}
