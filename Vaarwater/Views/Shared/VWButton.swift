//
//  VWButton.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 29/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

@IBDesignable

class VWButton: UIButton {
        
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    //Normal state bg and border
    @IBInspectable var borderColor: UIColor? = Globals.lightGrayColor {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var verticalPadding:CGFloat = 8
    @IBInspectable var horizontalPadding:CGFloat = 15
    
    //Highlighted state bg and border
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        updateView()
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            let labelSize = titleLabel?.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude)) ?? CGSize.zero
            let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right + (horizontalPadding * 2), height: labelSize.height + titleEdgeInsets.top + titleEdgeInsets.bottom + (verticalPadding * 2))
            
            return desiredButtonSize
        }
    }
    
    
    func updateView() {
        if let text = self.titleLabel?.text {
            self.titleLabel?.text = text.uppercased()
        }
        self.titleLabel?.textColor = Globals.blueColor
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -2, right: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 0
        clipsToBounds = true
        
        if borderWidth > 0 {
            layer.borderColor = borderColor?.cgColor
        }
        
        if let text = self.titleLabel?.text {
            self.titleLabel?.text = text.uppercased()
        }
      self.titleLabel?.textColor = Globals.blueColor
        
    }    
    
}
