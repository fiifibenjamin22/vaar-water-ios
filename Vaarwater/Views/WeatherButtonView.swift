//
//  WeatherButtonView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 21/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Lottie

class WeatherButtonView: UIButton {
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addAnimation()
    }
    
    func updateAnimation() {
        addAnimation()
    }
    
    func addAnimation() {
        
        guard let condition = WeatherManager.sharedInstance.latestForecastCondition else {
            print("No weather condition available")
            return
        }
        
        let animation = AnimationView(name: getAnimationName( forCondition: condition))
    
        // Clear subviews
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        animation.loopMode = LottieLoopMode.loop
        animation.frame = self.bounds
        animation.contentMode = .scaleAspectFit
        animation.isUserInteractionEnabled = false
        
        self.addSubview(animation)
        
        animation.play(completion: { (finished) in
            // Do Something
        })
        
    }
    
    func getAnimationName( forCondition condition: WeatherCondition) -> String {
        
        var name:String = ""
        
        switch ( condition ) {
        case .rain:
            name = "rain_alert"
        default:
            name = "rain_alert"
        }
        
        return name
        
    }
    
}
