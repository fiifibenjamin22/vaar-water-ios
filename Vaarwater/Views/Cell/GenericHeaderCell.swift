//
//  GenericHeaderCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 24/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class GenericHeaderCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
