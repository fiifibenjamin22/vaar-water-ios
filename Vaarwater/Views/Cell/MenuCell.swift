//
//  MenuCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var nameLabel:UILabel?
    @IBOutlet weak var iconImageView:UIImageView?
    @IBOutlet weak var unreadIndicator:BadgeSwift?
    
    @IBInspectable var isEventCell:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateIndicator( count:Int ) {

        if count > 0,
            let attributedText = self.unreadIndicator?.attributedText {
            
            let attributedString = NSMutableAttributedString(attributedString: attributedText )
            attributedString.mutableString.setString( String(format: "%d", count) )
            self.unreadIndicator?.attributedText = attributedString
            self.unreadIndicator?.isHidden = false
        } else {
            self.unreadIndicator?.isHidden = true
        }
        
        self.setNeedsDisplay()
        
    }

}
