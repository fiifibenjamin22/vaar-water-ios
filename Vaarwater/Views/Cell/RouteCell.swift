//
//  RouteCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class RouteCell: UICollectionViewCell {
    
    var route: Route? {
        didSet {
            setup()
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var routeDataView: RouteDataView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }
        
    func setup() {
        
        guard let route = self.route else {
            return
        }
        
        // Set title and taglines
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.textLabel.attributedText!)
        
        if Localize.currentLanguage() == "nl" {
            self.nameLabel.text = route.name_nl
            attributedString.mutableString.setString(route.tagline_nl ?? "")
        } else {
            self.nameLabel.text = route.name_en
            attributedString.mutableString.setString(route.tagline_en ?? "")
        }
        self.textLabel?.attributedText = attributedString        
        
        // Set image
        if let mediaUrl = route.mediaUrl {
            
            // Create loading indicator
            let loadingLayer = LoadingLayer(attachToView: self.imageView)
            
            
            self.imageView.kf.setImage(with: URL(string: mediaUrl)) { result in
                switch result {
                case .success(let value):
                    print("Image: \(value.image). Got from: \(value.cacheType)")
                    loadingLayer.stopAnimating()
                    //loadingLayer.isHidden = true
                case .failure(let error):
                    print("Error: \(error)")
                    loadingLayer.stopAnimating()
                }
            }
//            self.imageView.kf.setImage(with: URL(string: mediaUrl), completionHandler: {
//                (image, error, cacheType, imageUrl) in
//                loadingLayer.stopAnimating()
//            })
            
        }
        
        // Set duration/distance
        if let duration = route.duration as? Double {
            self.routeDataView.duration = duration
        }
        
        if let distance = route.distance as? Double {
            self.routeDataView.distance = distance
        }
        
    }
    
}
