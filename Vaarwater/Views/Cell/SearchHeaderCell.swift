//
//  SearchHeaderCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class SearchHeaderCell: UITableViewCell {
    
    var family: POIFamily? {
        didSet {
            self.icon.isHidden = (family == nil)
        }
    }
    

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code        
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setup() {
        
        guard let family = self.family else {
            self.icon.isHidden = true
            self.headerLabel?.text = "search.header.default".localized()
            return
        }
        
        self.icon.isHidden = false
        if Localize.currentLanguage() == "nl" {
            self.headerLabel?.text = family.name_nl
        } else {
            self.headerLabel?.text = family.name_en
        }
        
        // set family icon
        self.icon.image = MarkerManager.getImageForFamily( forTarget: "Search", family: family.slug! )

    }
    
}
