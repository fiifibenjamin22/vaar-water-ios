//
//  SettingsTextCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 31/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class SettingsTextCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var valueField: UITextField?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
