//
//  NewsCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class NewsCell: UITableViewCell {
    
    var news: News? {
        didSet {
            setup()
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var unreadIndicator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }    
    
    func setup() {
        
        guard let news = self.news else {
            return
        }
        
        // Set name or title
        self.nameLabel?.text = news.title

        // Set date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        if Localize.currentLanguage() == "nl" {
            dateFormatter.locale = Locale(identifier: "nl_NL")
        } else {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        self.dateLabel?.text = dateFormatter.string(from: news.createdDate! as Date )
        
        // If news is read, hide the unread indicator
        if news.read {
            unreadIndicator.isHidden = true
        } else {
            unreadIndicator.isHidden = false
        }
                
    }
    
}
