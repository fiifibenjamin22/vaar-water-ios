//
//  MenuHeaderCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class MenuHeaderCell: UITableViewCell {

    @IBOutlet weak var boatImageView:UIImageView?
    @IBOutlet weak var settingsButton:UIButton?
    @IBOutlet weak var boatNameLabel:UILabel? {
        didSet {
            if boatNameLabel?.text == "" {
                boatNameLabel?.text = "VaarWater"
            }
        }
    }
    @IBOutlet weak var engineButton:UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        updateBoatData()
        updateForEvent()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func updateBoatData() {
        
        let defaults = UserDefaults.standard
        
        // Check boat data
        if let storedBoatDataDict = defaults.object(forKey: Globals.kSettingsBoatDataKey) as? Dictionary<String, AnyObject> {
            
            print("storedBoatDataDict", storedBoatDataDict)
            
            let boatData = BoatData(fromDict: storedBoatDataDict)
            
            print("boatData", boatData)
            
            if boatData.name != "" {
                boatNameLabel?.text = boatData.name
            }
            
            if boatData.engine != .unknown {
                engineButton?.setTitle(boatData.engine.description, for: .normal)
                engineButton?.setTitle(boatData.engine.description, for: .highlighted)
            }
            
        }
        
        self.setNeedsDisplay()
        
    }
    
    public func updateForEvent() {

        guard let _ = EventManager.event else {
            print("No active event")
            return
        }
        
        // Change settings button image to event-variant
        if let settingsEventImage = UIImage(named: "Button Settings Event") {
            settingsButton?.setImage(settingsEventImage, for: .normal)
            settingsButton?.setImage(settingsEventImage, for: .highlighted)
        }
        
        // Change boat image to event-variant
        if let boatEventImage = UIImage(named: "Icon Menu Boat Event") {
            boatImageView?.image = boatEventImage
        }
        boatNameLabel?.textColor = .white
        
        self.setNeedsDisplay()
        
    }

}
