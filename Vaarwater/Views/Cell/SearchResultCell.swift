//
//  SearchResultCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class SearchResultCell: UITableViewCell {
    
    var poi: POI? {
        didSet {
            setup()
        }
    }

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var distanceView: POIDistanceView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup() {
        
        guard let poi = self.poi else {
            return
        }
        
        self.nameLabel?.text = poi.name

        let locale = Localize.currentLanguage()
        if var categories = poi.categories?.allObjects as? [POICategory] {
            categories = categories.sorted( by: { $0.id < $1.id })
            if locale == "nl" {
                if let category = categories.first {
                    self.categoryLabel?.text = category.name_nl
                }
            } else {
                if let category = categories.first {
                    self.categoryLabel?.text = category.name_en
                }
            }
        }
        
        guard poi.location != nil else {
            self.distanceView?.isHidden = true
            print("poi location is nil")
            return
        }
        
        if poi.distanceToUser > 0 {
            self.distanceView?.distanceLabel.text = poi.distanceToUser.distanceToString
            self.distanceView?.isHidden = false
        } else {
            self.distanceView?.isHidden = true
        }
    }

}
