//
//  CategoryCell.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class CategoryCell: UITableViewCell {
    
    var category: POICategory? {
        didSet {
            setup()
        }
    }
    var family: POIFamily? {
        didSet {
            setup()
        }
    }
    
    var shouldShowAccessory:Bool = false
    var autoSelect:Bool = false {
        didSet {
            var iconView:UIImageView
            if shouldShowAccessory {
                if autoSelect {
                     iconView = UIImageView(image: UIImage(named: "Icon Selected Auto" )! )
                } else {
                    if self.isSelected {
                        iconView = UIImageView(image: UIImage(named: "Icon Selected On" )! )
                    } else {
                        iconView = UIImageView(image: UIImage(named: "Icon Selected Off" )! )
                    }
                }
                self.accessoryView = iconView
            }
        }
    }
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        let iconView:UIImageView
        
        if shouldShowAccessory {
            if selected {
                iconView = UIImageView(image: UIImage(named: "Icon Selected On" )! )
            } else {
                iconView = UIImageView(image: UIImage(named: "Icon Selected Off" )! )
            }
            self.accessoryView = iconView as UIView
        }
        
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }
    
    
    func setup() {
        
        if self.category != nil {
    
            if Localize.currentLanguage() == "nl" {
                
                if self.category!.name_nl == "Zwemlocatie"{
                    self.isHidden = true
                    self.translatesAutoresizingMaskIntoConstraints = true
                    self.heightAnchor.constraint(equalToConstant: 0).isActive = true
                }else if self.category!.name_nl == "Bootverhuur"{
                    self.isHidden = true
                    self.translatesAutoresizingMaskIntoConstraints = true
                    self.heightAnchor.constraint(equalToConstant: 0).isActive = true
                    
                }else if self.category!.name_nl == "BBQ plek"{
                    self.isHidden = true
                    self.translatesAutoresizingMaskIntoConstraints = true
                    self.heightAnchor.constraint(equalToConstant: 0).isActive = true
                    self.removeFromSuperview()
                    
                }else if self.category!.name_nl == "Picnicplek"{
                    self.isHidden = true
                    self.translatesAutoresizingMaskIntoConstraints = true
                    self.heightAnchor.constraint(equalToConstant: 0).isActive = true
                    
                }else{
                   
                    if self.category!.name_nl! == "Drink water\r\n"{
                        
                        self.nameLabel.text = "Drinkwater"
                        
                    }else if self.category!.name_nl! == "Picnic plek\r\n"{
                        
                        self.nameLabel.text = "Picnicplek"
                    }else{
                    
                    self.nameLabel.text = self.category!.name_nl
                    self.icon.image = MarkerManager.getImageForCategory( forTarget: "Search", category: self.category!.slug! )
                    print("CATEGORIES_LIST: \(self.category!.name_nl)")
                        
                    }
                }
                
                
                
            } else {
                self.nameLabel.text = self.category!.name_en
            }
            
        }
        
        if self.family != nil {
            
//            if self.family!.name_nl == "Bruggen en sluizen" || self.family!.name_nl == "Toiletten" || self.family!.name_nl == "Hulpdiesten" || self.family!.name_nl == "Activiteiten"{
            
            if Localize.currentLanguage() == "nl" {
                
             
                self.nameLabel.text = self.family!.name_nl
                self.icon.image = MarkerManager.getImageForFamily( forTarget: "Search", family: family!.slug! )
                print("FamSlug: \(family!.slug!)")
              
                
            } else {
                self.nameLabel.text = self.family!.name_en
            }
            self.icon.image = MarkerManager.getImageForFamily( forTarget: "Search", family: family!.slug! )
            
        }
        
        if autoSelect {
            let iconView = UIImageView(image: UIImage(named: "Icon Selected Auto" )! )
            self.accessoryView = iconView
        }
        
    }

}
