//
//  VWMapView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import GoogleMaps

enum MapState {
  case tracking
  case manual
  case traffic
}

class VWMapView: GMSMapView {
  
  // Store marker images for performance
  let markerImageLeukeDingen = MarkerManager.getImageForCategory(forTarget: "POI", category: "nice-stuff", highlighted: false)
  let markerImageEHBO = MarkerManager.getImageForCategory(forTarget: "POI", category: "first-aid", highlighted: false)
  let markerImageAanmeerplek = MarkerManager.getImageForCategory(forTarget: "POI", category: "landing-stage", highlighted: false)
  let markerImageJachthaven = MarkerManager.getImageForCategory(forTarget: "POI", category: "marina", highlighted: false)
  let markerImageHoreca = MarkerManager.getImageForCategory(forTarget: "POI", category: "hospitality", highlighted: false)
  let markerImageSupermarkt = MarkerManager.getImageForCategory(forTarget: "POI", category: "supermarket", highlighted: false)
  let markerImageAvondwinkel = MarkerManager.getImageForCategory(forTarget: "POI", category: "convenience-store", highlighted: false)
  let markerImageTankstation = MarkerManager.getImageForCategory(forTarget: "POI", category: "gas-station", highlighted: false)
  let markerImageToilet = MarkerManager.getImageForCategory(forTarget: "POI", category: "toilet", highlighted: false)
  let markerImageUrinoir = MarkerManager.getImageForCategory(forTarget: "POI", category: "urinal", highlighted: false)
  let markerImageOplaadpunt = MarkerManager.getImageForCategory(forTarget: "POI", category: "charging-facility", highlighted: false)
  let markerImageWaterpolitie = MarkerManager.getImageForCategory(forTarget: "POI", category: "water-police", highlighted: false)
  let markerImageBrandweer = MarkerManager.getImageForCategory(forTarget: "POI", category: "fire-department", highlighted: false)
  let markerImageWatertappunt = MarkerManager.getImageForCategory(forTarget: "POI", category: "watertap", highlighted: false)
  let markerImageReparatiewerkplaats = MarkerManager.getImageForCategory(forTarget: "POI", category: "boat-repair", highlighted: false)
  let markerImageTrailerhelling = MarkerManager.getImageForCategory(forTarget: "POI", category: "slipway", highlighted: false)
  let markerImageNautischeWinkel = MarkerManager.getImageForCategory(forTarget: "POI", category: "nautical-store", highlighted: false)
  let markerImageBootverhuur = MarkerManager.getImageForCategory(forTarget: "POI", category: "boat-rental", highlighted: false)
  let markerImageZwemlocatie = MarkerManager.getImageForCategory(forTarget: "POI", category: "swim-location", highlighted: false)
  let markerImageVasteBrug = MarkerManager.getImageForCategory(forTarget: "POI", category: "set-bridge", highlighted: false)
  let markerImageBeweegbareBrug = MarkerManager.getImageForCategory(forTarget: "POI", category: "mobile-bridge", highlighted: false)
  let markerImageMelding = MarkerManager.getImageForCategory(forTarget: "POI", category: "report", highlighted: false)
  let markerImageStremming = MarkerManager.getImageForCategory(forTarget: "POI", category: "traffic-block", highlighted: false)
  let markerImageAfvalboot = MarkerManager.getImageForCategory(forTarget: "POI", category: "trash-boat", highlighted: false)
  let markerImageInvaarverbod = MarkerManager.getImageForCategory(forTarget: "POI", category: "no-entry", highlighted: false)
  let markerImageAnker = MarkerManager.getImageForCategory(forTarget: "POI", category: "anchor", highlighted: false)
  let markerImageLageBrug = MarkerManager.getImageForCategory(forTarget: "POI", category: "low-bridge", highlighted: false)
  let markerImageBoei = MarkerManager.getImageForCategory(forTarget: "POI", category: "buoy", highlighted: false)
  
  let markerImageKoffiehuis = MarkerManager.getImageForCategory(forTarget: "POI", category: "coffeehouse", highlighted: false)
  let markerImageBakkerij = MarkerManager.getImageForCategory(forTarget: "POI", category: "bakery", highlighted: false)
  let markerImageAED = MarkerManager.getImageForCategory(forTarget: "POI", category: "aed-defibrillator", highlighted: false)
  let markerImageInformatie = MarkerManager.getImageForCategory(forTarget: "POI", category: "information", highlighted: false)
  
  let markerImageBar = MarkerManager.getImageForCategory(forTarget: "POI", category: "bar", highlighted: false)
  let markerImageSluis = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: false)
  let markerImageIJssalon = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: false)
  let markerImageApotheek = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: false)
  let markerImagePicnic = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: false)
  let markerImageBBQ = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: false)
  let markerImageDrinkwater = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: false)
  
  // Store marker images for performance
  let markerImageLeukeDingenHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "nice-stuff", highlighted: true)
  let markerImageEHBOHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "first-aid", highlighted: true)
  let markerImageAanmeerplekHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "landing-stage", highlighted: true)
  let markerImageJachthavenHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "marina", highlighted: true)
  let markerImageHorecaHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "hospitality", highlighted: true)
  let markerImageSupermarktHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "supermarket", highlighted: true)
  let markerImageAvondwinkelHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "convenience-store", highlighted: true)
  let markerImageTankstationHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "gas-station", highlighted: true)
  let markerImageToiletHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "toilet", highlighted: true)
  let markerImageUrinoirHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "urinal", highlighted: true)
  let markerImageOplaadpuntHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "charging-facility", highlighted: true)
  let markerImageWaterpolitieHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "water-police", highlighted: true)
  let markerImageBrandweerHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "fire-department", highlighted: true)
  let markerImageWatertappuntHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "watertap", highlighted: true)
  let markerImageReparatiewerkplaatsHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "boat-repair", highlighted: true)
  let markerImageTrailerhellingHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "slipway", highlighted: true)
  let markerImageNautischeWinkelHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "nautical-store", highlighted: true)
  let markerImageBootverhuurHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "boat-rental", highlighted: true)
  let markerImageZwemlocatieHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "swim-location", highlighted: true)
  let markerImageVasteBrugHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "set-bridge", highlighted: true)
  let markerImageBeweegbareBrugHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "mobile-bridge", highlighted: true)
  let markerImageAfvalbootHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "trash-boat", highlighted: true)
  let markerImageAnkerHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "anchor", highlighted: true)
  let markerImageKoffiehuisHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "coffeehouse", highlighted: true)
  let markerImageBakkerijHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "bakery", highlighted: true)
  let markerImageAEDHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "aed-defibrillator", highlighted: true)
  let markerImageInformatieHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "information", highlighted: true)
  let markerImageBarHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "bar", highlighted: true)
  let markerImageSluisHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "lock", highlighted: true)
  let markerImageIJssalonHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "icecream-parlor", highlighted: true)
  let markerImageApotheekHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "pharmacy", highlighted: true)
  let markerImagePicnicHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "picnic", highlighted: true)
  let markerImageBBQHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "bbq", highlighted: true)
  let markerImageDrinkwaterHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "drinking-water", highlighted: true)
  
  let markerImageEmpty = MarkerManager.getImageForCategory(forTarget: "POI", category: "", highlighted: false)
  let markerImageEmptyHighlighted = MarkerManager.getImageForCategory(forTarget: "POI", category: "", highlighted: true)
  
  
  var userMarker:GMSMarker = GMSMarker()
  
  var poiMarkers:Array = [GMSMarker]()
  var reportPolylines:Array = [GMSPolyline]()
  
  var trafficPolylines:Array = [GMSPolyline]()
  
  var routePolylines:Array = [GMSPolyline]()
  
  var lastCameraPosition:GMSCameraPosition?
  var anchorMarker:GMSMarker = GMSMarker()
  
  var buoyMarker:GMSMarker = GMSMarker()
  
  var state:MapState = .tracking {
    didSet {
      updateForState()
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init( coder: aDecoder )
    setup()
  }
  
  internal func setup() {
    
    // set styling
    do {
      // Set the map style by passing the URL of the local file.
      if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
        self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
      } else {
        print("Unable to find style.json")
      }
    } catch {
      print("One or more of the map styles failed to load. \(error)")
    }
    
    // Create a GMSCameraPosition that tells the map to display at a default point
    let camera = GMSCameraPosition.camera(withLatitude: 52.370216, longitude: 4.895168, zoom: Globals.kDefaultZoom)
    
    // hook camera to self view
    self.camera = camera
    
    // Set additional options/settings for mapView
    self.isMyLocationEnabled = true
    //        self.isBuildingsEnabled = false
    self.isTrafficEnabled = false
    self.settings.indoorPicker = false
    self.settings.myLocationButton = false
    self.settings.rotateGestures = false
    self.settings.tiltGestures = false
    
  }
  
  public func addPOIToMap( poi: POI ) -> GMSMarker? {
    
    guard getMarkersForPOI(poi: poi).count == 0, let location = poi.location else {
      //            print("Map marker already exists or has no location", poi.slug)
      return nil
    }
    
    let marker = GMSMarker()
    
    if !poi.fitsThrough {
      marker.icon = getMarkerImageForCategory(slug: "low-bridge", highlighted: false)
    } else {
      
      if let categories = poi.categories?.allObjects as? [POICategory],
        let slug = categories.first?.slug {
        
        
        marker.icon = getMarkerImageForCategory(slug: slug, highlighted: false)
      }
    }
    
    //        marker.icon = getMarkerImageForCategory(poi: poi, highlighted: false)
    marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
    marker.position = location.coordinate
    marker.map = self
    marker.userData = poi
    
    poiMarkers.append(marker)
    
    return marker
    
  }
  
  public func addAnchorToMap( forLocation location: CLLocation ) -> GMSMarker? {
    
    anchorMarker = GMSMarker()
    anchorMarker.icon = getMarkerImageForCategory(slug: "anchor", highlighted: true)
    anchorMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
    anchorMarker.position = location.coordinate
    anchorMarker.map = self
    
    return anchorMarker
    
  }
  
  public func addBuoyToMap( forLocation location: CLLocation ) {
    
    if let currentLocation = LocationManager.sharedInstance.currentLocation {
      let distanceToBuoy = location.distance(from: currentLocation)
      if distanceToBuoy < Globals.kBuoyDistanceThreshold {
        buoyMarker.map = nil
        return
      }
    }
    
    buoyMarker.icon = getMarkerImageForCategory(slug: "buoy", highlighted: true)
    buoyMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
    buoyMarker.position = location.coordinate
    buoyMarker.map = self
    
  }
  
  public func addReportToMap( poi: POI ) {
    
    if getMarkersForPOI(poi: poi).count > 0 {
      print("Map markers for report already exist")
      return
    }
    
    // If the poi has a single location, add it like a regular POI
    guard poi.location == nil else {
      let _ = self.addPOIToMap( poi: poi )
      return
    }
    
    // check if poi has positions attached then, and if not, just return
    guard var positions = poi.positions?.allObjects as? [GeoPosition] else {
      print("Report has no positions")
      return
    }
    
    // Sort positions by id
    positions = positions.sorted( by: { $0.id < $1.id })
    
    let path = GMSMutablePath()
    for (index, position) in positions.enumerated() {
      path.add(CLLocationCoordinate2D(latitude: position.latitude, longitude: position.longitude))
      
      if index == 0 || index == (positions.count-1) {
        let marker = GMSMarker()
        
        if let categories = poi.categories?.allObjects as? [POICategory],
          let slug = categories.first?.slug {
          marker.icon = getMarkerImageForCategory(slug: slug, highlighted: false)
        }
        
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.position = position.location.coordinate
        marker.map = self
        marker.userData = poi
        
        poiMarkers.append(marker)
      }
    }
    
    let polyline = GMSPolyline(path: path)
    polyline.strokeColor = UIColor.red
    polyline.strokeWidth = 2
    polyline.map = self
    polyline.userData = poi
    
    var styles = [GMSStrokeStyle]()
    if let categories = poi.categories?.allObjects as? [POICategory] {
      if let category = categories.first {
        if (category.slug == "report" || category.slug == "delay") {
          styles.append(GMSStrokeStyle.solidColor(Globals.blueColor))
        } else {
          styles.append(GMSStrokeStyle.solidColor(Globals.redColor))
        }
      }
    }
    styles.append(GMSStrokeStyle.solidColor(UIColor.clear))
    
    let lengths:[NSNumber] = [
      NSNumber(value: 15.0),
      NSNumber(value: 10.0)
    ]
    polyline.spans = GMSStyleSpans(polyline.path!, styles, lengths, .rhumb)
    reportPolylines.append( polyline)
    
  }
  
  public func addTrafficToMap() {
    
    // If the poi has a single location, add it like a regular POI
    guard let trafficLink = TrafficManager.sharedInstance.currentTrafficLink else {
      return
    }
    
    // Draw own positions when traffic is
    if let traffic = trafficLink["traffic"] as? Int,
      traffic >= TrafficManager.kTrafficThreshold,
      var positions = trafficLink["positions"] as? Array<Dictionary<String, AnyObject>> {
      
      // Sort positions by id
      positions = positions.sorted( by: { ($0["id"] as! Int) < ($1["id"] as! Int) })
      
      // Get path and draw polyline
      let path = drawPath(forPositions: positions as [AnyObject])
      let polyline = GMSPolyline(path: path)
      polyline.strokeColor = UIColor.red.withAlphaComponent(0.6)
      polyline.strokeWidth = 6
      polyline.map = self
      
      trafficPolylines.append( polyline)
      
    }
    
    // check if poi has positions attached then, and if not, just return
    guard let neighbouring = trafficLink["neighbouring"] as? Array<Dictionary<String, AnyObject>> else {
      return
    }
    
    // Loop through neighbours to draw
    for neighbour in neighbouring {
      
      guard var positions = neighbour["positions"] as? Array<Dictionary<String, AnyObject>> else {
        break
      }
      
      if let traffic = neighbour["traffic"] as? Int,
        traffic >= TrafficManager.kTrafficThreshold {
        
        print("Traffic detected, positions for neighbour", positions )
        
        // Sort positions by id
        positions = positions.sorted( by: { ($0["id"] as! Int) < ($1["id"] as! Int) })
        
        // Get path and draw polyline
        let path = drawPath(forPositions: positions as [AnyObject])
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.red.withAlphaComponent(0.6)
        polyline.strokeWidth = 6
        polyline.map = self
        
        trafficPolylines.append( polyline)
        
      }
    }
  }
  
  public func deleteTrafficFromMap() {
    
    // Remove polylines from map
    for polyline in trafficPolylines {
      polyline.map = nil
    }
    
    // Reset trafficPolylines array
    self.trafficPolylines = [GMSPolyline]()
    
  }
  
  public func addRouteToMap( route: Route ) {
    
    // check if poi has positions attached then, and if not, just return
    guard var positions = route.positions?.allObjects as? [GeoPosition] else {
      print("Report has no positions")
      return
    }
    
    // Remove optional other route polylines
    self.deleteRouteFromMap()
    
    // Sort positions by id
    positions = positions.sorted( by: { $0.id < $1.id })
    
    let path = GMSMutablePath()
    for (_, position) in positions.enumerated() {
      path.add(CLLocationCoordinate2D(latitude: position.latitude, longitude: position.longitude))
    }
    
    let outlinePolyline = GMSPolyline(path: path)
    outlinePolyline.strokeColor = UIColor.white
    outlinePolyline.strokeWidth = 6
    outlinePolyline.map = self
    
    let routePolyline = GMSPolyline(path: path)
    routePolyline.strokeColor = UIColor(red: 98/255.0, green: 199/255.0, blue: 72/255.0, alpha: 1.0)
    routePolyline.strokeWidth = 4
    routePolyline.map = self
    
    routePolylines.append( outlinePolyline )
    routePolylines.append( routePolyline )
    
  }
  
  public func addItineraryToMap(itinerary: Itinerary) {
    
    self.deleteRouteFromMap()
    
    let path = GMSMutablePath()
    
    for waypoint in itinerary.waypoints {
      
      if let coordinate = waypoint.location {
        path.add(coordinate)
      }
    }
    
    let outlinePolyline = GMSPolyline(path: path)
    outlinePolyline.strokeColor = UIColor.white
    outlinePolyline.strokeWidth = 6
    outlinePolyline.map = self
    
    let itineraryPolyline = GMSPolyline(path: path)
    itineraryPolyline.strokeColor = UIColor(red: 98/255.0, green: 199/255.0, blue: 72/255.0, alpha: 1.0)
    itineraryPolyline.strokeWidth = 4
    itineraryPolyline.map = self
    
    routePolylines.append(outlinePolyline)
    routePolylines.append(itineraryPolyline)
  }
  
  public func deleteRouteFromMap() {
    for polyline in routePolylines {
      polyline.map = nil
    }
    routePolylines.removeAll()
  }
  
  public func selectPOIOnMap( poi: POI ) {
    
    // Check if the poi has a marker, and if so:
    var marker = getMarkersForPOI(poi: poi).first
    if marker == nil {
      marker = self.addPOIToMap(poi: poi)
    }
    
    guard marker != nil else {
      print("Something went wrong, still no poi on map")
      return
    }
    
    // Store last cameraposition just in case
    self.lastCameraPosition = self.camera
    
    // Change image to Highlighted
    if !poi.fitsThrough {
      marker?.icon = getMarkerImageForCategory(slug: "low-bridge", highlighted: false)
    } else {
      if let categories = poi.categories?.allObjects as? [POICategory],
        let slug = categories.first?.slug,
        let image = getMarkerImageForCategory(slug: slug, highlighted: true) {
        marker?.icon = image
      }
    }
    
    //        let markerImage = getMarkerImageForCategory(poi: poi, highlighted: true)
    
    guard let location = poi.location else {
      return
    }
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
      self.zoomToLocation(location: location)
    })
  }
  
  public func deselectPOIOnMap( poi: POI ) {
    
    let defaults = UserDefaults.standard
    let preferredArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as! Array<String>
    
    // Check if the poi has a marker, and if so:
    for marker in getMarkersForPOI(poi: poi) {
      
      guard let categories = poi.categories?.allObjects as? [POICategory] else {
        print("Geen categories gevonden")
        deleteMarkerFromMap(marker: marker)
        return
      }
      
      var shouldRemoveFromMap: Bool = !poi.pinned && !poi.anchor
      for category in categories {
        
        // Check if it matches preferred settings, and if so, don't remove
        if let slug = category.slug {
          
          // Check low bridges
          if slug == "set-bridge",
            !poi.fitsThrough {
            
            shouldRemoveFromMap = false
            break
          }
          
          if preferredArray.contains( String(format: "category_\(slug)") ) {
            shouldRemoveFromMap = false
            break
          }
        }
        
        if let familySlug = category.family?.slug {
          if preferredArray.contains( String(format: "family_\(familySlug)") ) {
            shouldRemoveFromMap = false
            break
          }
        }
        
        if category.map_default || Globals.kCategoriesForReports.contains(category.slug!) {
          shouldRemoveFromMap = false
          break
        }
      }
      
      if shouldRemoveFromMap {
        deleteMarkerFromMap(marker: marker)
      } else {
        
        if !poi.fitsThrough {
          let image = getMarkerImageForCategory(slug: "low-bridge", highlighted: false)
          marker.icon = image
        } else {
          
          // Change image back to non-Highlighted
          if let categories = poi.categories?.allObjects as? [POICategory],
            let slug = categories.first?.slug,
            let image = getMarkerImageForCategory(slug: slug, highlighted: false) {
            marker.icon = image
          }
        }
      }
    }
  }
  
  public func handlePOIPinned( notification:Notification ) {
    
    if let poi = notification.userInfo?["poi"] as? POI {
      _ = self.addPOIToMap(poi: poi)
    }
  }
  
  public func handlePOIUnpinned( notification:Notification ) {
    
    if let poi = notification.userInfo?["poi"] as? POI {
      for marker in getMarkersForPOI(poi: poi) {
        if !poi.pinned {
          self.deleteMarkerFromMap(marker: marker)
        }
      }
    }
  }
  
  public func deleteMarkerFromMap( marker: GMSMarker ) {
    
    if let markerIndex = poiMarkers.firstIndex(of: marker) {
      poiMarkers.remove(at: markerIndex )
      marker.map = nil
    }
  }
  
  public func deletePOIFromMap( poi: POI ) {
    
    // Else, remove the POI marker and distancelabel
    for marker in getMarkersForPOI(poi: poi) {
      deleteMarkerFromMap(marker: marker)
    }
  }
  
  public func deleteAnchorFromMap() {
    //anchorMarker.map = nil
    anchorMarker.map?.clear()
  }
  
  public func deleteAllPOIs() {
    for marker in poiMarkers {
      marker.map = nil
    }
    
    // Clear array
    poiMarkers = [GMSMarker]()
  }
  
  func drawPath( forPositions positions:[AnyObject]) -> GMSMutablePath {
    
    let path = GMSMutablePath()
    for (_, position) in positions.enumerated() {
      
      if let latValue = position["geo_lat"] as? String,
        let lngValue = position["geo_lng"] as? String,
        let latitude = Double(latValue),
        let longitude = Double(lngValue) {
        
        path.add(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
      }
    }
    
    return path
    
  }
  
  func getMarkerImageForCategory( slug: String , highlighted: Bool = false ) -> UIImage? {
    
    var markerIcon:UIImage? = nil
    
    switch (slug) {
    case "nice-stuff":
      markerIcon = highlighted ? markerImageLeukeDingenHighlighted : markerImageLeukeDingen
    case "landing-stage":
      markerIcon = highlighted ? markerImageAanmeerplekHighlighted : markerImageAanmeerplek
    case "marina":
      markerIcon = highlighted ? markerImageJachthavenHighlighted : markerImageJachthaven
    case "supermarket":
      markerIcon = highlighted ? markerImageSupermarktHighlighted : markerImageSupermarkt
    case "convenience-store":
      markerIcon = highlighted ? markerImageAvondwinkelHighlighted : markerImageAvondwinkel
    case "hospitality":
      markerIcon = highlighted ? markerImageHorecaHighlighted : markerImageHoreca
    case "toilet":
      markerIcon = highlighted ? markerImageToiletHighlighted : markerImageToilet
    case "urinal":
      markerIcon = highlighted ? markerImageUrinoirHighlighted : markerImageUrinoir
    case "swim-location":
      markerIcon = highlighted ? markerImageZwemlocatieHighlighted : markerImageZwemlocatie
    case "first-aid":
      markerIcon = highlighted ? markerImageEHBOHighlighted : markerImageEHBO
    case "gas-station":
      markerIcon = highlighted ? markerImageTankstationHighlighted : markerImageTankstation
    case "charging-facility":
      markerIcon = highlighted ? markerImageOplaadpuntHighlighted : markerImageOplaadpunt
    case "water-police":
      markerIcon = highlighted ? markerImageWaterpolitieHighlighted : markerImageWaterpolitie
    case "fire-department":
      markerIcon = highlighted ? markerImageBrandweerHighlighted : markerImageBrandweer
    case "watertap":
      markerIcon = highlighted ? markerImageWatertappuntHighlighted : markerImageWatertappunt
    case "boat-repair":
      markerIcon = highlighted ? markerImageReparatiewerkplaatsHighlighted : markerImageReparatiewerkplaats
    case "slipway":
      markerIcon = highlighted ? markerImageTrailerhellingHighlighted : markerImageTrailerhelling
    case "nautical-store":
      markerIcon = highlighted ? markerImageNautischeWinkelHighlighted : markerImageNautischeWinkel
    case "boat-rental":
      markerIcon = highlighted ? markerImageBootverhuurHighlighted : markerImageBootverhuur
    case "set-bridge":
      markerIcon = highlighted ? markerImageVasteBrugHighlighted : markerImageVasteBrug
    case "mobile-bridge":
      markerIcon = highlighted ? markerImageBeweegbareBrugHighlighted : markerImageBeweegbareBrug
    case "report":
      markerIcon = highlighted ? markerImageMelding : markerImageMelding
    case "traffic-block":
      markerIcon = highlighted ? markerImageStremming : markerImageStremming
    case "delay":
      markerIcon = highlighted ? markerImageMelding : markerImageMelding
    case "impediment":
      markerIcon = highlighted ? markerImageStremming : markerImageStremming
    case "trash-boat":
      markerIcon = highlighted ? markerImageAfvalbootHighlighted : markerImageAfvalboot
    case "no-entry":
      markerIcon = highlighted ? markerImageInvaarverbod : markerImageInvaarverbod
    case "anchor":
      markerIcon = highlighted ? markerImageAnkerHighlighted : markerImageAnker
    case "low-bridge":
      markerIcon = highlighted ? markerImageLageBrug : markerImageLageBrug
    case "buoy":
      markerIcon = highlighted ? markerImageBoei : markerImageBoei
    case "bar":
      markerIcon = highlighted ? markerImageBarHighlighted : markerImageBar
    case "lock":
      markerIcon = highlighted ? markerImageSluisHighlighted : markerImageSluis
    case "coffeehouse":
      markerIcon = highlighted ? markerImageKoffiehuisHighlighted : markerImageKoffiehuis
    case "bakery":
      markerIcon = highlighted ? markerImageBakkerijHighlighted : markerImageBakkerij
    case "aed-defibrillator":
      markerIcon = highlighted ? markerImageAEDHighlighted : markerImageAED
    case "information":
      markerIcon = highlighted ? markerImageInformatieHighlighted : markerImageInformatie
    case "ice-cream-parlor":
      markerIcon = highlighted ? markerImageIJssalonHighlighted : markerImageIJssalon
    case "pharmacy":
      markerIcon = highlighted ? markerImageApotheekHighlighted : markerImageApotheek
    case "picnic":
      markerIcon = highlighted ? markerImagePicnicHighlighted : markerImagePicnic
    case "bbq":
      markerIcon = highlighted ? markerImageBBQHighlighted : markerImageBBQ
    case "drinking-water":
      markerIcon = highlighted ? markerImageDrinkwaterHighlighted : markerImageDrinkwater
      
      
    default:
      markerIcon = highlighted ? markerImageEmptyHighlighted : markerImageEmpty
    }
    
    return markerIcon
  }
  
  public func getMarkersForPOI( poi: POI )-> [GMSMarker] {
    return poiMarkers.filter({ $0.userData as! POI == poi })
  }
  
  public func updateMarkers() {
    
    for marker in poiMarkers {
      
      if let poi = marker.userData as? POI,
        let categories = poi.categories?.allObjects as? [POICategory],
        let slug = categories.first?.slug {
        
        marker.icon = getMarkerImageForCategory(slug: slug, highlighted: false)
        
      }
    }
  }
  
  public func showVisibleMarkers() {
    
    // Get latlng bounds
    let visibleRegion = self.projection.visibleRegion()
    let regionBounds = GMSCoordinateBounds(region: visibleRegion)
    
    let currentZoomLevel = self.camera.zoom
    
    // loop through the items to check their visibility
    for marker in poiMarkers {
      
      let markerCoordinates = (marker as GMSMarker).position
      
      // Either attach to map, or nil, according to coordination containment
      if regionBounds.contains(markerCoordinates) && currentZoomLevel >= Globals.kMinimumZoom,
        self.state != .traffic {
        marker.map = self
      } else {
        marker.map = nil
      }
    }
  }
  
  public func updateMapToHeading( locationDirection: CLLocationDirection ) {
    // Animate/rotate map according to new bearing
    self.animate(toBearing: locationDirection )
  }
  
  public func updateForLocation( location: CLLocation ) {
    
    let cameraPosition = GMSCameraPosition(target: location.coordinate, zoom: Globals.kTrackingZoom, bearing: 0, viewingAngle: 0)
    let cameraUpdate = GMSCameraUpdate.setCamera(cameraPosition )
    
    // Set the user marker
    //        self.userMarker.position = location.coordinate
    
    self.padding = .zero
    
    // Animate the map
    self.animate(with: cameraUpdate )
    
    
    // Draw buoy for route
    if let _ = RouteManager.sharedInstance.currentActiveRoute,
      let nearestLocation = RouteManager.sharedInstance.nearestLocation {
      
      addBuoyToMap(forLocation: nearestLocation)
      
    }
  }
  
  func updateForState() {
    
    if state == .traffic {
      // draw traffic segments
      addTrafficToMap()
      zoomToTrafficBounds()
    } else {
      // Remove traffic polylines
      deleteTrafficFromMap()
      
      if state == .tracking {
        if let location = LocationManager.sharedInstance.currentLocation {
          zoomToLocation(location: location)
        }
      }
    }
  }
  
  public func zoomToLocation( location:CLLocation ) {
    
    let cameraPosition = GMSCameraPosition(target: location.coordinate, zoom: Globals.kTrackingZoom, bearing: 0, viewingAngle: 0)
    let cameraUpdate = GMSCameraUpdate.setCamera(cameraPosition)
    
    self.padding = UIEdgeInsets.init(top: 0, left: 0, bottom: self.bounds.height * 0.3, right: 0)
    
    // Animate the map
    self.animate(with: cameraUpdate )
  }
  
  public func zoomToTrafficBounds() {
    
    var bounds = GMSCoordinateBounds()
    for polyline in trafficPolylines {
      if let path = polyline.path {
        bounds = bounds.includingPath(path)
      }
    }
    
    self.padding = UIEdgeInsets.init(top: 0, left: 0, bottom: self.bounds.height * 0.3, right: 0)
    
    let cameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 100)
    //        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(0, 0, self.bounds.height * 0.5, 0))
    //        let cameraUpdate = GMSCameraUpdate.zoom(to: 15)
    self.animate(with: cameraUpdate)
    
  }
  
  public func zoomToActiveRoute() {
    
    guard routePolylines.count > 0 else {
      print("no active route")
      return
    }
    
    var bounds = GMSCoordinateBounds()
    for polyline in routePolylines {
      if let path = polyline.path {
        bounds = bounds.includingPath(path)
      }
    }
    
    let cameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 30)
    //        self.moveCamera(cameraUpdate)
    self.animate(with: cameraUpdate)
    
  }
  
  
}
