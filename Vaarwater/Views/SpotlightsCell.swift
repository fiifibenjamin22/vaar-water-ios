//
//  SpotlightsCell.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 10/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class SpotlightsCell: UICollectionViewCell {
    
    var downloadTask: URLSessionDownloadTask?
    
    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var spotlightsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderWidth = 2
        layer.borderColor = UIColor.white.cgColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        downloadTask?.cancel()
        downloadTask = nil
    }
    
    // MARK: - Public Methods
    
    func configure(for spotlight: Spotlight) {
        
        spotlightsLabel.text = Localize.currentLanguage() == "nl" ? spotlight.name_nl : spotlight.name_en
        
        if let thumbImageUrl = URL(string: spotlight.thumb_medium ?? "") {
            downloadTask = thumb.loadImage(url: thumbImageUrl)
        }
    }
    
}
