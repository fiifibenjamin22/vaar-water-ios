//
//  DesignableUITextField.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableUITextField: UITextField {
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    @IBInspectable var leftImageSize: CGFloat = 40 {
        didSet {
            updateView()
        }
    }    
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var topPadding: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var borderThickness: CGFloat = 2 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            updateView()
        }
    }
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.textRect(forBounds: bounds)
        textRect.size.height += topPadding
        return textRect
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds:bounds)
    }
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    func updateView() {
        
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: leftImageSize, height: leftImageSize))
            imageView.image = image
            
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderThickness
        self.layer.borderColor = borderColor.cgColor
        self.clipsToBounds = true
        
        self.borderStyle = .roundedRect
        
//        self.setNeedsDisplay()
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): color]))
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
