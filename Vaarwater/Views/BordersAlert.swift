//
//  OutOfBordersAlert.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 01/06/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class BordersAlert: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
  
  @IBOutlet weak var closeButton: UIButton!
  @IBOutlet weak var okButton: VWButton!
  @IBOutlet weak var top: UIView!
  @IBOutlet weak var bottom: UIView!
    
  @IBOutlet weak var primaryInformationText: UILabel!
  @IBOutlet weak var informationText: UITextView!
  
  
  override func awakeFromNib() {
    if Localize.currentLanguage() == "nl" {
        
        primaryInformationText.text = " Op dit moment bent u buiten het gebied van de Vaarwater app."
        
        
        
      informationText.text = """
        De gemeente Amsterdam breidt het gebied voor de Vaarwater App steeds verder uit, zodat je ook wanneer je buiten Amsterdam vaart de Vaarwater App kunt gebruiken.
        
        Voor nu vaar-wel!
        
        """
    }else{
        
        primaryInformationText.text = "You are currently out of the VaarWater app area. The city of Amsterdam is continously updating and expanding the VaarWater app with areas outside the city boundaries.."
        
        
        informationText.text = """
        The information in VaarWater is only available in Amsterdam and its surroundings.
                 Come (back) soon
        
        """
        
        
        
    }
  }
  
  private static var sharedView: BordersAlert!
  
  static func loadFromNib() -> BordersAlert {
    let nibName = "\(self)".split{ $0 == "." }.map(String.init).last!
    let nib = UINib(nibName: nibName, bundle: nil)
    return nib.instantiate(withOwner: self, options: nil).first as! BordersAlert
  }
  
  static func showIn(viewController: UIViewController) {
    let displayVC = viewController
    
    if sharedView == nil {
      sharedView = loadFromNib()
      sharedView.layer.masksToBounds = false
      sharedView.layer.shadowColor = UIColor.darkGray.cgColor
      sharedView.layer.shadowOpacity = 1
      sharedView.layer.shadowOffset = CGSize(width: 0, height: 3)
      sharedView.layer.borderColor = UIColor.white.cgColor
      sharedView.layer.borderWidth = 2
      sharedView.layer.cornerRadius = 10
      
      sharedView.top.layer.cornerRadius = 10
      sharedView.bottom.layer.cornerRadius = 10
      
    }
    
    if sharedView?.superview == nil {
      let y = displayVC.view.frame.height - sharedView.frame.size.height - 12
      sharedView.frame = CGRect(x: 12, y: y, width: displayVC.view.frame.size.width - 24, height: sharedView.frame.size.height)
      sharedView.alpha = 0.0
      
      displayVC.view.addSubview(sharedView)
      sharedView.fadeIn()
      
      // this call needs to be counter balanced on fadeOut [1]
      sharedView.perform(#selector(fadeOut), with: nil, afterDelay: 5.0)
    }

  }

  @IBAction func closePressed(_ sender: UIButton) {
    fadeOut()
  }
  
  
  // MARK: Animations
  func fadeIn() {
    UIView.animate(withDuration: 0.33, animations: {
      self.alpha = 1.0
    })
  }
  
  @objc func fadeOut() {
    
    // [1] Counter balance previous perfom:with:afterDelay
    NSObject.cancelPreviousPerformRequests(withTarget: self)
    
    UIView.animate(withDuration: 0.33, animations: {
      self.alpha = 0.0
    }, completion: { _ in
      self.removeFromSuperview()
    })
  }

}
