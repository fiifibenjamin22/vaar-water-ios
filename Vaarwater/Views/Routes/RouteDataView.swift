//
//  POIDistanceView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class RouteDataView: DesignableUIView {
    
    @IBInspectable var cornerRadius: CGFloat = 6 {
        didSet {
            updateUI()
        }
    }
    @IBInspectable var borderColor: UIColor = Globals.lightGrayColor {
        didSet {
            updateUI()
        }
    }
    @IBInspectable var borderWidth: CGFloat = 1 {
        didSet {
            updateUI()
        }
    }
    @IBInspectable var textColor: UIColor = Globals.darkGrayColor {
        didSet {
            updateUI()
        }
    }
    
    var distance:Double = 0 {
        didSet {
            if distance > 0 {
                self.label?.text = String(format: "%@ / %@", duration.durationToString, distance.distanceToString)
            }
            self.setNeedsDisplay()
        }
    }
    
    var duration:Double = 0 {
        didSet {
            if duration > 0 {
                self.label?.text = String(format: "%@ / %@", duration.durationToString, distance.distanceToString)
            }
            self.setNeedsDisplay()
        }
    }

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        updateUI()
    }
    
    func updateUI() {
        
        self.backgroundColor = UIColor.clear
        self.label?.textColor = textColor
        
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        
        self.label?.text = distance.distanceToString
        
        setNeedsDisplay()
    }

}
