//
//  POIDistanceLabel.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class POIDistanceLabel: UILabel, POIMarkerProtocol {
    
    var poi:POI?
    var forcedSlug: String?
    
    // MARK: Volatile variables
    
    var distance: Double = 0 {
        didSet {
            self.text = distance.distanceToString
        }
    }
    
    // MARK: Initializers
        
    init( frame: CGRect, poi: POI ) {
        super.init( frame: frame )
        
        self.poi = poi
        
        if let categories = poi.categories?.allObjects,
            let category = categories.first as! POICategory?,
            let slug = category.slug {
            
            self.forcedSlug = String( format: "category_%@", slug)
        }
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setup() {
        self.font = UIFont(name: "MaisonNeue-Bold", size: 12.0)
        self.textColor = UIColor.white         
        self.isUserInteractionEnabled = false
    }    

}
