//
//  POIMarkerView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 06/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import GoogleMaps

@objc protocol POIMarkerProtocol {
    var poi:POI? { get }
    @objc optional var forcedSlug: String? { get }
}

protocol POIMarkerViewDelegate {
    func didTap(poiMarkerView: POIMarkerView)
}

class POIMarkerView: DesignableUIView, POIMarkerProtocol {
    
    static let diameter:Double = 60.0
    
    var poi:POI?
    var forcedSlug: String?
    
    var location:CLLocation?
    var heading:CLLocationDirection = 0    
    
    var delegate:POIMarkerViewDelegate?
    
    @IBOutlet weak var markerIcon: UIImageView!
    @IBOutlet weak var directionalArrow: UIImageView!
    
    init( frame: CGRect, poi: POI ) {
        super.init( frame: frame )
        self.poi = poi
        
        if let categories = poi.categories?.allObjects,
            let category = categories.first as! POICategory?,
            let slug = category.slug {
                
            self.forcedSlug = String(format: "category_%@", slug)
        }
        
    }
    
    init(frame: CGRect, atLocation location: CLLocation, forSlug slug: String ) {
        super.init(frame: frame)
        self.forcedSlug = slug
        self.location = location
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.showMarkerForCategory()
    }
        
    func showMarkerForCategory() {
        
        if self.poi != nil {
            
            guard let forcedSlug = self.forcedSlug else { return }
            
            if forcedSlug.contains("_") {

                let components = forcedSlug.components(separatedBy: "_")
                let prefix = components[0]
                let slug = components[1]
                
                if prefix == "family" {
                    markerIcon.image = MarkerManager.getImageForFamily(forTarget: "Compass", family: slug)
                } else if prefix == "category" {
                    markerIcon.image = MarkerManager.getImageForCategory(forTarget: "Compass", category: slug)
                }
                
            } else {
                markerIcon.image = MarkerManager.getImageForFamily(forTarget: "Compass", family: forcedSlug)
            }
            
            
        }
        
    }
    
    func rotateArrowFor( userHeading: CLLocationDirection ) {
        
        let radians = CGFloat((self.heading - userHeading).degreesToRadians)
        
        UIView.animate(withDuration: 0.6) {
            self.directionalArrow.transform = CGAffineTransform(rotationAngle: radians )
        }
        
    }
    
    // MARK: POIMarker Delegate methods
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {        
        delegate?.didTap(poiMarkerView: self)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    }

}
