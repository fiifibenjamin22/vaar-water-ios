//
//  POIDistanceView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class POIDistanceView: DesignableUIView {
  
  @IBInspectable var cornerRadius: CGFloat = 6 {
    didSet {
      updateUI()
    }
  }
  @IBInspectable var borderColor: UIColor = Globals.lightGrayColor {
    didSet {
      updateUI()
    }
  }
  @IBInspectable var borderWidth: CGFloat = 1 {
    didSet {
      updateUI()
    }
  }
  @IBInspectable var textColor: UIColor = Globals.darkGrayColor {
    didSet {
      updateUI()
    }
  }
  
  @IBInspectable var hideDuration: Bool = false {
    didSet {
      updateUI()
    }
  }
  
  var distance:Double = 0 {
    didSet {
//      self.distanceLabel?.text = distance.distanceToString
//      self.setNeedsDisplay()
    }
  }
  
  var duration: Double = 0 {
    didSet {
//      self.durationLabel?.text = duration.durationToString
//      self.setNeedsDisplay()
    }
  }
  
  
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var durationLabel: UILabel!
  @IBOutlet weak var distanceUnknownIcon: UIImageView!
  @IBOutlet weak var distanceIcon: UIImageView!
  @IBOutlet weak var durationUnknownIcon: UIImageView!
  @IBOutlet weak var durationIcon: UIImageView!
  @IBOutlet weak var separatorView: UIView!
  
  @IBOutlet weak var distanceIconSeparatorLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var distanceIconSuperViewLeadingConstraint: NSLayoutConstraint!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    updateUI()
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    updateUI()
  }
  
  func updateUI() {
    
    self.backgroundColor = UIColor.clear
    self.distanceLabel?.textColor = textColor
    self.durationLabel?.textColor = textColor
    
    self.layer.cornerRadius = cornerRadius
    self.layer.borderWidth = borderWidth
    self.layer.borderColor = borderColor.cgColor
    
//    self.distanceLabel?.text = distance.distanceToString
//    self.durationLabel?.text = duration.durationToString
    
    if hideDuration {
      self.durationLabel.isHidden = true
      self.durationIcon.isHidden = true
      self.durationUnknownIcon.isHidden = true
      self.separatorView.isHidden = true
      
      distanceIconSeparatorLeadingConstraint.priority = UILayoutPriority(rawValue: 100)
      distanceIconSuperViewLeadingConstraint.priority = UILayoutPriority(rawValue: 1000)
    }
    
    setNeedsDisplay()
  }
  
}
