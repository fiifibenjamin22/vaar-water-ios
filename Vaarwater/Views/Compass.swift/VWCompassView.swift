//
//  VWCompassView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import GoogleMaps

@objc protocol VWCompassViewDelegate {
    @objc optional func didTap( poi: POI )
    @objc optional func didLongPress()
}

enum CompassState {
    case idle
    case centered
    case zoomed
}

private let kMaximumDistanceThreshold:Double = 50000.0

@IBDesignable class VWCompassView: DesignableUIView {
    
    // MARK: Static variables
    
    static var familiesOnCompass:Array<String> = {
        let defaults = UserDefaults.standard
        if let preferredArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as? Array<String> {
            return preferredArray
        } else {
            return [String]()
        }
    }()
    
    static let poiMarkerRingScalar:CGFloat = 0.567
    static let poiDistanceRingScalar:CGFloat = 0.868
    
    // MARK: Local variables
    
    var delegate: VWCompassViewDelegate?
    var state:CompassState = .idle
    var isAnimating:Bool = false
    
    var poiMarkerViews = [POIMarkerView]()
    var poiDistanceLabels = [POIDistanceLabel]()
    
    var closestPOIs: Dictionary<String, POI> = Dictionary<String, POI>()
    var pinnedPOI: POI?

    var anchorMarker: POIMarkerView?
    var anchorLabel: POIDistanceLabel?
    
    var speedingTimer = Timer()
    
//    var backgroundShapeLayer = CAShapeLayer()
    
    // MARK: IB variables
    
    @IBOutlet weak var backgroundShapeView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var speedingImageView: UIImageView!
    @IBOutlet weak var compassNeedle: UIImageView!
    @IBOutlet weak var poiMarkerContainerView: UIView!
    @IBOutlet weak var poiDistanceContainerView: UIView!
    
    @IBOutlet weak var compassCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var compassDefaultConstraint: NSLayoutConstraint!
    @IBOutlet weak var compassHiddenConstraint: NSLayoutConstraint!
    
  @IBOutlet weak var compassNeedleRingWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var compassNeedleWidthConstraint: NSLayoutConstraint!
  
  
  // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addObservers()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init( coder: aDecoder )
        addObservers()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
      
      let superWidth = self.frame.width + 8
      
      self.compassNeedleWidthConstraint.constant = 0.41875 * superWidth
      self.compassNeedleRingWidthConstraint.constant = 0.3375 * superWidth
      
      self.compassDefaultConstraint.constant = (-57/320 * superWidth) - 60
        
        self.backgroundShapeView.isHidden = true
        
        createBackgroundShape()
        
        updateForEvent()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
      
      let superWidth = self.frame.width + 8
      
      self.compassNeedleWidthConstraint.constant = 0.41875 * superWidth
      self.compassNeedleRingWidthConstraint.constant = 0.3375 * superWidth
      
      self.compassDefaultConstraint.constant = (-57/320 * superWidth) - 60
        createBackgroundShape()
    }
    
    override func setup() {
        super.setup()
        
        self.backgroundColor = UIColor.clear
        self.speedingImageView.alpha = 0
    }
    
    func createBackgroundShape() {
        self.backgroundShapeView.layer.frame = self.backgroundImageView.frame
        self.backgroundShapeView.layer.backgroundColor = UIColor(red: 1/255.0, green: 165/255.0, blue: 240/255.0, alpha: 0.75).cgColor
        self.backgroundShapeView.layer.cornerRadius = self.bounds.size.width * 0.5
        
    }
    
    func addObservers() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handlePOIPinned),
                                               name: NSNotification.Name(Globals.kPOIPinnedKey),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handlePOIUnpinned),
                                               name: NSNotification.Name(Globals.kPOIUnpinnedKey),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateForEvent),
                                               name: NSNotification.Name(Globals.kEventSwitched),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updatePreferredCategories),
                                               name: NSNotification.Name(Globals.kSettingsSavedKey),
                                               object: nil)
        

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Overrides
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        
        // If the touch point is not transparent, capture touch. Else send to super.
        if self.alphaAtPoint(point: point) > 0.1 {
            return true
        }
        return false
        
    }

    
    // MARK: Custom functions
    
    public func addPOIToCompass( poi:POI, withForcedSlug forcedSlug:String? = nil ) {
        
        if let _ = getMarkerViewForPOI(poi: poi) {
            print("Compass marker already exists")
            return
        }
        
        // Distance threshold
        if poi.distanceToUser > kMaximumDistanceThreshold { return }
        
        // Create poi views
        let view = POIMarkerView(frame: CGRect(x: 0, y:0 , width: POIMarkerView.diameter, height: POIMarkerView.diameter), poi: poi )
        view.transform = CGAffineTransform(translationX: self.bounds.width * 0.5, y: self.bounds.height * 0.5)
        view.delegate = self

        // Create distance labels
        let label = POIDistanceLabel(frame: CGRect(x: 0, y: 0, width: POIMarkerView.diameter, height: 15), poi: poi )
        label.textAlignment = NSTextAlignment.center
        
        // Set/force icon category if given
        if forcedSlug != nil {
            view.forcedSlug = forcedSlug
            label.forcedSlug = forcedSlug
        } else {
            
            // Check if it's the current closest poi and ignore deletion if so
            if let categories = poi.categories?.allObjects as? [POICategory] {
                for category in categories {
                    
                    if let slug = category.family?.slug {
                        view.forcedSlug = String(format: "category_%@", slug)
                        label.forcedSlug = String(format: "category_%@", slug)
                    }
                }
            }
            
        }
        
        if forcedSlug == "anchor" {
            anchorMarker = view
            anchorLabel = label
        }
        
        // add to compass as subview
        poiMarkerViews.append( view )
        poiMarkerContainerView.addSubview( view )
        
        poiDistanceLabels.append( label )
        poiDistanceContainerView.addSubview( label )

        // Update all markers if it's zoomed, else update only the added POI
        if self.state == .zoomed {
            updateAllMarkers()
        } else {
            if let location = LocationManager.sharedInstance.currentLocation {
                updatePOIMarkerView(view: view, forLocation: location, animated: true)
                updatePOIMarkerView(view: label, forLocation: location, animated: true)
                //            updatePOIDistanceLabel(label: label, forLocation: location, animated: true)
            }
        }
        
        // Rotate needle for heading
        view.rotateArrowFor(userHeading: LocationManager.sharedInstance.currentHeading )
        
        
    }
    
  public func deletePOIFromCompass( poi: POI ) {
    
    if !poi.anchor {
      // Check pinned
      if poi.pinned { return }
      
      // Check if it's the current closest poi and ignore deletion if so
      if let categories = poi.categories?.allObjects as? [POICategory] {
        for category in categories {
          
          if let family = category.family?.slug,
            closestPOIs[family] == poi {
            return
          }
        }
      }
    }
    
    // Else, remove the POI marker and distancelabel, animated
    if let markerView = getMarkerViewForPOI(poi: poi) {
      poiMarkerViews.remove( at: poiMarkerViews.firstIndex(of: markerView)!)
      
      UIView.animate(
        withDuration: 0.42,
        delay: 0,
        usingSpringWithDamping: 0.76,
        initialSpringVelocity: 0.2,
        animations: {
          markerView.alpha = 0
          markerView.transform = CGAffineTransform(translationX: self.bounds.width * 0.5, y: self.bounds.height * 0.5)
      })
      { (completed) in
        markerView.removeFromSuperview()
      }
    }
    
    // Remove the distanceLabel, animated
    if let distanceLabel = getDistanceLabelForPOI(poi: poi) {
      poiDistanceLabels.remove( at: poiDistanceLabels.firstIndex(of: distanceLabel)!)
      
      UIView.animate(
        withDuration: 0.42,
        delay: 0,
        usingSpringWithDamping: 0.76,
        initialSpringVelocity: 0.2,
        options: [],
        animations: {
          distanceLabel.alpha = 0
          distanceLabel.transform = CGAffineTransform(translationX: self.bounds.width * 0.5, y: self.bounds.height * 0.5)
      })
      { (completed) in
        distanceLabel.removeFromSuperview()
      }
    }
    
  }
    
    public func deleteAnchorFromCompass() {
        
        if let markerView = anchorMarker {
            poiMarkerViews.remove( at: poiMarkerViews.firstIndex(of: markerView)!)
            markerView.removeFromSuperview()
        }
        
        if let distanceLabel = anchorLabel {
            poiDistanceLabels.remove( at: poiDistanceLabels.firstIndex(of: distanceLabel)!)
            distanceLabel.removeFromSuperview()
        }
        
    }
    
    @objc public func handlePOIPinned( notification:Notification ) {
        
        // Clear currently pinned POI
        if let pinnedPOI = self.pinnedPOI {
            // Toggle pinned status
            pinnedPOI.updatePinToCompass(false)
            self.deletePOIFromCompass(poi: pinnedPOI)
            self.pinnedPOI = nil
        }
        
        if let poi = notification.userInfo?["poi"] as? POI {
            self.pinnedPOI = poi
            self.addPOIToCompass(poi: poi)
        }
        
    }
    
    @objc public func handlePOIUnpinned( notification:Notification ) {
        
        if let poi = notification.userInfo?["poi"] as? POI,
           let pinnedPOI = self.pinnedPOI {
            
            pinnedPOI.updatePinToCompass(false)
            self.deletePOIFromCompass(poi: poi)
            self.pinnedPOI = nil
            
        }
        
    }    
    
    public func updateForUserHeading( heading: CLLocationDirection ) {

        // Rotate compass needle 
        let radians = CGFloat(heading).degreesToRadians
        UIView.animate(withDuration: 0.6) {
            self.compassNeedle.transform = CGAffineTransform(rotationAngle: -radians )
        }
        
        for view in poiMarkerViews {
            
            // Position POIMarkers
            if self.state != .zoomed  {
                if let location = LocationManager.sharedInstance.currentLocation {
                    updatePOIMarkerView( view: view, forLocation: location, animated: true )
                }
            }
            
            // Rotate POI markers according to userheading
            view.rotateArrowFor(userHeading: LocationManager.sharedInstance.currentHeading )
        }
        
        // Position and update POI Distance labels
        for label in poiDistanceLabels {
            
//            if self.state != .zoomed  {
                if let location = LocationManager.sharedInstance.currentLocation {
                    updatePOIMarkerView( view: label, forLocation: location, animated: true )
//                updatePOIDistanceLabel( label: label, forLocation: location, animated: true )
                }
//            }
            
        }
        
    }
    
    public func updateForLocation( location: CLLocation ) {
        
        let completionHandler: ( _ closestPOI: POI?, _ combinedSlug: String?, _ success: Bool, _ error: String? ) -> Void = { ( closestPOI, combinedSlug, success, error ) in
            
            guard error == nil else {
                print("Error fetching closest POI:\(String(describing: error))")
                return
            }
            
            guard closestPOI != nil else {
                print("No closest POI for ", combinedSlug!)
                return
            }
            
            // check if there is already a POI with the same family
            if let storedPOI = self.closestPOIs[ combinedSlug! ] {
                
                // if the storedPOI is the same as the closestPOI, only update
                if closestPOI! == storedPOI {
                    if let markerView = self.getMarkerViewForPOI(poi: closestPOI!) {
                        self.updatePOIMarkerView( view: markerView, forLocation: location, animated: true )
                    }
                    if let distanceLabel = self.getDistanceLabelForPOI(poi: closestPOI!) {
                      //  print ("buoy? ", distanceLabel.forcedSlug as Any,"  ", distanceLabel.distance)
                        self.updatePOIMarkerView( view: distanceLabel, forLocation: location, animated: true )
                    }
                    
                } else {

                    // Update closest POI storage
                    self.closestPOIs.updateValue(closestPOI!, forKey: combinedSlug!)
                    // delete the POI from the compass
                    self.deletePOIFromCompass(poi: storedPOI)
                    // add for the new one
                    self.addPOIToCompass(poi: closestPOI!, withForcedSlug: combinedSlug)
                    
                }
                
            } else {
                // add for the new one
                self.addPOIToCompass(poi: closestPOI!, withForcedSlug: combinedSlug)
                // Update closest POI storage
                self.closestPOIs.updateValue(closestPOI!, forKey: combinedSlug!)
            }
        
        }
        
        // GET the closest POI for each category and then create a marker for it
        for slug in VWCompassView.familiesOnCompass {
            
            let components = slug.components(separatedBy: "_")
            if components.count > 1 {

                let prefix = components[0]
                let value = components[1]
                    
                POIDataController.sharedInstance.fetchClosestPOIForFamily(type: prefix, value: value, completion: completionHandler)
                    
            }
        }
    
    }
    
    public func updateForSpeed( speed: Double, maxSpeed: Double ) {
        
        if ( speed.kilometersPerHour >= maxSpeed) {
            
            speedingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(Globals.kSpeedingCheckInterval), target: self, selector: #selector(self.animateToSpeeding), userInfo: nil, repeats: false);
            
        } else {
            
            speedingTimer.invalidate()
            self.animateBackToNormal()
            
//            speedingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(Globals.kSpeedingCheckInterval), target: self, selector: #selector(self.animateBackToNormal), userInfo: nil, repeats: false);
            
        }
        
    }
    
    @objc func animateToSpeeding() {
        
        guard !isAnimating else { return }
        
        self.isAnimating = true
        UIView.animate(withDuration: 0.3, animations: {
            self.speedingImageView.alpha = 1
        }, completion: { (finished) in
            self.isAnimating = false
        })
        
    }
    
    func animateBackToNormal() {
        
        guard !isAnimating else { return }
        
        self.isAnimating = true
        UIView.animate(withDuration: 0.3, animations: {
            self.speedingImageView.alpha = 0
        }, completion: { (finished) in
            self.isAnimating = false
        })
        
    }
    
    @objc func updatePreferredCategories() {
        
        let defaults = UserDefaults.standard
        let preferredArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as! Array<String>
        
        VWCompassView.familiesOnCompass = preferredArray
        
        print("familiesOnCompass:", VWCompassView.familiesOnCompass)
        
        // Traverse closestPOIs and if it's no longer in familiesInCompass, remove it
        for (slug, poi) in closestPOIs {
            
            print("slug: ", slug)
            
            if !poi.anchor && !VWCompassView.familiesOnCompass.contains(slug) {
                deletePOIFromCompass(poi: poi)
            }
        }
        
    }
    
    @objc func updateForEvent() {
        
        guard let activeEvent = EventManager.event else {
            print("No active event")
            return
        }
        
        if activeEvent.compassBackground.large != "",
            let url = URL(string: activeEvent.compassBackground.large ),
            let data = NSData(contentsOf: url) {

            let scale = UIScreen.main.scale
            self.backgroundImageView.image = UIImage(data: data as Data, scale: scale)
        } else {
            // DEBUG UNTIL EVENT API
//            self.backgroundImageView.image = UIImage(named: "Icon Event Koningsdag")
            // DEBUG UNTIL EVENT API
            self.backgroundImageView.tintColor = activeEvent.eventColor
            self.backgroundImageView.image = UIImage(named: "Compass Background Event")
        }
        
    }
    
    func updateAllMarkers() {
        
        for marker in poiMarkerViews {
            updatePOIMarkerView(view: marker, forLocation: (self.state == .zoomed) ? nil : LocationManager.sharedInstance.currentLocation)
        }
        
        for label in poiDistanceLabels {
            updatePOIMarkerView(view: label, forLocation: (self.state == .zoomed) ? nil : LocationManager.sharedInstance.currentLocation)
        }
        
    }
    
    public func updatePOIMarkerView( view: POIMarkerProtocol, forLocation location: CLLocation?, animated: Bool = true ) {
        
        var position:CGPoint = CGPoint(x: 0, y: 0)
        
        var heading:Double = 0
        var calculatedDistance:Double = 0
        
        if let location = location {
            
            if let poiLocation = view.poi?.location {
                heading = GMSGeometryHeading( location.coordinate, poiLocation.coordinate )
                calculatedDistance = location.distance(from: poiLocation )
              //  print (calculatedDistance);
//                } else if let anchorLocation = view.anchor?.location {
//                    heading = GMSGeometryHeading( location.coordinate, anchorLocation.coordinate )
//                    calculatedDistance = location.distance(from: anchorLocation )
            }
            
            
            if view is POIMarkerView {
                position = calculatePositionOnCompass(for: view as! UIView, heading: heading, scalar: VWCompassView.poiMarkerRingScalar)
            } else if view is POIDistanceLabel {
                position = calculatePositionOnCompass(for: view as! UIView, heading: heading, scalar: VWCompassView.poiDistanceRingScalar)
            }
            
        }
    
        // Update heading on POI Marker itself for future usage
        if view is POIMarkerView {
            (view as! POIMarkerView).heading = heading
        } else if view is POIDistanceLabel {
            // Update distance label
            if calculatedDistance > 0 {
                (view as! POIDistanceLabel).distance = calculatedDistance
            } else {
                (view as! POIDistanceLabel).distance = view.poi?.distanceToUser ?? 0
               // print (view.poi?.distanceToUser as Any)
            }
//            (view as! POIDistanceLabel).distance = view.poi?.distanceToUser ?? 0
        }
        
        if self.state != .zoomed {
            
            UIView.animate(
                withDuration: animated ? 0.38 : 0,
                delay: 0,
                usingSpringWithDamping: 0.95,
                initialSpringVelocity: 0.8,
                options: .allowUserInteraction,
                animations: {
                    (view as! UIView).transform = CGAffineTransform(translationX: position.x, y: position.y)
            },
                completion: nil
            )
            
            
        } else {
            
            if let poi = view.poi {
                
                if let forcedSlug = view.forcedSlug,
                    forcedSlug != nil {
                    
                    if !poi.pinned || self.closestPOIs[ forcedSlug! ] == pinnedPOI {
                        position = getGridTranslationOffset(numberOfItems: poiMarkerViews.count, slug: forcedSlug!)
                    } else {
                        position = getGridTranslationOffset(numberOfItems: poiMarkerViews.count, slug: "pinned")
                    }
                    
                }
                
//                
//                
//                if !poi.pinned {
//                    if let category = view.categorySlug {
//                        position = getGridTranslationOffset(numberOfItems: poiMarkerViews.count, category: category)
//                    }
//                } else {
//                    position = getGridTranslationOffset(numberOfItems: poiMarkerViews.count, category: "pinned")
//                }
                
            }
            
            if view is POIMarkerView {
                position.y += 15
            }
            
            UIView.animate(
                withDuration: animated ? 0.38 : 0,
                delay: (Double(arc4random()) / Double(UInt32.max)) / 10,
                usingSpringWithDamping: 0.76,
                initialSpringVelocity: 0.2,
                options: .allowUserInteraction,
                animations: {
                    (view as! UIView).transform = CGAffineTransform(translationX: position.x, y: position.y)
            },
                completion: nil
            )
            
            
        }
        
//        // Check if it's a buoy and distance is over threshold. Else hide it
        if let poi = view.poi,
            let slug = poi.slug,
            slug == "buoy" {
            
            var alpha:CGFloat = 1.0
            if let currentLocation = LocationManager.sharedInstance.currentLocation,
                let poiLocation = poi.location {
                
                let distanceToBuoy = poiLocation.distance(from: currentLocation)
                if distanceToBuoy < Globals.kBuoyDistanceThreshold {
                    alpha = 0.0
                }
            }
            
            UIView.animate(
                withDuration: animated ? 0.24 : 0,
                delay: 0,
                options: .allowUserInteraction,
                animations: {
                    (view as! UIView).alpha = alpha
            },
                completion: nil
            )
            
        }
        
    }
    
    private func getMarkerViewForPOI( poi: POI ) -> POIMarkerView? {
        return poiMarkerViews.filter({ $0.poi == poi }).first
    }
    
    private func getDistanceLabelForPOI( poi: POI ) -> POIDistanceLabel? {
        return poiDistanceLabels.filter({ $0.poi == poi }).first
    }
    
    private func calculatePositionOnCompass( for view: UIView, heading: CLLocationDirection, scalar: CGFloat ) -> CGPoint {
        
        var point = CGPoint(x: 0, y: 0)
        let radius = self.bounds.width * 0.5
        
        // position POI View in Compass View
        // azimuth offset for map coordination offset
        let radians = CGFloat((LocationManager.sharedInstance.currentHeading - heading - Globals.kAzimuth).degreesToRadians)
        
        // minus/flip the sin-calculations because screen-coordinates have an inverted y-value
        point.x = round(radius + round(radius * cos( radians ) * scalar) - (view.bounds.width * 0.5))
        point.y = round(radius - round(radius * sin( radians ) * scalar) - (view.bounds.height * 0.5))
        
        return point
    }
    
    func zoomInBackground() {
        
        self.isAnimating = true
        self.state = .zoomed
        
        self.updateAllMarkers()
        
        let zoomTransform = CGAffineTransform.init(scaleX: 2.5, y: 2.5)
        let positionTransform = CGAffineTransform.init(translationX: 0, y: self.backgroundShapeView.bounds.size.height * 0.4)
        let transform = zoomTransform.concatenating(positionTransform)
        
        // Show the background shape layer
        self.backgroundShapeView.isHidden = false
        self.backgroundImageView.isHidden = true
        
        
        UIView.animate(
            withDuration: 0.38,
            delay: 0,
            usingSpringWithDamping: 0.76,
            initialSpringVelocity: 0.2,
            options: [],
            animations: {
                self.backgroundShapeView.transform = transform
                // Position the marker containers slighty up
                self.poiMarkerContainerView.transform = CGAffineTransform.init(translationX: 0, y: -60)
                self.poiDistanceContainerView.transform = CGAffineTransform.init(translationX: 0, y: -60)
            },
            completion: { (finished) in
//                self.state = .zoomed
                self.isAnimating = false
            }
        )
        
    }
    
    func zoomOutBackground() {
        
        self.isAnimating = true
        self.state = .idle
        
        self.updateAllMarkers()
        
        UIView.animate(
            withDuration: 0.38,
            delay: 0,
            usingSpringWithDamping: 0.76,
            initialSpringVelocity: 0.2,
            options: [],
            animations: {
                self.backgroundShapeView.transform = CGAffineTransform.identity
                // Position the marker containers slighty up
                self.poiMarkerContainerView.transform = CGAffineTransform.identity
                self.poiDistanceContainerView.transform = CGAffineTransform.identity
        },
            completion: { (finished) in
                
                self.isAnimating = false
                
                // Show the background shape layer
                self.backgroundShapeView.isHidden = true
                self.backgroundImageView.isHidden = false
            }
        )
        
    }
    
    func getPositionOnGrid(forSlug slug:String) -> Int {
        
//        let components = slug.components(separatedBy: "_")
//        let prefix = components[0]
//        let slug = components[1]
        
        switch (slug) {
            case "anchor":
                
                if pinnedPOI == nil {
                    return 5
                } else {
                    return 6
                }
            
            case "buoy":
                
                var returnValue = 5
                if pinnedPOI != nil {
                    returnValue += 1
                }
                if anchorMarker != nil {
                    returnValue += 1
                }
                return returnValue
            
            default:
                if VWCompassView.familiesOnCompass.contains(slug) {
                    return VWCompassView.familiesOnCompass.firstIndex(of: slug) ?? -1
                } else {
                    return -1
                }
            
        }
      
    }
    
    func getGridTranslationOffset( numberOfItems: Int, slug: String ) -> CGPoint {
        
        //let numberOfRows:Int = 2
        let rowSpacer: Double = 85
        let gridPosition:Double = Double(getPositionOnGrid(forSlug: slug))
        
        guard gridPosition > -1 else {
            print("GridPosition -1, category unknown for category:", slug )
            return CGPoint(x: 0, y: 0)
        }
        
        let maxItemsPerRow = Double(3) //round( Double(numberOfItems) / Double(numberOfRows) )
        let row = floor( gridPosition / maxItemsPerRow )
        let col = gridPosition - (row * maxItemsPerRow)
        let maxItemsInRow = Double(3) //min( Double(numberOfItems) - (row * maxItemsPerRow), maxItemsPerRow )
        let gridStep = Double(self.bounds.size.width) / maxItemsInRow
        
        let translationX = round(gridStep * (col + 0.5) - (POIMarkerView.diameter * 0.5))
        let translationY = round(row * rowSpacer)
        
        return CGPoint(x: translationX, y: translationY)
        
    }
    
    func handleZoomGesture() {
        
        // Send delegate event
//        delegate?.didLongPress?()
        
        // After that, animate the background
        if ( self.state != .zoomed ) {

            var when = DispatchTime.now()
            if self.state == .centered {
                self.animateCompassDown()
                when = DispatchTime.now() + 0.09
            }
            
            // If it's centered, delay the zoomIn request with 0.2 seconds (for cleaner transitions)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.zoomInBackground()
            }

        } else if self.state == .zoomed {
            
            zoomOutBackground()
        }
        
    }
    
    func animateCompassUp() {
        
        // increase constraint priority and let autoLayout animate
        self.compassCenterConstraint.priority = UILayoutPriority(rawValue: 999)
        self.compassHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
        
        // Make it an animation
        executeAnimation(
            startHandler: {
                self.isAnimating = true
        }, completionHandler: {
            self.isAnimating = false
            self.state = .centered
        }
        )
    }
    
    func animateCompassDown() {
        
        print("DEBUG calling animate compass down")
        
        // increase constraint priority and let autoLayout animate
        self.compassCenterConstraint.priority = UILayoutPriority(rawValue: 100)
        self.compassHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
        
        // Make it an animation
        executeAnimation(
            startHandler: {
                self.isAnimating = true
        }, completionHandler: {
            if self.isAnimating  {
                self.isAnimating = false
            }
        }
        )
    }
    
    func hideCompass( withAnimation: Bool ) {
        
        // increase constraint priority and let autoLayout animate
        self.compassCenterConstraint.priority = UILayoutPriority(rawValue: 100)
        self.compassHiddenConstraint.priority = UILayoutPriority(rawValue: 999)
        
        if ( withAnimation ) {
            // Make it an animation
            executeAnimation(
                startHandler: {
                    self.isAnimating = true
            }, completionHandler: {
                self.isAnimating = false
            }
            )
            
        } else {
            self.view.layoutIfNeeded()
        }
        
    }
    
    func showCompass( withAnimation: Bool ) {
        
        // increase constraint priority and let autoLayout animate
        self.compassCenterConstraint.priority = UILayoutPriority(rawValue: 100)
        self.compassHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
        
        if ( withAnimation ) {
            
            // Make it an animation
            executeAnimation(
                startHandler: {
                    self.isAnimating = true
            }, completionHandler: {
                self.isAnimating = false
            }
            )
            
        } else {
            self.view.layoutIfNeeded()
        }
        
    }
    
    func executeAnimation( withDamping:Bool = true, startHandler: @escaping() -> Void, completionHandler: @escaping() -> Void ) {
        
        // Perform start handler (pre-animating)
        startHandler()
        
        // Perform animation
        UIView.animate(
            withDuration: withDamping ? 0.53 : 0.4,
            delay: 0,
            usingSpringWithDamping: withDamping ? 0.76 : 1,
            initialSpringVelocity: withDamping ? 0.2 : 1,
            options: [],
            animations: {
                self.view.layoutIfNeeded()
        },
            completion: { (finished: Bool) in
                completionHandler()
        }
        )
        
    }
    
    @IBAction func handleSwipe(_ sender: UISwipeGestureRecognizer) {
        
//    @IBAction func handleSwipeUp( recognizer:UISwipeGestureRecognizer ) {
        
        print("swipe on compass detected! dirction:  ", sender.direction.rawValue)
        print("swipe on compass detected! self.state: ", self.state)
        
//        if ( mapState != .manual && state != .paused ) {
            
            if ( sender.direction.rawValue == 4 && self.state != .zoomed) { // swipe up
              
      
              
                animateCompassUp()
                
            } else if (sender.direction.rawValue == 8 ) { // swipe down
              
             
              
                print("swipe down: compass state: ", self.state );
                
                if self.state == .zoomed {
                    self.zoomOutBackground()
                } else {
                    self.animateCompassDown()
                }
                
            }
        
//        }
        
    }
    
    // Handler for compass button tap, state changes to normal
    @IBAction func handleCompassButtonTap(_ sender: Any) {
        
//        if ( mapState != .tracking ) {
//            mapState = .tracking
//        }
        
    }
    
    @IBAction func handleDoubleTap(_ sender: Any) {
      
     
        
        handleZoomGesture()
    }
    
    @IBAction func handleLongPress(_ sender: UILongPressGestureRecognizer) {
        
        if (sender.state == UIGestureRecognizer.State.began) {
          
       
            
            handleZoomGesture()
        }
        
    }
    
}

extension VWCompassView: POIMarkerViewDelegate {
    
    func didTap(poiMarkerView: POIMarkerView) {
        
        if let poi = poiMarkerView.poi {
            delegate?.didTap?(poi: poi )
        }
        
    }
    
}
