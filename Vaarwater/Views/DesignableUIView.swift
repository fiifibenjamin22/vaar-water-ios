//
//  UIViewFromNib.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

@IBDesignable

class DesignableUIView: UIView {
    
    var view: UIView!
        
    override init(frame: CGRect) {
        super.init( frame: frame )
        setup()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init( coder: aDecoder )
        setup()
    }
        
    func setup() {
        
        self.view = UINib(nibName: String(describing: type(of: self)),
                          bundle: Bundle(for:type(of: self))).instantiate(withOwner: self, options: nil)[0] as? UIView
        
        self.view.frame = bounds
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.addSubview(self.view)
        
    }

}
