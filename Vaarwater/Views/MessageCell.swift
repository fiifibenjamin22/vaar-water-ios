//
//  MessageCell.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 11/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    @IBOutlet weak var unreadIndicator: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }    
    
    // MARK: - Public Methods
    
//    func configure(for message: Message, using formatter: DateFormatter) {
//        nameLabel.text = message.name
//        if let created_at = message.created_at {
//            dateLabel.text = formatter.string(from: created_at)
//        } else {
//            dateLabel.text = nil
//        }
//    }
    
    func configure(for news: News, using formatter: DateFormatter) {
        nameLabel.text = news.title
        if let created_at = news.createdDate as Date? {
            dateLabel.text = formatter.string(from: created_at)
        } else {
            dateLabel.text = nil
        }
    }
}
