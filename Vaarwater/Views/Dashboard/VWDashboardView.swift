//
//  VWDashboardView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class VWDashboardView: DesignableUIView {
  
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var speedLabel: UILabel!
  @IBOutlet weak var unitLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var speedometerDial: UIImageView!    
  @IBOutlet weak var speedometerDialSpeeding: UIImageView!
  @IBOutlet weak var speedingLabel: UILabel!
  @IBOutlet weak var waypointArrow: UIImageView!
  
  var isAnimating:Bool = false
  var isSpeeding:Bool = false
  var speedometerMaskLayer:CAShapeLayer?
  
  var speedingTimer = Timer()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    // Set clear background
    self.backgroundColor = UIColor.clear
    self.speedometerDialSpeeding.alpha = 0
    self.speedingLabel.alpha = 0
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init( coder: aDecoder )
    
    // Set clear background
    self.backgroundColor = UIColor.clear
    self.speedometerDialSpeeding.alpha = 0
    self.speedingLabel.alpha = 0
    
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.setupMaskLayer()
  }
  
  override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
    return self.alphaAtPoint(point: point) >= 100
  }
  
  private func setupMaskLayer() {
    
    self.layoutIfNeeded()
    
    // Use UIBezierPath as an easy way to create the CGPath for the layer.
    // The path should be the entire circle.
    let bounds = speedometerDial.bounds
    let circlePath = UIBezierPath(arcCenter: CGPoint(x: bounds.size.width / 2.0, y: bounds.size.height / 1.18 ), radius: (bounds.size.width-20)/2, startAngle: CGFloat(.pi * 0.95), endAngle: CGFloat(.pi * 2.05), clockwise: true)
    
    if self.speedometerMaskLayer == nil {
      self.speedometerMaskLayer = CAShapeLayer()
      self.speedometerMaskLayer!.path = circlePath.cgPath
      self.speedometerMaskLayer!.fillColor = UIColor.clear.cgColor
      self.speedometerMaskLayer!.strokeColor = UIColor.red.cgColor
      self.speedometerMaskLayer!.lineWidth = 20.0
      self.speedometerMaskLayer!.opacity = 1
      self.speedometerDial.layer.mask = self.speedometerMaskLayer!
      
      // Don't draw the circle initially
      self.speedometerMaskLayer!.strokeEnd = 0
    }
  }
  
  func updateAddress( value: String) {
    self.addressLabel.text = value
  }
  
  func updateForSpeed( speed: Double, maxSpeed:Double ) {
    
    // Update speed label
    if ( speed >= 0 && speed < 400 ) {
      self.speedLabel.text = String(format: "%0.f", speed.kilometersPerHour )
    }
    animateMaskLayer(forSpeed: speed, maxSpeed: maxSpeed)
    
    // Check for speeding, and perform accordingly
    if ( speed.kilometersPerHour >= maxSpeed) {
      self.isSpeeding = true
      speedingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(Globals.kSpeedingCheckInterval), target: self, selector: #selector(self.animateToSpeeding(sender:)), userInfo: maxSpeed, repeats: false);
      
    } else {
      self.isSpeeding = false
      speedingTimer.invalidate()
      self.animateBackToNormal()
    }
  }
  
  func updateForRoute( route: Route? ) {
    
    // Set current active route name
    
  }
  
  func showItinerary(itinerary: Itinerary, location: CLLocation?, heading: CLLocationDirection) {
    self.unitLabel.isHidden = true
    self.speedLabel.isHidden = true
    self.distanceLabel.isHidden = false
    self.waypointArrow.isHidden = false
    
    if let distance = itinerary.distance, distance < Globals.kNavDestinationTreshold {
      waypointArrow.image = UIImage(named: "Waypoint Destination") 
      self.waypointArrow.transform = CGAffineTransform(rotationAngle: 0)
      self.addressLabel.text = "nav.destination.reached".localized()
      return
    } else {
      waypointArrow.image = UIImage(named: "Waypoint Arrow")
    }
    
    if let location = location, itinerary.waypoints.count > 0 {
      let location2D = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
      
      var nextWaypoints: [(index: Int, heading: Double, distance: Double)] = []
      var index = 0
      var waypointHeading: CLLocationDirection = 0
      var waypointDistance: Double = 0.0
      
      for waypoint in itinerary.waypoints {
        if let waypointLocation = waypoint.location {
          let distance = GMSGeometryDistance(waypointLocation, location2D)
          
          if distance > Globals.kNavDestinationTreshold {
            let relativeHeading = GMSGeometryHeading(location2D, waypointLocation)
            nextWaypoints.append((index: index, heading: relativeHeading, distance: distance))
            index += 1
            
            if index == 3 {
              break
            }
          }
        }
      }
      
      if nextWaypoints.count > 1 {
        // at least 2 upcoming waypoints at least treshold distance away from current location
        if abs(nextWaypoints[0].heading - nextWaypoints[1].heading) >= 120 {
          waypointHeading = nextWaypoints[1].heading
          waypointDistance = nextWaypoints[1].distance
        } else if nextWaypoints.count > 2 && abs(nextWaypoints[1].heading - nextWaypoints[2].heading) >= 120 {
          // alternative option if at least another upcoming waypoint available
          waypointHeading = nextWaypoints[2].heading
          waypointDistance = nextWaypoints[2].distance
        } else {
          // no alternative option, take first waypoint
          waypointHeading = nextWaypoints[0].heading
          waypointDistance = nextWaypoints[0].distance
        }
      } else if nextWaypoints.count == 1 {
        // exactly 1 upcoming waypoint, which must be destination, but it's more than treshold distance away from current location
        waypointHeading = nextWaypoints[0].heading
        waypointDistance = nextWaypoints[0].distance
      } else {
        // no upcoming waypoints more than treshold distance away from current location
        if let lastWaypoint = itinerary.waypoints.last, let lastWaypointLocation = lastWaypoint.location {
          waypointHeading = GMSGeometryHeading(location2D, lastWaypointLocation)
          waypointDistance = GMSGeometryDistance(location2D, lastWaypointLocation)
        }
      }
      
      if(!self.isSpeeding) {
        self.distanceLabel.text = waypointDistance.distanceToString
      }
        
      let radians = CGFloat(waypointHeading - heading).degreesToRadians
      UIView.animate(withDuration: 0.6) {
        self.waypointArrow.transform = CGAffineTransform(rotationAngle: radians )
      }
    }
  }
  
  func hideItinerary() {
    self.unitLabel.isHidden = false
    self.speedLabel.isHidden = false
    self.distanceLabel.isHidden = true
    self.waypointArrow.isHidden = true
  }
  
  func updateForUserHeading( heading: CLLocationDirection ) {
    
    // Rotate arrow 
    let radians = CGFloat(heading).degreesToRadians
    UIView.animate(withDuration: 0.6) {
      self.waypointArrow.transform = CGAffineTransform(rotationAngle: -radians )
    }
    
    // for view in poiMarkerViews {
    //
    //      // Position POIMarkers
    //      if self.state != .zoomed  {
    //        if let location = LocationManager.sharedInstance.currentLocation {
    //          updatePOIMarkerView( view: view, forLocation: location, animated: true )
    //        }
    //      }
    //
    //      // Rotate POI markers according to userheading
    //      view.rotateArrowFor(userHeading: LocationManager.sharedInstance.currentHeading )
    //    }
    
    // Position POI Distance labels
    //    for label in poiDistanceLabels {
    //
    //      if self.state != .zoomed  {
    //        if let location = LocationManager.sharedInstance.currentLocation {
    //          updatePOIMarkerView( view: label, forLocation: location, animated: true )
    //          //                updatePOIDistanceLabel( label: label, forLocation: location, animated: true )
    //        }
    //      }
    //
    //    }
    
  }
  
  @objc func animateToSpeeding(sender: Timer) {
    
    guard let maxSpeed = sender.userInfo as? Double, !isAnimating else { return }
    
    self.isAnimating = true
    UIView.animate(withDuration: 0.3, animations: {
      self.speedometerDialSpeeding.alpha = 1
      self.speedingLabel.alpha = 1
      self.addressLabel.alpha = 0
      
      self.unitLabel.text = String(format: "Max %0.f km/u", maxSpeed)
      self.distanceLabel.text = String(format: "Max %0.f km/u", maxSpeed)
      
      self.speedLabel.textColor = Globals.redColor
      self.unitLabel.textColor = Globals.redColor
      self.distanceLabel.textColor = Globals.redColor
    }, completion: { (finished) in
      self.isAnimating = false
    })
  }
  
  func animateBackToNormal() {
    
    guard !isAnimating else { return }
    
    self.isAnimating = true
    UIView.animate(withDuration: 0.3, animations: {
      self.speedometerDialSpeeding.alpha = 0
      self.speedingLabel.alpha = 0
      self.addressLabel.alpha = 1
      
      self.unitLabel.text = "km/u"
      self.distanceLabel.text = ""
      
      self.speedLabel.textColor = Globals.darkBlueColor
      self.unitLabel.textColor = Globals.darkBlueColor
      self.distanceLabel.textColor = Globals.darkBlueColor
    }, completion: { (finished) in
      self.isAnimating = false
    })
  }
  
  func animateMaskLayer( forSpeed speed: Double, maxSpeed: Double ) {
    
    let speedScalar:Double = speed.kilometersPerHour / maxSpeed
    
    // We want to animate the strokeEnd property of the circleLayer
    let animation = CABasicAnimation(keyPath: "strokeEnd")
    
    // Set the animation duration appropriately
    animation.duration = 0.74
    animation.fromValue = self.speedometerMaskLayer?.strokeEnd
    animation.toValue = min(speedScalar, 1.0)
    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    
    // Set the circleLayer's strokeEnd property to 1.0 for persistance
    self.speedometerMaskLayer?.strokeEnd = CGFloat(min(speedScalar, 1.0))
    
    // Do the actual animation
    self.speedometerMaskLayer?.add(animation, forKey: "animateCircle")
    
  }
}
