//
//  ReportCollectionViewCell.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 14/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class ReportCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var typeLabel: UILabel!
  @IBOutlet weak var typeIcon: UIImageView!
  

    
    
  override func prepareForReuse() {
    super.prepareForReuse()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func configure(for reportType: ReportType) {
    
    typeLabel.text = Localize.currentLanguage() == "nl" ? reportType.name_nl : reportType.name_en
    
    if let image = UIImage(named: reportType.imageName) {
      typeIcon.image = image
    }
  }
    
}
