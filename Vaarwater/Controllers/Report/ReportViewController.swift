import UIKit
import Localize_Swift
import SafariServices

class ReportViewController: UIViewController {
  
  private let kFeedbackURL_en: String = "https://www.kcmsurvey.com/qSwubc095e90e7663c4179794a3fMaSd"
  private let kFeedbackURL_nl: String = "https://www.kcmsurvey.com/qSwubc095e90e7663c4179794a3fMaSd"
  
  override func viewDidAppear(_ animated: Bool) {
    trackScreenView()
    super.viewDidAppear(animated)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showReportForm" {
      if let controller = segue.destination as? ReportFormTableViewController {
        controller.reportType = "Iets anders"
      }
    }
  }
  
  @IBAction func feedbackButton(_ sender: Any) {
    let feedbackURL = (Localize.currentLanguage() == "nl") ? kFeedbackURL_nl : kFeedbackURL_en
    
    let svc = SFSafariViewController(url: URL(string: feedbackURL)!)
    UIApplication.topViewController()?.present(svc, animated: true, completion: nil)
  }
  
  @IBAction func handleBackButton(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func handleCloseButton(_ sender: Any) {
    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
    NotificationCenter.default.post(name: name, object: nil)
  }
}
