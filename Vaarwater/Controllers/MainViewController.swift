//
//  MapViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 31/01/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import GoogleMaps
import Localize_Swift
import RxSwift
import RxCocoa
import Firebase

enum State {
  case standard
  case paused
  case warning
}

enum GPSSignalStrength {
  case invalid
  case weak
  case strong
}

class MainViewController: UIViewController, AnchorDataControllerDelegate {
  
  // MARK: - RX variables
  let bag = DisposeBag()
  let locationManagerRx = CLLocationManager()
    
  let currentItinerary = BehaviorRelay<Itinerary>(value: Itinerary.empty())
  
  
  // MARK: - Local variables
  
  let kMinimumReloadInterval:Double = 60.0
  
  let kCalculationInterval:Double = 8.0
  var timeSinceLastCalculation:TimeInterval = 0
  
  let kItineraryInterval: Double = 8.0
  var timeSinceLastItinerary: TimeInterval = 0
  
  var weatherManager = WeatherManager.sharedInstance
  var trafficManager = TrafficManager.sharedInstance
  
  var POIs:NSArray = [POI]() as NSArray
  
  var anchorDataController = AnchorDataController()
  var newsDataController = NewsDataController.sharedInstance
  var poiDataController = POIDataController.sharedInstance
  
  var state:State = .standard {
    didSet {
      updateForState()
    }
  }
  
  var isPaused:Bool = false {
    didSet {
      updateForState()
    }
  }
  
  var dismissedWarning:Bool = false
  var passedBorders: Bool = false
  
  var debugSegmentPolyline:GMSPolyline = GMSPolyline()
  
  // MARK: - IB Outlets
  
  @IBOutlet weak var mapView: VWMapView!
  @IBOutlet weak var compassView: VWCompassView!
  @IBOutlet weak var dashboardView: VWDashboardView!
  @IBOutlet weak var menuContainerView: UIView!
  
  @IBOutlet weak var directionalityButton: UIButton!
  @IBOutlet weak var weatherButton: WeatherButtonView!
  @IBOutlet weak var compassButton: UIButton!
  @IBOutlet weak var dashboardHiddenConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var compassLeadingConstraint: NSLayoutConstraint!
  @IBOutlet weak var compassTrailingConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var trafficButtonHiddenConstraint: NSLayoutConstraint!
  @IBOutlet weak var directionalityButtonHiddenConstraint: NSLayoutConstraint!
  @IBOutlet weak var weatherButtonHiddenConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var eventButton:UIButton!
  @IBOutlet weak var todayButton:UIButton!
  @IBOutlet weak var routeButton:UIButton!
  @IBOutlet weak var unreadNewsIndicator: BadgeSwift!
  
  //@IBOutlet weak var iterinaryButton: UIButton!
  @IBOutlet weak var itineraryView: UIView!
  @IBOutlet weak var itineraryDurationLabel: UILabel!
    
    
 override var preferredStatusBarStyle: UIStatusBarStyle {
     return .lightContent
 }
    

  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    
    self.compassButton.isHidden = true
    self.unreadNewsIndicator.isHidden = true
    
    setup()
    checkLaunchCount()
    
    checkLocationAuthorization()
    loadInitialData()
    
//     Crashlytics.sharedInstance().crash()
//    DispatchQueue.main.async {
//        
//    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    
    URLCache.shared.removeAllCachedResponses()
    URLCache.shared.diskCapacity = 0
    URLCache.shared.memoryCapacity = 0
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if #available(iOS 13.0, *) {
           let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
           let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
           statusBar.backgroundColor = .red
           window?.addSubview(statusBar)
    } else {
//           UIApplication.shared.statusBarView?.backgroundColor = .white
//           UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // Hide the navigation bar on the this view controller
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
    
    // 'Reset' UI to default
    if mapView.state == .tracking {
      if let location = LocationManager.sharedInstance.currentLocation {
        self.mapView.updateForLocation(location: location)
      }
    }
    self.state = .standard
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    checkBorders()
      navigationController?.navigationBar.barStyle = .black
    navigationController?.navigationBar.backgroundColor = .red
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    // Show the navigation bar on other view controllers
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
  }
  
  @objc func applicationDidBecomeActive() {
    
    //checkLocationAuthorization()
    loadInitialData()
    
    checkBorders()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Custom methods
  func setup() {
    
    // Set delegates
    self.mapView.delegate = self
    self.compassView.delegate = self
    self.weatherManager.delegate = self
    self.trafficManager.delegate = self
    
    self.anchorDataController.delegate = self
    
    addObservers()
  }
  
  func addObservers() {
    
    // Create notification observers
    NotificationCenter.default.addObserver(self, selector: #selector(handlePOISelected), name: NSNotification.Name(Globals.kPOISelectedKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(handlePOIDeselected), name: NSNotification.Name(Globals.kPOIDeselectedKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(handleAnchorRemove), name: NSNotification.Name(Globals.kAnchorRemovedKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(loadPOIsOnMap), name: NSNotification.Name(Globals.kPoiUpdatedNotificationKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(handleLocationUpdate), name: NSNotification.Name(Globals.kLocationUpdatedKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(handleHeadingUpdate), name: NSNotification.Name(Globals.kHeadingUpdatedKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(updateForEvent), name: NSNotification.Name(Globals.kEventSwitched), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(handleSailingDirectionChange), name: NSNotification.Name(Globals.kSailingDirectionChange), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(updateNewsIndicator), name: NSNotification.Name(Globals.kNewsUpdatedNotificationKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(updateForLanguage), name: NSNotification.Name(Globals.kLanguageChangeKey), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(updateForRoute), name: NSNotification.Name(Globals.kActiveRouteChange), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(updateForDestination), name: NSNotification.Name(Globals.kActiveDestinationChange), object: nil)

    NotificationCenter.default.addObserver(self, selector: #selector(loadPOIsOnMap), name: NSNotification.Name(Globals.kSettingsSavedKey), object: nil)
    
  }
  
  func checkLocationAuthorization() {
    
    // Check Location status
    LocationManager.sharedInstance.authorize()
    
    if !LocationManager.sharedInstance.checkLocationEnabled() {
      performSegue(withIdentifier: "locationBlockingSegue", sender: self)
    }
  }
  
  func loadInitialData() {
    
    // Get the locally cached pois
    loadPOIsOnMap()
    
    // Load anchor(s)
    loadAnchors()
    
    // Check for updates
    checkEvent()
    checkSync()
    
    // Clear all news and it that succeeds, fetch new news
    checkNews()
    
    checkLaunchedToday()
  }
  
  func checkEvent() {
    
    guard let _ = EventManager.event else {
      
      print("No event! Deleting.")
      
      // delete all eventPOIs
      let predicate = NSPredicate(format: "eventId != 0")
      if let deletedPOIs = try? poiDataController.deleteAllPOIs( withPredicate: predicate ),
        deletedPOIs.count > 0 {
        
        print("deletedPOIs count", deletedPOIs.count )
        
        for poi in deletedPOIs {
          self.mapView.deletePOIFromMap(poi: poi)
        }
      }
      
      return
      
    }
    
    // Update UI for event status
    updateForEvent()
    
    // Retrieve event POIs
    retrieveEventPOIs()
    
  }
  
  func checkNews() {
    
    let completionHandler: ( _ data: Array<Dictionary<String, AnyObject>>?, _ success : Bool, _ error : String?) -> Void = { ( data, success, error ) in
      
      guard error == nil else {
        print("Error fetching POI data:\(String(describing: error))")
        return
      }
      
      guard data != nil else {
        print("no data fetched")
        return
      }
      
      // Update fetch
      if success {
        do {
          try self.newsDataController.executeFetch()
        }
        catch {}
      }
      DispatchQueue.main.async {
        self.updateNewsIndicator()
      }
    }
    
    API.sharedInstance.getNews(completion: completionHandler)
    
  }
  
  func checkLaunchedToday() {
    let defaults = UserDefaults.standard
    if let lastLaunchedToday = defaults.object(forKey: Globals.kLastLaunchTodayKey) as? Date {
      if Calendar.current.compare(lastLaunchedToday, to: Date(), toGranularity: .day) == .orderedSame {
        return
      }
    }
    
    defaults.set(Date(), forKey: Globals.kLastLaunchTodayKey)
    performSegue(withIdentifier: "todaySegue", sender: nil)
  }
  
  func checkSync() {
    
    // Check if last full sync is more than 7 days ago, and if so, do a full sync
    let defaults = UserDefaults.standard
    if let storedDate = defaults.object(forKey: Globals.kPoiLastFullSyncKey) as? Date {
      
      let calendar = Calendar.current
      let startOfNow = calendar.startOfDay(for: Date() )
      let startOfStoredDate = calendar.startOfDay(for: storedDate )
      let components = calendar.dateComponents([.hour, .day], from: startOfStoredDate, to: startOfNow)
      let day = components.day
      if day! > 7 {
        
        print("Performing full sync")
        
        // Clear existing POI data
        let _ = try? poiDataController.deleteAllPOIs( withPredicate: nil )
        // Retrieve full data sync
        retrieveRemoteData( asFullSync: true )
        
      } else {
        // Less than 7 days ago, do partial sync
        retrieveRemoteData( asFullSync: false )
      }
      
    } else {  // or if stored Date is not set or not a date, perform a full sync
        
        print("Performing full sync")
        
        // Clear existing POI data
        let _ = try? self.poiDataController.deleteAllPOIs( withPredicate: nil )
        // Retrieve full data sync
        self.retrieveRemoteData( asFullSync: true )

    }
  }
  
  @objc func loadPOIsOnMap() {
    

    print("Loading locally stored POIs ")
    
    let completionHandler: ( _ data: [POI]?, _ success: Bool, _ error: String? ) -> Void = { ( data, success, error ) in
      
      guard error == nil else {
        print("Error fetching POI data:\(String(describing: error))")
        return
      }
      
      guard data != nil else {
        print("no data fetched")
        return
      }
      
      if ( data!.count > 0 ) {
        
        print("Data count:", data!.count)
        
        // First delete all existing POIs
        self.mapView.deleteAllPOIs()
        
        let defaults = UserDefaults.standard
        
        let preferredArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as! Array<String>
        
          //print("CHECK_GLOBALS_PREFFERED_FAMILY: \(preferredArray)")
        
        
        // Add POI markers for the fetched pois
        for poi in data! {

            //print("poi:::::::::",poi)

          if let categories = poi.categories?.allObjects as? [POICategory] {

            for category in categories {

                if let familySlug = category.family?.slug {

                  if preferredArray.contains( String(format: "family_\(familySlug)") ) {

                      if familySlug == "bridges-and-locks"
                      || familySlug == "toilets"
                      || familySlug == "activities"
                      || familySlug == "emergency"
                      {
                        DispatchQueue.main.async {
                           _ = self.mapView.addPOIToMap(poi: poi)
                        }
                          
                      }

                  }

                }

            }

          }

        }

        
        // Store for future usage
        self.POIs = data! as NSArray
        
      } else {
        print("Error: Data count is 0!")
        
        // Remove full sync key, to force full sync
        DispatchQueue.global(qos: .background).async {
          let defaults = UserDefaults.standard
          defaults.removeObject(forKey: Globals.kPoiLastFullSyncKey)
          defaults.synchronize()
        }
      }
    }
    
    // Load local data
    DispatchQueue.main.async {
        self.poiDataController.fetchPOIs( withPredicate: nil, completion: completionHandler )

    }
    
  }
  
  func loadAnchors() {
    
    anchorDataController.executeFetch()
    for anchorPOI in anchorDataController.fetchedItems {
              
      self.compassView.addPOIToCompass(poi: anchorPOI, withForcedSlug: "anchor")
      let _ = self.mapView.addPOIToMap(poi: anchorPOI)
      
    }
  }
  
  func retrieveRemoteData( asFullSync: Bool ) {
    
    print("Retrieving remote data as full sync: ", asFullSync)
    
    let defaults = UserDefaults.standard
    let storedDate = defaults.object(forKey: Globals.kPoiLastUpdateKey) ?? Date()
    guard let minDate = storedDate as? Date else {
      //print("Last update stored isn't a date:", storedDate )
      return
    }
    
    let completionHandler: ( _ fetchedPOIs: [POI]?, _ success : Bool, _ error : String?) -> Void = { (fetchedPOIs, success, error) in
      
      guard error == nil else {
        print("Error loading initial data:\(String(describing: error))")
        return
      }
      
      guard fetchedPOIs != nil else {
        print("no data fetched")
        return
      }
        
        // Load the fetched POIs on the map
        self.loadPOIsOnMap()
        // Get the changed POIs
        self.retrievePOIChangeLog()
      
      // Update last updated date
      if ( asFullSync == true ) {
        DispatchQueue.global(qos: .background).async {
          let defaults = UserDefaults.standard
          defaults.set(Date(), forKey: Globals.kPoiLastFullSyncKey)
          defaults.synchronize()
        }
      }
        
    }
    
    // Only send minDate if full sync is NOT true
    var parameters:Dictionary<String, AnyObject> = [:]
    if !asFullSync {
      parameters["minDate"] = minDate as AnyObject?
    }
    
    API.sharedInstance.getPOIs(parameters: parameters, completion: completionHandler)
    // Retrieve routes
    API.sharedInstance.getRoutes(completion: nil)
    
  }
  
  func retrievePOIChangeLog() {
    
    let defaults = UserDefaults.standard
    let storedDate = defaults.object(forKey: Globals.kPoiLastUpdateKey) ?? Date()
    guard let minDate = storedDate as? Date else {
      //print("Last update stored isn't a date:", storedDate )
      return
    }
    
    let completionHandler: ( _ deletedPOIs: [POI]?, _ success : Bool, _ error : String?) -> Void = { (deletedPOIs, success, error) in
      
      guard error == nil else {
        print("Error updating POI with changelog:\(String(describing: error))")
        return
      }
      
      guard success == true else {
        print("Error updating POI with changelog. Success false.")
        return
      }
      
      if let deletedPOIs = deletedPOIs,
        deletedPOIs.count > 0 {
      
        // delete the pois
        for poi in deletedPOIs {
          self.mapView.deletePOIFromMap(poi: poi)
          self.compassView.deletePOIFromCompass(poi: poi)
        }
      }
      
      // Update last updated date
      DispatchQueue.global(qos: .background).async {
        let defaults = UserDefaults.standard
        defaults.set(Date(), forKey: Globals.kPoiLastUpdateKey)
        defaults.synchronize()
      }
    }
    
    // Check updated stuff
    API.sharedInstance.getPOIChangelog(minDate: minDate, completion: completionHandler)
    
  }
  
  func retrieveEventPOIs() {
    
    let completionHandler: ( _ eventPOIs: [POI]?, _ success : Bool, _ error : String?) -> Void = { (eventPOIs, success, error) in
      
      guard error == nil else {
        print("Error retrieving event POIs:\(String(describing: error))")
        return
      }
      
      guard success == true else {
        print("Error retrieving event POIs. Success false.")
        return
      }
      
      guard let eventPOIs = eventPOIs else {
        print("Error, eventPOIs array is empty")
        return
      }
      
      // Add eventPOIs to POIs array
      self.POIs = self.POIs.addingObjects(from: eventPOIs) as NSArray
      
      // Debug temp
        self.loadPOIsOnMap()
      
    }
    
    // Check updated stuff
    let parameters:Dictionary<String, AnyObject> = ["eventId": 5 as AnyObject]
    API.sharedInstance.getPOIs(parameters: parameters, completion: completionHandler)
    
  }
  
  func reverseGeocodeFromLocation( location: CLLocation ) {
    
    CLGeocoder().reverseGeocodeLocation( location, completionHandler: {(placemarks, error)-> Void in
      
      if error != nil {
        print("Reverse geocoder failed with error", error!.localizedDescription)
        return
      }
      
      if placemarks != nil {
        if placemarks!.count > 0 {
          
          let placemark = placemarks![0] as CLPlacemark
          let address = placemark.thoroughfare
          
          self.dashboardView.updateAddress( value: address ?? "")
          
        }
        
      } else {
        print("Problem with the data received from geocoder")
      }
      
    })
    
    
  }
  
  func updateForState() {
    
    // Reset mapState to .tracking if from .traffic
    if mapView.state == .traffic {
      mapView.state = .tracking
    }
    
    if ( state != .standard ) {
      
      // NOTE: hideCompass call in own dispatch call because the Google Maps delegation willMove method also (or mostly) calls this one, and the willMove method is already within a Core Animation transaction.
      DispatchQueue.main.async {
        self.compassView.hideCompass( withAnimation: true )
        self.hideDashboard(withAnimation: true)
        
        if (self.mapView.state == .manual ) {
          self.showCompassButton()
        }
      }
      
    } else if ( mapView.state == .tracking ) {
      
      // Hide the compass button when state changes to tracking
      hideCompassButton()
      // Show the compass view again, by animation
      compassView.showCompass( withAnimation: true )
      showDashboard( withAnimation: true )
      
      // Also re-position to the last stored/updated/known location
      if let location = LocationManager.sharedInstance.currentLocation {
        updateForLocation(location: location)
      }
    }
  }
  
  func updateForLocation( location:CLLocation ) {
    
    if ( self.state != .paused ) {
      
      // If calculation interval has been passed, update distance labels
      if ( (NSDate.timeIntervalSinceReferenceDate - timeSinceLastCalculation) > kCalculationInterval) {
        timeSinceLastCalculation = NSDate.timeIntervalSinceReferenceDate
        updatePOIDistanceToLocation( location: location )
        // search for location and update address if found
        reverseGeocodeFromLocation(location: location)
        // request traffic update
        trafficManager.requestTrafficUpdate(forLocation: location)
      }
      
      // If a route is active, update its position
      if let _ = RouteManager.sharedInstance.currentActiveRoute,
        let _ = RouteManager.sharedInstance.buoyPOI {
        RouteManager.sharedInstance.calculateClosestLocationToRoute()
      }
      
      // Update mapview, compassview and dashboard UI according to new location data
      if mapView.state != .manual {
        mapView.updateForLocation(location: location)
      }
      
      // Check maxSpeed
      compassView.updateForLocation( location: location )
      compassView.updateForSpeed( speed: location.speed, maxSpeed: TrafficManager.sharedInstance.currentMaxSpeed )
      dashboardView.updateForSpeed( speed: location.speed, maxSpeed: TrafficManager.sharedInstance.currentMaxSpeed )
      
      // Check directionality
      checkDirectionality()
      
      
    }
  }
  
  @objc func updateNewsIndicator() {
    
    let unreadCount = newsDataController.numberOfUnreadNewsItems
    
    if unreadCount > 0 {
      
      let attributedString = NSMutableAttributedString(attributedString: self.unreadNewsIndicator.attributedText!)
      attributedString.mutableString.setString( String(format: "%d", newsDataController.numberOfUnreadNewsItems) )
      self.unreadNewsIndicator.attributedText = attributedString
      
      self.unreadNewsIndicator.isHidden = RouteManager.sharedInstance.currentActiveRoute != nil
      
    } else {
      self.unreadNewsIndicator.isHidden = true
    }
  }
  
  @objc func updateForLanguage() {
    
    // Reload our root view controller
    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
      appDelegate.reloadRootViewController()
    }
  }
  
  func debugDrawNearestSegment() {
    
    debugSegmentPolyline.map = nil
    
    if let currentSegment = TrafficManager.sharedInstance.currentTrafficLink {
      
      print("debug drawing segment")
      
      // check if poi has positions attached then, and if not, just return
      guard let positions = currentSegment["positions"] as? Array<Dictionary<String, AnyObject>> else {
        print("Report has no positions")
        return
      }
      
      let path = GMSMutablePath()
      for (index, position) in positions.enumerated() {
        
        if let latValue = position["geo_lat"] as? String,
          let longValue = position["geo_lng"] as? String,
          let latitude = Double(latValue),
          let longitude = Double(longValue) {
          
          let location = CLLocation(latitude: latitude, longitude: longitude)
          path.add(CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
          
          if currentSegment["directionality"] as! Int == 1 {
            
            if index == 0 {
              let marker = GMSMarker()
              marker.icon = self.mapView.getMarkerImageForCategory(slug: "no-entry", highlighted: false)
              //                marker.icon = getMarkerImageForCategory(poi: poi, highlighted: false)
              marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
              marker.position = location.coordinate
              marker.map = self.mapView
              
            }
            
          } else if currentSegment["directionality"] as! Int == 2 {
            
            if index == (positions.count-1) {
              let marker = GMSMarker()
              marker.icon = self.mapView.getMarkerImageForCategory(slug: "no-entry", highlighted: false)
              
              marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
              marker.position = location.coordinate
              marker.map = self.mapView
              
            }
            
          }
        }
      }
      
      debugSegmentPolyline = GMSPolyline(path: path)
      debugSegmentPolyline.strokeColor = UIColor.red
      debugSegmentPolyline.strokeWidth = 6
      debugSegmentPolyline.map = self.mapView
      
    }
    
  }
  
  func updatePOIDistanceToLocation( location: CLLocation ) {
    
    POIs.enumerateObjects({ (poi, index, stop) in
      if let poi = poi as? POI {
        poi.updateDistanceToUser( userLocation: location )
      }
    })
  }
  
  func showItinerary(skipInterval: Bool = false) {
    
    if (skipInterval || (NSDate.timeIntervalSinceReferenceDate - timeSinceLastItinerary) > kItineraryInterval) {
      timeSinceLastItinerary = NSDate.timeIntervalSinceReferenceDate
      
      if skipInterval {
        self.mapView.deleteRouteFromMap()
        currentItinerary.accept(Itinerary.empty())
      }
      
      ItineraryManager.shared.itinerary()
        .subscribe(onNext: { [weak self] itinerary in
          self?.currentItinerary.accept(itinerary)
        })
        .disposed(by: bag)
    }
    
    if currentItinerary.value.waypoints.count > 0 {
      
      //draw itinerary on mapView
      self.mapView.addItineraryToMap(itinerary: currentItinerary.value)
      self.todayButton.isHidden = true
      self.unreadNewsIndicator.isHidden = true
      self.itineraryView.isHidden = false
      
      if let distance = currentItinerary.value.distance, distance < Globals.kNavDestinationTreshold {
        self.itineraryDurationLabel.textColor = Globals.blueColor
        self.itineraryDurationLabel.text = "Stop"
      } else {
        self.itineraryDurationLabel.textColor = Globals.greenColor
        self.itineraryDurationLabel.text = currentItinerary.value.duration?.minutesDurationToString ?? " - "
      }
      
      self.dashboardView.showItinerary(itinerary: currentItinerary.value, location: LocationManager.sharedInstance.currentLocation, heading: LocationManager.sharedInstance.currentHeading)
    }
  }
  
  func hideItinerary() {
    self.mapView.deleteRouteFromMap()
    currentItinerary.accept(Itinerary.empty())
    self.todayButton.isHidden = false
    self.unreadNewsIndicator.isHidden = false
    self.updateNewsIndicator()
    self.itineraryView.isHidden = true
    self.itineraryDurationLabel.textColor = Globals.greenColor
    self.itineraryDurationLabel.text = " - "
    
    self.dashboardView.hideItinerary()
  }
  
  func checkBorders() {
    
    if let currentLocation = LocationManager.sharedInstance.currentLocation, !passedBorders {
      BorderManager.shared.contains(location: currentLocation)
        .filter { $0 != true }
        .take(1)
        .subscribe(onNext: { [weak self] contains in
          DispatchQueue.main.async {
            if let weakself = self {
              self?.passedBorders = true
              BordersAlert.showIn(viewController: weakself)
            }
          }
          }
        )
        .disposed(by: bag)
    }
  }
  
  func checkDirectionality() {
    
    let currentSailingDirection = LocationManager.sharedInstance.currentSailingDirection
    let requiredDirectionality = trafficManager.currentDirectionality
    if requiredDirectionality > 0 && currentSailingDirection > 0 && currentSailingDirection != requiredDirectionality {
      
      showDirectionalityButton(withAnimation: true)
      
      if !dismissedWarning && state != .warning {
        self.state = .warning
        performSegue(withIdentifier: "oneWaySegue", sender: directionalityButton)
      }
      
    } else {
      hideDirectionalityButton(withAnimation: true)
    }
  }
  
  func checkTraffic( hasTraffic: Bool ) {
    if hasTraffic {
      self.showTrafficButton(withAnimation: true)
    } else {
      self.hideTrafficButton(withAnimation: true)
    }
  }
  
  // Handler for compass button tap, state changes to normal
  @IBAction func handleCompassButtonTap(_ sender: Any) {
    if ( mapView.state != .tracking ) {
      mapView.state = .tracking
      state = .standard
    }
  }
  
  @IBAction func handleRouteButtonTap(_ sender: Any) {
    
    // perform segue for showing route detail with currently active route
    let storyboard = UIStoryboard.init(name: "Route", bundle: nil)
    let routeVC = storyboard.instantiateViewController(withIdentifier: "RouteDetailViewController") as! RouteDetailViewController
    let navController = VWNavigationController(rootViewController: routeVC)
    //        let navController = UINavigationController(rootViewController: routeVC)
    
    if let route = RouteManager.sharedInstance.currentActiveRoute {
      routeVC.route = route
    }
    self.present(navController, animated:true, completion: nil)
  }
  
  @IBAction func handleItineraryViewTap(_ sender: Any) {
    performSegue(withIdentifier: "destinationDetail", sender: sender)
  }
  
  // MARK: - Notification handlers
  
  @objc func handlePOISelected( notification: Notification ) {
    
    if let poi = notification.userInfo?["poi"] as? POI {
      self.mapView.selectPOIOnMap(poi: poi)
    }
    
  }
  
  @objc func handlePOIDeselected( notification: Notification ) {
    
    if let poi = notification.userInfo?["poi"] as? POI {
      self.mapView.deselectPOIOnMap(poi: poi)
    }
    
    if mapView.state == .tracking {
      
      var location:CLLocation! = nil
      if let currentLocation = LocationManager.sharedInstance.currentLocation {
        location = currentLocation
      }
      
      if location != nil {
        // move mapView to POI location
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
          self.mapView.updateForLocation(location: location)
        })
      }
      
    } else {
      
      self.mapView.padding = .zero
      
      //            // move mapView to POI location
      //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05, execute: {
      //                self.mapView.backToLastCameraPosition()
      //            })
      
    }
  }
  
  func handleAnchorPlace( atCoordinate coordinate: CLLocationCoordinate2D) {
    
    // Temp show marker on map
    let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
    let _ = self.mapView.addAnchorToMap(forLocation: location)
    
    // Load the anchors on the map and compass
    self.loadAnchors()
    
    var message = "anchor.explanation".localized()
    if ( self.anchorDataController.hasAnchor ) {
      message = "anchor.explanation.replace".localized()
    }
    
    // Show alert window
    let alertController = UIAlertController(title: "anchor.title".localized(), message: message, preferredStyle: UIAlertController.Style.alert)
    
    // Clear Action
    let cancelAction = UIAlertAction(title: "button.cancel".localized(), style: .default) {
      (result : UIAlertAction) -> Void in
      
      // Remove temp anchor from map
      self.mapView.deleteAnchorFromMap()
      
      self.dismiss(animated: true)
    }
    
    alertController.addAction(cancelAction)
    
    let okAction = UIAlertAction(title: "anchor.button.drop".localized(), style: .default) {
      (result : UIAlertAction) -> Void in
        
        //self.mapView.deleteAnchorFromMap()
      
      // Remove existing anchor
      if self.anchorDataController.hasAnchor && self.anchorDataController.fetchedItems.count > 0 {
        // check if existing anchor is navigation destination
                
        // Remove temp anchor from map
         self.mapView.deleteAnchorFromMap()
        
        let existingAnchor = self.anchorDataController.fetchedItems[0]
        
        if let destination = ItineraryManager.shared.currentActiveDestination, destination == existingAnchor {
          ItineraryManager.shared.currentActiveDestination = nil
        }

        self.compassView.deletePOIFromCompass(poi: existingAnchor)
        self.mapView.deletePOIFromMap(poi: existingAnchor)
        
        
        if let _ = try? self.anchorDataController.deleteAllAnchors() {}
        
        let success = try? self.anchorDataController.storeAnchor( atCoordinate: coordinate)
        if success == true {
            
            print("..........",coordinate,".........")
            
          self.loadAnchors()

        }
        
        return
      }
              
      let success = try? self.anchorDataController.storeAnchor( atCoordinate: coordinate)
      if success == true {

        self.loadAnchors()

      }
 
    }
    alertController.addAction(okAction)
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  @objc func handleAnchorRemove( notification: Notification ) {
    
    if let _ = notification.userInfo?["anchor"] as? POI {
      
      if self.anchorDataController.fetchedItems.count > 0 {
        self.compassView.deletePOIFromCompass(poi: self.anchorDataController.fetchedItems[0])
        self.mapView.deletePOIFromMap(poi: self.anchorDataController.fetchedItems[0])
        
      }
      
      // Remove the anchor from the map and compass
      if let _ = try? self.anchorDataController.deleteAnchor() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
          self.loadAnchors()
        })
      }
    }
  }
  
  @objc func handleLocationUpdate( notification: Notification ) {
    
    // Update UI for the location change (speed and address)
    if let location = LocationManager.sharedInstance.currentLocation {
      
      updateForLocation( location: location )
      // Also update for heading (rotation of the arrow i.e.)
      compassView.updateForUserHeading( heading: LocationManager.sharedInstance.currentHeading );
      
        if ItineraryManager.shared.currentActiveDestination != nil {
        showItinerary()
      }
    }
  }
  
  @objc func handleHeadingUpdate( notification: Notification ) {
    
    if ( state != .paused ) {
      // Update UI for the location change (speed and address)
      compassView.updateForUserHeading( heading: LocationManager.sharedInstance.currentHeading );
      
        if ItineraryManager.shared.currentActiveDestination != nil {
        showItinerary()
      }
    }
  }
  
  @objc func handleSailingDirectionChange( notification: Notification ) {
    
    if LocationManager.sharedInstance.currentSailingDirection == 0 {
      dismissedWarning = false
    }
  }
  
  @objc func updateForEvent() {
    
    guard let activeEvent = EventManager.event else {
      self.eventButton.isHidden = true
      print("No active event")
      return
    }
    
    self.eventButton.isHidden = false
    
    if activeEvent.eventIcon.large != "",
      let url = URL(string: activeEvent.eventIcon.large ),
      let data = NSData(contentsOf: url) {
      
      let scale = UIScreen.main.scale
      self.eventButton.setImage(UIImage(data: data as Data, scale: scale), for: .normal)
      self.eventButton.setImage(UIImage(data: data as Data, scale: scale), for: .highlighted)
    } else {
      self.eventButton.setImage(UIImage(named: "Icon Menu Koningsdag"), for: .normal)
      self.eventButton.setImage(UIImage(named: "Icon Menu Koningsdag"), for: .highlighted)
    }
  }
  
  @objc func updateForRoute() {
    
    // check if active route is set
    if let route = RouteManager.sharedInstance.currentActiveRoute {
      
      // Draw buoyPOI on compass
      if let buoyPoi = RouteManager.sharedInstance.buoyPOI {
         if Localize.currentLanguage() == "nl" {
            buoyPoi.name=route.name_nl
        }
        else
        {
            buoyPoi.name=route.name_en
        }
        buoyPoi.html=route.description
        self.compassView.addPOIToCompass(poi: buoyPoi, withForcedSlug: "buoy")
      }
      
      // draw route on mapView
      self.mapView.addRouteToMap(route: route)
      
      // Hide event button
      self.eventButton.isHidden = true
      
      // Hide today button
      self.todayButton.isHidden = true
      self.unreadNewsIndicator.isHidden = true
      
      // Show route label and set text
      let labelText = (Localize.currentLanguage() == "nl" ? route.name_nl : route.name_en) ?? ""
      self.routeButton.isHidden = false
      self.routeButton.setTitle(labelText, for: .normal)
      self.routeButton.setTitle(labelText, for: .highlighted)
    } else {
      
      // Remove bouyPOI from compass
      if let buoyPOI = RouteManager.sharedInstance.buoyPOI {
        self.compassView.deletePOIFromCompass(poi: buoyPOI)
      }
      
      self.mapView.deleteRouteFromMap()
      // hide route label and set text
      self.routeButton.isHidden = true
      self.routeButton.setTitle("", for: .normal)
      self.routeButton.setTitle("", for: .highlighted)
      // Check for current event, and set button accordingly
      self.updateForEvent()
      self.todayButton.isHidden = false
      self.unreadNewsIndicator.isHidden = false
    }
  }
  
  @objc func updateForDestination() {
    if let _ = ItineraryManager.shared.currentActiveDestination {
      showItinerary(skipInterval: true)
    } else {
      hideItinerary()
    }
  }
}

// MARK: - Animation EXTENSION

extension MainViewController {    
  
  func showDashboard( withAnimation:Bool) {
    self.dashboardHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
    
  }
  
  func hideDashboard( withAnimation:Bool) {
    self.dashboardHiddenConstraint.priority = UILayoutPriority(rawValue: 999)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func hideWeatherButton( withAnimation: Bool ) {
    
    // increase constraint priority and let autoLayout animate
    self.weatherButtonHiddenConstraint.priority = UILayoutPriority(rawValue: 999)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func showWeatherButton( withAnimation: Bool ) {
    
    // Update animation to last forecasted condition
    self.weatherButton.updateAnimation()
    
    // increase constraint priority and let autoLayout animate
    self.weatherButtonHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func showCompassButton() {
    
    self.compassButton.alpha = 0
    self.compassButton.isHidden = false
    UIView.animate(
      withDuration: 0.3,
      animations: {
        // Show the compass button when state changes to manual
        self.compassButton.alpha = 1
    }
    )
  }
  
  func hideCompassButton() {
    
    self.compassButton.alpha = 0
    
    UIView.animate(
      withDuration: 0.3,
      animations: {
        // Show the compass button when state changes to manual
        self.compassButton.alpha = 0
    },
      completion: { (finished: Bool) in
        self.compassButton.isHidden = true
    }
    )
  }
  
  func hideDirectionalityButton( withAnimation: Bool ) {
    
    // increase constraint priority and let autoLayout animate
    self.directionalityButtonHiddenConstraint.priority = UILayoutPriority(rawValue: 999)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func showDirectionalityButton( withAnimation: Bool ) {
    
    // increase constraint priority and let autoLayout animate
    self.directionalityButtonHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func hideTrafficButton( withAnimation: Bool ) {
    
    // increase constraint priority and let autoLayout animate
    self.trafficButtonHiddenConstraint.priority = UILayoutPriority(rawValue: 999)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func showTrafficButton( withAnimation: Bool ) {
    
    // increase constraint priority and let autoLayout animate
    self.trafficButtonHiddenConstraint.priority = UILayoutPriority(rawValue: 100)
    
    if ( withAnimation ) {
      // Make it an animation
      executeAnimation( withDamping: false, startHandler: {}, completionHandler: {} )
    } else {
      self.view.layoutIfNeeded()
    }
  }
  
  func executeAnimation( withDamping:Bool = true, startHandler: @escaping() -> Void, completionHandler: @escaping() -> Void ) {
    
    // Perform start handler (pre-animating)
    startHandler()
    
    // Perform animation
    UIView.animate(
      withDuration: withDamping ? 0.65 : 0.6,
      delay: 0,
      usingSpringWithDamping: withDamping ? 0.76 : 1,
      initialSpringVelocity: withDamping ? 0.2 : 1,
      options: [],
      animations: {
        self.view.layoutIfNeeded()
    },
      completion: { (finished: Bool) in
        completionHandler()
    }
    )
  }
}

// MARK: TodayViewControllerDelegate

extension MainViewController: TodayViewControllerDelegate {
  
  func spotlightPoiSelected(sender: Any?) {
    if let poi = sender as? POI {
      dismiss(animated: true, completion: nil)
      
      self.mapView.state = .manual
      self.state = .paused
      
      self.mapView.selectPOIOnMap(poi: poi)
      
      // Perform segue
      performSegue(withIdentifier: "poiDetailSegue", sender: poi)
      
      // Send notification to observers
      let name:NSNotification.Name = NSNotification.Name(Globals.kPOISelectedKey)
      let userInfo:[String: Any] = ["poi": poi]
      NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
      
    }
  }
  
  func spotlightRouteSelected(sender: Any?) {
    if let route = sender as? Route {
      dismiss(animated: true, completion: nil)
      performSegue(withIdentifier: "routeDetailSegue", sender: route)
    }
  }
}

// MARK: Hipe sliding view controller

extension MainViewController: SlidingViewControllerDelegate {
  
  func slidingViewDidDismiss(controller: SlidingViewController) {
    
    print("did dismiss slidingview, current state:", self.state, "and Mapstate:", self.mapView.state)
    
    state = .standard
    dismissedWarning = true
    
  }
  
  func slidingViewWillDismiss(controller: SlidingViewController) {
    
    print("will dismiss slidingview, current state:", self.state, "and Mapstate:", self.mapView.state)
    
    self.state = .standard
    dismissedWarning = true
    
    if let controller = controller as? POIDetailViewController,
      let poi = controller.poi {
      
      // Send notification to observers
      let name:NSNotification.Name = NSNotification.Name(Globals.kPOIDeselectedKey)
      let userInfo:[String: POI] = ["poi": poi]
      NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
      
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    self.state = .paused
    
    if let controller = segue.destination as? SlidingViewController {
      
      controller.modalPresentationStyle = .custom
      controller.slidingViewDelegate = self
      controller.transitioningDelegate = controller
      controller.visibleTapView = false
      
      if segue.identifier == "menuSegue" {
        controller.direction = .left
        controller.isExpandable = false
        controller.visibleTapView = true
      }
      
      if segue.identifier == "searchSegue" {
        controller.isExpandable = false
      }
      
      if segue.identifier == "todaySegue", let todayViewController = controller as? TodayViewController {
        controller.isExpandable = false
        todayViewController.todayViewControllerDelegate = self
      }
      
      if segue.identifier == "poiDetailSegue" || (controller.isKind(of: POIDetailViewController.self) && segue.identifier != "destinationDetail"),
        let poi = sender as? POI {
        
        let detailController = controller as! POIDetailViewController
        detailController.poi = poi
        
        controller.allowPassthrough = true
      }
      
      if segue.identifier == "destinationDetail" {
        let poi = ItineraryManager.shared.currentActiveDestination
        let detailController = controller as! POIDetailViewController
        detailController.poi = poi
      }
      
      if segue.identifier == "weatherSegue" || segue.identifier == "oneWaySegue" {
        controller.isExpandable = false
        controller.fittedSize = true
      }
      
      if segue.identifier == "trafficSegue" {
        controller.isExpandable = false
        controller.fittedSize = true
        
        self.mapView.state = .traffic
        
      }
    }
    
    if segue.identifier == "routeDetailSegue", let detailController = segue.destination as? RouteDetailViewController, let route = sender as? Route {
      
      detailController.route = route
      detailController.backButton.isEnabled = false
    }
  }
}

// MARK: - Google Maps Mapview delegate EXTENSION

extension MainViewController: GMSMapViewDelegate {
  
  // Delegate method for checking user gestures
  func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    
    if (gesture && self.mapView.state != .manual) {
      // Change map state to manual
      self.mapView.state = .manual
      self.state = .paused
    }
  }
  
  func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
    
    // Update the visible markers
    self.mapView.showVisibleMarkers()
  }
  
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    
    if let poi = marker.userData as? POI {
      
      //            self.state = .manual
      
      if poi.slug == "buoy" {
        return true
      }
      
      //            isPaused = true
      self.state = .paused
      
      // Perform segue
      performSegue(withIdentifier: "poiDetailSegue", sender: poi)
      
      // Send notification to observers
      let name:NSNotification.Name = NSNotification.Name(Globals.kPOISelectedKey)
      let userInfo:[String: Any] = ["poi": poi]
      NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
      
    }
    
    // return true to prevent map from propagating to default behavior
    return true
  }
  
  func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
    
    // Show AnchorAlert info thingy
    self.handleAnchorPlace(atCoordinate: coordinate)
    
  }
}

extension MainViewController: VWCompassViewDelegate {
  
  func didTap(poi: POI) {
    
    // Perform segue
    performSegue(withIdentifier: "poiDetailSegue", sender: poi)
    
    // Send notification to observers
    let name:NSNotification.Name = NSNotification.Name(Globals.kPOISelectedKey)
    let userInfo:[String: Any] = ["poi": poi]
    NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
  }
}

extension MainViewController: WeatherManagerDelegate {
  
  func weatherAlert(forCondition condition: WeatherCondition) {
    
    switch (condition) {
    case .rain:
      self.showWeatherButton(withAnimation: true)
    case .clear:
      self.hideWeatherButton(withAnimation: true)
    default:
      self.hideWeatherButton(withAnimation: true)
    }
  }
}

extension MainViewController: TrafficManagerDelegate {
  
  func didUpdateMaximumSpeed( maxSpeed: Double ) {
    
    if let speed = LocationManager.sharedInstance.averageSpeed {
      compassView.updateForSpeed( speed: speed, maxSpeed: maxSpeed )
      dashboardView.updateForSpeed( speed: speed, maxSpeed: maxSpeed )
    }
  }
  
  func didUpdateDirectionality(requiredDirectionality: Int) {
    dismissedWarning = false
    LocationManager.sharedInstance.currentSailingDirection = 0
    checkDirectionality()
  }
  
  func didUpdateTraffic(hasTraffic: Bool)  {
    checkTraffic( hasTraffic: hasTraffic )
  }
  
  func didUpdateEnvironmentalZone(isEnvironmentalZone: Bool) {
    
    if isEnvironmentalZone {
      
      // check if userdefaults flag is set for showedEnvironmentalWarning
      // if not, show the alert.
      
      let defaults = UserDefaults.standard
      if let _ = defaults.object(forKey: Globals.kShowedEnvironmentalWarning) {
        // Do nothing
      } else {
        
        // Show alert
        let title = "traffic.environmental.title".localized()
        let message = "traffic.environmental.message".localized()
        
        let moreInfo = "traffic.environmental.moreinfo".localized()
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // Clear Action
        let moreInfoAction = UIAlertAction(title: moreInfo, style: .default) {
          (result : UIAlertAction) -> Void in
          
          let detailVC = self.storyboard!.instantiateViewController(withIdentifier: "HelpDetailViewController") as! HelpDetailViewController
          detailVC.filename = "Info-Milieuzone"
          //                    let navController = UINavigationController(rootViewController: detailVC)
          let navController = VWNavigationController(rootViewController: detailVC)
          self.present(navController, animated:true, completion: nil)
          
        }
        alertController.addAction(moreInfoAction)
        
        let okAction = UIAlertAction(title: "OK", style: .default)
        {
          (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        // Store flag in userdefault
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: Globals.kShowedEnvironmentalWarning)
        defaults.synchronize()
        
      }
      
    }
    
  }
  
  func checkLaunchCount() {
    
    let defaults = UserDefaults.standard
    guard let launchCount = defaults.object(forKey: Globals.kLaunchCountKey) as? Int else {
      return
    }
    
    print("launchcount!: ", launchCount)
    
    switch (launchCount) {
      
    case 5:
      showRatingDialog()
    default:
      break
    }
  }
  
  func showRatingDialog() {
    
    let title:String = "dialog.rating.title".localized()
    let message:String = "dialog.rating.message".localized()
    
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    
    // Clear Action
    let cancelAction = UIAlertAction(title: "button.cancel".localized(), style: .default) {
      (result : UIAlertAction) -> Void in
    }
    alertController.addAction(cancelAction)
    
    let okAction = UIAlertAction(title: "button.rate".localized(), style: .default) {
      (result : UIAlertAction) -> Void in
      
      // Rdirect to App store in safari
      let urlString = Localize.currentLanguage() == "nl" ? Globals.kAppStoreURL_nl : Globals.kAppStoreURL_en
      if let url = URL(string: urlString) {
        UIApplication.shared.open(url)
      }
      
    }
    alertController.addAction(okAction)
    
    self.present(alertController, animated: true, completion: nil)
    
  }
}


extension DispatchQueue {

    static func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    
    static func background_sync(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).sync {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }

}
