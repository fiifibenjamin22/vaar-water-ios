//
//  HelpTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 14/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

struct HelpItem {
    var title_nl:String
    var title_en:String
    var filename:String
}

class HelpTableViewController: UITableViewController {
    
    private let helpItemCellIdentifier = "helpCell"
    private let helpItems = [
        HelpItem(title_nl: "Over deze app", title_en: "About this app", filename: "Info-Vaarwater"),
        HelpItem(title_nl: "Tour", title_en: "Tour", filename: ""),
        HelpItem(title_nl: "Regels op het water", title_en: "Boating rules", filename: "Info-Regels"),
        HelpItem(title_nl: "Borden", title_en: "Signs", filename: "Info-Borden"),
        HelpItem(title_nl: "Bruggen", title_en: "Bridges", filename: "Info-Bruggen"),
        //HelpItem(title_nl: "Binnenhavengeld", title_en: "Inland harbor dues", filename: "Info-Binnenhavengeld"),
        HelpItem(title_nl: "Milieuzone", title_en: "Environmental Area", filename: "Info-Milieuzone"),
//        HelpItem(title_nl: "Proclaimer", title_en: "Proclaimer", filename: "Info-Proclaimer")
    ]
    
    var shouldDismissModally: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
        
         if #available(iOS 13.0, *) {
                   let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                   let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                   statusBar.backgroundColor = .red
                   window?.addSubview(statusBar)
            } else {
        //           UIApplication.shared.statusBarView?.backgroundColor = .white
        //           UIApplication.shared.statusBarStyle = .lightContent
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.helpItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: helpItemCellIdentifier, for: indexPath) as! HelpCell

        let helpItem = self.helpItems[ indexPath.row ] as HelpItem
        
        let locale = Localize.currentLanguage()
        if (locale == "nl") {
            cell.nameLabel?.text = helpItem.title_nl
        } else {
            cell.nameLabel?.text = helpItem.title_en
        }
        
        cell.selectionStyle = .none        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "helpHeaderCell") as! GenericHeaderCell
        cell.headerLabel.text = "help.header.explanation".localized()
        cell.headerImageView.image = UIImage(named: "Icon Header Help")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 160.0
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = Globals.lightGrayColor
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.clear
    }
        
    @IBAction func handleBackTap(_ sender: Any) {
        // Default dismiss
        self.dismiss( animated: true, completion: nil)
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
    }
        
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            performSegue(withIdentifier: "firstRunSegue", sender: self)
        } else {
            performSegue(withIdentifier: "helpDetailSegue", sender: self)
        }
        
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let controller = segue.destination as? HelpDetailViewController,
            let itemIndex = tableView.indexPathForSelectedRow?.row {
            
            let helpItem = self.helpItems[ itemIndex ]
            
            if (Localize.currentLanguage() == "nl") {
                controller.title = helpItem.title_nl
                controller.filename = helpItem.filename
            } else {
                controller.title = helpItem.title_en
                controller.filename = helpItem.filename
            }
            
            
        }
        
        if segue.identifier == "firstRunSegue",
            let controller = segue.destination as? FirstRunViewController {
            
            controller.isFirstRun = false
            
        }
        
    }

}
