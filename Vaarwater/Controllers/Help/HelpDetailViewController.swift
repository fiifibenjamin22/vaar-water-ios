//
//  HelpDetailViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 14/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import WebKit
import Localize_Swift

class HelpDetailViewController: UIViewController {

    var filename:String?
    
//    @IBOutlet weak var webView: WKWebView!
    
    let webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("HELP_RUNNING......")
        
      
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9254901961, green: 0, blue: 0, alpha: 1)
        setupWebView()
        
        // Do any additional setup after loading the view.
        var directoryPath:String
        
        if Localize.currentLanguage() == "nl" {
            directoryPath = "HTML/nl"
        } else {
            directoryPath = "HTML/en"
        }
        
//        if let path = Bundle.main.path(forResource: filename, ofType: "html", inDirectory: directoryPath) {
//            let fileURL = URL(fileURLWithPath: path)
//            self.webView?.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
//        }
        
        if let path = Bundle.main.path(forResource: filename, ofType: "html", inDirectory: directoryPath) {
            let request = URLRequest(url: URL(fileURLWithPath: path))
            webView.load( request )
        }
        
    }
  
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView(prefix: "Help: ")
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupWebView(){
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(webView)
        
        webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        
        webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        
        
    }
        
    @IBAction func handleCloseTap(_ sender: Any) {
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
        
        
        
        
        
        
        
        
        
        

//        if let presentingVC = self.presentingViewController?.presentingViewController {
//            presentingVC.dismiss(animated: true, completion: nil)
//        } else {
//            self.dismiss(animated: true, completion: nil)
//        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
