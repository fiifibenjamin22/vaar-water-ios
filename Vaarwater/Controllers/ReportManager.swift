//
//  ReportManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 16/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class ReportManager {
  static func submit(data: Data?, parameters: [String: String]) -> Single<String> {
    return Single.create(subscribe: { observer in
      
      let url = API.sharedInstance.host + "file-report"
      
      
      do {
        Alamofire
          .upload(multipartFormData: { (multipartFormData) in
            if let data = data {
              multipartFormData.append(data, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
            }
            
            for(key, value) in parameters {
              multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url) { result in
          switch result {
          case .success(let uploadRequest, _, _):
            uploadRequest.authenticate(user: "ulpreview", password: "demopreview")
              .responseString { response in
                DispatchQueue.main.async {
                  if let value = response.value {
                    observer(.success(value))
                  } else {
                    
                  }
                }
            }
          case .failure(let error):
            DispatchQueue.main.async {
              observer(.error(error))
            }
          }
        }
      }
      return Disposables.create()
    })
  }
}




//if let image = self.image.value, let imageData = UIImageJPEGRepresentation(image, 100) {
//
//  let url = URL(string: "http://grachten.waternet.nl.preview.uselab.com/api/v2/file-report")!
//  let credential : URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
//
//  do
//  {
//    let request = try! URLRequest(url: url, method: .post)
//
//    //        Alamofire
//    //          .upload(multipartFormData: { (multipartFormData) in
//    //            multipartFormData.append(imageData, withName: "image", fileName: "image.jpg", mimeType: "image/jpg")
//    //          }, to: url, method: .post) { (result) in
//    //            switch result {
//    //            case .success(let upload, _, _):
//    //              upload.authenticate(usingCredential: credential)
//    //                .responseString { response in
//    //                  print(response)
//    //              }
//    //            case .failure(let error):
//    //              print(error)
//    //            }
//    //        }
//
//    let parameters = ["name": "image"] //Optional for extra parameter
//
//    Alamofire.upload(multipartFormData: { multipartFormData in
//      multipartFormData.append(imageData, withName: "fileset",fileName: "file.jpg", mimeType: "image/jpg")
//      for (key, value) in parameters {
//        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//      } //Optional for extra parameters
//    },
//                     to: url)
//    { (result) in
//      switch result {
//      case .success(let upload, _, _):
//
//        upload.authenticate(usingCredential: credential).uploadProgress(closure: { (progress) in
//
//        })
//
//        upload.responseJSON { response in
//          print(response)
//        }
//
//      case .failure(let encodingError):
//        print(encodingError)
//      }
//    }
//
//}
