//
//  SettingsPreferredCategoriesTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 18/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class SettingsPreferredCategoriesTableViewController: UITableViewController {
    
    let familyDataController = POIFamilyDataController.sharedInstance
    let categoryDataController = POICategoryDataController.sharedInstance
    var categoryPositionInArray:Int = -1
    var selectedFamilyIndex:Int = -1
    var selectedCategoryIndexPath:IndexPath?
    var currentSelectedGroup: (type:String, slug: String) = ("","")
    var languageSelected = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Initially set rowHeight to 72
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 72
        
        print(familyDataController.fetchedItems)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if languageSelected == 0 {
            navigationItem.title = "Kies een categorie"
        } else {
            navigationItem.title = "Choose a category"
        }
        checkStoredValues()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let family = familyDataController.fetchedItems[section]
               
        if family.name_nl == "Bruggen en sluizen" || family.name_nl == "Toiletten"  || family.name_nl == "Hulpdiensten" || family.name_nl == "Activiteiten"{
                
              return 72
               
         }else{
                
            return 0
        }
        
    
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        print("SECTIONS: \(familyDataController.fetchedItems.count)")
        return familyDataController.fetchedItems.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let family = familyDataController.fetchedItems[section]
        
        
        
    if family.name_nl == "Bruggen en sluizen" || family.name_nl == "Toiletten"  || family.name_nl == "Hulpdiensten" || family.name_nl == "Activiteiten"{
        if let categoriesForFamily = categoryDataController.fetchCategoriesForFamily(family: family),
            categoriesForFamily.count > 1 {
            return categoriesForFamily.count
        }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if cell.isKind(of: CategoryCell.self) {
            
            // Enable accessoryview in the first place
            (cell as! CategoryCell).shouldShowAccessory = true
            
            // Auto-select if this section is selected
            if indexPath.section == selectedFamilyIndex || !cell.isUserInteractionEnabled {
                (cell as! CategoryCell).autoSelect = true
            } else {
                (cell as! CategoryCell).autoSelect = false
                
                if indexPath == selectedCategoryIndexPath {
                    (cell as! CategoryCell).setSelected(true, animated: true)
                } else {
                    (cell as! CategoryCell).setSelected(false, animated: false)
                }
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var row: CGFloat = 0

        let family = familyDataController.fetchedItems[indexPath.section]
        if family.name_nl == "Bruggen en sluizen" || family.name_nl == "Toiletten"  || family.name_nl == "Hulpdiensten" || family.name_nl == "Activiteiten"{

            
            
        if let categoriesForFamily = categoryDataController.fetchCategoriesForFamily(family: family) {

            if  categoriesForFamily[indexPath.row].name_nl ==  "Picnic plek" {
                
                row = 0
            }
             print("PICNIC_PLEK: \(categoriesForFamily[indexPath.row].name_nl!)")
            if categoriesForFamily[indexPath.row].name_nl ==  "Picnic plek"
                || categoriesForFamily[indexPath.row].name_nl ==  "Bootverhuur"
                || categoriesForFamily[indexPath.row].name_nl ==  "BBQ plek"
                || categoriesForFamily[indexPath.row].name_nl ==  "Zwemlocatie"{
                    
                    

               row =  0
            }else {
                
                row = 70
            }



        }else{
            

            row = 80

            }



        }

        return row

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryCell

        let family = familyDataController.fetchedItems[indexPath.section]
        if family.name_nl == "Bruggen en sluizen" || family.name_nl == "Toiletten"  || family.name_nl == "Hulpdiensten" || family.name_nl == "Activiteiten"{
        
        if let categoriesForFamily = categoryDataController.fetchCategoriesForFamily(family: family) {
            
          
            
            cell.category = categoriesForFamily[indexPath.row]
            
            print("CELL_CATEGORY: \(cell.category!.name_nl!)")
            
            if currentSelectedGroup.type == "category",
                cell.category?.slug == currentSelectedGroup.slug {
                selectedCategoryIndexPath = indexPath
            } else {
                
                // Check if it is already in the current stored settings
                let defaults = UserDefaults.standard
                if let storedArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as? Array<String> {
                    cell.isUserInteractionEnabled = !storedArray.contains( String(format: "category_%@", cell.category?.slug ?? "") )
                }
                
            }
            
        }
        
        return cell
            
        }else {
            
            return UITableViewCell()
        }
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        if view.isKind(of: CategoryCell.self) {
            (view as! CategoryCell).shouldShowAccessory = true
            
            if selectedFamilyIndex == section {
                (view as! CategoryCell).setSelected(true, animated: true)
            } else {
                (view as! CategoryCell).setSelected(false, animated: true)
                (view as! CategoryCell).autoSelect = !view.isUserInteractionEnabled
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView()
         
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "familyCell") as! CategoryCell
        let family = familyDataController.fetchedItems[section]
        
        print("FAMILY_SECTIONS: \(family.name_nl!)")
  
        if family.name_nl == "Bruggen en sluizen" || family.name_nl == "Toiletten"  || family.name_nl == "Hulpdiensten" || family.name_nl == "Activiteiten"{
            
     
             headerCell.family = family
        
        }
        
        if currentSelectedGroup.type == "family", family.slug == currentSelectedGroup.slug {
            
            selectedFamilyIndex = section
            
            
        } else {
            let defaults = UserDefaults.standard
            if let storedArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as? Array<String> {
                headerCell.isUserInteractionEnabled = !storedArray.contains( String(format: "family_%@", family.slug ?? "") )
            }
        }
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleSectionHeaderTap))
        headerCell.addGestureRecognizer(tapRecognizer)
        
        if family.name_nl == "Bruggen en sluizen" || family.name_nl == "Toiletten"  || family.name_nl == "Hulpdiensten" || family.name_nl == "Activiteiten"{
                   
                    headerCell.family = family
               
            return headerCell
        
        }else{
            
            headerCell.isHidden = true
            vw.removeFromSuperview()
            
            return nil
            
        }
        
        
        
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
          let family = familyDataController.fetchedItems[section]
          if family.name_nl != "Bruggen en sluizen" || family.name_nl != "Toiletten"  || family.name_nl != "Hulpdiensten" || family.name_nl != "Activiteiten"{
                 
          
                return nil
             
          }else{
            
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedFamilyIndex = -1
        selectedCategoryIndexPath = indexPath
        
        let family = familyDataController.fetchedItems[indexPath.section]
        if let categoriesForFamily = categoryDataController.fetchCategoriesForFamily(family: family) {
            let category = categoriesForFamily[indexPath.row]
            currentSelectedGroup.type = "category"
            currentSelectedGroup.slug = category.slug ?? ""
        }
        
        self.saveCategory()
        
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
    }
    
    @objc func handleSectionHeaderTap(gestureRecognizer: UIGestureRecognizer) {
        
        // Select chosen view
        if let view = gestureRecognizer.view as? CategoryCell,
           let family = view.family {
            self.selectedFamilyIndex = self.familyDataController.fetchedItems.firstIndex(of: family) ?? -1
            self.selectedCategoryIndexPath = nil
            
            currentSelectedGroup.type = "family"
            currentSelectedGroup.slug = family.slug ?? ""
            
            self.saveCategory()
            
            self.tableView.reloadData()
        }
        
    }
    
    func checkStoredValues() {
        
        let defaults = UserDefaults.standard
        let preferredFamiliesArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey)
        
        if let currentSelectedSlug = preferredFamiliesArray?[categoryPositionInArray] as? String {
            
            let components = currentSelectedSlug.components(separatedBy: "_")
            if components.count > 1 {
                let prefix = components[0]
                let slug = components[1]
                
                print("CHECK_SLUGS: \(slug)")
                
                currentSelectedGroup.type = prefix
                currentSelectedGroup.slug = slug                
            }
        }
        
    }
    
    func saveCategory() {
        
        let defaults = UserDefaults.standard
        var preferredFamiliesArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey)
        
        if preferredFamiliesArray != nil,
           (preferredFamiliesArray?.count)! > 0,
            categoryPositionInArray > -1 {
            
            if selectedFamilyIndex > -1 {
                
                let family = familyDataController.fetchedItems[selectedFamilyIndex]
                preferredFamiliesArray?[ categoryPositionInArray ] = String(format: "family_%@", family.slug!)
                
                
                
            } else if selectedCategoryIndexPath != nil {
                
                if let section = selectedCategoryIndexPath?.section,
                   let row = selectedCategoryIndexPath?.row,
                   let categoriesForFamily = categoryDataController.fetchCategoriesForFamily(family: familyDataController.fetchedItems[section])
                   {
                        preferredFamiliesArray?[ categoryPositionInArray ] = String(format: "category_%@", categoriesForFamily[row].slug!)
                    }
            }

        }
        
        // Store compass family settings
        defaults.set(preferredFamiliesArray, forKey: Globals.kSettingsPreferredFamiliesKey)
        defaults.synchronize()

        // Post update
        let name:NSNotification.Name = NSNotification.Name(Globals.kSettingsSavedKey)
        NotificationCenter.default.post(name: name, object: nil)        
        
    }
    
    @IBAction func handleBackTap(_ sender: Any) {
        // Default dismiss
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {

        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)

//        self.dismiss( animated: true, completion: nil)
    }

}
