//
//  SettingsTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 30/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class SettingsTableViewController: UITableViewController {
    
    var shouldDismissModally: Bool = false
    
    let languageSectionIndex:Int = 0
    let boatDataSectionIndex:Int = 1
    
    var languageSelectedIndex:Int = 0

    var languageDidChange: Bool = false
    
    var boatData: BoatData = BoatData()
    var preferredCategoriesOrFamilies = [String]()

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var engineTextField: UITextField!
    @IBOutlet weak var widthTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var lengthTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        addToolBar(toTextField: nameTextField)
        addToolBar(toTextField: engineTextField )
        addToolBar(toTextField: widthTextField )
        addToolBar(toTextField: heightTextField )
        addToolBar(toTextField: lengthTextField )
        
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        
        engineTextField!.inputView = pickerView
        
        // initially set the preferredCategories to the default
        for i in 0...Globals.kDefaultFamiliesOnCompass.count - 1 {
            preferredCategoriesOrFamilies.append( String(format:"family_%@", Globals.kDefaultFamiliesOnCompass[i] ) )
            
            print("PREFERED_FAMMILY: \(preferredCategoriesOrFamilies)")
        }
        
        
//        self.setEditing(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        trackScreenView()
        super.viewWillAppear(animated)
        checkStoredValues()
                
        // Reload table data
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (section) {
        case 0:
            return 2
        case 1:
            return 5
        case 2:
            return preferredCategoriesOrFamilies.count
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let section = indexPath.section
        let row = indexPath.row
        
        switch (section) {
        case 0:
            if (indexPath.row == languageSelectedIndex) {
                cell.accessoryType = UITableViewCell.AccessoryType.checkmark;
            } else {
                cell.accessoryType = UITableViewCell.AccessoryType.none;
            }
            
            print("Settings")
        case 1:
            
            switch (row) {
            case 0:
                break
            default:
                break
            }
            
        case 2:
            
            if let cell = cell as? CategoryCell {
                
                print("indexpath row", indexPath.row)
                print("----")
                print("preferredCategoriesOrFamilies", preferredCategoriesOrFamilies)
                
                let storedSlug = self.preferredCategoriesOrFamilies[indexPath.row]
                let slugComponents = storedSlug.components(separatedBy: "_")
                
                guard slugComponents.count > 1 else {
                    let family = POIFamilyDataController.sharedInstance.preferredFamilies[indexPath.row]
                    cell.family = family
                    cell.selectionStyle = .none
                    return
                }
                
                let prefix = slugComponents[0]
                let slug = slugComponents[1]
                
                print("SLUG COMPONENTSN", slugComponents)
                    
                if prefix == "family" {
                    
                    print("CHECK_SLUGS: \(slug)")
                    
                    
                    let family = POIFamilyDataController.sharedInstance.fetchedItems.filter { $0.slug == slug }
                    
                  
                    cell.family = family.first
                    cell.category = nil
                    
                  
                    
                } else if prefix == "category" {
                    let category = POICategoryDataController.sharedInstance.fetchedItems.filter { $0.slug == slug }
                    cell.family = nil
                    cell.category = category.first
                }
                cell.selectionStyle = .none
                cell.setEditing(true, animated: false)
                cell.showsReorderControl = true
            }
            
            break
            
        default:
            break
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = Globals.lightGrayColor
        header.textLabel?.font = UIFont(name: "Maison Neue-Bold", size: 17)
        header.textLabel?.textColor = Globals.darkGrayColor
        header.textLabel?.textAlignment = .center
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            
            return 0
        }else{
        
        return 44
            
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selecting row")
        
        let section = indexPath.section
        let row = indexPath.row
        
        switch (section) {
        case 0:
            
            // if they are selecting the same row, keep it checked
            if indexPath.row == languageSelectedIndex {
                return
            }
            
            // toggle old one off and the new one on
            let newCell = tableView.cellForRow(at: indexPath)
            if newCell?.accessoryType == UITableViewCell.AccessoryType.none {
                newCell?.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
            let oldCell = tableView.cellForRow(at: IndexPath(row: languageSelectedIndex, section: languageSectionIndex) )
            if oldCell?.accessoryType == UITableViewCell.AccessoryType.checkmark {
                oldCell?.accessoryType = UITableViewCell.AccessoryType.none
            }

            languageSelectedIndex = indexPath.row
            
        case 1:
            
            switch (row) {
            case 0:
                break
            default:
                break
            }
            
        case 2:
            
            print("selected in section 2")
            performSegue(withIdentifier: "showPreferredCategoriesSegue", sender: tableView.cellForRow(at: indexPath))
            
        default:
            break
        }
        
        // Save the settings
        self.saveSettings()
        
        
    }
    
    // MARK: Compass categories/families methods 
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return (indexPath.section == 2)
        return true
    }
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
                
        if indexPath.section == 2 {
            return true
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let oldSourceValue = preferredCategoriesOrFamilies[ sourceIndexPath.row ]
        let oldDestinationValue = preferredCategoriesOrFamilies[ destinationIndexPath.row ]
        
        preferredCategoriesOrFamilies[ destinationIndexPath.row ] = oldSourceValue
        preferredCategoriesOrFamilies[ sourceIndexPath.row ] = oldDestinationValue
        
        saveSettings()        
    }
    
    override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            var row = 0
            if sourceIndexPath.section < proposedDestinationIndexPath.section {
                row = self.tableView(tableView, numberOfRowsInSection: sourceIndexPath.section) - 1
            }
            
            return IndexPath(row: row, section: sourceIndexPath.section)
        }
        return proposedDestinationIndexPath
        
    }
    
    @IBAction func handleBackTap(_ sender: Any) {
        // Default dismiss
        self.dismiss( animated: true, completion: {
            
            if self.languageDidChange {
                let name:NSNotification.Name = NSNotification.Name(Globals.kLanguageChangeKey)
                NotificationCenter.default.post(name: name, object: nil)
            }
        })
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        
        if languageDidChange {
            let name:NSNotification.Name = NSNotification.Name(Globals.kLanguageChangeKey)
            NotificationCenter.default.post(name: name, object: nil)
        }
        
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    // MARK: - Custom methods
    
    func checkStoredValues() {
        
        let defaults = UserDefaults.standard
        
        // Check language
        if let storedValue = defaults.object(forKey: Globals.kSettingsLanguageKey) as? Int {
            let cell = self.tableView.cellForRow( at: IndexPath(row: storedValue, section: languageSectionIndex) )
            cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
            self.languageSelectedIndex = storedValue
        }
        
        // reset languageDidChange
        languageDidChange = false
        
        // Check boat data
        if let storedBoatDataDict = defaults.object(forKey: Globals.kSettingsBoatDataKey) as? Dictionary<String, AnyObject> {
            self.boatData = BoatData(fromDict: storedBoatDataDict)
            
        } else {
            print("Error with stuff")
        }
        
        if boatData.name != "",
            let cell = self.tableView.cellForRow( at: IndexPath(row: 0, section: boatDataSectionIndex) ) as? SettingsTextCell {
            cell.valueField?.text = boatData.name
        }
        
        if boatData.engine != .unknown,
            let cell = self.tableView.cellForRow( at: IndexPath(row: 1, section: boatDataSectionIndex) ) as? SettingsTextCell {
            cell.valueField?.text = boatData.engine.description
        }
        
        if boatData.width != 0,
            let cell = self.tableView.cellForRow( at: IndexPath(row: 2, section: boatDataSectionIndex) ) as? SettingsTextCell {
            cell.valueField?.text = String(format: "%d cm", boatData.width)
        }

        if boatData.height != 0,
            let cell = self.tableView.cellForRow( at: IndexPath(row: 3, section: boatDataSectionIndex) ) as? SettingsTextCell {
            cell.valueField?.text = String(format: "%d cm", boatData.height)
        }

        if boatData.length != 0,
                   let cell = self.tableView.cellForRow( at: IndexPath(row: 4, section: boatDataSectionIndex) ) as? SettingsTextCell {
                   cell.valueField?.text = String(format: "%d cm", boatData.length)
               }
        
        // Preferred categories/families, and load default if none are set
        if let preferredFamiliesArray = defaults.array(forKey: Globals.kSettingsPreferredFamiliesKey) as? Array<String> {
            self.preferredCategoriesOrFamilies = preferredFamiliesArray
        }
        
    }
    
    func saveSettings() {
        
        // Store frequency settings in user defaults
        let defaults = UserDefaults.standard
        
        print("saving settings, boatdata:", boatData )
        
        // Store boatData
        defaults.set(boatData.encode(), forKey: Globals.kSettingsBoatDataKey)
        
        // Change language on-the-fly
        
        let currentLanguageIndex = defaults.integer(forKey: Globals.kSettingsLanguageKey)
        
        if languageSelectedIndex != currentLanguageIndex {
            defaults.set(languageSelectedIndex, forKey: Globals.kSettingsLanguageKey)
            
            switch (languageSelectedIndex) {
            case 0:
                defaults.set(["nl"], forKey: "AppleLanguages")
                Localize.setCurrentLanguage("nl")
                break;
            case 1:
                defaults.set(["en"], forKey: "AppleLanguages")
                Localize.setCurrentLanguage("en")
                break;
            default:
                defaults.set(["nl"], forKey: "AppleLanguages")
                Localize.setCurrentLanguage("nl")
                break;
            }
            
//            defaults.set(["nl"], forKey: "AppleLanguages")
//            Localize.setCurrentLanguage("nl")
            
            languageDidChange = true
        } else {
            languageDidChange = false
        }
        
        // Store compass family settings
        defaults.set(self.preferredCategoriesOrFamilies, forKey: Globals.kSettingsPreferredFamiliesKey)
        
        // Store/synchronize defaults
        defaults.synchronize()
        
        let name:NSNotification.Name = NSNotification.Name(Globals.kSettingsSavedKey)
        NotificationCenter.default.post(name: name, object: nil)
        
    }
    
//    @IBAction func handle
    
    @IBAction func handleEditingDidEnd(_ sender: Any) {
        
        if let name = self.nameTextField.text {
            boatData.name = name
        }
        if let width = Int(self.widthTextField.text!) {
            boatData.width = width
        }
        if let height = Int(self.heightTextField.text!) {
            boatData.height = height
        }
        
        if let length = Int(self.lengthTextField.text!){

            boatData.length = length
        }
        saveSettings()
    }    
    
    func doneButtonAction(_ sender: Any?) {
        
        (sender as! UITextField).resignFirstResponder()
        saveSettings()

    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let destination = segue.destination as? SettingsPreferredCategoriesTableViewController {
            print("language_set: ",languageSelectedIndex)
            destination.languageSelected = languageSelectedIndex
            print("POS_IN_ARRAY: \(tableView.indexPathForSelectedRow?.row ?? -1)")
            destination.categoryPositionInArray = tableView.indexPathForSelectedRow?.row ?? -1
        }
        
    }

}

extension SettingsTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return EngineType.count - 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let engineType = EngineType(rawValue: row + 1) else {
            fatalError("Unknown EngineType")
        }
        
        return engineType.description
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let engineType = EngineType(rawValue: row + 1) else {
            fatalError("Unknown EngineType")
        }
        
        print("EngineType.description: \(engineType.description) | EngineType.value: \(engineType.rawValue)")
        
        boatData.engine = engineType
        engineTextField.text = engineType.description
        
        
    }
    
}
