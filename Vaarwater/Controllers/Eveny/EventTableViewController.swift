//
//  EventTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class EventTableViewController: UITableViewController {
    
    private let eventItemCellIdentifier = "eventCell"
    
    var shouldDismissModally: Bool = false    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Initially set rowHeight to 55
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 55
        
        updateForEvent()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
    }
    
    // MARK: - Custom methods
    
    func updateForEvent() {
        
        guard let activeEvent = EventManager.event else {
            print("No active event")
            return
        }
        
        // set navigation color
//        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = activeEvent.eventColor
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue : UIColor.white])
//        if let img = UIImage(named: "Bar Header Koningsdag")?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 22, right: 0)) {
//            self.navigationController?.navigationBar.setBackgroundImage(img, for: .default)
//        }
        
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Bar Header Koningsdag"), for: .default)
        
        if Localize.currentLanguage() == "nl" {
            self.navigationController?.navigationBar.topItem?.title = activeEvent.name_nl
        } else {
            self.navigationController?.navigationBar.topItem?.title = activeEvent.name_en
        }
        
    }
    
    @IBAction func handleBackTap(_ sender: Any) {
        // Default dismiss
        self.dismiss( animated: true, completion: nil)
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let activeEvent = EventManager.event {
            return activeEvent.pages.count
        } else {
            return 0
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: eventItemCellIdentifier, for: indexPath) as! GenericCell
        
        if let activeEvent = EventManager.event {
            let eventPage = activeEvent.pages[ indexPath.row ]
            let locale = Localize.currentLanguage()
            if (locale == "nl") {
                cell.nameLabel?.text = eventPage.name_nl
            } else {
                cell.nameLabel?.text = eventPage.name_en
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "eventHeaderCell") as! GenericHeaderCell
        
        if let activeEvent = EventManager.event {
            
            if Localize.currentLanguage() == "nl" {
                cell.headerLabel.text = activeEvent.intro_nl
            } else {
                cell.headerLabel.text = activeEvent.intro_en
            }
            
            if let url = URL(string: activeEvent.eventScreenIcon.large ),
                let data = NSData(contentsOf: url) {
                
                let scale = UIScreen.main.scale
                cell.headerImageView.image = UIImage(data: data as Data, scale: scale)
            } else {
                cell.headerImageView.image = UIImage(named: "Icon Header Koningsdag")
            }
            
            let backgroundView = UIImageView(image: UIImage(named: "Event Header Koningsdag"))
            backgroundView.backgroundColor = UIColor.clear
            backgroundView.isOpaque = false
            cell.backgroundView = backgroundView
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 160.0
    }
    

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let activeEvent = EventManager.event else {
            print("No active event")
            return
        }
        
        if let controller = segue.destination as? EventDetailViewController,
            let indexPath = tableView.indexPathForSelectedRow {
            
            let eventPage = activeEvent.pages[indexPath.row]
            controller.eventPage = eventPage
            
        }
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
