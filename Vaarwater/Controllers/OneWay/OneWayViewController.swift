//
//  OneWayViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 21/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Lottie

class OneWayViewController: SlidingViewController {
    
    @IBOutlet weak var animationView: AnimationView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
        //        addAnimation()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleSailingDirectionChange),
                                               name: NSNotification.Name(rawValue: Globals.kSailingDirectionChange),
                                               object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
    }
    
    
    // MARK: - Custom methods
    
    @objc func handleSailingDirectionChange( notification: Notification ) {

        if LocationManager.sharedInstance.currentSailingDirection == 0 {
            self.dismiss(animated: true, completion: nil)
        }
        
    }    
    
    
//    func addAnimation() {
//        
//        guard let condition = WeatherManager.sharedInstance.latestForecastCondition else {
//            print("No weather condition available")
//            return
//        }
//        
//        if let lottieView = LOTAnimationView(name: getAnimationName( forCondition: condition)) {
//            lottieView.loopAnimation = true
//            lottieView.frame = animationView.bounds
//            lottieView.contentMode = .scaleAspectFit
//            animationView.addSubview(lottieView)
//            
//            lottieView.play(completion: { (finished) in
//                // Do Something
//            })
//        }
//        
//    }
//    
//    func getAnimationName( forCondition condition: WeatherCondition) -> String {
//        
//        var name:String = ""
//        
//        switch ( condition ) {
//        case .rain:
//            name = "rain"
//        default:
//            name = "rain"
//        }
//        
//        return name
//        
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
