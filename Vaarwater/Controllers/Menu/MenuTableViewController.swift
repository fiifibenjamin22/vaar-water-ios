//
//  MenuTableTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 14/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift
import SafariServices

private var newsIndicatorRowIndex:Int = 3

class MenuTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Create notification observers
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateMenuHeader),
                                               name: NSNotification.Name(Globals.kEventSwitched),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateMenuHeader),
                                               name: NSNotification.Name(Globals.kSettingsSavedKey),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateNewsIndicator),
                                               name: NSNotification.Name(Globals.kNewsUpdatedNotificationKey),
                                               object: nil)
        
        if let activeEvent = EventManager.event {
            self.view.backgroundColor = activeEvent.eventColor
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.updateNewsIndicator()
    }
    
    // MARK: - Custom methods
    
    @objc func updateMenuHeader( notification: NSNotification ) {
        if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? MenuHeaderCell {
            cell.updateBoatData()
            cell.updateForEvent()
        }
    }
    
    @objc func updateNewsIndicator() {
        
        print("updating news indicator")
        
        if let cell = tableView.cellForRow(at: IndexPath(row: newsIndicatorRowIndex, section: 0)) as? MenuCell {
            let unreadCount = NewsDataController.sharedInstance.numberOfUnreadNewsItems
            cell.updateIndicator( count: unreadCount )
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        // If no event is set, make event cell height 0
        if EventManager.event == nil {
            if indexPath.row == 1 {
                return 0
            }
        }     
        return super.tableView(tableView, heightForRowAt: indexPath)
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let activeEvent = EventManager.event {
            
            // Set the color for the first row cell
            if indexPath.row == 0 {
                // Set background color
                cell.backgroundColor = activeEvent.eventColor
            }
            
            if indexPath.row == 1 {
                
                if let cell = cell as? MenuCell {
                    if Localize.currentLanguage() == "nl" {
                        
                        
                        cell.nameLabel?.text = activeEvent.name_nl
                    } else {
                        cell.nameLabel?.text = activeEvent.name_en
                        
                          print("CHECK_MENU_NAMES")
                    }
                }
                
            }
            
        }
        
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = Globals.lightGrayColor
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.clear
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let controller = segue.destination as? VWNavigationController {
            
            controller.modalPresentationStyle = .custom
            controller.transitioningDelegate = controller
            
        }
        
    }
    
}
