//
//  EventDetailViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 28/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class EventDetailViewController: UIViewController {
    
    var eventPage:EventPage?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateForEvent()
        
        fillContentWithEventPageData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView(prefix: "Event: ")
        super.viewDidAppear(animated)
    }
    
    // MARK: - Custom methods
    
    func updateForEvent() {
        
        guard let activeEvent = EventManager.event else {
            print("No active event")
            return
        }
        
        // set navigation color
        //        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = activeEvent.eventColor
        self.navigationController?.navigationBar.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue : UIColor.white])
        
//        if Localize.currentLanguage() == "nl" {
//            self.navigationController?.navigationBar.backItem?.title = activeEvent.name_nl
//        } else {
//            self.navigationController?.navigationBar.backItem?.title = activeEvent.name_en
//        }
        
        if Localize.currentLanguage() == "nl" {
            self.navigationController?.navigationBar.topItem?.title = activeEvent.name_nl
        } else {
            self.navigationController?.navigationBar.topItem?.title = activeEvent.name_en
        }
    }    
    
    func fillContentWithEventPageData( ) {
        
        guard let eventPage = self.eventPage else {
            print("no event data available")
            return
        }
        
        // Set title
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.textLabel.attributedText!)
        
        if Localize.currentLanguage() == "nl" {
            self.titleLabel?.text = eventPage.name_nl
            attributedString.mutableString.setString(eventPage.text_nl)
//            self.textLabel?.text = eventPage.text_nl
        } else {
            self.titleLabel?.text = eventPage.name_en
            attributedString.mutableString.setString(eventPage.text_en)
//            self.textLabel?.text = eventPage.text_en
        }
        
        self.textLabel?.attributedText = attributedString
        
    }
    
    @IBAction func handleBackTap(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
        
//        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
