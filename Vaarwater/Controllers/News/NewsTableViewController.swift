//
//  HelpTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 14/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import Localize_Swift
import Firebase

private let newsItemCellIdentifier = "newsCell"
private let newsHeaderCellIdentifier = "newsHeaderCell"

class NewsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
        
    let fetchedResultsController = NewsDataController.sharedInstance.fetchedResultsController
    var shouldDismissModally: Bool = false
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        print("Message Loaded....")
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
                
        // Initially set rowHeight to 60
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 60
        
        // Perform fetch
        do {
//            try NewsDataController.sharedInstance.executeFetch()
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
        
        if #available(iOS 13.0, *) {
                   let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                   let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                   statusBar.backgroundColor = .red
                   window?.addSubview(statusBar)
            } else {
        //           UIApplication.shared.statusBarView?.backgroundColor = .white
        //           UIApplication.shared.statusBarStyle = .lightContent
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {

        // Get index before calling super
        let index = tableView.indexPathForSelectedRow
        super.viewWillAppear(animated)
        
        if (index != nil) {
            self.tableView.reloadRows(at: [index!], with: UITableView.RowAnimation.automatic)
        }
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let news = fetchedResultsController.fetchedObjects else { return 0 }
        return news.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: newsItemCellIdentifier, for: indexPath) as! NewsCell
        
        // Configure cell with fetched news
//        let news = fetchedResultsController.object(at: indexPath)
        let news = fetchedResultsController.object(at: indexPath)
        cell.news = news
        
        print("News is read!: ", news.read)
        
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: newsHeaderCellIdentifier) as! GenericHeaderCell
        cell.headerLabel.text = "news.header.explanation".localized()
        cell.headerImageView.image = UIImage(named: "Icon Header Berichten")
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 160.0
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = Globals.lightGrayColor
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.clear
    }
    
    @IBAction func handleBackTap(_ sender: Any) {
        // Default dismiss
        self.dismiss( animated: true, completion: nil)
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let controller = segue.destination as? NewsDetailViewController,
            let indexPath = tableView.indexPathForSelectedRow {
            
            controller.news = self.fetchedResultsController.object(at: indexPath)
        }
    }    
}
