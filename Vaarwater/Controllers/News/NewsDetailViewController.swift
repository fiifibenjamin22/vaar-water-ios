//
//  NewsDetailViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 28/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift
import Kingfisher

class NewsDetailViewController: UIViewController {
    
    var news:News?
//    var containedVC:UIViewController?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
//    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
  
  override func viewDidLoad() {
        super.viewDidLoad()
    
      navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9254901961, green: 0, blue: 0, alpha: 1)

 
        fillContentWithNewsData()
        
        textView.textContainer.lineFragmentPadding = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView(prefix: "News: ")
        super.viewDidAppear(animated)
        
        let name:NSNotification.Name = NSNotification.Name(Globals.kNewsUpdatedNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
        
    }

    // MARK: - Custom methods
        
    func fillContentWithNewsData( ) {
        
        guard let news = self.news else {
            print("no news data available")
            return
        }
        
        // Update news read flag
        news.updateRead(true)
        
        // Set title
        self.titleLabel?.text = news.title
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        
        if Localize.currentLanguage() == "nl" {
            dateFormatter.locale = Locale(identifier: "nl_NL")
        } else {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        
        // Set date
        if let date = news.createdDate as Date? {
            self.dateLabel?.text = dateFormatter.string(from: date )
        }
        
        // Set text
        do {
            let htmlString = try NSMutableAttributedString(data: news.html!.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: convertToNSAttributedStringDocumentReadingOptionKeyDictionary([convertFromNSAttributedStringDocumentAttributeKey(NSAttributedString.DocumentAttributeKey.documentType) : convertFromNSAttributedStringDocumentType(NSAttributedString.DocumentType.html)]), documentAttributes: nil)
            
            if htmlString.length > 0 {
                let attributes = convertFromNSAttributedStringKeyDictionary(self.textView.attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: self.textView.attributedText.length - 1)))
                
                htmlString.addAttributes(convertToNSAttributedStringKeyDictionary(attributes), range: NSMakeRange(0, htmlString.length - 1))
                self.textView?.attributedText = htmlString.trimEmptyTagsAndWhiteSpace()
            }
            
        } catch {
            print(error)
        }

      let sizeThatFits = textView.sizeThatFits(CGSize(width: self.view.frame.size.width - 60, height: CGFloat(MAXFLOAT)))
      print(self.view.frame.size.width - 60)
        textViewHeightConstraint.constant = sizeThatFits.height - 10
        
        // Set image
        if let mediaUrl = news.mediaUrl {
            
            // Create loading indicator
            let loadingLayer = LoadingLayer(attachToView: self.imageView)
            
            self.imageView.kf.setImage(with: URL(string: mediaUrl)) { result in
                switch result {
                case .success(let value):
                    print("Image: \(value.image). Got from: \(value.cacheType)")
                    loadingLayer.stopAnimating()
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
            
//            self.imageView.kf.setImage(with: URL(string: mediaUrl), completionHandler: {
//                (image, error, cacheType, imageUrl) in
//                // image: Image? `nil` means failed
//                // error: NSError? non-`nil` means failed
//                // cacheType: CacheType
//                //               .none - Just downloaded
//                //               .memory - Got from memory cache
//                //               .disk - Got from memory Disk
//                // imageUrl: URL of the image
//                loadingLayer.stopAnimating()
//            })
          
        }
        
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
        
//        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringDocumentReadingOptionKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.DocumentReadingOptionKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.DocumentReadingOptionKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentAttributeKey(_ input: NSAttributedString.DocumentAttributeKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentType(_ input: NSAttributedString.DocumentType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKeyDictionary(_ input: [NSAttributedString.Key: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
