//
//  POIDetailViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import QuartzCore
import CoreLocation
import Localize_Swift
import MobileCoreServices
import RxSwift
import RxCocoa

class POIDetailViewController: SlidingViewController {
  
  var poi:POI?
  var isSearchResult: Bool = false
  let bag = DisposeBag()
  
  let locationManager = CLLocationManager()
  
  @IBOutlet weak var nameLabel: UILabel!
  
  @IBOutlet weak var categoryLabel: UILabel!
  @IBOutlet weak var distanceView: POIDistanceView!
  @IBOutlet weak var pinToCompassButton: UIButton!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var removeAnchorButton: UIButton!
  @IBOutlet weak var startNavigationButton: UIButton!
  @IBOutlet weak var stopNavigationButton: UIButton!
  
  @IBOutlet weak var betaLabel: UILabel!
  @IBOutlet weak var mainTextView: UITextView!
  
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var addressTextView: UITextView!
  
  @IBOutlet weak var measurementsLabel: UILabel!
  @IBOutlet weak var measurementsTextView: UITextView!
  @IBOutlet weak var measurementsWarningView: UIView!
  @IBOutlet weak var measurementsWarningLabel: UILabel!
  
  @IBOutlet weak var contactLabel: UILabel!
  @IBOutlet weak var contactTextView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let poi = poi, let poiLocation = poi.location {
      
//      let poiInRange = BorderManager.shared.contains(location: poi.location)
//      let currentInRange = BorderManager.shared.contains(location: LocationManager.sharedInstance.currentLocation)
//            
//      let inRangeObservable = Observable.combineLatest(poiInRange, currentInRange) { ($0 && $1) }
      
      
      // calculate iterinary from current location to poi
      let itinerary = ItineraryManager.shared.itinerary(to: poiLocation)
        .asDriver(onErrorJustReturn: Itinerary.empty())
      
      itinerary.map { $0.distance?.distanceToString ?? " - " }
        .drive(distanceView.distanceLabel.rx.text)
        .disposed(by: bag)
      
      itinerary.map { $0.duration?.minutesDurationToString ?? " - " }
        .drive(distanceView.durationLabel.rx.text)
        .disposed(by: bag)
      
      itinerary.map { $0.duration == nil }
        .drive(distanceView.durationIcon.rx.isHidden)
        .disposed(by: bag)
      
      itinerary.map { $0.duration != nil }
        .drive(distanceView.durationUnknownIcon.rx.isHidden)
        .disposed(by: bag)
      
      itinerary.map { $0.distance == nil }
        .drive(distanceView.distanceIcon.rx.isHidden)
        .disposed(by: bag)
      
      itinerary.map { $0.duration != nil }
        .drive(distanceView.distanceUnknownIcon.rx.isHidden)
        .disposed(by: bag)
      
      // check if poi is destination of current itinerary
      if let destination = ItineraryManager.shared.currentActiveDestination, destination == self.poi {
        startNavigationButton.setTitle("nav.startNavigationButton.resume".localized(), for: .normal)
        stopNavigationButton.isHidden = false
        pinToCompassButton.isHidden = true
      } else {
        // poi is not current itinerary
        startNavigationButton.setTitle("nav.startNavigationButton.start".localized(), for: .normal)
        stopNavigationButton.isHidden = true
      }
      
      var showNavOption = true
      
      if let categories = self.poi?.categories?.allObjects as? [POICategory] {
        for category in categories {
          if let familyslug = category.family?.slug, familyslug == "reports" {
            showNavOption = false
            break
          }
//          if let slug = category.slug, slug == "report" || slug == "traffic-block" || slug == "impediment" || slug == "delay" , slug == "no-entry" {
//            showNavOption = false
//            break
//          }
        }
      }
      
      if showNavOption {
        
        startNavigationButton.isHidden = false
        
        itinerary.map { $0.distance != nil && $0.duration != nil && $0.waypoints.count > 1 }
          .drive(startNavigationButton.rx.isEnabled)
          .disposed(by: bag)
        
        itinerary.map { $0.distance == nil || $0.duration == nil || $0.waypoints.count < 1 }
          .drive(betaLabel.rx.isHidden)
          .disposed(by: bag)
      } else {
        
        startNavigationButton.isHidden = true
        betaLabel.isHidden = true
      }
    }
    
    
    
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    // Check if POI is set, and return if not
    guard let poi = poi, poi.isKind( of: POI.self) else {
      print("No valid POI data found, closing view controller")
      self.dismiss(animated: true, completion: nil)
      return
    }
    
    fillWithPOIData()
    
    if !isSearchResult {
      self.backButton.isHidden = true
    }
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    trackScreenView()
    super.viewDidAppear(animated)
  }
  
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    
    // Send notification to observers
    let name:NSNotification.Name = NSNotification.Name(Globals.kPOIDeselectedKey)
    let userInfo:[String: Any] = ["poi": self.poi as Any]
    NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
    
  }
  
  private func fillWithPOIData() {
    
    guard let poi = self.poi else {
      print("no poi data available")
      return
    }
    
    var showPinToCompass = true
    
    // Set 'top-view' data: name and distance
    if (poi.anchor) {
      nameLabel.text = "anchor.name".localized()
    } else {
      nameLabel.text = poi.name
    }
    
    if poi.pinned {
      self.pinToCompassButton.setTitle( "pinToCompass.button.unpinned".localized(), for: .normal)
      self.pinToCompassButton.setTitle( "pinToCompass.button.unpinned".localized(), for: .highlighted)
    } else {
      self.pinToCompassButton.setTitle( "pinToCompass.button.pinned".localized(), for: .normal)
      self.pinToCompassButton.setTitle( "pinToCompass.button.pinned".localized(), for: .highlighted)
    }
    
    // Check if it fits through, or show warning
    if !poi.fitsThrough {
      self.measurementsWarningLabel.setAttributedText(text: "poi.detail.bridgeWarning".localized() )
      self.measurementsWarningView.isHidden = false
    } else {
      self.measurementsWarningView.isHidden = true
    }
    
    // Check for report categories, and if it contains, hide the pin-to-compassbutton
    if let categories = poi.categories?.allObjects as? [POICategory] {
      
      var showMeasurements:Bool = false
      
      for category in categories {
        
        if let slug = category.slug,
          slug == "set-bridge" {
          
          showMeasurements = true
          
        }
        
        if let familyslug = category.family?.slug, familyslug == "reports" {
          print("contained true" )
          
          self.hideDistanceView()
          showPinToCompass = false
          break
        }
        
      }
      
      if showMeasurements {
        
        self.setMeasurementsText()
        
        self.measurementsLabel.isHidden = false
        self.measurementsTextView.isHidden = false
        
        //                self.mainTextView.isHidden = true
        //                self.addressLabel.isHidden = true
        //                self.addressTextView.isHidden = true
        //                self.contactLabel.isHidden = true
        //                self.contactTextView.isHidden = true
        
      } else {
        
        self.measurementsLabel.isHidden = true
        self.measurementsTextView.isHidden = true
        
      }
      
      let locale = Localize.currentLanguage()
      if var categories = poi.categories?.allObjects as? [POICategory] {
        categories = categories.sorted( by: { $0.id < $1.id })
        if locale == "nl" {
          if let category = categories.first {
            self.categoryLabel?.text = category.name_nl
          }
        } else {
          if let category = categories.first {
            self.categoryLabel?.text = category.name_en
          }
        }
      }
      
       // if (poi.categories != nil)
       // {
          self.setMainText()
          self.setAddressText()
          self.setContactText()
       // }
    }
    else
    {
        //this a a route startpoint
        self.measurementsLabel.isHidden=true
        self.contactLabel.isHidden=true
        self.addressLabel.isHidden=true
        self.mainTextView.isHidden = true
        self.addressTextView.isHidden = true
        self.contactTextView.isHidden = true
    }
    
    if let location = poi.location {
      
      //            self.hideDistanceView()
      
      //            if let calculatedDistance = LocationManager.sharedInstance.currentLocation?.distance(from: location ),
      //                calculatedDistance > 0 {
      //                distanceView.distance = calculatedDistance
      //            } else {
      //                self.hideDistanceView()
      //            }
      
      // Set address in case of anchor
      if poi.anchor {
        
        // Hide the pin to compass button
        pinToCompassButton.isHidden = true
        removeAnchorButton.isHidden = false || ItineraryManager.shared.currentActiveDestination == self.poi
        
        CLGeocoder().reverseGeocodeLocation( location, completionHandler: {(placemarks, error)-> Void in
          
          if error != nil {
            print("Reverse geocoder failed with error", error!.localizedDescription)
            return
          }
          
          print("reverser geocoding anchor")
          
          if placemarks != nil {
            
            var addressText = ""
            if placemarks!.count > 0 {
              
              print("Found placemark!", placemarks![0])
              
              let placemark = placemarks![0] as CLPlacemark
              if let address = placemark.thoroughfare {
                addressText += String(format: "%@ \n", address)
              }
              if let postal = placemark.postalCode {
                addressText += String(format: "%@ ", postal)
              }
              if let city = placemark.locality {
                addressText += String(format: "%@ \n", city)
              }
            }
            
            self.addressLabel.isHidden = false
            self.addressTextView.isHidden = false
            self.addressTextView.text = addressText
          }
          
        })
        
      } else {
        pinToCompassButton.isHidden = !showPinToCompass || ItineraryManager.shared.currentActiveDestination == self.poi
        removeAnchorButton.isHidden = true
      }
      
    } else {
      self.hideDistanceView()
    }
    
  }
  
  private func setMainText() {
    
    guard let poi = self.poi else {
      print("no poi data available")
      return
    }
    
    let textAttributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.mainTextView.attributedText!)
    if poi.html != nil {
      
      var htmlEntry:String = poi.html ?? ""
      if ( Localize.currentLanguage() == "nl") {
        if let html_nl = poi.html_nl {
          htmlEntry = html_nl
        }
      } else {
        if let html_en = poi.html_en {
          htmlEntry = html_en
        }
      }
      
      do {
        let htmlString = try NSMutableAttributedString(data: htmlEntry.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: convertToNSAttributedStringDocumentReadingOptionKeyDictionary([convertFromNSAttributedStringDocumentAttributeKey(NSAttributedString.DocumentAttributeKey.documentType) : convertFromNSAttributedStringDocumentType(NSAttributedString.DocumentType.html)]), documentAttributes: nil)
        
        if htmlString.length > 0 {
          let attributes = convertFromNSAttributedStringKeyDictionary(self.mainTextView.attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: self.mainTextView.attributedText.length - 1)))
          
          htmlString.addAttributes(convertToNSAttributedStringKeyDictionary(attributes), range: NSMakeRange(0, htmlString.length - 1))
          self.mainTextView?.attributedText = htmlString.trimEmptyTagsAndWhiteSpace()
        }
        
      } catch {
        print(error)
      }
      
    } else {
      
      var textEntry:String = poi.text ?? ""
      if ( Localize.currentLanguage() == "nl") {
        if let text_nl = poi.text_nl {
          textEntry = text_nl
        }
      } else {
        if let text_en = poi.text_en {
          textEntry = text_en
        }
      }
      
      textAttributedString.mutableString.setString( textEntry )
      mainTextView.attributedText = textAttributedString
      
    }
    
    // If text is empty, hide the text block
    if mainTextView.attributedText.length == 0 {
      mainTextView.isHidden = true
    }
    
  }
  
  private func setAddressText() {
    
    guard let poi = self.poi else {
      print("no poi data available")
      return
    }
    
    // Set address
    var addressText:String = ""
    if let address = poi.address, address != "" {
      addressText += String(format: "%@ \n", address)
    }
    if let postcode = poi.postalCode, postcode != "" {
      addressText += String(format: "%@ ", postcode)
    }
    if let city = poi.city, city != "" {
      addressText += String(format: "%@ \n", city)
    }
    
    
    // If nothing is set, hide the address blocks
    if addressText.count > 0 {
      addressTextView.text = addressText
    } else {
      addressLabel.isHidden = true
      addressTextView.isHidden = true
    }
    
  }
  
  private func setContactText() {
    
    guard let poi = self.poi else {
      print("no poi data available")
      return
    }
    
    // Set contact
    var contactText:String = ""
    if let website = poi.website, website != "" {
      contactText += String(format: "%@ \n", website)
    }
    if let email = poi.email, email != "" {
      contactText += String(format: "%@ \n", email)
    }
    if let phone = poi.phone, phone != "" {
      contactText += String(format: "%@ \n", phone)
    }
    
    if contactText.count > 0 {
      contactTextView.text = contactText
    } else {
      contactLabel.isHidden = true
      contactTextView.isHidden = true
    }
    
  }
  
  private func setMeasurementsText() {
    
    guard let poi = self.poi else {
      print("no poi data available")
      return
    }
    
    // Set contact
    var measurementsText:String = ""
    
    if let width = poi.width as? Double, width > 0.0 {
      measurementsText += String(format: "%@: %.0f cm\n", "unit.width".localized(), width)
    }
    if let height = poi.height as? Double, height > 0.0 {
      measurementsText += String(format: "%@: %.0f cm\n", "unit.height".localized(), height)
    }
    
    if measurementsText.count > 0 {
      measurementsTextView.text = measurementsText
    } else {
      measurementsLabel.isHidden = true
      measurementsTextView.isHidden = true
    }
    
  }
  
  private func removeScrollViewShadow() {
    // cleanup shadow layers
    if let sublayers = self.scrollView.layer.sublayers {
      for layer in sublayers {
        if (layer.name == "shadowLayer") {
          layer.removeFromSuperlayer()
        }
      }
    }
  }
  
  private func createScrollViewShadow() {
    
    removeScrollViewShadow()
    
    let layer = CALayer()
    
    let path = UIBezierPath(rect: scrollView.layer.bounds.insetBy(dx: -5, dy: -1))
    let cutout = UIBezierPath(rect: scrollView.layer.bounds).reversing()
    path.append(cutout)
    layer.name = "shadowLayer"
    layer.shadowPath = path.cgPath
    layer.masksToBounds = false
    
    // Shadow properties
    layer.shadowColor = UIColor(white: 0, alpha: 1).cgColor
    layer.shadowOffset = .zero
    layer.shadowOpacity = 0.2
    layer.shadowRadius = 1
    
    self.scrollView.layer.addSublayer( layer )
    
  }
  
  private func hideDistanceView() {
    distanceView.isHidden = true
    var frame = distanceView.frame
    frame.size.height = 0
    distanceView.frame = frame
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    mainTextView.textContainer.lineFragmentPadding = 0
    addressTextView.textContainer.lineFragmentPadding = 0
    contactTextView.textContainer.lineFragmentPadding = 0
    
    createScrollViewShadow()
  }
  
  override func slidingViewPresentationDidAdjustToFullscreen() {
    super.slidingViewPresentationDidAdjustToFullscreen()
    self.scrollView.isScrollEnabled = true
  }
  
  override func slidingViewPresentationDidAdjustToHalfscreen() {
    super.slidingViewPresentationDidAdjustToHalfscreen()
    self.scrollView.isScrollEnabled = false
  }
  
  @IBAction func handlePinTap(_ sender: Any) {
    
    guard let poi = self.poi else {
      return
    }
    
    let wasPinned = self.poi?.pinned ?? false
    let isPinned = !wasPinned
    
    if isPinned {
      self.pinToCompassButton.setTitle( "pinToCompass.button.unpinned".localized(), for: .normal)
      self.pinToCompassButton.setTitle( "pinToCompass.button.unpinned".localized(), for: .highlighted)
      
      let name:NSNotification.Name = NSNotification.Name(Globals.kPOIPinnedKey)
      let userInfo:[String: POI] = ["poi": poi]
      NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
      
    } else {
      self.pinToCompassButton.setTitle( "pinToCompass.button.pinned".localized(), for: .normal)
      self.pinToCompassButton.setTitle( "pinToCompass.button.pinned".localized(), for: .highlighted)
      
      let name:NSNotification.Name = NSNotification.Name(Globals.kPOIUnpinnedKey)
      let userInfo:[String: POI] = ["poi": poi]
      NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
      
    }
    
    // Toggle pinned status
    self.poi?.updatePinToCompass(!wasPinned)
    
  }
  
  @IBAction func handleRemoveAnchorTap(_ sender: Any) {
    
    guard let poi = self.poi else {
      return
    }
    
    // Send notification to observers
    let name:NSNotification.Name = NSNotification.Name(Globals.kAnchorRemovedKey)
    let userInfo:[String: POI] = ["anchor": poi]
    NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
    
    self.dismiss(animated: true, completion: nil)
    
  }
  
  @IBAction func handleBackTap(_ sender: Any) {
    self.slidingViewDelegate?.slidingViewWillDismiss?(controller: self)
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func handleShareLocationTap( _ sender: Any) {
    
    guard let poi = self.poi,
      let latitude = poi.latitude,
      let longitude = poi.longitude,
      let name = poi.name else {
        print("No poi data available when trying to share poi location")
        return
    }
    
    //        let urlPath = String(format: "maps.google.com/maps/?z=17&q=loc:%f+%f", latitude, longitude)
    let urlPath = "https://maps.google.com/maps/?z=17&q=loc:\(latitude),\(longitude) (\(name))"
    if let url = URL(string: urlPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
      
      let activityArray = [url]
      let activityViewController = UIActivityViewController(activityItems: activityArray, applicationActivities: nil)
      activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print,
                                                      UIActivity.ActivityType.assignToContact,
                                                      UIActivity.ActivityType.saveToCameraRoll,
                                                      UIActivity.ActivityType.postToVimeo]
      present(activityViewController, animated: true, completion: nil)
      
    }
    
  }
  
  @IBAction func handleStartNavigationTap(_ sender: Any) {
    guard let poi = self.poi else { return }
    
    ItineraryManager.shared.currentActiveDestination = poi
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func handleStopNavigationTap(_ sender: Any) {
    ItineraryManager.shared.currentActiveDestination = nil
    dismiss(animated: true, completion: nil)
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringDocumentReadingOptionKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.DocumentReadingOptionKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.DocumentReadingOptionKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentAttributeKey(_ input: NSAttributedString.DocumentAttributeKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentType(_ input: NSAttributedString.DocumentType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKeyDictionary(_ input: [NSAttributedString.Key: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
