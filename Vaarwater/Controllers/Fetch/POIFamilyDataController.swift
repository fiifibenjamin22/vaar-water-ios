//
//  POIFamilyDataController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

class POIFamilyDataController: NSObject {
    
    static let sharedInstance: POIFamilyDataController = {
        
        let instance = POIFamilyDataController()
        return instance
        
    }()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<POIFamily> = {
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<POIFamily> = POIFamily.fetchRequest()
        
        // Add sort descriptor
        let sortDescriptor = NSSortDescriptor(key: "sortorder", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let predicate = NSPredicate(format: "(slug != 'reports') AND (slug != 'boat-rental') AND (slug != 'swim-location')")
        fetchRequest.predicate = predicate
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
    var fetchedItems:Array<POIFamily> {
        get {
            
            print(fetchedResultsController.fetchedObjects)
            return fetchedResultsController.fetchedObjects ?? [POIFamily]()
            
            
            
        }
    }
    
    var preferredFamilies:Array<POIFamily> {
        get {
            return self.fetchedItems.filter { $0.search_default == true }
        }
    }
    
    override init() {
        super.init()
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(contextDidChangeNotificationHandler(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
        
        // Execute initial fetch command
        try? executeFetch()
    }
    
    public func executeFetch() throws {
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
    }
    
    func getFamilyBySlug( slug: String ) -> POIFamily? {
        return self.fetchedItems.filter { $0.slug == slug }.first
    }

}
