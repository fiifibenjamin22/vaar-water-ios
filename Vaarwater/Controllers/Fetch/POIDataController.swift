//
//  POIDataController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

protocol POIDataControllerDelegate {
    func poiControllerAddedPOIs() -> [POI]
    func poiControllerDeletedPOIs() -> [POI]
    func poiControllerUpdatedPOIs() -> [POI]
    func shouldReloadPOIs()
}

class POIDataController: NSObject {

    static let sharedInstance: POIDataController = {
        
        let instance = POIDataController()
        return instance
        
    }()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<POI> = {
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<POI> = POI.fetchRequest()
    
        // Add sort descriptor
        let sortDescriptor = NSSortDescriptor(key: "distanceToUser", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
    var fetchedItems:Array<POI> {
        get {
            return fetchedResultsController.fetchedObjects ?? [POI]()
        }
    }
    
    override init() {
        super.init()
        // Execute initial fetch command
        try? executeFetch()
    }
        
    public func executeFetch() throws {
        
        // Perform fetch
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
    }
    
    func fetchPOIs( withPredicate predicate: NSPredicate? ,completion : ((_ data: [POI]?, _ success : Bool, _ error : String?) -> Void)? ) {
        
        // Set predicate
        self.fetchedResultsController.fetchRequest.predicate = predicate
        
        // Reset fetch limit
        self.fetchedResultsController.fetchRequest.fetchLimit = 9999
        
        // Add sort descriptor
        let distanceDescriptor = NSSortDescriptor(key: "distanceToUser", ascending: true)
        self.fetchedResultsController.fetchRequest.sortDescriptors = [distanceDescriptor]
        
        //Execute Fetch request
        
        do {
            
            try self.fetchedResultsController.performFetch()
        } catch let fetchError as NSError {
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
            completion?([], false, fetchError.localizedDescription)
        }
        
        //print("fetched items::::::::", self.fetchedResultsController.fetchedObjects!)
        DispatchQueue.main.async {
            completion?(self.fetchedItems, true, nil)
        }
    }
    
    func fetchClosestPOIForFamily( type: String, value: String, completion : ((_ data: POI?, _ combinedSlug: String?, _ success : Bool, _ error : String?) -> Void)? ) {
        
        self.fetchedResultsController.fetchRequest.fetchLimit = 1
        
        var predicate:NSCompoundPredicate = NSCompoundPredicate()
        if type == "family" {
            let categoryPredicate = NSPredicate(format: "ANY categories.family.slug = [cd] %@", value )
            let distancePredicate = NSPredicate(format: "distanceToUser > 0" )
            
            predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [categoryPredicate, distancePredicate])
        } else if type == "category" {
            let categoryPredicate = NSPredicate(format: "ANY categories.slug = [cd] %@", value )
            let distancePredicate = NSPredicate(format: "distanceToUser > 0" )
            
            predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [categoryPredicate, distancePredicate])
        }
        self.fetchedResultsController.fetchRequest.predicate = predicate
        
        //Execute Fetch request
        DispatchQueue.background(delay: 0.0, background: {
            do {
                try self.fetchedResultsController.performFetch()
            } catch let fetchError as NSError {
                print("retrieveById error: \(fetchError.localizedDescription)")
                let combinedSlug = String(format: "%@_%@", type, value)
                completion?(self.fetchedItems.first, combinedSlug, false, "retrieveById error: \(fetchError.localizedDescription)" )
            }
        }) {
            let combinedSlug = String(format: "%@_%@", type, value)
            completion?(self.fetchedItems.first, combinedSlug, true, nil)
        }
    }
    
    //var gameTimer: Timer?
        
    func storePOIs( pois: Array<Dictionary<String, AnyObject>> ) throws -> [POI] {
        
        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        var storedPOIs = [POI]()
        
        var startPage: UInt = 0
        let ObjectLength: UInt = 1000
        var batchedList = Array<Dictionary<String, AnyObject>>()


        Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (time) in
            let batch = getPageItems(page: startPage, allItems: pois, maxItemsPerPage: ObjectLength)
            batchedList.append(contentsOf: batch)
            
            taskContext.perform {
                batch.forEach { poiDictionary in

                    if let poi = try? POI.createPOIEntity(withDictionary: poiDictionary, inContext: taskContext) {

                         storedPOIs.append(poi)
                    } else {
                         print("Couldn't update POI data for id %d: ", poiDictionary["id"]!)
                    }
                }
                
                if taskContext.hasChanges {
                    do {
                        try taskContext.save()
                    }
                    catch {
                        print("Error: \(error)\nCould not save Core Data context.")
                        //return storedPOIs
                    }
                    //taskContext.reset()
                }
                
                print("batch of objects:::::",batch.count, startPage, batchedList.count, pois.count, pois.count != batchedList.count)
                
                NotificationCenter.default.post(name: NSNotification.Name(Globals.kPoiUpdatedNotificationKey), object: nil)
                
                if batch.count == 0 {
                    time.invalidate()
                }
                startPage += 1
                
                
            }
        })
        
        return storedPOIs
    }
    
    func deletePOIs( pois: Array<Dictionary<String, AnyObject>> ) throws -> [POI] {
        
        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        //        var success:Bool = false
        
        var deletedPOIs = [POI]()
        
        taskContext.performAndWait() {
            
            for poiDictionary in pois {
                
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "POI")
                request.predicate = NSPredicate(format: "id == %@", argumentArray: [poiDictionary["poi_id"] as Any])
                
                let result = try? taskContext.fetch( request )
                
                let resultData = result as! [POI]
                for object in resultData {
                    deletedPOIs.append(object)
                    taskContext.delete(object)
                }
                
            }
            
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
                taskContext.reset()
            }
        }
        
        return deletedPOIs
        
    }    
    
    func deleteAllPOIs( withPredicate predicate: NSPredicate? ) throws -> [POI] {
        
        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        var deletedPOIs = [POI]()
        
        taskContext.performAndWait() {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "POI")
            fetchRequest.returnsObjectsAsFaults = false
            
            if predicate != nil {
                fetchRequest.predicate = predicate
            }
            
            do {
                let results = try taskContext.fetch(fetchRequest)
                for object in results as! [POI]
                {
                    let objectData:NSManagedObject = object as NSManagedObject
                    
                    taskContext.delete(objectData)
                    deletedPOIs.append(object)
                }
                
            } catch let error as NSError {
                print("Delete all data in POI error : \(error) \(error.userInfo)")
            }
            
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
                taskContext.reset()
            }
            
        }
        return deletedPOIs
    }    
    
}
