//
//  POIFetchController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

protocol POIFetchControllerDelegate {
    func poiControllerAddedPOIs() -> [POI]
    func poiControllerDeletedPOIs() -> [POI]
    func poiControllerUpdatedPOIs() -> [POI]
    func shouldReloadPOIs()
}

class POIFetchController: NSObject {
    
//    var delegate: POIFetchControllerDelegate? {
//        set {
//            
//        }
//    }
    
    private lazy var fetchedResultsController: NSFetchedResultsController = { () -> NSFetchedResultsController<NSFetchRequestResult> in 
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "POI")
        
        // Add Sort Descriptors
//        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: true)
//        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
//    var fetchedItems:Array<POI> {
//        get {
//            return self.fetchedResults
//        }
//    }
    
    
//    func fetchPOIs( completion : ((_ data: [POI]?, _ success : Bool, _ error : String?) -> Void)? ) {
//        
//        
//        //Execute Fetch request
//        do {
//            fetchedResults = try taskContext.fetch(fetchRequest) as! [POI]
//        } catch let fetchError as NSError {
//            print("retrieveById error: \(fetchError.localizedDescription)")
//            fetchedResults = Array<POI>()
//            completion?(fetchedResults, false, "retrieveById error: \(fetchError.localizedDescription)" )
//            return
//        }
//        
//        completion?(fetchedResults, true, nil)
//        return
//        
//    }
    
    
}
