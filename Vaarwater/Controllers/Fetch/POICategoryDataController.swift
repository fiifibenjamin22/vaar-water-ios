//
//  POICategoryDataController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

class POICategoryDataController: NSObject {
    
    static let sharedInstance: POICategoryDataController = {
        
        let instance = POICategoryDataController()
        return instance
        
    }()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<POICategory> = {
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<POICategory> = POICategory.fetchRequest()
        
        // Add sort descriptor
        let sortDescriptor = NSSortDescriptor(key: "slug", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
    var fetchedItems:Array<POICategory> {
        get {
            return fetchedResultsController.fetchedObjects ?? [POICategory]()
        }
    }
        
    override init() {
        super.init()
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(contextDidChangeNotificationHandler(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
        
        // Execute initial fetch command
        try? executeFetch()
    }
    
    public func executeFetch() throws {
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
    }
    
    func fetchCategoriesForFamily( family: POIFamily ) -> [POICategory]? {
        
        let taskContext = CoreDataStackManager.sharedInstance.mainQueueContext
        var fetchedResults:[POICategory] = [POICategory]()
        
        // Create request on Event entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "POICategory")
        let predicate = NSPredicate(format: "family == %@", family)
        fetchRequest.predicate = predicate
        
        //        // Add sort descriptor
        //        let sortDescriptor = NSSortDescriptor(key: "sortorder", ascending: true)
        //        fetchRequest.sortDescriptors = [sortDescriptor]
        
        //Execute Fetch request
        do {
            fetchedResults = try taskContext.fetch(fetchRequest) as! [POICategory]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = [POICategory]()
        }
        
        return fetchedResults
        
    }
    
}
