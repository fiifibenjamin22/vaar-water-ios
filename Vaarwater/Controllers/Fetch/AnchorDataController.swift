//
//  AnchorFetchController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

protocol AnchorDataControllerDelegate {
//    func anchorControllerAddedAnchor() -> [Anchor]
//    func anchorControllerDeletedAnchor() -> [Anchor]
//    func anchorControllerUpdatedAnchor() -> [Anchor]
//    func shouldReloadAnchors()
}

class AnchorDataController: NSObject {
    
    var delegate: AnchorDataControllerDelegate? {
        didSet {
            self.executeFetch()
        }
    }
    
    private lazy var fetchedResultsController: NSFetchedResultsController<POI> = {
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<POI> = POI.fetchRequest()
        let predicate = NSPredicate(format: "anchor == 1")
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
    var fetchedItems:Array<POI> {
        get {
            return fetchedResultsController.fetchedObjects ?? [POI]()
        }
    }
    
    var hasAnchor:Bool {
        get {
            return fetchedItems.contains( where: { $0.anchor == true } ) 
        }
    }
    
    func executeFetch() {
        
        // Perform fetch
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
    }
    
    func storeAnchor( atCoordinate coordinate: CLLocationCoordinate2D ) throws -> Bool {
        
        // Place anchor stuff
        let anchorDict:Dictionary<String, AnyObject> = [
            "id": 999999 as AnyObject,
            "name": "Anchor" as AnyObject,
            "slug": "anchor" as AnyObject,
            "text": "" as AnyObject,
            "html": "" as AnyObject,
            "views": 0 as AnyObject,
            "location": [
                "latitude": coordinate.latitude,
                "longitude": coordinate.longitude
                ] as AnyObject,
            "categories": [
                [
                    "id": 999999 as AnyObject,
                    "slug": "anchor" as AnyObject,
                    "name_nl": "Anker" as AnyObject,
                    "name_en": "Anchor" as AnyObject,
                    "map_default": 1 as AnyObject
                ]
                ] as AnyObject
        ]
        
        let taskContext = CoreDataStackManager.sharedInstance.mainQueueContext
      
        var success:Bool = false
        
        taskContext.performAndWait() {
            
            let request = NSFetchRequest<POI>(entityName: "POI")
            request.predicate = NSPredicate(format: "anchor == 1")
            
            let result = try? taskContext.fetch( request )
            
            if let resultData = result {
                for object in resultData {
                  taskContext.delete(object)
                }
            }
            
            do {
                let _ = try POI.createPOIEntity(withDictionary: anchorDict, inContext: taskContext)
            }
            catch {
                print("Couldn't update POI data for ANCHOR. %d: ", anchorDict )
                print("Error info: \(error)")
            }
          
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
            }
            success = true
            
        }
        return success
        
    }
    
    func deleteAnchor() throws -> Bool {
        
        let taskContext = CoreDataStackManager.sharedInstance.mainQueueContext
        var success:Bool = false
        
        taskContext.performAndWait() {
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "POI")
            request.predicate = NSPredicate(format: "anchor == 1")
                        
            let result = try? taskContext.fetch( request )
            let resultData = result as! [POI]
          
            for object in resultData {
              taskContext.delete(object)
            }
          
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
            }
            success = true
            
        }
        return success
        
    }
    
    // delete all anchors
    func deleteAllAnchors() throws -> Bool {
        
        let taskContext = CoreDataStackManager.sharedInstance.mainQueueContext
        var success:Bool = false
        
        taskContext.performAndWait() {
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "POI")
            request.predicate = NSPredicate(format: "anchor == 2")
                        
            let result = try? taskContext.fetch( request )
            let resultData = result as! [POI]
          
            for object in resultData {
              taskContext.delete(object)
            }
          
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
            }
            success = true
            
        }
        return success
        
    }
}
