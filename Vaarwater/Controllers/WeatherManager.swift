//
//  WeatherManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 21/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Alamofire

enum WeatherCondition {
    case drizzle
    case rain
    case thunderstorm
    case snow
    case atmosphere
    case clear
    case clouds
    case extreme
    case wind
    case unknown
}

protocol WeatherManagerDelegate {
    func weatherAlert( forCondition: WeatherCondition )
}

class WeatherManager: NSObject {
    
    static let sharedInstance:WeatherManager = {
        
        let instance = WeatherManager()
        return instance
        
    }()
    
    let kLocationCheckUpdateInterval:Double = 30.0 // in seconds, when no location is available
    let kUpdateInterval:Double = 1800.0 // in seconds
    let kNumberOfLines: Int = 12 // 5 minute per line, 12 means 60 minute forecast check
    let kMinimumRainThreshold:Int = 50
    
    var weatherTimer = Timer()
    var latestForecastCondition: WeatherCondition?
    
    var delegate: WeatherManagerDelegate? {
        didSet {
            requestWeatherUpdate()
        }
    }
    
    override init() {
        
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        weatherTimer.invalidate()
    }
    
    @objc func applicationDidEnterBackground() {
        weatherTimer.invalidate()
    }
    
    @objc func applicationDidBecomeActive() {
        weatherTimer.invalidate()
        self.requestWeatherUpdate()
    }
    
    @objc public func requestWeatherUpdate() {
        
        guard let location = LocationManager.sharedInstance.currentLocation else {
            print("No location available for weather update, checking again in a couple of seconds.")
            
            // Set timer to locationcheckinterval
            weatherTimer.invalidate()
            weatherTimer = Timer.scheduledTimer(timeInterval: TimeInterval(kLocationCheckUpdateInterval), target: self, selector: #selector(requestWeatherUpdate), userInfo: nil, repeats: false);
            
            return
        }
    
        let completionHandler: ( _ data: String?, _ success: Bool, _ error: String? ) -> Void = { ( data, success, error ) in
            
            // Check for errors
            guard error == nil else {
                print("Error retrieving weather update: \(String(describing: error))")
                return
            }
            
            // Check for data validity
            guard data != nil,
                let count = data?.count,
                count > 0 else {
                return
            }
            self.parseWeather( data: data! )
            
        }
        API.sharedInstance.getWeatherStatus( forLocation: location, completion: completionHandler )
        
        // Set timer to check again in 30 minutes
        weatherTimer.invalidate()
        weatherTimer = Timer.scheduledTimer(timeInterval: TimeInterval(kUpdateInterval), target: self, selector: #selector(requestWeatherUpdate), userInfo: nil, repeats: false);
        
    }
    
    public func parseIdToWeatherCondition( id: Int ) -> WeatherCondition {
        
        switch (id) {
        case 200, 201, 202, 210, 211, 212, 221, 230, 231, 232:
            return WeatherCondition.thunderstorm
        case 300, 301, 302, 310, 311, 312, 313, 314, 321:
            return WeatherCondition.drizzle
        case 500, 501, 502, 503, 504, 511, 520, 521, 531:
            return WeatherCondition.rain
        case 600, 601, 602, 611, 612, 615, 616, 620, 621, 622:
            return WeatherCondition.snow
        case 701, 711, 721, 731, 741, 751, 761, 762, 771, 781:
            return WeatherCondition.atmosphere
        case 800:
            return WeatherCondition.clear
        case 801, 802, 803, 804:
            return WeatherCondition.clouds
        case 900, 901, 902, 903, 904, 905, 906:
            return WeatherCondition.extreme
        case 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962:
            return WeatherCondition.wind
        default:
            return WeatherCondition.unknown
            
        }
        
    }
    
    public func parseWeather( data: String ) {
        
        let newlineChars = NSCharacterSet.newlines
        let newLineSplit:Array = data.components(separatedBy:newlineChars).filter{!$0.isEmpty}
        
        if newLineSplit.count >= kNumberOfLines {

            for i in 0...(kNumberOfLines-1) {
                let line = newLineSplit[i]
                if let value = Int(line.components(separatedBy: "|")[0]) {
                    
                    if value > kMinimumRainThreshold {
                        latestForecastCondition = WeatherCondition.rain
                        self.delegate?.weatherAlert(forCondition: WeatherCondition.rain )
                        return
                    }
                }
            }
            
            latestForecastCondition = WeatherCondition.clear
            
            self.delegate?.weatherAlert(forCondition: WeatherCondition.clear )
            
        }
        
    }

}
