//
//  OpenWeatherManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 07/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa

class OpenWeatherManager {
  
  static let service = OpenWeatherManager()
  
  private let appid = "7f6113193d7c9e26dfd28135fff73417"
  let baseUrl = URL(string: "https://api.openweathermap.org/data/2.5/")!
  
  var location: CLLocation? {
    return LocationManager.sharedInstance.currentLocation
  }
  
  func getUV() -> Observable<UV> {
    let url = baseUrl.appendingPathComponent("uvi")
    var request = URLRequest(url: url)
    let keyQueryItem = URLQueryItem(name: "appid", value: appid)
    let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
    
    
    var queryItems = [keyQueryItem]
    if let location = location {
      queryItems.append(URLQueryItem(name: "lat", value: "\(location.coordinate.latitude)"))
      queryItems.append(URLQueryItem(name: "lon", value: "\(location.coordinate.longitude)"))
    } else { // Amsterdam
      queryItems.append(URLQueryItem(name: "lat", value: "52.373367"))
      queryItems.append(URLQueryItem(name: "lon", value: "4.885375"))
    }
    
    urlComponents.queryItems = queryItems
    
    request.url = urlComponents.url!
    
    let session = URLSession.shared
    return session.rx.data(request: request)
      .map { data in
        do {
          let decoder = JSONDecoder()
          let uv = try decoder.decode(UV.self, from: data)
          return uv
        } catch {
          return UV()
        }
    }
  }
  
  func getWeatherInfo() -> Observable<WeatherInfo> {
    let url = baseUrl.appendingPathComponent("weather")
    var request = URLRequest(url: url)
    let keyQueryItem = URLQueryItem(name: "appid", value: appid)
    let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
    
    
    var queryItems = [keyQueryItem]
    if let location = location {
      queryItems.append(URLQueryItem(name: "lat", value: "\(location.coordinate.latitude)"))
      queryItems.append(URLQueryItem(name: "lon", value: "\(location.coordinate.longitude)"))
    } else { // Amsterdam
      queryItems.append(URLQueryItem(name: "lat", value: "52.373367"))
      queryItems.append(URLQueryItem(name: "lon", value: "4.885375"))
    }
    urlComponents.queryItems = queryItems
    
    request.url = urlComponents.url!
    
    let session = URLSession.shared
    return session.rx.data(request: request)
      .map { data in
        do {
          let decoder = JSONDecoder()
          let weatherInfo = try decoder.decode(WeatherInfo.self, from: data)
          return weatherInfo
        } catch {
          fatalError()
        }
    }
  }
  
  struct UV: Codable {
    var value: Double?
    
    var valueText: String {
      if let value = value {
        return String(format: "%.0f", value)
      }
      return ""
    }
    
    var valueDescription: String {
      if let value = value {
        if value < 3.0 {
          return "weather.uv.low"
        } else if value < 6.0 {
          return "weather.uv.moderate"
        } else if value < 8.0 {
          return "weather.uv.high"
        } else if value < 11.0 {
          return "weather.uv.very_high"
        } else {
          return "weather.uv.extreme"
        }
      } else {
        return "weather.uv.none"
      }
    }
    
    init() {
      value = nil
    }
  }
  
  struct WeatherInfo: Codable {
    var wind: Wind?
    fileprivate var main: Main?
    fileprivate var weather: [Weather]
    fileprivate var sys: Sys?
    
    init() {
      wind = nil
      main = nil
      weather = []
      sys = nil
    }
    
    var weatherDescription: String {
      return weather.first?.main ?? ""
    }
    
    var rain: Int {
      if let id = weather.first?.id, id >= 300, id <= 600 {
        return 1
      } else {
        return 0
      }
    }
    
    var temperature: Int? {
      if let temp = main?.temp {
        return Int((temp - 273.15).rounded())
      }
      return nil
    }
    
    var sunset: Date? {
      if let timestamp = sys?.sunset {
        return Date(timeIntervalSince1970: timestamp)
      } else {
        return nil
      }
    }
  }
  
  struct Weather: Codable {
    var id: Int?
    var main: String?
  }
  
  struct Main: Codable {
    var temp: Double?
  }
  
  struct Sys: Codable {
    var sunset: Double?
  }
  
  struct Wind: Codable {
    var speed: Double?
    var deg: Double?
    
    var beaufort: Int? {
      if let speed = speed {
        return Int((pow(speed / 0.8360, 2.0 / 3.0)).rounded())
      } else {
        return nil
      }
    }
    
    var valueDescription: String {
      if let deg = deg {
        
        let val = 45 * Int((deg.truncatingRemainder(dividingBy: 360) / 45).rounded())
        if val == 0 {
          return "weather.wind.north"
        } else if val == 45 {
          return "weather.wind.north_east"
        } else if val == 90 {
          return "weather.wind.east"
        } else if val == 135 {
          return "weather.wind.south_east"
        } else if val == 180 {
          return "weather.wind.south"
        } else if val == 225 {
          return "weather.wind.south_west"
        } else if val == 270 {
          return "weather.wind.west"
        } else if val == 315 {
          return "weather.wind.north_west"
        }
        
        
//        let val = Int((deg / 360).rounded())
//        if 0...22 ~= val || 338...360 ~= val {
//          return "weather.wind.north"
//        } else if 23...67 ~= val {
//          return "weather.wind.north_east"
//        } else if 68...112 ~= val {
//          return "weather.wind.east"
//        } else if 113...157 ~= val {
//          return "weather.wind.south_east"
//        } else if 158...202 ~= val {
//          return "weather.wind.south"
//        } else if 203...247 ~= val {
//          return "weather.wind.south_west"
//        } else if 248...292 ~= val {
//          return "weather.wind.west"
//        } else if 293...337 ~= val {
//          return "weather.wind.north_west"
//        }
      }
      
      return "weather.wind.none"
    }
  }
}
