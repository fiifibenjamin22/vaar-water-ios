//
//  SpotlightDataController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 10/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

class SpotlightDataController: NSObject {
    static let sharedInstance: SpotlightDataController = {
        let instance = SpotlightDataController()
        return instance
    }()
}
