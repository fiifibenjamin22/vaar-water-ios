//
//  LocationBlockingViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation
import Localize_Swift

class LocationBlockingViewController: UIViewController {

    @IBOutlet weak var explanationHeader: UILabel!
    @IBOutlet weak var explanationLabel: UILabel!
  @IBOutlet weak var settingsButton: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsButton.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0, blue: 0, alpha: 1)
        
        let labelAttributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.explanationLabel.attributedText!)
        
        let headerAttributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.explanationLabel.attributedText!)
        
        
        if CLLocationManager.locationServicesEnabled() {
          
          settingsButton.isHidden = false
            
            switch(CLLocationManager.authorizationStatus()) {
                
            case .denied, .restricted:
                headerAttributedString.mutableString.setString("locationBlocking.header.denied".localized())
                labelAttributedString.mutableString.setString("locationBlocking.explanation.denied".localized())
//                self.explanationLabel.te
            default:
                headerAttributedString.mutableString.setString("locationBlocking.header.disabled".localized())
                labelAttributedString.mutableString.setString("locationBlocking.explanation.disabled".localized())
            }
            
        } else {
          settingsButton.isHidden = true
                headerAttributedString.mutableString.setString("locationBlocking.header.disabled".localized())
            labelAttributedString.mutableString.setString("locationBlocking.explanation.disabled".localized())
        }
        
        self.explanationHeader.attributedText = headerAttributedString
        self.explanationLabel.attributedText = labelAttributedString

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Custom Methods
    
    @objc func applicationDidBecomeActive() {
        
        if CLLocationManager.locationServicesEnabled(),
            CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        
            self.dismiss(animated: true, completion: nil)
        }

    }
    
    @IBAction func handleSettingsTap(_ sender: Any) {
    
        var settingsURLString:String = UIApplication.openSettingsURLString
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch(CLLocationManager.authorizationStatus()) {
             case .denied, .restricted:
                settingsURLString = UIApplication.openSettingsURLString
            default:
                settingsURLString = UIApplication.openSettingsURLString
            }
            
        } else {
            print("Location services are not enabled")
          settingsURLString = UIApplication.openSettingsURLString
        }
        
        if let url = URL(string: settingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
