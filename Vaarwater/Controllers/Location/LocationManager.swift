//
//  LocationManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 18/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation

// the interval (seconds) at which we calculate the user's distance and speed
private let kMinimumLocationUpdateInterval:Double = 10.0
private let kNumSpeedHistoriesToKeep = 5 // the number of locations to store in history
private let kNumLocationHistoriesToKeep = 5 // the number of locations to store in history
private let kValidLocationHistoryDeltaInterval:Double = 3.0 // the maximum valid age in seconds of a location stored in the location history
private let kMinLocationsNeededToUpdate:Int = 3
private let kRequiredStrongHorizontalAccuracy = 30.0
private let kMaximumAcceptableHorizontalAccuracy = 100.0 // the maximum acceptable accuracy in meters for a location.
private let kPrioritizeFasterSpeeds: Bool = false
private let kMinimumDistanceRequiredForDirectionality: Double = 1.0

class LocationManager: NSObject {
    
    static let sharedInstance:LocationManager = {
        
        let instance = LocationManager()
        instance.setup()
        return instance
        
    }()
    
    let locationManager = CLLocationManager()
    let trafficManager = TrafficManager.sharedInstance
    
    let kFullBatteryDistanceFilter = 1.0
    let kFullBatteryLocationAccuracy = kCLLocationAccuracyBestForNavigation
    var currentHeading:CLLocationDirection = 0 {
        didSet {
            if currentHeading != oldValue {
                NotificationCenter.default.post(name:
                    NSNotification.Name(rawValue: Globals.kHeadingUpdatedKey), object: nil)
            }
        }
    }
    var currentLocation:CLLocation? {
        didSet {
            NotificationCenter.default.post(name:
                NSNotification.Name(rawValue: Globals.kLocationUpdatedKey), object: nil)
        }
    }
    var currentSailingDirection:Int = 0 {
        didSet {
            NotificationCenter.default.post(name:
                NSNotification.Name(rawValue: Globals.kSailingDirectionChange), object: nil)
        }
    }
    
    var currentSpeed: Double?
    var averageSpeed:Double?
    var speedHistory = [Double]()
    
    var locationHistory = [CLLocation]()
    var locationTimer = Timer()
    
    var allowMaximumAcceptableAccuracy:Bool = true
    var signalStrength:GPSSignalStrength = .strong {
        didSet {
            self.allowMaximumAcceptableAccuracy = (signalStrength == .strong) ? false : true
        }
    }
    
    override init() {
        
        super.init()
                
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func applicationDidEnterBackground() {
        locationManager.stopUpdatingHeading()
        locationManager.stopUpdatingLocation()
        locationTimer.invalidate()
    }
    
    @objc func applicationDidBecomeActive() {
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
        
        currentSailingDirection = 0
    }
    
    
    func setup() {
        
        // Set delegate
        locationManager.delegate = self
        
        // Additional setup and switches/flags for performance
        locationManager.distanceFilter = kFullBatteryDistanceFilter
        locationManager.desiredAccuracy = kFullBatteryLocationAccuracy
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.headingOrientation = .portrait
        locationManager.headingFilter = kCLHeadingFilterNone
        locationManager.activityType = CLActivityType.otherNavigation
        
        if ( self.checkLocationEnabled() ) {
            
            // Start heading updates if heading is available
            if ( CLLocationManager.headingAvailable() ) {
                locationManager.headingFilter = 3
                locationManager.startUpdatingHeading()
            }
            locationManager.startUpdatingLocation()
        } else {
            
            locationManager.stopUpdatingHeading()
            locationManager.stopUpdatingLocation()
        }
        
    }
    
    func authorize() {
        // Request user location authorization
        locationManager.requestAlwaysAuthorization()
    }
    
    func checkLocationEnabled() -> Bool {
        
        var isEnabled = false
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        
        // Check for authorization
        if CLLocationManager.locationServicesEnabled() {
            print("Location services are enabled, checking authorization")
            
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                print("CLLocationManager authorization status: notDetermined")
                isEnabled = false
            case .denied, .restricted:
                print("CLLocationManager authorization status: denied/restricted")
                isEnabled = false
            case .authorizedAlways, .authorizedWhenInUse:
                print("CLLocationManager authorization status: authorized")                
                isEnabled = true
            @unknown default:
                print ("DEBUG unknown default")
            }
            
        } else {
            print("Location services are not enabled")
        }
        
        return isEnabled
        
    }
    
    @objc func requestLocationUpdate() {
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    
    func calculateCurrentLocationData( withLocations locations: [CLLocation] ) {
        
        // Check to see if it receives a location at all
        if let location = locations.last {
            
            // Check signal strength arbitrairily
            var horizontalAccuracyThreshold:Double
            if (location.horizontalAccuracy <= kRequiredStrongHorizontalAccuracy) {
                self.signalStrength = .strong
            } else {
                self.signalStrength = .weak
            }
            
            if (self.allowMaximumAcceptableAccuracy) {
                horizontalAccuracyThreshold = kMaximumAcceptableHorizontalAccuracy
            } else {
                horizontalAccuracyThreshold = kRequiredStrongHorizontalAccuracy
            }
            
            currentLocation = (locationHistory.last ?? location)!
            
            // check if the horizontalaccuracy threshold is met
            if (location.horizontalAccuracy >= 0 && location.horizontalAccuracy <= horizontalAccuracyThreshold) {
                // If accuracy threshold is met, add it to history array
                locationHistory.append(location)
                if (locationHistory.count > kNumLocationHistoriesToKeep) {
                    locationHistory.remove(at: 0)
                }
                
                if location.speed <= 0 && speedHistory.count == 0 {} else {
                    speedHistory.append(location.speed)
                }
                if (speedHistory.count > kNumSpeedHistoriesToKeep) {
                    speedHistory.remove(at: 0)
                }
                
            }
            
            // Check if mininum locations is met to calculate
            if (locationHistory.count >= kMinLocationsNeededToUpdate) {
                
                // Get best/most accurate location, and if tht returns nil, use current location update
                var bestLocation:CLLocation = location
                var bestAccuracy = kRequiredStrongHorizontalAccuracy;
                for location in locationHistory {
                    if (NSDate.timeIntervalSinceReferenceDate - location.timestamp.timeIntervalSinceReferenceDate <= kValidLocationHistoryDeltaInterval) {
                        if (location.horizontalAccuracy <= bestAccuracy && location != currentLocation) {
                            bestAccuracy = location.horizontalAccuracy
                            bestLocation = location
                        }
                    }
                }
                
                var totalSpeed:Double = 0
                for speed in speedHistory {
                    totalSpeed += speed
                }
                let newSpeed = totalSpeed / Double(speedHistory.count)
                averageSpeed = newSpeed
                
                currentLocation = bestLocation
                
            }
            currentSpeed = currentLocation?.speed
            
            // calculate sailing direction
            calculateSailingDirection()
            
        }
        
    }
    
    public func calculateSailingDirection() {
        
        // for each entry in traffic links history, check the distance from the first position and whether it's getting smaller or larger
        if let currentTrafficLink = trafficManager.currentTrafficLink,
            let positions = currentTrafficLink["positions"] as? Array<Dictionary<String, AnyObject>>,
            let firstPosition = positions.first,
            let latValue = firstPosition["geo_lat"] as? String,
            let lngValue = firstPosition["geo_lng"] as? String,
            let latitude = Double(latValue),
            let longitude = Double(lngValue) {
            
            let trafficLocation = CLLocation(latitude: latitude, longitude: longitude)
            var oldDistance:Double = 0
            
            var calculatedSailingDirection:Int = 0
            for location in LocationManager.sharedInstance.locationHistory {
                
                var distance:Double = 0
                distance = location.distance(from: trafficLocation)
                
                // If oldDistance is positive, and the delta is positive, currentDirection is 1
                // else, currentDirection is 2
                if oldDistance != 0 {
                    
                    if calculatedSailingDirection == 0 {
                        calculatedSailingDirection = checkDirectionBetween(distance: distance, oldDistance: oldDistance )
                    } else {
                        
                        let nextDirection = checkDirectionBetween(distance: distance, oldDistance: oldDistance )
                        if calculatedSailingDirection != nextDirection {
                            return
                        }
                    }
                    
                }
                
                oldDistance = distance
                
            }
            
            currentSailingDirection = calculatedSailingDirection

        }
        
    }
    
    func checkDirectionBetween( distance: Double, oldDistance: Double ) -> Int {
        
        if (oldDistance - distance) > kMinimumDistanceRequiredForDirectionality {
            return 2
        } else if (distance - oldDistance) > kMinimumDistanceRequiredForDirectionality {
            return 1
        } else {
            return 0
        }
        
    }
    
}

// MARK: - CLLocationManager Delegate EXTENSION
extension LocationManager: CLLocationManagerDelegate {
    
    // Check for authorization for location tracking, and react accordingly
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch(status) {
        case .notDetermined:
            print("Authorization status changed, now undetermined")            
            locationManager.stopUpdatingLocation()
            locationManager.stopUpdatingHeading()
        case .denied, .restricted:
            print("Authorization status changed, now denied")
            locationManager.stopUpdatingLocation()
            locationManager.stopUpdatingHeading()
        case .authorizedAlways, .authorizedWhenInUse:
            print("Authorized with status, aurhotized successfully")
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        @unknown default:
            print ("DEBUG unknown default")
        }
        
    }
    
    // Heading readings tend to be widely inaccurate until the system has calibrated itself
    // Return true here allows iOS to show a calibration view when iOS wants to improve itself
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        return false
    }
    
    // Error-handling
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error locationManager, failing with error: ", error)
    }
    
    // Bearing/heading delegate method
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        // check for heading accuracy
        guard newHeading.headingAccuracy >= 0 else {
            return
        }
        
        // Store current heading and update/draw map
        let heading = (newHeading.trueHeading > 0) ? newHeading.trueHeading : newHeading.magneticHeading
        currentHeading = heading
                
    }
    
    // Update locations delegate method
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // Invalidate timer for regular location pings
        self.locationTimer.invalidate()
        
        // Calculate averages and best locations
        self.calculateCurrentLocationData( withLocations: locations )
        
        locationTimer = Timer.scheduledTimer(timeInterval: TimeInterval(kMinimumLocationUpdateInterval), target: self, selector: #selector(self.requestLocationUpdate), userInfo: nil, repeats: true);
        
    }

}

