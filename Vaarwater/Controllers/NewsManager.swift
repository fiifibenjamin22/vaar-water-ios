//
//  NewsManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 23/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Alamofire
import Localize_Swift

class NewsManager: NSObject {
    
    static let service = NewsManager()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<News> = {
        
        let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "createdDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext, sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    struct NewsDTO: Codable {
        var id: Int?
        var created_at: Date?
        var updated_at: Date?
        var name: String?
        var text: String?
        var html: String?
        var media: String?
//        var active_at: Date?
//        var active_till: Date?
        var lang: String?
        var media_url: String?
        var thumb_large: String?
        var thumb_medium: String?
        var thumb_small: String?
        var media_tag: String?
    }
    
    struct NewsWrapper: Codable {
        var count = 0
        var newsItems = [NewsDTO]()
        
        enum CodingKeys: String, CodingKey {
            case newsItems = "data"
            case count = "total"
        }
    }
    
    private enum API {
        private static let basePath = "https://grachten.waternet.nl/api/v3/"
        
        case news
        
        func fetch(completion: @escaping (Data) -> ()) {
            Alamofire.request(path(), parameters: parameters())
                .responseJSON { (response) -> () in
                    guard response.result.isSuccess, let data = response.data else {
                        print("Error: \nNo data found in API call")
                        return
                    }
                    completion(data)
                    
            }
        }
        
        private func path() -> String {
            switch self {
            case .news:
                return API.basePath + "news"
            }
        }
        
        private func parameters() -> Dictionary<String, AnyObject> {
            return [:]
        }
    }
    
    func getNews(completion: @escaping (NewsWrapper) -> Void) {
        
        API.news.fetch { data in
            do {
                let decoder = JSONDecoder()
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "nl_NL")
                dateFormatter.calendar = Calendar(identifier: .gregorian)
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                decoder.dateDecodingStrategy = .formatted(dateFormatter)
                
                let newsWrapper = try decoder.decode(NewsWrapper.self, from: data)
                
                completion(newsWrapper)
            } catch let error as NSError {
                print("Error: \(error)")
            }
        }
    }
    
    func updateNews(with newsWrapper: NewsWrapper) throws -> () {
        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        var readNews: [News] = []
        
        taskContext.performAndWait {
            let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
            let predicate: NSPredicate
            if Localize.currentLanguage() == "nl" {
                predicate = NSPredicate(format: "(read == true) AND (lang == %@)", "nl")
            } else {
                predicate = NSPredicate(format: "(read == true) AND (lang == %@)", "en")
            }
            fetchRequest.predicate = predicate
            
            do {
                readNews = try taskContext.fetch(fetchRequest) as [News]
            
            } catch {
                return
            }
            
            for newsItem in newsWrapper.newsItems {
                if let itemId = newsItem.id {
                    if readNews.contains(where: { $0.id == itemId as NSNumber }) {
                        
                    }
                }
                var _ = NSEntityDescription.insertNewObject(forEntityName: "News", into: taskContext) as! News
                
                
            }
            
        }
    }
    
    func fetchRead() -> [News]? {
        var predicate: NSPredicate
        predicate = NSPredicate(format: "(read == true) AND (lang == %@)", "nl")
        
        self.fetchedResultsController.fetchRequest.predicate = predicate
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Error: \(error), \(error.userInfo)")
            return nil
        }
        
        return fetchedResultsController.fetchedObjects
    }
    
    
    func fetchAll() throws {
        
        var predicate:NSPredicate
        if Localize.currentLanguage() == "nl" {
            predicate = NSPredicate(format: "lang == %@", "nl")
        } else {
            predicate = NSPredicate(format: "lang == %@", "en")
        }
        self.fetchedResultsController.fetchRequest.predicate = predicate
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Error: \(error), \(error.userInfo)")
        }
    }
    
    
}

extension NewsManager: NSFetchedResultsControllerDelegate {
    
    
}
