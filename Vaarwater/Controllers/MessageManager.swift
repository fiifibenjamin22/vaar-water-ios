//
//  MessageManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 11/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class MessageManager {
    
    enum State {
        case notRetrievedYet
        case loading
        case noResults
        case results([Message])
    }
    
    private(set) var state: State = .notRetrievedYet
    private var dataTask: URLSessionDataTask? = nil
    
    func getMessages(completion: @escaping (Bool) -> Void) {
        
        state = .loading
        
        let api = API.sharedInstance
        let url = api.host + "news"
        
        let credential : URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
        
        if api.logging {
            print(url)
        }
        
        Alamofire.request(url, parameters: nil)
            .authenticate(usingCredential: credential)
            .responseJSON { (response) -> Void in
                var newState = State.notRetrievedYet
                var success = false
                
                guard response.result.isSuccess else {
                    print("error")
                    return
                }
                
                guard let data = response.data else {
                    print("Error: \nNo data found in News API call.")
                    return
                }
                
                let messages = self.parse(data: data)
                
                if messages.isEmpty {
                    newState = .noResults
                } else {
                    newState = .results(messages)
                }
                
                success = true
                
                DispatchQueue.main.async {
                    self.state = newState
                    completion(success)
                }
        }
        
        //        dataTask = session.dataTask(with: url!, completionHandler: {
        //            data, response, error in
        //
        //            var newState = State.notRetrievedYet
        //            var success = false
        //
        //            if (error as NSError?) != nil {
        //                return
        //            }
        //
        //            if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data {
        //                let spotlights = self.parse(data: data)
        //
        //                if spotlights.isEmpty {
        //                    newState = .noResults
        //                } else {
        //                    newState = .results(spotlights)
        //                }
        //
        //                success = true
        //            }
        //
        //            DispatchQueue.main.async {
        //                self.state = newState
        //                completion(success)
        //            }
        //        })
        
    }
    
    func parse(data: Data) -> [Message] {
        do {
            let decoder = JSONDecoder()
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "nl_NL")
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
            
            let result = try decoder.decode(MessagesWrapper.self, from: data)
            
            return result.messages
        } catch {
            print("JSON Error: \(error)")
            return []
        }
    }
}
