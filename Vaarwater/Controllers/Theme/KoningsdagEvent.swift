//
//  KoningsdagEvent.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class KoningsdagEvent: Event {
    
    var name_en:String
    var name_nl:String
    
    var intro_en:String
    var intro_nl:String
    
    var slug:String
    
    var eventColor:UIColor
    var eventIcon:EventImage
    var eventScreenIcon:EventImage

    var eventImage:EventImage
    var compassBackground:EventImage
    
    var startDate:Date
    var endDate:Date
    
    var pages: [EventPage]

    init() {
        
        name_en = "Kingsday"
        name_nl = "Koningsdag"
        
        intro_en = "Extra Kingsday information"
        intro_nl = "Extra info voor Koningsdag"
        
        slug = "kings-day"
        
        eventColor = UIColor(red: 255.0/255.0, green: 165.0/255.0, blue: 35.0/255.0, alpha: 1.0)
        eventIcon = EventImage(small: "",
                               medium: "",
                               large: "",
                               xlarge: "")
        
        eventScreenIcon = EventImage(small: "",
                                     medium: "",
                                     large: "",
                                     xlarge: "")
        
        eventImage = EventImage(small: "",
                                medium: "",
                                large: "",
                                xlarge: "")
        
        compassBackground = EventImage(small: "",
                                       medium: "",
                                       large: "",
                                       xlarge: "")
        
        startDate = Date()
        endDate = Date()
        
        pages = [
            EventPage(name_nl: "Doe een duit in de afvalschuit",
                      name_en: "Toss it in the garbage barge",
                      text_nl: "Ga je varen op Koningsdag? Verzamel lege blikjes, flesjes, chipszakken en andere troep uit je sloep. Gooi alles in onze afvalboten. Deze liggen op de volgende plekken:\n\n- Westerdok: Hoek Hollandse Tuin\n- Oosterdok: Steiger zuidzijde Oosterdok ten westen van de IJ-tunnel\n- Amstel: tegenover Hermitage\n- Singel: ter hoogte van de Spuistraat\n",
                      text_en: "Heading out on the water on King’s Day? Collect your empty cans, bottles, bags and other junk and toss it into our rubbish boats. You’ll find them here:\n\n- Westerdok: corner of Hollandse Tuin\n- Oosterdok: jetty on the south side of Oosterdok, west of the IJ tunnel\n- Amstel: opposite Hermitage Museum\n- Singel: at Spuistraat\n"),
            
            EventPage(name_nl: "Boot (ver)huren",
                      name_en: "Hiring or letting a boat on King’s Day",
                      text_nl: "Wil je een boot huren op Koningsdag? Kies dan altijd voor een boot met een vergunning. Verhuur je jouw boot? Dan heb je een vergunning nodig.\n\nVoor het verhuren van een boot heb je een vergunning nodig: een exploitatievergunning. De gemeente Amsterdam bepaalt wie een vergunning krijgt.",
                      text_en: "Thinking of hiring a boat on King’s Day? Make sure you check the permit. Letting your boat? Then you need a permit.\n\nYou must have an operating permit to rent out your boat. The City of Amsterdam is in charge of issuing permits."),
            
            EventPage(name_nl: "Hou ‘m binnenboord!",
                      name_en: "Keep it on board!",
                      text_nl: "Plas niet in de gracht. Hoge nood? Vaar naar onze toileteilanden. Deze liggen op de volgende plekken:\n\n- Westerdok: Hoek Hollandse Tuin\n- Oosterdok: Steiger zuidzijde Oosterdok ten westen van de IJ-tunnel\n- Amstel: tegenover Hermitage",
                      text_en: "Do not pee in the canal. Can’t hold it in? Public toilets and urinals are available. You’ll find them here:\n\n- Westerdok: corner of Hollandse Tuin\n- Oosterdok: jetty on the south side of Oosterdok, west of the IJ tunnel\n- Amstel: opposite Hermitage Museum"),
            
            EventPage(name_nl: "Varen",
                      name_en: "Heading out on the water on King’s Day?",
                      text_nl: "Er is een speciale Koningsdagvaarroute. Op deze route geldt eenrichtingsverkeer. Op een aantal grachten kan je maar één kant op varen:\n\n- op de Prinsengracht alleen richting de Amstel\n- op het Singel alleen richting het IJ\n- op het Rokin en de Oudezijds Voorburgwal alleen richting het Oosterdok\n- op de Amstel alleen richting het Oosterdok\n\nEen deel van de route is alleen voor boten kleiner dan 10 meter. Let dus goed op de borden en spandoeken langs het water.",
                      text_en: "There is a special King’s Day route on the canals. One-way traffic is in force along the route. Some canals are also subject to one-way traffic:\n\n- Prinsengracht one-way to the River Amstel\n- Singel one-way to the River IJ\n- Rokin and Oudezijdsvoorburgwal one-way to Oosterdok\n- River Amstel one-way to Oosterdok\n\nPart of the route is reserved for boats of less than 10 metres. Be sure to observe the signs and banners posted along the canals!"),
            
            EventPage(name_nl: "Vaarregels",
                      name_en: "King’s Day waterway rules",
                      text_nl: "Er is een speciale Koningsdagroute. Op deze route geldt eenrichtingsverkeer. Je mag dus maar 1 kant op varen. Een deel van de route is alleen voor boten kleiner dan 10 meter. Let dus goed op de borden en spandoeken langs het water.\n\nVersterkte muziek draaien mag niet. Waternet uw geluidsapparatuur in beslag nemen.\n\nDrink als schipper geen alcohol. Een passagier mag maximaal 1 blikje of plastic flesje (zwak) alcoholische drank bij zich hebben.\n\nVaar niet binnen de Singelgracht met drijvende platforms, boten zonder motor of iets wat daarop lijkt. Ook niet als een andere boot jouw sleept of duwt.\n\nEen boot huren of verhuren zonder vergunning is verboden. Bekijk welke boten een vergunning hebben.\nDaarnaast gelden natuurlijk de normale vaarregels in Amsterdam.",
                      text_en: "There is a special King’s Day route on the canals. One-way traffic is in force along the route. Part of the route is reserved for boats of less than 10 metres. Be sure to observe the signs and banners posted along the canals!\n\nYou may not play amplified music. Waternet is authorized to confiscate your audio equipment.\n\nDo not use intoxicating substances, including alcohol, if you are skippering a boat. Passengers may have no more than one can or bottle of alcohol on their person.\n\nFloating platforms, boats with no engine or similar watercraft are not allowed within Singelgracht, even if being towed or pushed by another boat.\n\nYou may not hire or let a boat on King’s Day without a permit. See a list of licenced boats.\nAdditionally, the usual rules for navigation in Amsterdam remain in force.")
            
        ]
        
    }
    
}
