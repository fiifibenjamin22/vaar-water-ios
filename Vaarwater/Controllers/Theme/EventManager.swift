//
//  EventManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class EventManager {
    
    static var event:Event? = nil {
        didSet {
            NotificationCenter.default.post(name:
                NSNotification.Name(rawValue: Globals.kEventSwitched), object: nil)
        }
    }
    
}
