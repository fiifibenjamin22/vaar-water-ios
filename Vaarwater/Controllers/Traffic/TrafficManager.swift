//
//  TrafficManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 24/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation

protocol TrafficManagerDelegate: class {
    func didUpdateMaximumSpeed( maxSpeed: Double )
    func didUpdateDirectionality( requiredDirectionality: Int )
    func didUpdateEnvironmentalZone( isEnvironmentalZone: Bool )
    func didUpdateTraffic( hasTraffic: Bool )
}

class TrafficManager:NSObject {
    
    static let sharedInstance:TrafficManager = {
        let instance = TrafficManager()
        return instance
    }()
    
    static let kTrafficThreshold:Int = 4
    
    var delegate:TrafficManagerDelegate?
    
    var currentTrafficLink: Dictionary<String, AnyObject>?
    var currentMaxSpeed:Double = Globals.kMaximumSpeed
    var currentDirectionality:Int = 0
    var isEnvironmentalZone:Bool = false
    
    func requestTrafficUpdate( forLocation location: CLLocation ) {
        
        let completionHandler: ( _ data: Dictionary<String, AnyObject>?, _ success: Bool, _ error: String? ) -> Void = { ( data, success, error ) in
                        
            // Check for errors
            guard error == nil else {
                print("Error retrieving maximum speed: \(String(describing: error))")
                self.currentTrafficLink = nil
                return
            }
            
            // Check for data
            guard data != nil else {
                self.currentTrafficLink = nil
                return
            }
            
            DispatchQueue.main.async {
                self.parseTrafficData( data! )
            }
            
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            API.sharedInstance.getNearestTraffic( forLocation: location, completion: completionHandler )
        }
    }
    
    public func parseTrafficData(_ data: Dictionary<String, AnyObject>) {
        
        // Check trafficLink id, and if it's the same segment, store in history
        // else clear history.
        if let dataId = data["id"] as? Int {

            // In case there's already a traffic link stored, compare the id's
            if let storedId = currentTrafficLink?["id"] as? Int {
                if dataId == storedId {
                    return
                }
            }
            
            currentTrafficLink = data
            
            // check if a speed is set, and use last stored value otherwise
            if let maxSpeed = data["speed"] as? Double {
                self.currentMaxSpeed = maxSpeed
                self.delegate?.didUpdateMaximumSpeed( maxSpeed: maxSpeed )
            }
            
            if let environmentalZoneValue = data["environmental_zone"] as? Int {
                self.isEnvironmentalZone = (environmentalZoneValue == 1) ? true : false
                self.delegate?.didUpdateEnvironmentalZone( isEnvironmentalZone: self.isEnvironmentalZone )
            }
            
            if let directionality = data["directionality"] as? Int {
                self.currentDirectionality = directionality
                self.delegate?.didUpdateDirectionality( requiredDirectionality: directionality )
            }
            
            // Check for neighbouring segments and their data
            if let neighbouring = data["neighbouring"] as? Array<Dictionary<String, AnyObject>> {
                
                var hasTraffic:Bool = false
                for neighbourDict in neighbouring {
                    if let neighbourTraffic = neighbourDict["traffic"] as? Int,
                        neighbourTraffic >= TrafficManager.kTrafficThreshold {
                        
                        hasTraffic = true
                        break
                        
                    }
                }
                self.delegate?.didUpdateTraffic( hasTraffic: hasTraffic )
            }
            
        }
        
    }

}
