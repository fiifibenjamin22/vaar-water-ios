//
//  TrafficViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 21/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Lottie

class TrafficViewController: SlidingViewController {
    
    @IBOutlet weak var animationView: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
        //        addAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
    }
    
    
    // MARK: - Custom methods
    
    //    func addAnimation() {
    //
    //        guard let condition = WeatherManager.sharedInstance.latestForecastCondition else {
    //            print("No weather condition available")
    //            return
    //        }
    //
    //        if let lottieView = LOTAnimationView(name: getAnimationName( forCondition: condition)) {
    //            lottieView.loopAnimation = true
    //            lottieView.frame = animationView.bounds
    //            lottieView.contentMode = .scaleAspectFit
    //            animationView.addSubview(lottieView)
    //
    //            lottieView.play(completion: { (finished) in
    //                // Do Something
    //            })
    //        }
    //
    //    }
    //
    //    func getAnimationName( forCondition condition: WeatherCondition) -> String {
    //
    //        var name:String = ""
    //
    //        switch ( condition ) {
    //        case .rain:
    //            name = "rain"
    //        default:
    //            name = "rain"
    //        }
    //
    //        return name
    //
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
