//
//  ItineraryManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 06/06/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import CoreLocation
import RxSwift

class ItineraryManager {
  
  enum ItineraryError: Error {
    case serverError
  }
  
  static let shared: ItineraryManager = {
    return ItineraryManager()
  }()
  
  var currentActiveDestination: POI? {
    didSet {
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: Globals.kActiveDestinationChange), object: nil)
    }
  }
  
//  var itinerary = Variable<Itinerary>(Itinerary.empty())
  
  
  private func itinerary(from location: CLLocation, to destinationLocation: CLLocation) -> Observable<Itinerary> {
    
    let api = API.sharedInstance
    let url = URL(string: api.host)!.appendingPathComponent("waterways/path")
    var request = URLRequest(url: url)
    let _: URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
    
    request.setValue("Basic dWxwcmV2aWV3OmRlbW9wcmV2aWV3", forHTTPHeaderField: "Authorization")
    
    let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
    
    var queryItems = [URLQueryItem]()
    queryItems.append(URLQueryItem(name: "startLat", value: String(location.coordinate.latitude)))
    queryItems.append(URLQueryItem(name: "startLng", value: String(location.coordinate.longitude)))
    queryItems.append(URLQueryItem(name: "destLat", value: String(destinationLocation.coordinate.latitude)))
    queryItems.append(URLQueryItem(name: "destLng", value: String(destinationLocation.coordinate.longitude)))
    
    urlComponents.queryItems = queryItems
    
    request.url = urlComponents.url
    
    let session = URLSession.shared
    
    return session.rx.response(request: request)
      .map { response, data in
//        do {
//          print(data)
//          let decoder = JSONDecoder()
//          let itinerary = try decoder.decode(Itinerary.self, from: data)
//          return itinerary
//        } catch {
//          return Itinerary.empty()
//        }
        if 200..<300 ~= response.statusCode {
          do {
           // print(data)
            let decoder = JSONDecoder()
            let itinerary = try decoder.decode(Itinerary.self, from: data)
            return itinerary
          } catch {
            throw ItineraryError.serverError
          }
        } else if 400..<500 ~= response.statusCode {
          throw ItineraryError.serverError
        } else {
          throw ItineraryError.serverError
        }
    }
  }
  
  // Calculate initerary from current location to current active destination location
  public func itinerary() -> Observable<Itinerary> {
    
    guard let destination = currentActiveDestination, let destinationLocation = destination.location else {
      print("No currently active destination found.")
      return Observable.just(Itinerary.empty())
    }
    
    guard let location = LocationManager.sharedInstance.currentLocation else {
      print("No current location found.")
      return Observable.just(Itinerary.empty())
    }
    
    return itinerary(from: location, to: destinationLocation)
  }
  
  // Calculate initerary from current location to a destination location
  public func itinerary(to destinationLocation: CLLocation) -> Observable<Itinerary> {
    
    guard let location = LocationManager.sharedInstance.currentLocation else {
      print("No current location found.")
      return Observable.just(Itinerary.empty())
    }
    
    return itinerary(from: location, to: destinationLocation)
  }
}
