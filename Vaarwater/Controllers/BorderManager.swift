//
//  BorderManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 01/06/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift

class BorderManager {
  static var shared = BorderManager()
  
  func contains(location: CLLocation?) -> Observable<Bool> {
    
    struct BorderContains: Codable {
      let contains: Bool
    }
    
    let api = API.sharedInstance
    let url = URL(string: api.host)!.appendingPathComponent("borders/contains")
    var request = URLRequest(url: url)
    let _: URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
    
    request.setValue("Basic dWxwcmV2aWV3OmRlbW9wcmV2aWV3", forHTTPHeaderField: "Authorization")
    
    let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
    
    if let location = location {
      
      var queryItems = [URLQueryItem]()
      queryItems.append(URLQueryItem(name: "lat", value: String(location.coordinate.latitude)))
      queryItems.append(URLQueryItem(name: "lng", value: String(location.coordinate.longitude)))
      
      urlComponents.queryItems = queryItems
      
    }
    
    request.url = urlComponents.url
    
    let session = URLSession.shared
    
    return session.rx.data(request: request)
      .map { data in
        do {
          let decoder = JSONDecoder()
          let contains = try decoder.decode(BorderContains.self, from: data)
          return contains.contains
        } catch {
          return false
        }
    }
  }
}




