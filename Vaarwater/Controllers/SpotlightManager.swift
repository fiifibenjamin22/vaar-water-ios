//
//  SpotlightManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 10/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import UIKit
//import Alamofire

import RxSwift
import RxCocoa

class SpotlightManager {
  
  static let service = SpotlightManager()
  
  static var spotlights: Observable<[Spotlight]> = {
    return SpotlightManager.getSpotlights()
    .catchErrorJustReturn([])
    .share(replay: 1, scope: .forever)
  }()
  
  
  static func getSpotlights() -> Observable<[Spotlight]> {
    let api = API.sharedInstance
    let url = URL(string: api.host)!.appendingPathComponent("spotlights")
    var request = URLRequest(url: url)
       
    request.setValue("Basic dWxwcmV2aWV3OmRlbW9wcmV2aWV3", forHTTPHeaderField: "Authorization")
    
    let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
    
    request.url = urlComponents.url
    
    let session = URLSession.shared

    return session.rx.data(request: request)
      .map { data in
        do {
          let decoder = JSONDecoder()
          let wrapper = try decoder.decode(SpotlightsWrapper.self, from: data)
          print(wrapper.count)
          return wrapper.spotlights
        } catch {
          print("JSON Error: \(error)")
          return []
        }
    }
  }
  
 
  func parse(data: Data) -> [Spotlight] {
    do {
      let decoder = JSONDecoder()
      let result = try decoder.decode(SpotlightsWrapper.self, from: data)
      
      return result.spotlights
    } catch {
      print("JSON Error: \(error)")
      return []
    }
  }
}
