//
//  WaypointManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 22/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift

class WaypointManager {
//  static var shared = WaypointManager()
//
//  var destination = Variable<POI?>(nil)
//
//  func itinerary(from location: CLLocation?, to destination: CLLocation?) -> Observable<Itinerary> {
//
//    let api = API.sharedInstance
//    let url = URL(string: api.host)!.appendingPathComponent("waterways/path")
//    var request = URLRequest(url: url)
//    let credential: URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
//
//    request.setValue("Basic dWxwcmV2aWV3OmRlbW9wcmV2aWV3", forHTTPHeaderField: "Authorization")
//
//    let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: true)!
//
//    if let location = location, let destination = destination {
//
//      var queryItems = [URLQueryItem]()
//      queryItems.append(URLQueryItem(name: "startLat", value: String(location.coordinate.latitude)))
//      queryItems.append(URLQueryItem(name: "startLng", value: String(location.coordinate.longitude)))
//      queryItems.append(URLQueryItem(name: "destLat", value: String(destination.coordinate.latitude)))
//      queryItems.append(URLQueryItem(name: "destLng", value: String(destination.coordinate.longitude)))
//
//      urlComponents.queryItems = queryItems
//
//    }
//
//    request.url = urlComponents.url
//
//    let session = URLSession.shared
//
//    return session.rx.data(request: request)
//      .map { data in
//        do {
//          print(data)
//          let decoder = JSONDecoder()
//          let itinerary = try decoder.decode(Itinerary.self, from: data)
//          return itinerary
//        } catch {
//          return Itinerary.empty()
//        }
//    }
//  }
}

//struct Itinerary: Codable {
//  var summary: Summary?
//  var geometry: Geometry?
//
//  var distance: Double? {
//    return summary?.distance
//  }
//  var duration: Double? {
//    return summary?.duration
//  }
//
//  var waypoints: [Waypoint] {
//    if let coordinates = geometry?.coordinates {
//      return coordinates.map {
//        Waypoint(latitude: ($0[1]), longitude: $0[0])
//      }
//    } else {
//      return [Waypoint]()
//    }
//  }
//
//  static func empty() -> Itinerary {
//    return Itinerary(summary: nil, geometry: nil)
//  }
//
//  enum CodingKeys: String, CodingKey {
//    case summary = "properties"
//    case geometry = "geometry"
//  }
//}
//
//struct Geometry: Codable {
//  let type: String?
//  let coordinates: [[Double]]
//}
//
//struct Waypoint: Codable {
//  var latitude: Double?
//  var longitude: Double?
//
////  var location: CLLocation? {
////    if let latitude = latitude, let longitude = longitude {
////      return CLLocation(latitude: latitude, longitude: longitude)
////    } else {
////      return nil
////    }
////  }
//
//  var location: CLLocationCoordinate2D? {
//    if let latitude = latitude, let longitude = longitude {
//      return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//    } else {
//      return nil
//    }
//  }
//}
//
//struct Summary: Codable {
//  let duration: Double
//  let distance: Double
//}

