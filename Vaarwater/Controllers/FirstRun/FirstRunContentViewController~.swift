//
//  FirstRunContentViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 28/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class FirstRunContentViewController: UIViewController, FirstRunViewControllerProtocol {

    var pageIndex:Int = 0
    var firstRunScreenItem:FirstRunScreenItem?

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textView.textContainer.lineFragmentPadding = 0
        fillWithScreenItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillWithScreenItem() {
        
        // Set image
        guard let content = firstRunScreenItem else {
            return
        }
        
        if Localize.currentLanguage() == "nl" {
            imageView.image = UIImage(named: String(format: "%@ NL", content.imageName) )
        } else {
            imageView.image = UIImage(named: String(format: "%@ EN", content.imageName) )
        }
        titleLabel.text = content.titleKey.localized()
        textView.text = content.textKey.localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
