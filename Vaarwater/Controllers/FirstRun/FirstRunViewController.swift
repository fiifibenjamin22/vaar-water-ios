//
//  FirstRunViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 28/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

struct FirstRunScreenItem {
    var imageName:String
    var titleKey:String
    var textKey:String
    var buildNumber:Int
}

protocol FirstRunViewControllerProtocol {    
    var pageIndex:Int {get}
    var firstRunScreenItem:FirstRunScreenItem? {get}
}

private let firstRunScreenItems:[FirstRunScreenItem] = [
//    FirstRunScreenItem(imageName: "First Run Intro",
//                       titleKey: "firstRun.intro.title",
//                       textKey: "firstRun.intro.text",
//                       buildNumber: 0),
    FirstRunScreenItem(imageName: "First Run 1",
                       titleKey: "firstRun.screen1.title",
                       textKey: "firstRun.screen1.text",
                       buildNumber: 0),
    FirstRunScreenItem(imageName: "First Run 2",
                       titleKey: "firstRun.screen2.title",
                       textKey: "firstRun.screen2.text",
                       buildNumber: 0),
    FirstRunScreenItem(imageName: "First Run 3",
                       titleKey: "firstRun.screen3.title",
                       textKey: "firstRun.screen3.text",
                       buildNumber: 0),
    FirstRunScreenItem(imageName: "First Run 4",
                       titleKey: "firstRun.screen4.title",
                       textKey: "firstRun.screen4.text",
                       buildNumber: 68),
    FirstRunScreenItem(imageName: "First Run 5",
                       titleKey: "firstRun.screen5.title",
                       textKey: "firstRun.screen5.text",
                       buildNumber: 68),
    FirstRunScreenItem(imageName: "First Run 6",
                       titleKey: "firstRun.screen6.title",
                       textKey: "firstRun.screen6.text",
                       buildNumber: 68)
]

class FirstRunViewController: UIViewController, UIPageViewControllerDataSource {
    
    class var requiredBuild:Int {
        get {
            if let firstRequiredItem = firstRunScreenItems.first( where: { $0.buildNumber > 0 }) {
                return firstRequiredItem.buildNumber
            } else {
                return 0
            }
        }
    }
    
    var numberOfPages:Int = 0
    
    var currentIndex: Int = 0
    var pendingIndex: Int = 0
    
    var pageController: FirstRunPageViewController?
    var isFirstRun:Bool = true
    
    var onClose:(() -> Void)? = nil
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var closeBtnOutlet: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberOfPages = firstRunScreenItems.count
        self.pageControl.numberOfPages = numberOfPages
        
        if Localize.currentLanguage() == "nl" {
            
            closeBtnOutlet.setTitle("Sluit", for: .normal)
        }else{
            
            closeBtnOutlet.setTitle("Close", for: .normal)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
        
        self.pageControl.currentPage = currentIndex
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // You might want to check if this is your embed segue here
        // in case there are other segues triggered from this view controller.
        
        // Set to correct page according to buildnumber
        if isFirstRun {
//            skipToBuildNumber()
        }
        
        if let containedVC = segue.destination as? FirstRunPageViewController {
            
            containedVC.delegate = self
            containedVC.dataSource = self
            
            var contentVC:UIViewController?
//            if currentIndex == 0 {
//                contentVC = firstRunIntroViewController()
//            } else {
                contentVC = firstRunContentViewController(forIndex: currentIndex)
//            }
            
            if contentVC != nil {
                
                containedVC.setViewControllers(
                    [contentVC!],
                    direction: .forward,
                    animated: true,
                    completion: nil
                )
                containedVC.numberOfPages = self.numberOfPages
                
            }
            
            self.pageController?.currentIndex = self.currentIndex
            self.pageController?.numberOfPages = self.numberOfPages            
            
            self.pageController? = containedVC
            self.pageController?.delegate = self
        }
        
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        
        // if onClose is set, then it's run as first run
        if onClose != nil {
            onClose!()
            
        // if not, then it's modally opened from another page (help i.e.)
        } else {
            self.dismiss( animated: true, completion: nil)
        }
        
    }
    
    func skipToBuildNumber() {
        
        let defaults = UserDefaults.standard
        if let storedBuild = defaults.object( forKey: Globals.kStoredBuildKey) as? String,
            Int(storedBuild)! <= FirstRunViewController.requiredBuild {
            
//            let firstRequiredItem = firstRunScreenItems.first( where: { $0.buildNumber >= storedBuild })
            let firstRequiredIndex = firstRunScreenItems.firstIndex(where: { ( item ) -> Bool in
                item.buildNumber >= Int(storedBuild)!
            })
            
            currentIndex = firstRequiredIndex ?? 0
        }
        
    }
    
    func firstRunIntroViewController() -> FirstRunIntroViewController? {
        
        if let storyboard = storyboard,
            let contentVC = storyboard.instantiateViewController(withIdentifier: "FirstRunIntroViewController")
                as? FirstRunIntroViewController {
            
            contentVC.firstRunScreenItem = firstRunScreenItems[currentIndex]
            contentVC.pageIndex = currentIndex
            
            return contentVC
        }
        
        return nil
        
    }
    
    func firstRunContentViewController(forIndex index: Int) -> FirstRunContentViewController? {
        
        if let storyboard = storyboard,
            let contentVC = storyboard.instantiateViewController(withIdentifier: "FirstRunContentViewController")
                as? FirstRunContentViewController {
            
            contentVC.firstRunScreenItem = firstRunScreenItems[ index ]
            contentVC.pageIndex = index
            
            currentIndex = index
            
            return contentVC
        }
        
        return nil
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let contentVC = viewController as? FirstRunViewControllerProtocol {
            
            var index = contentVC.pageIndex
            guard index != NSNotFound && index != 0 else { return nil }
            
            index = index - 1
            currentIndex = index
            
            if currentIndex == 0 {
//                return firstRunIntroViewController()
                return firstRunContentViewController(forIndex: index)
            } else {
                return firstRunContentViewController(forIndex: index)
            }
        
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
  
        if let contentVC = viewController as? FirstRunViewControllerProtocol {
            
            var index = contentVC.pageIndex
            currentIndex = index
            
            guard index != NSNotFound else { return nil }
            index = index + 1
//            currentIndex = index
            guard index != firstRunScreenItems.count else { return nil }
            return firstRunContentViewController(forIndex: index)
        }
        return nil
    }

}

extension FirstRunViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let contentVC = pendingViewControllers.first as? FirstRunContentViewController {
            pendingIndex = contentVC.pageIndex
        }
        
    }
    
//    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        guard completed else { return }
        
        currentIndex = pendingIndex
        self.pageControl.currentPage = currentIndex
        
        if let firstRunPageVC = pageViewController as? FirstRunPageViewController {

            firstRunPageVC.currentIndex = self.currentIndex
            firstRunPageVC.numberOfPages = self.numberOfPages
            
        }
        
    }
    
}
