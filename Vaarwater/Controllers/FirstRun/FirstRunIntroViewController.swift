//
//  FirstRunIntroViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class FirstRunIntroViewController: UIViewController, FirstRunViewControllerProtocol {

    var pageIndex:Int = 0
    var firstRunScreenItem:FirstRunScreenItem?
    
    @IBOutlet weak var titleLabel: PaddedLabel!
    @IBOutlet weak var textLabel: PaddedLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fillWithScreenItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillWithScreenItem() {
        
        // Set image
        guard let content = firstRunScreenItem else {
            return
        }
        
        let titleAttributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.titleLabel.attributedText!)
        titleAttributedString.mutableString.setString(content.titleKey.localized())

        let textAttributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.textLabel.attributedText!)
        textAttributedString.mutableString.setString(content.textKey.localized())
        
        
        titleLabel.attributedText = titleAttributedString
        textLabel.attributedText = textAttributedString
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
