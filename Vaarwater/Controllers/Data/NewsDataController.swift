//
//  NewsDataController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Localize_Swift

class NewsDataController: NSObject {
    
    var oldNews = [News]()
    var insertedNews = [News]()
    
    static let sharedInstance: NewsDataController = {
        
        let instance = NewsDataController()
        return instance
        
    }()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<News> = {
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "createdDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        fetchedResultsController.delegate = self
                
        return fetchedResultsController
    }()
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(contextDidChangeNotificationHandler(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
        
        // Execute fetch command
        try? executeFetch()
        
    }    
    
    var fetchedItems:Array<News> {
        get {
            return fetchedResultsController.fetchedObjects ?? [News]()
        }
    }
    var numberOfUnreadNewsItems:Int {
        get {
            return self.fetchedItems.filter { $0.read == false }.count
        }
    }
    
    public func executeFetch() throws {

        var predicate:NSPredicate
        if Localize.currentLanguage() == "nl" {
            predicate = NSPredicate(format: "lang == %@", "nl")
        } else {
            predicate = NSPredicate(format: "lang == %@", "en")
        }
        self.fetchedResultsController.fetchRequest.predicate = predicate
        
        // Perform fetch
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
    }
    
//    
//    func storeNews( newsArray: Array<Dictionary<String, AnyObject>> ) throws -> [News] {
//        
//        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
//        //        var success:Bool = false
//        
//        var storedNews = [News]()
//        
//        taskContext.performAndWait() {
//            
//            for newsDictionary in newsArray {
//                
//                // Create POI, and if the data is not valid delete the object and continue to process the next
//                let news = NSEntityDescription.insertNewObject(forEntityName: "News", into: taskContext) as! News
//                do {
//                    try news.updateFromDictionary(dictionary: newsDictionary)
//                    storedNews.append(news)
//                }
//                catch {
//                    taskContext.delete(news)
//                }
//                
//            }
//            
//            // Save all the changes just made and reset the taskContext to free the cache.
//            if taskContext.hasChanges {
//                do {
//                    try taskContext.save()
//                }
//                catch {
//                    print("Error: \(error)\nCould not save Core Data context.")
//                    return
//                }
//                taskContext.reset()
//            }
//            //            success = true
//            
//        }
//        
//        return storedNews
//        
//        //        return success
//        
//    }
    
    func storeNews( newsArray: Array<Dictionary<String, AnyObject>> ) throws -> Bool {
        
        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        var success:Bool = false
        
        taskContext.performAndWait() {

            for newsDictionary in newsArray {
                
                let news:News?

                // Create POI, and if the data is not valid delete the object and continue to process the next
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "News")
                let predicate = NSPredicate(format: "id == %d", argumentArray: [newsDictionary["id"] as Any])
                fetchRequest.predicate = predicate
                do {
                    
                    if let fetchResults = try taskContext.fetch(fetchRequest) as? [News],
                        fetchResults.count > 0 {
                        news = fetchResults.first
                    } else {
                        news = NSEntityDescription.insertNewObject(forEntityName: "News", into: taskContext) as? News
                    }
                    
                    do {
                        try news!.updateFromDictionary(dictionary: newsDictionary)
                    }
                    catch {
                        taskContext.delete(news!)
                    }                    
                    
                } catch {
                    return
                }
                
                // Check if news is set
            }
            
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
                taskContext.reset()
            }
            success = true
            
        }
        return success
        
        //        return success
        
    }
    
    @objc func contextDidChangeNotificationHandler(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        
        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject>, inserts.count > 0 {
            
            // Clean inserted news
            insertedNews.removeAll()
            
            // Check the new inserts
            for insert in inserts {
                if insert.isKind(of: News.self) {
                    // Add to inserted news
                    insertedNews.append(insert as! News);                    
                }
            }
            
        }
        
//        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject>, updates.count > 0 {
//            
//            print("--- UPDATES ---")
//            for update in updates {
//                print(update.changedValues())
//            }
//            
//        }        

//
//        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject>, deletes.count > 0 {
//            //            print("--- DELETES ---")
//            //            print(deletes)
//            //            print("+++++++++++++++")
//        }
        
    }
    
    
}

extension NewsDataController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
//        self.oldNews = self.fetchedItems.map { $0.copy() as! News }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
//        for news in self.oldNews {
//            
//            if news.read {
//                // find entity with the same id as news.id
//                let fetchedNewsFilter = self.fetchedItems.filter { $0.id == news.id }
//                for fetchedNews in fetchedNewsFilter {
//                    fetchedNews.updateRead( true )
//                }
//            }            
//        }
//        
        // Post notification
        let name = NSNotification.Name(Globals.kNewsUpdatedNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
        
        
    }
    
    
}
