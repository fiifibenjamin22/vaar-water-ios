//
//  DataStoreController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//


import UIKit
import CoreData

class CoreDataStackManager: NSObject {
    
    static let errorDomain = "CoreDataStackManager"
    static let mainStoreFileName = "Vaarwater.storedata"
    
    static let sharedInstance: CoreDataStackManager = {
        let instance = CoreDataStackManager()
        return instance
    }()
    
    override init() {
        super.init()
        
        // Add context notifications
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(contextDidChangeNotificationHandler(notification:)), name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: nil)
        notificationCenter.addObserver(self, selector: #selector(contextDidSaveNotificationHandler(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: nil)
        
    }
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file.
        // This code uses a directory named "com.srmds.<dbName>" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional.
        // It is a fatal error for the application not to be able to find and load its model.
        
        // This resource is the same name as your xcdatamodeld contained in your project.
        guard let modelURL = Bundle.main.url(forResource: Globals.kDataStoreName, withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        
        // The persistent store coordinator for the application. This implementation creates and return a coordinator,
        // having added the store for the application to it. This property is optional since there are legitimate error
        // conditions that could cause the creation of the store to fail.
        
        // Create the coordinator and store
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(CoreDataStackManager.mainStoreFileName).sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        
        // Do async on background thread for safety
//        DispatchQueue.global(qos: .background).async {
            
            do {
                let options = [
                    NSMigratePersistentStoresAutomaticallyOption: true,
                    NSInferMappingModelAutomaticallyOption: true
                ]
                try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
            } catch {
                // Report any error we got.
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
                
                dict[NSUnderlyingErrorKey] = error as NSError
                let wrappedError = NSError(domain: "nl.waternet.vaarwater", code: 9999, userInfo: dict)
                // Replace this with code to handle the error appropriately.
                NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")

                self.deleteAllData()

//                abort()
                
            }
            
//        }
        
        return persistentStoreCoordinator
    }()
    
    lazy var mainQueueContext: NSManagedObjectContext = {
        
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        
        // Avoid using default merge policy in multi-threading environment:
        // when we delete (and save) a record in one context,
        // and try to save edits on the same record in the other context before merging the changes,
        // an exception will be thrown because Core Data by default uses NSErrorMergePolicy.
        // Setting a reasonable mergePolicy is a good practice to avoid that kind of exception.
        
        managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        return managedObjectContext
    }()
    
    
    // Creates a new Core Data stack and returns a managed object context associated with a private queue.
    func newPrivateQueueContextWithNewPSC() throws -> NSManagedObjectContext {
        
        // Stack uses the same store and model, but a new persistent store coordinator and context.
        //
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: CoreDataStackManager.sharedInstance.managedObjectModel)
        
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(CoreDataStackManager.mainStoreFileName).sqlite")
        
        // Attempting to add a persistent store may yield an error--pass it out of
        // the function for the caller to deal with.
        //
        try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        context.performAndWait() {
            
            context.persistentStoreCoordinator = coordinator
            
            // Avoid using default merge policy in multi-threading environment:
            // when we delete (and save) a record in one context,
            // and try to save edits on the same record in the other context before merging the changes,
            // an exception will be thrown because Core Data by default uses NSErrorMergePolicy.
            // Setting a reasonable mergePolicy is a good practice to avoid that kind of exception.
            //
            context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        }
        
        return context
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func deleteAllData() {
        guard let persistentStore = persistentStoreCoordinator.persistentStores.last else {
            return
        }
        
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(CoreDataStackManager.mainStoreFileName).sqlite")
        
        mainQueueContext.performAndWait { 
            self.mainQueueContext.reset()
            do
            {
                try self.persistentStoreCoordinator.remove(persistentStore)
                try FileManager.default.removeItem(at: url!)
                try self.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
            }
            catch { /*dealing with errors up to the usage*/ }
        }
        
    }
    
    // MARK: - Core Data Notification Handlers
    
    // Handler for NSManagedObjectContextDidSaveNotification.
    // Observe NSManagedObjectContextDidSaveNotification and merge the changes to the main context from other contexts.
    // We rely on this to sync between contexts, thus avoid most of merge conflicts and keep UI refresh.
    //
    
    @objc func contextDidChangeNotificationHandler(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        
        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject>, inserts.count > 0 {
//            print("--- INSERTS ---")
//            print(inserts)
//            print("+++++++++++++++")
        }
        
        if let updates = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject>, updates.count > 0 {
//            print("--- UPDATES ---")
//            for update in updates {
//                print(update.changedValues())
//            }
//            print("+++++++++++++++")
        }
        
        if let deletes = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject>, deletes.count > 0 {
//            print("--- DELETES ---")
//            print(deletes)
//            print("+++++++++++++++")
        }
                
    }
        
    @objc func contextDidSaveNotificationHandler(notification: NSNotification){
        
        let sender = notification.object as! NSManagedObjectContext
        if sender !== mainQueueContext {
            mainQueueContext.perform {
                self.mainQueueContext.mergeChanges(fromContextDidSave: notification as Notification)
            }
        }
        
    }
    
}
