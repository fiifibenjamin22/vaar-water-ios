//
//  API+Traffic.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 27/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

extension API {
    
    func getNearestTraffic( forLocation location: CLLocation?, completion : ((_ data: Dictionary<String, AnyObject>?, _ success : Bool, _ error : String?) -> Void)?) {
        
        let url:String = self.host + "trafficlinks/nearest"
        let credential : URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
        
        guard let location = location else {
            print("Something wrong with the location")
            completion?(nil, false, "No location given")            
            return
        }
        
        let parameters : Dictionary<String,AnyObject> =  [
            "lat": location.coordinate.latitude as AnyObject,
            "lng": location.coordinate.longitude as AnyObject
        ]
        
        if self.logging {
            print(url)
        }
        
        Alamofire.request( url, parameters: parameters )
            .authenticate(usingCredential: credential )
            .responseJSON { response in
                
                let statusCode = response.response?.statusCode
                
                // If we don't get data back, return
                guard let data = response.data else {
                    print("Error: \nNo data found in POI API call.")
                    return
                }
                
                // Parse as JSON, catch if it doesn't work
                do {
                    let result:JSON = try JSON(data: data , options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if self.logging {
                        print(request)
                        print(response)
                        print("JSON RESULT", result)
                    }
                    
                    if (statusCode == 200) {
                        
                        if let result = result.rawValue as? Dictionary<String, AnyObject> {
                            completion?(result, true, nil)
                        } else {
                            completion?(nil, false, "Invalid Data")
                        }
                        
                    } else {
                        
                        //not logedin
                        if (statusCode == 401) {
                            let message = result["message"].string
                            if self.logging {
                                print("error " + message!)
                            }
                            self.expiredSession()
                            
                            completion?(nil, false, message)
                            return
                        }
                        
                        //not found
                        if (statusCode == 404) {
                            if let message = result["message"].string {
                                if self.logging {
                                    print("error " + message)
                                }
                                completion?(nil, false, message)
                                return
                            }
                        }
                        
                        completion?(nil, false, "Server fout")
                        return
                    }
                } catch {
                    completion?(nil, false, "Server fout")
                    return
                }
        }
        
        
    }
        
}
