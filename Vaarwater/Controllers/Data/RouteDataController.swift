//
//  RouteDataController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Localize_Swift

class RouteDataController: NSObject {
    
    static let sharedInstance: RouteDataController = {
        
        let instance = RouteDataController()
        return instance
        
    }()
    
    public lazy var fetchedResultsController: NSFetchedResultsController<Route> = {
        
        // Initialize Fetch Request
        let fetchRequest:NSFetchRequest<Route> = Route.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Initialize Fetched Results Controller
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
    override init() {
        super.init()
        
        // Execute fetch command
        try? executeFetch()
    }
    
    var fetchedItems:Array<Route> {
        get {
            return fetchedResultsController.fetchedObjects ?? [Route]()
        }
    }
    
    public func executeFetch() throws {
                
        // Perform fetch
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }
        
    }
        
    func storeRoutes( routeArray: Array<Dictionary<String, AnyObject>> ) throws -> Bool {
        
        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        var success:Bool = false
        
        taskContext.performAndWait() {
            
            for routeDictionary in routeArray {
                
                do {
                    try Route.createRouteEntity(withDictionary: routeDictionary, inContext: taskContext)
                }
                catch {
                    print("Couldn't update Route data for id %d: ", routeDictionary["id"]!)
                    print("Error info: \(error)")
                }
            }
            
            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
                taskContext.reset()
            }
            success = true
            
        }
        return success
        
    }
    
    public class func deleteAllRoutes() throws -> Bool {

        let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
        var success:Bool = false

        taskContext.performAndWait() {

            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Route")
            fetchRequest.returnsObjectsAsFaults = false

            do {
                let results = try taskContext.fetch(fetchRequest)
                for object in results
                {
                    let objectData:NSManagedObject = object as! NSManagedObject
                    taskContext.delete(objectData)
                }
                success = true

            } catch let error as NSError {
                print("Delete all data in Route error : \(error) \(error.userInfo)")
                success = false
            }

            // Save all the changes just made and reset the taskContext to free the cache.
            if taskContext.hasChanges {
                do {
                    try taskContext.save()
                }
                catch {
                    print("Error: \(error)\nCould not save Core Data context.")
                    return
                }
                taskContext.reset()
            }
            
        }
        return success
    }
    
}
