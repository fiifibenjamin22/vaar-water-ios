//
//  API+POI.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 27/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

extension API {
    
    // MARK: Custom methods
    

    
    func fetchFamilies( completion : ((_ data: [POIFamily]?, _ success : Bool, _ error : String?) -> Void)? ) {

        let taskContext = CoreDataStackManager.sharedInstance.mainQueueContext
        var fetchedResults:[POIFamily] = [POIFamily]()
//        var fetchedResults:Dictionary<String, AnyObject>?
        
        // Create request on Event entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "POIFamily")
        let predicate = NSPredicate(format: "search_default == 1")
        fetchRequest.predicate = predicate
        
        // Add sort descriptor
        let sortDescriptor = NSSortDescriptor(key: "sortorder", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        //Execute Fetch request
        do {
            fetchedResults = try taskContext.fetch(fetchRequest) as! [POIFamily]
            
            print("Fetched Results: \(fetchedResults)")
            
            
            
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = [POIFamily]()
            completion?(fetchedResults, false, "retrieveById error: \(fetchError.localizedDescription)" )
            return
        }
        
        completion?(fetchedResults, true, nil)
        return
        
    }
    
    func fetchAnchors( completion : ((_ data: POI?, _ success : Bool, _ error : String?) -> Void)? ) {
    
        let taskContext = CoreDataStackManager.sharedInstance.mainQueueContext
        var fetchedResults:[POI] = [POI]()
        //        var fetchedResults:Dictionary<String, AnyObject>?
        
        // Create request on Event entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "POI")
        let predicate = NSPredicate(format: "anchor == 1")
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        
        // Add sort descriptor
        let sortDescriptor = NSSortDescriptor(key: "sortorder", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        //Execute Fetch request
        do {
            fetchedResults = try taskContext.fetch(fetchRequest) as! [POI]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = [POI]()
            completion?(fetchedResults.first, false, "retrieveById error: \(fetchError.localizedDescription)" )
            return
        }
        
        completion?(fetchedResults.first, true, nil)
        return
    
    }

    
    func getPOIs( parameters : Dictionary<String, AnyObject>?, completion : ((_ fetchedPOIs: [POI]?, _ success : Bool, _ error : String?) -> Void)?) {
        
        print("Getting POIs from API")
        
        let url:String = self.host + "pois"
        
        var parsedParameters : Dictionary<String,AnyObject> =  [:]
        
        if let parameters = parameters {
            
            if let minDate = parameters["minDate"] {
                let dateformatter : DateFormatter = DateFormatter()
                dateformatter.locale = Locale(identifier: "nl_NL")
                dateformatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                parsedParameters.updateValue(dateformatter.string(from: minDate as! Date) as AnyObject, forKey: "mindate")
            }
            
            if let eventId = parameters["eventId"] {
                parsedParameters.updateValue(eventId as AnyObject, forKey: "event_id")
            }
            
        }
        
        if self.logging {
            print("url:", url)
        }
                
        Alamofire.request( url, parameters: parsedParameters )
           // .authenticate(usingCredential: credential )
            .responseJSON { response in
                                
                // If we don't get data back, return
                guard let data = response.data else {
                    print("Error: \nNo data found in POI API call.")
                    return
                }
                
                // Parse as JSON, catch if it doesn't work
                do {
                    let result:JSON = try JSON(data: data , options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    
                    if let status = response.response?.statusCode {
                        
                        switch status {
                            
                        case 200...300:
                            print("success")
                            if let poiArray = result["data"].rawValue as? Array<Dictionary<String, AnyObject>> {
                                
//                                _ = try? POIDataController.sharedInstance.storePOIs(pois: poiArray, completion: { (storePOIs) in
//                                    completion?(storePOIs, true, nil)
//                                })
                                
                                if let storePOIs = try? POIDataController.sharedInstance.storePOIs( pois: poiArray ) {
                                    completion?(storePOIs, true, nil)
                                } else {
                                    print("Error: \nCouldn't store JSON data in POI entities for changelog")
                                    completion?(nil, false, "Invalid Data")
                                }

                            } else {

                                completion?(nil, false, "Invalid Data")
                            }

                            break
                        case 401:
                            let message = result["message"].string
                            if self.logging {
                                print("error " + message!)
                            }
                            self.expiredSession()
                            
                            completion?(nil, false, message)
                            break
                        case 404:
                            if let message = result["message"].string {
                                if self.logging {
                                    print("error " + message)
                                }
                                completion?(nil, false, message)
                                return
                            }
                        default:
                            print("default")
                        }

                    }
                } catch {
                    completion?(nil, false, "Server fout")
                    return
                }
        }
    }
    
    // MARK: POI Changelog
    func getPOIChangelog(minDate : Date, completion : ((_ deletedPOIs: [POI]?, _ succes : Bool, _ error : String?) -> Void)?) {
        
        let url : String = self.host + "poichangelog"
        let credential : URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
        let dateformatter : DateFormatter = DateFormatter()
        dateformatter.locale = Locale(identifier: "nl_NL")
        dateformatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let parameters : Dictionary<String,AnyObject> =  [
            "mindatetime":dateformatter.string(from: minDate) as AnyObject,
            "action":"delete"  as AnyObject
        ]
        
        if self.logging {
            print(url)
        }
        
        Alamofire.request(url, parameters:parameters )
            .authenticate(usingCredential: credential)
            .responseJSON { response in
                let statusCode = response.response?.statusCode
                
                // If we don't get data back, return
                guard let data = response.data else {
                    print("Error: \nNo data found in POI API call.")
                    return
                }
                
                // Parse as JSON, catch if it doesn't work
                do {
                    let result:JSON =  try JSON(data: data , options: JSONSerialization.ReadingOptions.allowFragments)
                    
                    if self.logging {
                        print(request)
                        print(response)
                        print("JSON RESULT", result)
                    }
                    
                    if (statusCode == 200) {
                        
                        if let poiArray = result["data"].rawValue as? Array<Dictionary<String, AnyObject>> {
                            
                            if let deletedPOIs = try? POIDataController.sharedInstance.deletePOIs( pois: poiArray ) {
                                completion?(deletedPOIs, true, nil)
                            } else {
                                print("Error: \nCouldn't store JSON data in POI entities for changelog")
                                completion?(nil, false, "Invalid Data")
                            }
                            
                        } else {
                            completion?(nil, false, "Invalid Data")
                        }
                        
                    } else {
                        
                        //not loggedin
                        if (statusCode == 401) {
                            let message = result["message"].string
                            if self.logging {
                                print("error " + message!)
                            }
                            self.expiredSession()
                            
                            completion?(nil, false, message)
                            return
                        }
                        
                        //not found
                        if (statusCode == 404) {
                            if let message = result["message"].string {
                                if self.logging {
                                    print("error " + message)
                                }
                                completion?(nil, false, message)
                                return
                            }
                        }
                        
                        completion!(nil, false, "Server fout")
                        return
                    }
                } catch {
                    completion?(nil, false, "Server fout")
                    return
                }
                
        }
        
    }
        
}
