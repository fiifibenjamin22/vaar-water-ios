//
//  SlidingViewAnimator.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class SlidingViewAnimator: NSObject {
    
    let direction: SlidingViewDirection
    let isPresentation: Bool
    
    init(direction: SlidingViewDirection, isPresentation: Bool) {
        self.direction = direction
        self.isPresentation = isPresentation
        super.init()
    }
    
}

// MARK: UIViewControllerAnimatedTransitioning delegate methods

extension SlidingViewAnimator: UIViewControllerAnimatedTransitioning {
    
    // Get/set duration for the transition
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.45
    }

    // Animate the transition
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        guard
            let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
            else {
                return
        }
        
        let key = isPresentation ? UITransitionContextViewControllerKey.to : UITransitionContextViewControllerKey.from
        let controller = transitionContext.viewController(forKey: key)!
        
        if isPresentation {
            transitionContext.containerView.addSubview(controller.view)
        }
        
        let presentedFrame = transitionContext.finalFrame(for: controller)
        var dismissedFrame = presentedFrame
        
        switch direction {
        case .left:
            dismissedFrame.origin.x = -presentedFrame.width
        case .right:
            dismissedFrame.origin.x = transitionContext.containerView.frame.size.width
        case .down:
            dismissedFrame.origin.y = -presentedFrame.height
        case .up:
            dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
        }
        
//        dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
        
        let initialFrame = isPresentation ? dismissedFrame : presentedFrame
        let finalFrame = isPresentation ? presentedFrame : dismissedFrame
        
        let animationDuration = transitionDuration(using: transitionContext)
        controller.view.frame = initialFrame
        
        UIView.animate(withDuration: animationDuration,
                        delay: 0,
                        usingSpringWithDamping: 0.96,
                        initialSpringVelocity: 0.2,
                        options: [],
                        animations: {
                            controller.view.frame = finalFrame
                        },
                        completion: { finished in
                            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                        }
        )
        
        
//        UIView.animate(withDuration: animationDuration, animations: {
//            controller.view.frame = finalFrame
//        }) { finished in
//            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//        }
//
        
//        let screenBounds = UIScreen.main.bounds
//        let finalFrame = CGRect(x: 0, y: screenBounds.size.height, width: screenBounds.size.width, height: screenBounds.size.height)
//                
//        UIView.animate(
//            withDuration: transitionDuration(using: transitionContext),
//            animations: {
//                fromVC.view.frame = finalFrame
//            },
//            completion: { _ in
//                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//            }
//        )
    
    }
    
}
