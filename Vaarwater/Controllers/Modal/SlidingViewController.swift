//
//  SlidingViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

@objc protocol SlidingViewControllerDelegate {
    @objc optional func slidingViewWillDismiss( controller: SlidingViewController )
    @objc optional func slidingViewDidDismiss( controller: SlidingViewController)
    @objc optional func slidingViewWillPresent( controller: SlidingViewController)
    @objc optional func slidingViewDidPresent( controller: SlidingViewController )
}

enum SlidingViewDirection {
    case up
    case down
    case left
    case right
}

class SlidingViewInteractor: UIPercentDrivenInteractiveTransition {
    var hasStarted:Bool = false
    var shouldComplete:Bool = false
    var isDismissing:Bool = false
}

class SlidingViewController: UIViewController {
    
    var interactor:SlidingViewInteractor!
    var hasScrolledToTop:Bool = true
    
    var direction = SlidingViewDirection.up
    var isExpandable:Bool = true
    var fittedSize:Bool = false
    var visibleTapView:Bool = true
    var allowPassthrough:Bool = false
    
    var slidingViewDelegate:SlidingViewControllerDelegate?
    
    var panGestureRecognizer: UIPanGestureRecognizer?
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        
        interactor = SlidingViewInteractor()
        
        // Create PanGestureRecognizer
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)) )
        panGestureRecognizer!.delegate = self
        self.view.addGestureRecognizer(panGestureRecognizer!)
        
        guard let scrollView = self.scrollView else {
            print("No scrollView in ModalViewController subclassed ViewController")
            return
        }
        
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        scrollView.isScrollEnabled = false
        scrollView.delegate = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.slidingViewDelegate?.slidingViewWillPresent?( controller: self )
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.slidingViewDelegate?.slidingViewDidPresent?(controller: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.slidingViewDelegate?.slidingViewDidDismiss?(controller: self)
    }
    
    
    private func calculateProgress(translationInView:CGPoint, viewBounds:CGRect, direction:SlidingViewDirection) -> CGFloat {
        
        let pointOnAxis:CGFloat
        let axisLength:CGFloat
        
        switch direction {
        case .up, .down:
            pointOnAxis = translationInView.y
            axisLength = viewBounds.height
        case .left, .right:
            pointOnAxis = translationInView.x
            axisLength = viewBounds.width
        }
        let movementOnAxis = pointOnAxis / axisLength
        let positiveMovementOnAxis:Float
        let positiveMovementOnAxisPercent:Float
        switch direction {
        case .right, .down: // positive
            positiveMovementOnAxis = fmaxf(Float(movementOnAxis), 0.0)
            positiveMovementOnAxisPercent = fminf(positiveMovementOnAxis, 1.0)
            return CGFloat(positiveMovementOnAxisPercent)
        case .up, .left: // negative
            positiveMovementOnAxis = fminf(Float(movementOnAxis), 0.0)
            positiveMovementOnAxisPercent = fmaxf(positiveMovementOnAxis, -1.0)
            return CGFloat(-positiveMovementOnAxisPercent)
        }
    }
    
    @IBAction func handlePan(sender: UIPanGestureRecognizer) -> Void {
                
        let percentThreshold:CGFloat = 0.1
        
//        // convert y-position to downward pull progress (percentage)
        let translation = sender.translation(in: sender.view?.superview)
        
        let progressLeft = calculateProgress(
            translationInView: translation,
            viewBounds: view.bounds,
            direction: .left )
        
        let progressRight = calculateProgress(
            translationInView: translation,
            viewBounds: view.bounds,
            direction: .right )
        
        let progressDown = calculateProgress(
            translationInView: translation,
            viewBounds: view.bounds,
            direction: .down )

        let progressUp = calculateProgress(
            translationInView: translation,
            viewBounds: view.bounds,
            direction: .up )
        
        guard let presentationVC = presentationController as? SlidingViewPresentationController else {
            print("no presentation controller found")
            return
        }
        
        let progressPresent:CGFloat, progressDismiss:CGFloat
        switch direction {
        case .up:
            progressPresent = progressUp
            progressDismiss = progressDown
        case .down:
            progressPresent = progressDown
            progressDismiss = progressUp
        case .left:
            progressPresent = progressRight
            progressDismiss = progressLeft
        case .right:
            progressPresent = progressLeft
            progressDismiss = progressRight
        }
        
//        print("progress", progressPresent)
//        print("progress dismiss", progressDismiss )
        
        switch sender.state {
        case .began:
            interactor.hasStarted = true
            
//            if ( progressDown > 0) {
//                dismiss(animated: true, completion: nil)
//            }
            
        case .changed:
            
            if ( progressPresent > 0.05) {
                
                if ( !presentationVC.isMaximized && !presentationVC.isAnimating && self.isExpandable && !interactor.isDismissing ) {
                    presentationVC.adjustToFullScreen()
                }
                
            } else {
                
                if ( progressDismiss > 0.05 ) {
                    
                    if ( presentationVC.isMaximized && self.isExpandable ) {
                        if ( !presentationVC.isAnimating ) {
                            presentationVC.adjustToHalfScreen()
                        }
                    } else {
                        
                        if ( !interactor.isDismissing ) {
                            dismiss(animated: true, completion: nil )
                            interactor.isDismissing = true
                        }
                        interactor.shouldComplete = progressDismiss > percentThreshold
                        
                        if (direction == .left || direction == .right ) {
                            interactor.update(progressDismiss / 2)
                        } else {
                            interactor.update(progressDismiss / 2)
                        }
                    }
                }
            }            
            
        case .cancelled:
            interactor.isDismissing = false
            interactor.hasStarted = false
            interactor.cancel()
            
        case .ended:
            
            interactor.isDismissing = false
            interactor.hasStarted = false
            if interactor.shouldComplete {
                self.slidingViewDelegate?.slidingViewWillDismiss?(controller: self)                
                interactor.finish()
            } else {
                interactor.cancel()
            }
            
        default:
            interactor.cancel()
            break
        }
        
    }
    
//    @IBAction func handleBackTap(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
//    }    
        
    @IBAction func handleCloseTap(_ sender: Any) {
        self.slidingViewDelegate?.slidingViewWillDismiss?(controller: self)
        
        if let presentingVC = self.presentingViewController?.presentingViewController {
            presentingVC.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        
//        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // You might want to check if this is your embed segue here
        // in case there are other segues triggered from this view controller.

        segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
        
    }

}

// MARK: Transition Delegate methods

extension SlidingViewController: UIViewControllerTransitioningDelegate {
    
    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {

        let presentationController = SlidingViewPresentationController(presentedViewController: presented,
                                                                 presenting: presenting,
                                                                 direction: direction,
                                                                 isExpandable: isExpandable,
                                                                 fittedSize: fittedSize,
                                                                 visibleTapView: visibleTapView,
                                                                 allowPassthrough: allowPassthrough)
        presentationController.slidingViewPresentationDelegate = self
        return presentationController
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlidingViewAnimator(direction: direction, isPresentation: true )
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlidingViewAnimator(direction: direction, isPresentation: false )
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
}

// MARK: - Scrollview delegate methods

extension SlidingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y <= 0 ) {
            self.hasScrolledToTop = true
        } else {
            self.hasScrolledToTop = false
        }
    }
    
}

// MARK: - Gesture recognizer delegate methods

extension SlidingViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if let presentationVC = self.presentationController as? SlidingViewPresentationController {
            if presentationVC.isMaximized || (direction == .left || direction == .right) {
                return hasScrolledToTop
            }
        }
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
        return false
    }
    
}

// MARK: - SlidingView Presentation Delegate methods

extension SlidingViewController: SlidingViewPresentationDelegate {
    
    func slidingViewPresentationDimmedViewTap() {
        self.slidingViewDelegate?.slidingViewWillDismiss?(controller: self)
    }
    
    func slidingViewPresentationWillAdjustToHalfscreen() {
//        scrollView.isScrollEnabled = false
    }
  
  func slidingViewPresentationDidAdjustToHalfscreen() {
//
  }
    
    func slidingViewPresentationDidAdjustToFullscreen() {
//        scrollView.isScrollEnabled = true
    }
    
}
