//
//  ModalTransitionManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

enum PresentationDirection {
    case left
    case top
    case right
    case bottom
}

class ModalPresentationManager: NSObject {
    
    var direction = PresentationDirection.left    

}

// MARK: Transition Delegate methods

extension ModalPresentationManager: UIViewControllerTransitioningDelegate {
    
//    func presentationController(forPresented presented: UIViewController,
//                                presenting: UIViewController?,
//                                source: UIViewController) -> UIPresentationController? {
//        
//        let presentationController = ModalPresentationController(presentedViewController: presented,
//                                                                 presenting: presenting,
//                                                                 direction: direction)
//        return presentationController
//    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ModalAnimator()
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
}
