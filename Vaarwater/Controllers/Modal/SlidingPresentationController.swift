//
//  SlidingViewPresentationController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 17/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

@objc protocol SlidingViewPresentationDelegate {
    @objc func slidingViewPresentationDimmedViewTap()
    @objc optional func slidingViewPresentationWillAdjustToFullscreen()
    @objc optional func slidingViewPresentationWillAdjustToHalfscreen()
    @objc optional func slidingViewPresentationDidAdjustToFullscreen()
    @objc optional func slidingViewPresentationDidAdjustToHalfscreen()
}

class SlidingViewPresentationController: UIPresentationController {
    
    // MARK: - Local variables
    
    var isExpandable:Bool = true
    var isMaximized:Bool = false
    var isMinimizing:Bool = false
    var isAnimating:Bool = false
    var fittedSize: Bool = false
    var visibleTapView:Bool = true
    var allowPassthrough: Bool = false
    
    var direction: SlidingViewDirection = .down
    
    var slidingViewPresentationDelegate:SlidingViewPresentationDelegate?
    
    fileprivate var tapView: UIView!
    
//    private var direction: PresentationDirection
    
    // MARK: - Initializers
    
    init(presentedViewController: UIViewController,
         presenting presentingViewController: UIViewController?,
         direction: SlidingViewDirection,
         isExpandable: Bool?,
         fittedSize: Bool?,
         visibleTapView: Bool?,
         allowPassthrough: Bool?) {
        
        self.direction = direction
        
        if let isExpandable = isExpandable {
            self.isExpandable = isExpandable
        }
        
        if let fittedSize = fittedSize {
            self.fittedSize = fittedSize
        }
        
        if let visibleTapView = visibleTapView {
            self.visibleTapView = visibleTapView
        }
        
        if let allowPassthrough = allowPassthrough {
            self.allowPassthrough = allowPassthrough
        }
        
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setupTapView()
        
    }
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setupTapView()
    }
    
    func setupTapView() {
        
        // If tapview should be disabled, don't draw or connect it
//        if self.disableTapView { return }
        
        tapView = UIView()
        tapView.translatesAutoresizingMaskIntoConstraints = false
        tapView.backgroundColor = UIColor(white: 0.0, alpha: (self.visibleTapView ? 0.15 : 0) )
        tapView.alpha = 0.0
        
        // Add tap gesture recognizer for dismissal (when tapped outside presented view)
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleDimmedViewTap(recognizer:)))
        tapView.addGestureRecognizer( recognizer )
        
    }
    
    // Dismiss presenting view controller
    @objc dynamic func handleDimmedViewTap(recognizer: UITapGestureRecognizer) {
        
        if self.allowPassthrough { return }
        
        self.slidingViewPresentationDelegate?.slidingViewPresentationDimmedViewTap()
        presentingViewController.dismiss(animated: true)
    }
    
    // MARK: - Transition override methods
    
    // Fade tapView in with presenting controller transition
    override func presentationTransitionWillBegin() {
        
        // If tapview should be disabled, don't draw or connect it
//        if self.disableTapView { return }
        
        // Place the tapView in the container at the bottom position
        containerView?.insertSubview(tapView, at: 0)
        
        // Dynamically create constraints to make tapView fill screen
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[tapView]|",
                                           options: [], metrics: nil, views: ["tapView": tapView!]))
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[tapView]|",
                                           options: [], metrics: nil, views: ["tapView": tapView!]))
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            tapView.alpha = 1.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.tapView.alpha = 1.0
        })
    }
    
    // Hide the tapView when presenting controller is dismissed
    override func dismissalTransitionWillBegin() {
        
        // If tapview should be disabled, don't draw or connect it
//        if self.disableTapView { return }
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            tapView.alpha = 0.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.tapView.alpha = 0.0
        })
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        
        if completed {
            
            // If tapview should be disabled, don't draw or connect it
            if tapView != nil {
                tapView.removeFromSuperview()
            }
            
            isMaximized = false
        }
    }
    
    // Make sure presented view will keep fitting on layout change
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    // MARK: - Size calculation methods
    
//    // Calculate size
    override func size(forChildContentContainer container: UIContentContainer,
                       withParentContainerSize parentSize: CGSize) -> CGSize {
        
        var size = super.size(forChildContentContainer: container, withParentContainerSize: parentSize)
                
        switch direction {
            
            case .left, .right:
                if ( !self.isMaximized || self.isMinimizing ) && isExpandable {
                    size.width = parentSize.width * 0.5
                } else {
                    size.width = parentSize.width * 0.85
                }
                
            case .up, .down:
                if ( !self.isMaximized || self.isMinimizing ) && isExpandable {
                    size.height = parentSize.height * 0.5
                } else {
                    size.height = parentSize.height * 0.9
                }
            
        }
        
        return size
    }
    
    // This one sets the frame size for the slidingview viewcontroller
    override var frameOfPresentedViewInContainerView: CGRect {
                
        var frame: CGRect = .zero
        frame.size = size(forChildContentContainer: presentedViewController,
                          withParentContainerSize: containerView!.bounds.size)
        
        
        switch direction {
        case .right:
            frame.origin.x = containerView!.bounds.width - frame.size.width
        case .up:
            
            // If the presentedView is smaller than half of the containerView, make it non-expandable and
            // adjust height to fittedSize instead
            if fittedSize {
                if let size = presentedView?.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize) {
//                    if size.height > 0 && size.height < containerView!.bounds.height * 0.5 {
                    if size.height > 0 {                    
                        frame.size.height = size.height
                    }
                }
            }
            
            if (isExpandable) {
                frame.origin.y = containerView!.bounds.height * 0.5
            } else {
                frame.origin.y = max(containerView!.bounds.height * 0.1, (containerView!.bounds.height - frame.size.height) )
            }
        default:
            frame.origin = .zero
        }
        
        return frame
        
    }

}

// MARK: - Custom methods for user interaction

extension SlidingViewPresentationController {

    func adjustToFullScreen() {
        
        if (!isExpandable) { return }
        
        isAnimating = true
        self.slidingViewPresentationDelegate?.slidingViewPresentationWillAdjustToFullscreen?()
        
        if let presentedView = presentedView, let containerView = self.containerView {
            
            UIView.animate(
                withDuration: 0.56,
                delay: 0,
                usingSpringWithDamping: 0.88,
                initialSpringVelocity: 0.7,
                options: .curveEaseInOut,
                animations: { () -> Void in
                    
                    self.isMaximized = true
                    
                    var frame: CGRect = .zero
                    frame.size = self.size(forChildContentContainer: self.presentedViewController,
                                      withParentContainerSize: containerView.bounds.size)
                    
                    switch self.direction {
                        case .right:
                            frame.origin.x = containerView.bounds.width * 0.1
                        case .up:
                            frame.origin.y = containerView.bounds.height * 0.1
                        default:
                            frame.origin = .zero
                    }
                    
//                    frame.origin.y = containerView.bounds.height * 0.1
                    presentedView.frame = frame
                
                },
                completion: { (completed: Bool) -> Void in
                    self.isAnimating = false
                    self.slidingViewPresentationDelegate?.slidingViewPresentationDidAdjustToFullscreen?()
                })
        }
        
    }
    
    func adjustToHalfScreen() {
        
        if (!isExpandable) { return }
        
        isAnimating = true
        self.slidingViewPresentationDelegate?.slidingViewPresentationWillAdjustToHalfscreen?()
        
        if let presentedView = presentedView, let containerView = self.containerView {
            
            UIView.animate(
                withDuration: 0.56,
                delay: 0,
                usingSpringWithDamping: 0.88,
                initialSpringVelocity: 0.7,
                options: .curveEaseInOut,
                animations: { () -> Void in
                    
                    self.isMinimizing = true
                    
                    var frame: CGRect = .zero
                    frame.size = self.size(forChildContentContainer: self.presentedViewController,
                                           withParentContainerSize: containerView.bounds.size)
                    
                    switch self.direction {
                    case .right:
                        frame.origin.x = containerView.bounds.width * 0.5
                    case .up:
                        frame.origin.y = containerView.bounds.height * 0.5
                    default:
                        frame.origin = .zero
                    }
                    
//                    frame.origin.y = containerView.bounds.height * 0.5
                    presentedView.frame = frame
                    
                },
                completion: { (completed: Bool) -> Void in
                    self.isAnimating = false
                    self.isMaximized = false
                    self.isMinimizing = false
                    
                    self.slidingViewPresentationDelegate?.slidingViewPresentationDidAdjustToHalfscreen?()
                }
            )
        }
        
    }
    
}

