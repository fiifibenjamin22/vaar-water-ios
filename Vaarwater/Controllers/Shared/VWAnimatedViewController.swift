//
//  HelpNavigationController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 14/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class VWAnimatedViewController: UIViewController {
    
    var shouldDismissModally:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let dismissNavigationController:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dismissModally),
                                               name: dismissNavigationController,
                                               object: nil)
        
    }
    
    func dismissModally() {
        
        self.shouldDismissModally = true
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension VWAnimatedViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return HelpViewAnimator(isPresentation: true )
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return HelpViewAnimator(isPresentation: false, isModalDismiss: shouldDismissModally )
    }
    
}
