//
//  SearchTableViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

private let searchFamiliesCellIdentifier = "searchFamiliesCell"
private let searchResultCellIdentifier = "searchResultCell"
private let familyHeaderCellIdentifier = "familyHeaderCell"

class SearchTableViewController: UITableViewController, SlidingViewControllerDelegate {
    
    static let maximumNumberOfSearchResults = 50
    var showingEmptyDataset:Bool = true
    var selectedFamily: POIFamily?
    
    var searchViewDelegate:SearchViewControllerDelegate?
    
    var searchFamilies = [POIFamily]() {
        didSet {
            showingEmptyDataset = true
            self.tableView.estimatedRowHeight = 70
//            self.tableView.reloadSections(IndexSet(integer: 0) , with: UITableViewRowAnimation.top)
            self.tableView.reloadData()
        }
    }
    var foundPOIs = [POI]() {
        didSet {
            showingEmptyDataset = false
//            selectedFamily = nil
            self.tableView.estimatedRowHeight = 40
            self.tableView.reloadData()
//            self.tableView.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.top)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // This will remove extra separators from tableview
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Initially set rowHeight to 80 for families
        self.tableView.rowHeight = UITableView.automaticDimension
//        self.tableView. rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 70
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear( animated)
        // Fetch families initially
        loadInitialData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadInitialData() {
        
        print("Loading search families")
        if searchFamilies.count == 0 {
            
            let completionHandler: ( _ data: [POIFamily]?, _ success: Bool, _ error: String? ) -> Void = { ( data, success, error ) in
                
                guard error == nil else {
                    print("Error fetching POI data:\(String(describing: error))")
                    return
                }
                guard data != nil else {
                    print("no data fetched")
                    return
                }
                
                if ( data!.count > 0 ) {
                    
                    let sortedFamilies = data!.sorted( by: { $0.sortorder < $1.sortorder })
                    
                    // Store for future usage
                    self.searchFamilies = sortedFamilies
                }
            }
            
            // Load local data
            API.sharedInstance.fetchFamilies( completion: completionHandler )
            
        } else {
//            showingEmptyDataset = true
            self.tableView.reloadData()
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if foundPOIs.count == 0 {
            return searchFamilies.count
        } else {
            return min(foundPOIs.count, SearchTableViewController.maximumNumberOfSearchResults)
        }
    
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if showingEmptyDataset {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: searchFamiliesCellIdentifier, for: indexPath) as? CategoryCell {
                
                let family = searchFamilies[ indexPath.row ]
                
                cell.family = family
                
//                cell.icon?.image = MarkerManager.getImageForFamily( forTarget: "Search", family: family.slug! )
//                
//                let locale = Localize.currentLanguage()
//                if locale == "nl" {
//                    cell.nameLabel.text = family.name_nl
//                } else {
//                    cell.nameLabel.text = family.name_en
//                }
                
                cell.selectionStyle = .none
                return cell
                
            }
            
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: searchResultCellIdentifier, for: indexPath) as! SearchResultCell
            
            guard foundPOIs.count > 0 else {
                return cell
            }
            
            let poi = foundPOIs[ indexPath.row ]
            cell.poi = poi
            cell.selectionStyle = .none
            
            return cell
        }
        
        let cell = UITableViewCell()
        return cell
        
    }
        
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if showingEmptyDataset {
            
            print("showingEmptyDataset")
            
            // Perform search with family slug
            let family = searchFamilies[ indexPath.row ]
            selectedFamily = family
            
            let parentVC = self.parent as! SearchViewController
            
            let locale = Localize.currentLanguage()
            if locale == "nl" {
                parentVC.performSearch(withString: family.name_nl ?? "", isCategory: true)
            } else {
                parentVC.performSearch(withString: family.name_en ?? "", isCategory: true)
            }
            
        } else {
            self.searchViewDelegate?.hideSearchViewController()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = Globals.lightGrayColor
    }
    
    override func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.clear
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: familyHeaderCellIdentifier) as! SearchHeaderCell
        
        // Do something here
        if selectedFamily != nil && !showingEmptyDataset {
            
//            let locale = Localize.currentLanguage()
//            if locale == "nl" {
//                cell.headerLabel.text = selectedFamily?.name_nl ?? ""
//            } else {
//                cell.headerLabel.text = selectedFamily?.name_en ?? ""
//            }
            
            cell.family = selectedFamily
        } else {
            
//            cell.headerLabel.text = "search.header.default".localized()
            cell.family = nil
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if selectedFamily != nil || showingEmptyDataset {
            return 70.0
        } else {
            return 0
        }
    }
        
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.alpha = 0.3
        
        UIView.animate(withDuration: 0.37,
                        delay: (Double(indexPath.row) * 0.01),
                        options: UIView.AnimationOptions.curveEaseInOut,
                        animations: {
                            cell.alpha = 1
                        },
                        completion: { finished in
                            // do something
                        }
        )
        
    }
    
    func slidingViewWillDismiss( controller: SlidingViewController ) {
        self.searchViewDelegate?.showSearchViewController()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let controller = segue.destination as? SlidingViewController {
            
            controller.slidingViewDelegate = self
            controller.transitioningDelegate = controller
            controller.modalPresentationStyle = .custom
            controller.visibleTapView = false
            
            if segue.identifier == "poiDetailSegue" || controller.isKind(of: POIDetailViewController.self),
                let indexForSelectedRow:IndexPath = self.tableView.indexPathForSelectedRow {
                
                let poi = self.foundPOIs[ indexForSelectedRow.row ]
                
                let detailController = controller as! POIDetailViewController
                detailController.isSearchResult = true
                detailController.poi = poi
                
                // Send notification to observers
                let name:NSNotification.Name = NSNotification.Name(Globals.kPOISelectedKey)
                let userInfo:[String: POI] = ["poi": poi]
                NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
                
            }
            
        }
        
    }

}
