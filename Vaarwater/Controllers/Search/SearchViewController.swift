//
//  SearchViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

protocol SearchViewControllerDelegate {
    func hideSearchViewController()
    func showSearchViewController()
}

class SearchViewController: SlidingViewController, SearchViewControllerDelegate {
    
    @IBOutlet weak var searchField: UITextField!    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backButton: UIButton!

    var trackingTimer = Timer()
    let kTrackingUpdateInterval:Double = 3.0
    
    var searchResultsVC: SearchTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backButton.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // You might want to check if this is your embed segue here
        // in case there are other segues triggered from this view controller.
        
        segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
        
        if let containedVC = segue.destination as? SearchTableViewController {            
            self.searchResultsVC = containedVC
            self.searchResultsVC?.searchViewDelegate = self
        }
        
    }
    
    func performSearch( withString searchString: String, isCategory: Bool = false ) {
        
        let completionHandler: ( _ data: [POI]?, _ success: Bool, _ error: String? ) -> Void = { ( data, success, error ) in
            
            guard error == nil else {
                print("Error fetching POI data:\(String(describing: error))")
                return
            }
            
            guard data != nil else {
                print("no data fetched")
                return
            }
            
            if ( data!.count > 0 ) {
                
                // temp store the POIs to add
                var foundPOIs = [POI]()
                
                for poi in data! {
                    
                    var isReport:Bool = false
                    
                    // Check for report categories, and if it contains, don't show in search results
                    if let categories = poi.categories?.allObjects as? [POICategory] {
                        for category in categories {
                            if Globals.kCategoriesForReports.contains(category.slug!) {
                                isReport = true
                            }
                        }
                    }
                    if !isReport {
                        foundPOIs.append(poi)
                    }
                    
                }
                
//                self.searchResultsVC?.foundPOIs = foundPOIs
                
                self.searchResultsVC?.foundPOIs = foundPOIs
                
                if !isCategory {
                    self.searchResultsVC?.showingEmptyDataset = false
                    self.searchResultsVC?.selectedFamily = nil
                }
                self.backButton.isHidden = false
                
            }
        }
        
        // Search localized data
        var predicate:NSPredicate
        if Localize.currentLanguage() == "nl" {
            
            if isCategory {
                predicate = NSPredicate(format: "ANY categories.family.name_nl CONTAINS[c] %@", searchString)
            } else {
                let predicateName = NSPredicate(format: "name CONTAINS[c] %@", searchString)
                let predicateText = NSPredicate(format: "text_nl CONTAINS[c] %@", searchString)
                let predicateCategory = NSPredicate(format: "ANY categories.name_nl CONTAINS[c] %@", searchString)
                
                predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateName, predicateText, predicateCategory])
            }
            
        } else {
            
            if isCategory {
                predicate = NSPredicate(format: "ANY categories.family.name_en CONTAINS[c] %@", searchString)
            } else {
                let predicateName = NSPredicate(format: "name CONTAINS[c] %@", searchString)
                let predicateText = NSPredicate(format: "text_en CONTAINS[c] %@", searchString)
                let predicateCategory = NSPredicate(format: "ANY categories.name_en CONTAINS[c] %@", searchString)
                
                predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateName, predicateText, predicateCategory])
                
            }
            
        }
        
//        API.sharedInstance.fetchPOIs( withPredicate: predicate, completion: completionHandler )
        POIDataController.sharedInstance.fetchPOIs( withPredicate: predicate, completion: completionHandler )
        
        trackingTimer.invalidate()
        trackingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(kTrackingUpdateInterval), target: self, selector: #selector(self.trackSearchToAnalytics), userInfo: ["query": searchString], repeats: false)
        
    }
    
    @objc func trackSearchToAnalytics( timer:Timer ) {
        
        if let userInfo = timer.userInfo as? Dictionary<String, AnyObject>,
            let _ = userInfo["query"] as? String {
          
          // Removed GA tracker
            // Track event for placing an anchor
//            guard let tracker = GAI.sharedInstance().defaultTracker else {return}
//            let eventTracker: NSObject = GAIDictionaryBuilder.createEvent(
//                withCategory: "poi",
//                action: "search",
//                label: query,
//                value: nil).build()
//            tracker.send(eventTracker as! [AnyHashable: Any])
            
        }
        
    }
    
    func showSearchViewController() {
        
        if let frame = self.presentationController?.frameOfPresentedViewInContainerView {
            
            UIView.animate(withDuration: 0.45,
                            delay: 0,
                            usingSpringWithDamping: 0.96,
                            initialSpringVelocity: 0.2,
                            options: [],
                            animations: {
                                self.view.frame = frame
                            },
                            completion: { finished in
                                // completed
                            }
            )
            
        }
        
    }
    
    func hideSearchViewController() {
        
        if var frame = self.presentationController?.frameOfPresentedViewInContainerView {
            
            frame.origin.y = frame.size.height
            
            UIView.animate(withDuration: 0.45,
                            delay: 0,
                            usingSpringWithDamping: 0.96,
                            initialSpringVelocity: 0.2,
                            options: [],
                            animations: {
                                self.view.frame = frame
                            },
                            completion: { finished in
                            }
            )
        }
        
    }
    
    @IBAction func handleBackTap(_ sender: Any) {
        self.searchResultsVC?.foundPOIs = [POI]()
        self.searchResultsVC?.showingEmptyDataset = true
        self.searchResultsVC?.tableView.reloadData()
        self.backButton.isHidden = true        
    }
    
    @IBAction func searchValueChanged(_ sender: Any) {
        
        guard let value = searchField.text else {
            self.searchResultsVC?.showingEmptyDataset = true
            self.searchResultsVC?.tableView.reloadData()
            self.backButton.isHidden = true
            return
        }
        
        if value != "" {
            performSearch( withString: value, isCategory: false )
        } else {
            
            self.searchResultsVC?.foundPOIs = [POI]()
            self.searchResultsVC?.showingEmptyDataset = true
            self.searchResultsVC?.tableView.reloadData()
            self.backButton.isHidden = true
        }
        
    }

}
