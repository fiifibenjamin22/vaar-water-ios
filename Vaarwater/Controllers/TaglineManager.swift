//
//  TaglineManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 10/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift

struct TaglineManager {
  
  var beaufort: Int?
  
  var temp: Int?
  
  var rain: Int?
  
  var date: Date = Date()
  
  var isWeekend: Bool {
    return Calendar.current.dateComponents([.weekday], from: date).weekday ?? 0 > 5
  }
  
  var tagline: String {
    return getTagline()
  }
  
  init(beaufort: Int?, temp: Int?, rain: Int?) {
    self.beaufort = beaufort
    self.temp = temp
    self.rain = rain
  }
  
 func getTagline() -> String {
    
    // Event active
    
    if let activeEvent = EventManager.event {
      let eventName = (Localize.currentLanguage() == "nl" ? activeEvent.name_nl : activeEvent.name_en)
      return "\(getRandomTagline(from: "tagline.event".localized()))\(eventName)"
    }
    // April
    if Calendar.current.dateComponents([.month], from: date).month ?? 0 == 4 {
      return getRandomTagline(from: "tagline.april".localized())
    }
    
    // Wind
    if let beaufort = beaufort, beaufort > 5 {
      return getRandomTagline(from: "tagline.windy".localized())
    }
    
    // Boatname empty
    
//    if getBoatName().isEmpty {
//      return getRandomTagline(from: "tagline.noboatname".localized())
//    }
  
    // Temperature > 22
    if let temp = temp {
      if temp > 22 {
        return isWeekend ? getRandomTagline(from: "tagline.warmweekendday".localized()) : getRandomTagline(from: "tagline.warmweekday".localized())
      }
    }
    
    // Rain
    if let rain = rain, rain > 0 {
      return getRandomTagline(from: "tagline.rain".localized())
    }
    
    // Default
    return getRandomTagline(from: "tagline.default".localized())
  }
  
  func getBoatName() -> String {
    if let storedBoatDataDict = UserDefaults.standard.object(forKey: Globals.kSettingsBoatDataKey) as? Dictionary<String, AnyObject> {
      return BoatData(fromDict: storedBoatDataDict).name
    } else {
      return ""
    }
  }
  
  func getRandomTagline(from taglineData: String) -> String {
    let taglines = taglineData.split(separator: "|")
    if taglines.count > 0 {
      let randomIndex = Int(arc4random_uniform(UInt32(taglines.count)))
      return String(taglines[randomIndex])
    }
    return ""
  }
}
