//
//  StackController.swift
//
//  Created by Blaine Fahey on 3/20/16.
//  Copyright © 2016 Blaine Fahey. All rights reserved.
//
//  Before you change anything, read this: https://developer.apple.com/library/ios/technotes/tn2154/_index.html

import UIKit

/// A vertically scrolling container view controller.
@objc public class StackController: UIViewController {
    
    /// The scroll view containing all the child views.
    public let scrollView = UIScrollView()
    
    /// The child view controllers to be stacked vertically.
    public var viewControllers = [UIViewController]() {
        willSet {
            viewControllers.forEach { removeViewController(viewController: $0) }
            heightConstraints.removeAll()
            didUpdateConstraints = false
        }
        didSet {
            viewControllers.forEach { addViewController(viewController: $0) }
            view.setNeedsUpdateConstraints()
        }
    }
    
    /// The layout animation duration used when a child view controller's `preferredContentSize` is changed.
    public var layoutAnimationDuration: TimeInterval = 0.2
    
    /// The minimum vertical spacing added between each child view.
    @IBInspectable public var minimumSpacing: Float = 0.0
    
    private var heightConstraints = [NSLayoutConstraint]()
    
    private var didUpdateConstraints = false
    
    // MARK: - View lifecycle
    
    public override func loadView() {
        super.loadView()
        
        scrollView.alwaysBounceVertical = true
        scrollView.isDirectionalLockEnabled = true
        scrollView.showsVerticalScrollIndicator = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
    }
    
    public override func updateViewConstraints() {
        if didUpdateConstraints == false {
            
            if scrollView.constraints.isEmpty == true {
                if #available(iOS 9.0, *) {
                    view.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
                    view.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
                    view.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
                    view.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
                    
                } else {
                    let views = ["scrollView": scrollView]
                    
                    view.addConstraints(
                        NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollView]|",
                                                                       options: NSLayoutConstraint.FormatOptions(),
                                                                       metrics: nil,
                                                                       views: views))
                    
                    view.addConstraints(
                        NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollView]|",
                                                                       options: NSLayoutConstraint.FormatOptions(),
                                                                       metrics: nil,
                                                                       views: views))
                }
            }
            
            addChildViewConstraints()
            
            didUpdateConstraints = true
        }
        super.updateViewConstraints()
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    // MARK: - Private
    
    private func addChildViewConstraints() {
        var previousView: UIView?
        
        heightConstraints.removeAll()
        
        viewControllers.forEach { controller in
            
            guard let childView = controller.view else {
                print("No childview")
                return
            }
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[view(==child)]",
                                                               options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["child": childView, "view": view!]))
            
            let heightConstraint = NSLayoutConstraint(item: childView, attribute: .height, relatedBy: .equal,
                                                      toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: childView.frame.height)
            
            childView.addConstraint(heightConstraint)
            
            heightConstraints.append(heightConstraint)
            
            if let previousView = previousView {
                // Pin to previous view.
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[previous]-spacing-[child]",
                                                                                         options: NSLayoutConstraint.FormatOptions(),
                                                                                         metrics: ["spacing": NSNumber(value: minimumSpacing)],
                                                                                         views: ["child": childView, "previous": previousView]))
                
            } else {
                // Pin first view to top.
                scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[child]",
                                                                                         options: NSLayoutConstraint.FormatOptions(),
                                                                                         metrics: nil,
                                                                                         views: ["child": childView]))
            }
            
            previousView = childView
        }
        
        if let previousView = previousView {
            // Finally, pin to the bottom of the scroll view to determine content size.
            scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[previous]|",
                                                                                     options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["previous": previousView]))
        }
    }
    
    private func addViewController(viewController: UIViewController) {
        addChild(viewController)
        
        if let childView = viewController.view {
            childView.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(childView)
        }
        viewController.didMove(toParent: self)
    }
    
    private func removeViewController(viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        
        viewController.view.removeFromSuperview()
        
        viewController.removeFromParent()
    }
    
    // MARK: - UIContentContainer
    
    public override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        super.size(forChildContentContainer: container, withParentContainerSize: parentSize)
        
        return CGSize(width: parentSize.width, height: container.preferredContentSize.height)
    }
    
    public override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)
        
        if let controller = container as? UIViewController, let i = viewControllers.firstIndex(of: controller) {
            guard i < heightConstraints.count else { return }
            
            let heightConstraint = heightConstraints[i]
            
            // Use the controller's `preferredContentSize` height value to set as our constant.
            heightConstraint.constant = container.preferredContentSize.height
            
            view.setNeedsUpdateConstraints()
            
            UIView.animate(withDuration: layoutAnimationDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
}
