//
//  RouteDetailViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class RouteDetailViewController: UIViewController {
  
  var route:Route?
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var routeDataView: RouteDataView!
  @IBOutlet weak var mapView: VWMapView!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var startButton: UIButton!
  @IBOutlet weak var backButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.textView?.textContainer.lineFragmentPadding = 0
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    fillContentWithRouteData()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    trackScreenView(prefix: "Route: ")
    super.viewDidAppear(animated)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: Custom methods
  
  func fillContentWithRouteData() {
    
    guard let route = self.route else {
      return
    }
    
    // Set name and text
    var textString:String = ""
    if Localize.currentLanguage() == "nl" {
      self.nameLabel?.text = route.name_nl
      textString = route.text_nl ?? route.text ?? ""
    } else {
      self.nameLabel?.text = route.name_en
      textString = route.text_en ?? route.text ?? ""
    }
    
    // Set text
    do {
      let htmlString = try NSMutableAttributedString(data: textString.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: convertToNSAttributedStringDocumentReadingOptionKeyDictionary([convertFromNSAttributedStringDocumentAttributeKey(NSAttributedString.DocumentAttributeKey.documentType) : convertFromNSAttributedStringDocumentType(NSAttributedString.DocumentType.html)]), documentAttributes: nil)
      
      if htmlString.length > 0 {
        let attributes = convertFromNSAttributedStringKeyDictionary(self.textView.attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: self.textView.attributedText.length - 1)))
        
        htmlString.addAttributes(convertToNSAttributedStringKeyDictionary(attributes), range: NSMakeRange(0, htmlString.length - 1))
        self.textView?.attributedText = htmlString.trimEmptyTagsAndWhiteSpace()
      }
      
    } catch {
      print(error)
    }
    
    // Set duration/distance
    if let duration = route.duration as? Double {
      self.routeDataView.duration = duration
    }
    
    if let distance = route.distance as? Double {
      self.routeDataView.distance = distance
    }
    
    // Hide your own position
    self.mapView.isMyLocationEnabled = false
    
    // Add route to map
    self.mapView.addRouteToMap(route: route)
    
    // Zoom to fit route
    self.mapView.zoomToActiveRoute()
    
    if let activeRoute = RouteManager.sharedInstance.currentActiveRoute,
      self.route == activeRoute {
      self.startButton.setTitle( "route.startButton.stop".localized(), for: .normal)
      self.startButton.setTitle( "route.startButton.stop".localized(), for: .highlighted)
    } else {
      self.startButton.setTitle( "route.startButton.start".localized(), for: .normal)
      self.startButton.setTitle( "route.startButton.start".localized(), for: .highlighted)
    }
    
  }
  
  
  // MARK: IB Actions
  @IBAction func handleBackTap(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
  
  @IBAction func handleCloseTap(_ sender: Any) {
    
    print("handling close tap")
    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
    NotificationCenter.default.post(name: name, object: nil)
    
    navigationController?.popViewController(animated: true)
    
    //        self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func handleStartTap(_ sender: Any) {
    
    // Set current active route
    if let route = RouteManager.sharedInstance.currentActiveRoute,
      route == self.route {
      
      // Reset current active route if it's the currently active one
      RouteManager.sharedInstance.currentActiveRoute = nil
    } else {
      // Else set the (new) route as currently active
      RouteManager.sharedInstance.currentActiveRoute = route
    }
    
    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
    NotificationCenter.default.post(name: name, object: nil)
    
    navigationController?.popViewController(animated: true)
    
    
    //        self.dismiss(animated: true, completion: nil)
    
  }
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringDocumentReadingOptionKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.DocumentReadingOptionKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.DocumentReadingOptionKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentAttributeKey(_ input: NSAttributedString.DocumentAttributeKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentType(_ input: NSAttributedString.DocumentType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKeyDictionary(_ input: [NSAttributedString.Key: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
