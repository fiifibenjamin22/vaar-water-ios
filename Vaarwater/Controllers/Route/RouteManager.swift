//
//  LocationManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 18/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class RouteManager: NSObject {
    
    var buoyPOI:POI?
    var nearestLocation:CLLocation?
    
    static let sharedInstance:RouteManager = {
        
        let instance = RouteManager()
        instance.buoyPOI = instance.createBuoyPOI()
        return instance
        
    }()
    
    var currentActiveRoute:Route? {
        didSet {
            NotificationCenter.default.post(name:
                NSNotification.Name(rawValue: Globals.kActiveRouteChange), object: nil)
        }
    }
    
    public func createBuoyPOI() -> POI? {
        
        // Place anchor stuff
        let buoyDict:Dictionary<String, AnyObject> = [
            "id": 999998 as AnyObject,
            "name": "Buoy" as AnyObject,
            "slug": "buoy" as AnyObject,
            "text": "" as AnyObject,
            "html": "" as AnyObject,
            "views": 0 as AnyObject,
            "location": [
                "latitude": 0,
                "longitude": 0
                ] as AnyObject,
            "categories": [
                [
                    "id": 999998 as AnyObject,
                    "slug": "buoy" as AnyObject,
                    "name_nl": "Boei" as AnyObject,
                    "name_en": "Buoy" as AnyObject,
                    "map_default": 0 as AnyObject
                ]
                ] as AnyObject
        ]
        
        // Create poi without context (without insert)
        var poi:POI?
        do {
            let taskContext = try CoreDataStackManager.sharedInstance.newPrivateQueueContextWithNewPSC()
            taskContext.performAndWait() {                
                do {
                    poi = try POI.createPOIEntity(withDictionary: buoyDict, inContext: taskContext)
                }
                catch {
                    print("Couldn't create buoy POI")
                }
            }
        }
        catch {
            print("Couldn't create taskContext for creating a buoy")
        }
        
        return poi
        
    }
    
    public func calculateClosestLocationToRoute() {
        
        guard let route = currentActiveRoute else {
            print("No currently active route found.")
            return
        }
        
        guard let location = LocationManager.sharedInstance.currentLocation else {
            print("No current location found")
            return
        }
        
        // check if poi has positions attached then, and if not, just return
        guard var positions = route.positions?.allObjects as? [GeoPosition] else {
            print("Report has no positions")
            return
        }
        
        // Sort positions by id
        positions = positions.sorted( by: { $0.id < $1.id })
        
        let path = GMSMutablePath()
        for (_, position) in positions.enumerated() {
            path.add(CLLocationCoordinate2D(latitude: position.latitude, longitude: position.longitude))
        }
        let outlinePolyline = GMSPolyline(path: path)
        let nearestCoordinates = nearestCoordinateForRoute(forCoordinate: location.coordinate, onPolyline: outlinePolyline)
        
        self.nearestLocation = CLLocation(latitude: nearestCoordinates.latitude, longitude: nearestCoordinates.longitude)
        
        
//        var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
//        if let firstPosition = positions.first {
//            coordinate.latitude = firstPosition.latitude
//            coordinate.longitude = firstPosition.longitude
//        }
        
        buoyPOI?.updateLocation(forCoordinate: nearestCoordinates)
        
    }
    
    public func nearestCoordinateForRoute( forCoordinate coordinate: CLLocationCoordinate2D, onPolyline polyline: GMSPolyline ) -> CLLocationCoordinate2D {
        
        var bestDistance:CGFloat = CGFloat(Float.greatestFiniteMagnitude)
        var bestPointData:(point: CGPoint, distance: CGFloat)?
        let originPoint:CGPoint = CGPoint(x: coordinate.longitude, y: coordinate.latitude)
        
        let pathCount = polyline.path?.count() ?? 0
        if pathCount < 2 {
            return kCLLocationCoordinate2DInvalid
        }
        
        for index in 0...pathCount {
            
            if let startCoordinate = polyline.path?.coordinate(at: index),
                let endCoordinate = polyline.path?.coordinate(at: index + 1) {
                
                let startPoint = CGPoint(x: startCoordinate.longitude, y: startCoordinate.latitude)
                let endPoint = CGPoint(x: endCoordinate.longitude, y: endCoordinate.latitude)
                
                let nearestPointData = self.nearestPointToPoint(origin: originPoint, onLineSegmentPointA: startPoint, pointB: endPoint)
                let distance = nearestPointData.distance
                
                if (distance < bestDistance) {
                    bestDistance = distance
                    bestPointData = nearestPointData
                }
                
            }
            
        }
        
        if bestPointData != nil {
            return CLLocationCoordinate2DMake(Double(bestPointData!.point.y), Double(bestPointData!.point.x))
        } else {
            return kCLLocationCoordinate2DInvalid
        }
        
    }
    
    public func nearestPointToPoint( origin: CGPoint, onLineSegmentPointA pointA: CGPoint, pointB: CGPoint ) -> (point: CGPoint, distance: CGFloat) {
        
        let dAP = CGPoint(x: origin.x - pointA.x, y: origin.y - pointA.y)
        let dAB = CGPoint(x: pointB.x - pointA.x, y: pointB.y - pointA.y)
        let dot = dAP.x * dAB.x + dAP.y * dAB.y
        let squareLength = dAB.x * dAB.x + dAB.y * dAB.y
        
        let param = dot / squareLength
        
        var nearestPoint:CGPoint = CGPoint(x: 0, y: 0)
        if (param < 0 || (pointA.x == pointB.x && pointA.y == pointB.y)) {
            nearestPoint.x = pointA.x
            nearestPoint.y = pointA.y
        } else if (param > 1) {
            nearestPoint.x = pointB.x
            nearestPoint.y = pointB.y
        } else {
            nearestPoint.x = pointA.x + param * dAB.x
            nearestPoint.y = pointA.y + param * dAB.y
        }
        
        let dx = origin.x - nearestPoint.x
        let dy = origin.y - nearestPoint.y
        
        let distance:CGFloat = sqrt(dx * dx + dy * dy)
        
        return (point: nearestPoint, distance: distance)
        
        
    }
    
        
}

