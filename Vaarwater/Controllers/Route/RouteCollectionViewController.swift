//
//  RouteCollectionViewController.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

private let reuseIdentifier = "routeCell"

class RouteCollectionViewController: UICollectionViewController {
    
    let fetchedResultsController = RouteDataController.sharedInstance.fetchedResultsController
    var selectedRouteIndexPath:IndexPath?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.delegate = self
                
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        
        // Perform fetch
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            let fetchError = error as NSError
            print("Unable to Perform Fetch Request")
            print("\(fetchError), \(fetchError.localizedDescription)")
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        trackScreenView()
        super.viewDidAppear(animated)
        
        if #available(iOS 13.0, *) {
                   let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                   let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                   statusBar.backgroundColor = .red
                   window?.addSubview(statusBar)
            } else {
        //           UIApplication.shared.statusBarView?.backgroundColor = .white
        //           UIApplication.shared.statusBarStyle = .lightContent
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let routes = fetchedResultsController.fetchedObjects else { return 0 }
        return routes.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RouteCell

        // Configure the cell
        let route = fetchedResultsController.object(at: indexPath)
        cell.route = route
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let controller = segue.destination as? RouteDetailViewController,
            let indexPaths = self.collectionView?.indexPathsForSelectedItems {
            controller.route = self.fetchedResultsController.object(at: indexPaths[0])
        }
    }
    
    // MARK: IB Actions
    
    @IBAction func handleBackTap(_ sender: Any) {
        // Default dismiss
        self.dismiss( animated: true, completion: nil)
    }
    
    @IBAction func handleCloseTap(_ sender: Any) {
        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)
    }
    
}

extension RouteCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 1.1
        var widthRemainingForCellContent = collectionView.bounds.width
        var heightRemainingForCellContent = collectionView.bounds.height
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            let borderSize: CGFloat = flowLayout.sectionInset.left + flowLayout.sectionInset.right
            let verticalBorderSize: CGFloat = flowLayout.sectionInset.top + flowLayout.sectionInset.bottom
            widthRemainingForCellContent -= borderSize + ((cellsAcross - 1) * flowLayout.minimumInteritemSpacing)
            heightRemainingForCellContent -= verticalBorderSize
            
            print("vertical border size:", verticalBorderSize)
            
        }
//        let cellWidth = round(widthRemainingForCellContent / cellsAcross)
//        let cellHeight = cellWidth * 1.9
//        let cellHeight:CGFloat = self.view.bounds.height * 0.8
        
        let cellWidth:CGFloat = 300
        let cellHeight:CGFloat = 600
        
        print("collectionView bounds", collectionView.bounds )
        print("cellHeight", cellHeight)
        
        return CGSize(width: cellWidth, height: cellHeight)
        
    }

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
