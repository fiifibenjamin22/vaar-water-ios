//
//  HelpViewAnimator.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 14/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class HelpViewAnimator: NSObject {
    
    let isPresentation: Bool
    var isModalDismiss: Bool = false
    
    init(isPresentation: Bool, isModalDismiss: Bool? = false) {
        self.isPresentation = isPresentation
        if let dismiss = isModalDismiss {
            self.isModalDismiss = dismiss
        }
        super.init()
    }
    
}

// MARK: UIViewControllerAnimatedTransitioning delegate methods

extension HelpViewAnimator: UIViewControllerAnimatedTransitioning {
    
    // Get/set duration for the transition
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.45
    }
    
    // Animate the transition
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
            else {
                return
        }
        
        let key = isPresentation ? UITransitionContextViewControllerKey.to : UITransitionContextViewControllerKey.from
        let controller = transitionContext.viewController(forKey: key)!
        
        if isPresentation {
            transitionContext.containerView.addSubview(controller.view)
        }
        
        let presentedFrame = transitionContext.finalFrame(for: controller)
        var dismissedFrame = presentedFrame
        
        if isModalDismiss {
            dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
        } else {
            dismissedFrame.origin.x = transitionContext.containerView.frame.size.width
        }
        //        dismissedFrame.origin.y = transitionContext.containerView.frame.size.height
        
        let initialFrame = isPresentation ? dismissedFrame : presentedFrame
        let finalFrame = isPresentation ? presentedFrame : dismissedFrame
        
        let animationDuration = transitionDuration(using: transitionContext)
        controller.view.frame = initialFrame
        
        UIView.animate(withDuration: animationDuration,
                        delay: 0,
                        usingSpringWithDamping: 0.96,
                        initialSpringVelocity: 0.2,
                        options: [],
                        animations: {
                            controller.view.frame = finalFrame
                        },
                        completion: { finished in
                            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                        }
        )
        
//        UIView.animate(withDuration: animationDuration, animations: {
//            controller.view.frame = finalFrame
//        }) { finished in
//            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//        }
        
    }
    
}
