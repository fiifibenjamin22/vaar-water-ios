//
//  RxAlamofire+Multipart.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 17/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

extension Reactive where Base: SessionManager {
  func encodeMultipartUpload(to url: URLConvertible, method: HTTPMethod = .post, headers: HTTPHeaders = [:], data: @escaping (MultipartFormData) -> Void) -> Observable<UploadRequest> {
    return Observable.create { observer in
      self.base.upload(multipartFormData: data,
                       to: url,
                       method: method,
                       headers: headers,
                       encodingCompletion: { (result: SessionManager.MultipartFormDataEncodingResult) in
                        switch result {
                        case .failure(let error):
                          observer.onError(error)
                        case .success(let request, _, _):
                          observer.onNext(request)
                          observer.onCompleted()
                        }
      })
      
      return Disposables.create()
    }
  }
}
