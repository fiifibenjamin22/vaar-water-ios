//
//  PersistencyManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 07/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

class PersistencyManager: NSObject {

//    private var POIs = [POI]()
    
    private var mainContext:NSManagedObjectContext
    
    static let sharedInstance: PersistencyManager = {
        let instance = PersistencyManager()
        return instance
    }()    
    
    override init() {
        self.mainContext = CoreDataStackManager.sharedInstance.mainQueueContext
    }
    
    func getMainContextInstance() -> NSManagedObjectContext {
        return self.mainContext
    }
    
    /**
     Save the current work/changes done on the worker contexts (the minion workers).
     
     - Parameter workerContext: NSManagedObjectContext The Minion worker Context that has to be saved.
     - Returns: Void
     */
    func saveWorkerContext(workerContext: NSManagedObjectContext){
        //Persist new Event to datastore (via Managed Object Context Layer).
        do {
            try workerContext.save()
        } catch let saveError as NSError {
            print("save minion worker error: \(saveError.localizedDescription)")
        }
    }
    
    /**
     Save and merge the current work/changes done on the minion workers with Main context.
     
     - Returns: Void
     */
    func mergeWithMainContext(){
        do {
            try self.mainContext.save()
        } catch let saveError as NSError {
            print("synWithMainContext error: \(saveError.localizedDescription)")
        }
    }
        
}
