//
//  Waterinfo.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 18/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import Alamofire

class Waterinfo {
    static let service = Waterinfo()
    
    struct Detail: Codable {
        var latest: Temp
    }
    
    struct Temp: Codable {
        var data: Double
        
        var valueText: String {
            return String(format: "%.1f", locale: Locale(identifier: "nl-NL"), data)
        }
    }
    
    private enum API {
        private static let basePath = "https://waterinfo.rws.nl/api/details/detail"
        
        case temp
        
        func fetch(completion: @escaping (Data) -> ()) {
            Alamofire.request(path(), parameters: parameters())
                .responseJSON { (response) -> () in
                    guard response.result.isSuccess, let data = response.data else {
                        print("Error: \nNo data found in API call")
                        return
                    }
                    completion(data)
                    
            }
        }
        
        private func path() -> String {
            return API.basePath
        }
        
        private func parameters() -> Dictionary<String, AnyObject> {
            return [
                "expertParameter": "Temperatuur___20Oppervlaktewater___20oC" as AnyObject,
                "locationSlug": 1614 as AnyObject
            ]
        }
    }
    
    func getTemp(completion: @escaping (Temp) -> ()) {
        API.temp.fetch { data in
            do {
                let decoder = JSONDecoder()
                let temp = try decoder.decode(Detail.self, from: data).latest
                DispatchQueue.main.async {
                    completion(temp)
                }
            } catch {
                print("Error: \nCould not find temp")
            }
        }
    }
}
