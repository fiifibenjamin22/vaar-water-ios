//
//  API+Weather.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 21/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

extension API {
    
    func getWeatherStatus( forLocation location: CLLocation?, completion : ((_ data: String?, _ success : Bool, _ error : String?) -> Void)?) {
        
        let url: String = "http://gpsgadget.buienradar.nl/data/raintext"
//        let credential : URLCredential = URLCredential(user: "ulpreview", password: "demopreview", persistence: URLCredential.Persistence.forSession)
        
        guard let location = location else {
            print("Something wrong with the location")
            return
        }
        
        let parameters : Dictionary<String,AnyObject> =  [
            "lat": location.coordinate.latitude as AnyObject,
            "lon": location.coordinate.longitude as AnyObject
        ]
        
        if self.logging {
            print("Weather url: ", url)
        }
        
        Alamofire.request( url, method: .get, parameters: parameters )
//            .authenticate(usingCredential: credential )
            .response { response in
                                
                // If we don't get data back, return
                guard response.data != nil else {
                    print("Error: \nNo data found in Weather API call.")
                    completion?(nil, false, "Server fout")
                    return
                }
                
                if self.logging {
                    print(response.request ?? "")
                    print(response.response ?? "")
                    print(response.data ?? "")
                }
                
                if let data = response.data,
                    let encodedText = String(data: data, encoding: .utf8) {
                    completion?(encodedText, true, nil)
                } else {
                    completion?(nil, false, "Invalid Data")
                }
                
        }
    
    }
    
}
