//
//  API.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 31/01/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let APIV2ExpiredSessionNotification : String = "APIV2ExpiredSessionNotification"

enum ApiType : Int {
    case acceptation = 1, production, development
    
    func getHost() -> String
    {
        switch self {
        case .acceptation :
            return "https://vaarwater.portpay.nl/api/v3/" //"http://grachten-acc.waternet.preview.uselab.com/api/v3/"
        case .production :
            return "https://vaarwater.portpay.nl/api/v3/"
                //"https://grachten.waternet.nl/api/v3/"
        case .development :
            return "https://vaarwater.portpay.nl/api/v3/"
                //"http://grachten.waternet.nl.preview.uselab.com/api/v3/"
        }
    }
  
  func getNewHost() -> String
  {
    switch self {
    case .acceptation :
      return "https://vaarwater.portpay.nl/api/v2/"
            //"http://grachten-acc.waternet.preview.uselab.com/api/v2/"
    case .production :
      return "https://vaarwater.portpay.nl/api/v3/"
            //"http://grachten.waternet.nl/api/v3/"
    case .development :
      return "https://vaarwater.portpay.nl/api/v3/"
            //"http://grachten.waternet.nl.preview.uselab.com/api/v3/"
      
    }
  }
    
    func getUrlHost() -> String
    {
        switch self {
        case .acceptation :
            return "https://vaarwater.portpay.nl"
            //"grachten-acc.waternet.preview.uselab.com"
        case .production :
            return "https://vaarwater.portpay.nl"
            //"grachten.waternet.nl"
        case .development :
            return "https://vaarwater.portpay.nl"
            //"grachten-acc.waternet.preview.uselab.com"
            
        }
    }
    
    func getUrlPath() -> String
    {
        switch self {
        case .acceptation :
            return "api/v2"
        case .production :
            return "api/v2"
        case .development:
            return "api/v2"
        }
    }
    
    func getJsonFileName() -> String
    {
        switch self {
        case .acceptation :
            return "pois"
        case .production :
            return "pois-prod"
        case .development:
            return "pois"
        }
    }
    
    func getTypesFileName() -> String
    {
        switch self {
        case .acceptation :
            return "types"
        case .production :
            return "types-prod"
        case .development:
            return "types"
            
        }
    }
    
}

class API:NSObject {
    
    static let sharedInstance:API = {
        
        let instance = API()
        let defaults : UserDefaults = UserDefaults.standard
        
        if let token : String = defaults.object(forKey: "X-Api-Token") as? String {
            instance.apiToken = token
            print("token : \(token)")
        }
        
        instance.host = instance.type.getHost()
      instance.newhost = instance.type.getNewHost()
        instance.urlHost = instance.type.getUrlHost()
        instance.urlPath = instance.type.getUrlPath()
        
        return instance
    }()

    // MARK: Local variables
    var type = ApiType.production
    var host = ""
    var newhost = ""
    
    var urlHost = ""
    var urlPath = ""
    let logging:Bool = true
    
    var apiToken : String? {
        willSet {
            let defaults : UserDefaults = UserDefaults.standard
            defaults.set(newValue, forKey: "X-Api-Token")
            defaults.synchronize()
        }
    }
    
    // MARK: Init
    
    override init() {
        super.init()        
    }
                    
    func expiredSession() {
        
//        let mainContext = CoreDataStackManager.sharedInstance.mainQueueContext
            
//        NotificationCenter.default.post(name: Notification.Name(rawValue: UserWillChangeNotification), object: nil)
//        NotificationCenter.default.post(name: Notification.Name(rawValue: MessagesWillChangeNotification), object: nil)
//        
//        self.deleteUserRoutes(context: mainContext!)
//        self.deleteLocalUser(context: mainContext!)
//        self.deleteLocalBoat(context: mainContext!)
//        self.deleteAllMessages(context: mainContext!)
//        self.deleteLogBookItems(context: mainContext!)
//        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: UserDidChangeNotification), object: nil)
//        NotificationCenter.default.post(name: Notification.Name(rawValue: MessagesDidChangeNotification), object: nil)
        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: APIV2ExpiredSessionNotification), object: nil)
        
    }
    
    // Post update notification to let the registered listeners refresh it's datasource.
    final public func postUpdateNotification() {
        let name = NSNotification.Name(Globals.kPoiUpdatedNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    
}
