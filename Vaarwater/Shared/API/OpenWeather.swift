//
//  DashboardManager.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 17/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import Alamofire
import Localize_Swift

class OpenWeather {
    
    static let service = OpenWeather()
    
    struct UV: Codable {
        var value: Double
        
        var valueText: String {
            return String(format: "%.0f", value)
        }
        
        var valueDescription: UVDescription {
            return UVDescription(value)
        }

        enum UVDescription {
            case low
            case moderate
            case high
            case very_high
            case extreme

            private var range: Range<Double> {
                switch self {
                case .low:
                    return -Double.greatestFiniteMagnitude..<3
                case .moderate:
                    return 3.0..<6.0
                case .high:
                    return 6.0..<8.0
                case .very_high:
                    return 8.0..<11.0
                case .extreme:
                    return 11.0..<Double.greatestFiniteMagnitude
                }
            }

            init(_ value: Double) {
                switch value {
                case UVDescription.low.range:
                    self = .low
                case UVDescription.moderate.range:
                    self = .moderate
                case UVDescription.high.range:
                    self = .high
                case UVDescription.very_high.range:
                    self = .very_high
                case UVDescription.extreme.range:
                    self = .extreme
                default:
                    fatalError()
                }
            }
        }
    }
    
    struct Weather: Codable {
        var wind: Wind
    }
    
    struct Wind: Codable {
        var speed: Double
        var deg: Double
        
        var valueText: String {
            return String(format: "%.0f", speed)
        }
        var valueDescription: WindDescription {
            return WindDescription(deg)
        }
        
        enum WindDescription {
            case north
            case north_east
            case east
            case south_east
            case south
            case south_west
            case west
            case north_west
            
            private var range: CountableClosedRange<Int> {
                switch self {
                case .north_east:
                    return 23...67
                case .east:
                    return 68...112
                case .south_east:
                    return 112...157
                case .south:
                    return 158...202
                case .south_west:
                    return 203...247
                case .west:
                    return 248...292
                case .north_west:
                    return 293...337
                case .north:
                    return 0...22
                }
            }
            
            init(_ value: Double) {
                switch Int(value) % 360 {
                case WindDescription.north_east.range:
                    self = .north_east
                case WindDescription.east.range:
                    self = .east
                case WindDescription.south_east.range:
                    self = .south_east
                case WindDescription.south_west.range:
                    self = .south_west
                case WindDescription.west.range:
                    self = .west
                case WindDescription.north_west.range:
                    self = .north_west
                default:
                    self = .north
                }
            }
        }
    }
    
    
    private enum API {
        private static let basePath = "https://api.openweathermap.org/data/2.5/"
        private static let appid = "7f6113193d7c9e26dfd28135fff73417"
        
        case uv(CLLocation)
        case wind(CLLocation)
        
        func fetch(completion: @escaping (Data) -> ()) {
            Alamofire.request(path(), parameters: parameters())
                .responseJSON { (response) -> () in
                    guard response.result.isSuccess, let data = response.data else {
                        print("Error: \nNo data found in API call")
                        return
                    }
                    completion(data)
            }
        }
        
        private func path() -> String {
            switch self {
            case .uv:
                return API.basePath + "uvi"
            case .wind:
                return API.basePath + "weather"
            }
        }
        
        private func parameters() -> Dictionary<String, AnyObject> {
            switch self {
            case .uv(let location), .wind(let location):
                return [
                    "lat": location.coordinate.latitude as AnyObject,
                    "lon": location.coordinate.longitude as AnyObject,
                    "appid": "7f6113193d7c9e26dfd28135fff73417" as AnyObject
                ]
            }
        }
        
    }
    
    func getUV(for location: CLLocation, completion: @escaping (UV) -> ()) {
        API.uv(location).fetch { data in
            do {
                let decoder = JSONDecoder()
                let uv = try decoder.decode(UV.self, from: data)
                
                DispatchQueue.main.async {
                    completion(uv)
                }
            } catch {
                
            }
        }
    }
    
    
    func getWind(for location: CLLocation, completion: @escaping (Wind) -> Void) {
        API.wind(location).fetch { data in
            do {
                let decoder = JSONDecoder()
                let wind = try decoder.decode(Weather.self, from: data).wind
                
                DispatchQueue.main.async {
                    completion(wind)
                }
            } catch {
                
            }
        }
    }
}
