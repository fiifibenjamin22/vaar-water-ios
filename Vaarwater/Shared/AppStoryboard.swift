//
//  AppStoryboard.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 05/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

enum AppStoryboard: String {
    
    case Main
    
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
    
    
}

extension UIViewController {
    
    class var storyboardID: String {
        return "\(self)"
    }
    
    static func instantiateFromStoryboard( appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController( viewControllerClass: self )
    }
    
}
