//
//  UITextField+Extensions.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 16/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

extension UITextField {
  @IBInspectable var placeholderColor: UIColor {
    get {
      guard let currentAttributedPlaceholderColor = attributedPlaceholder?.attribute(NSAttributedString.Key.foregroundColor, at: 0, effectiveRange: nil) as? UIColor else {
        return UIColor.clear
      }

      return currentAttributedPlaceholderColor
    }

    set {
      guard let currentAttributedString = attributedPlaceholder else {
        return
      }

      let attributes = [convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): newValue]
      attributedPlaceholder = NSAttributedString(string: currentAttributedString.string, attributes: convertToOptionalNSAttributedStringKeyDictionary(attributes))
    }
  }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
