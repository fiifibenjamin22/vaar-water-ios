//
//  GuardDiagnose.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 27/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class GuardDiagnose: NSObject {

    func check(file: String = #file, line: Int = #line) -> Bool {
        print("Testing \(file):\(line)")
        return true
    }
    
}
