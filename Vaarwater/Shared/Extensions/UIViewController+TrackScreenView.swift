//
//  UIViewController+TrackScreenView.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func trackScreenView( prefix:String? = nil ) {
        
        var screenName: String = prefix != nil ? prefix! : ""
        guard let title = self.title else {
            return
        }        
        screenName.append(title)
      
      // Removed GA tracker 
//        guard let tracker = GAI.sharedInstance().defaultTracker else {return}
//        tracker.set(kGAIScreenName, value: screenName)
//        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
//        tracker.send(builder.build() as [NSObject : AnyObject])
      
    }    
    
}
