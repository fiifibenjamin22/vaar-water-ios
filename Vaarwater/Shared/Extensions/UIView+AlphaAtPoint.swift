//
//  ImageView+AplhaAtPoint.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 28/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

extension UIView {
    
    
    func colorOfPoint(point: CGPoint) -> UIColor {
        
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue:CGImageAlphaInfo.premultipliedLast.rawValue)
        if let context = CGContext(data: pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) {

            context.translateBy(x: -point.x, y: -point.y)
            self.layer.render(in: context)
            
//            CGContextRelease(context)
//            CGColorSpaceRelease(colorSpace)
            
            return UIColor(red: CGFloat(pixel[0]) / 255.0, green: CGFloat (pixel[1]) / 255.0, blue: CGFloat (pixel[2]) / 255.0 , alpha: CGFloat (pixel[3]) / 255.0)
            
        } else {
            return UIColor.red
        }
        
    }
    
    func alphaAtPoint(point: CGPoint) -> CGFloat {
        
        var pixel: [UInt8] = [0, 0, 0, 0]
        let colorSpace = CGColorSpaceCreateDeviceRGB();
        let alphaInfo = CGImageAlphaInfo.premultipliedLast.rawValue
        
        guard let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: alphaInfo) else {
            return 0
        }
        
        context.translateBy(x: -point.x, y: -point.y);
        self.layer.render(in: context)
        
        let floatAlpha = CGFloat(pixel[3])
        return floatAlpha
    }
    
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        
//        if !super.point(inside: point, with: event) {
//            return false
//        }
//        
//        UIGraphicsBeginImageContext(bounds.size)
//        layer.render(in: UIGraphicsGetCurrentContext()!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//
//        // ↓ cf. UIImage+ColorAtPixel from https://github.com/ole/OBShapedButton
//        let pointX = trunc(point.x)
//        let pointY = trunc(point.y)
//        let cgImage = image?.cgImage
//        let width = image?.size.width
//        let height = image?.size.height
//        var pixelData: [CUnsignedChar]  = [0, 0, 0, 0]
//        let bytesPerPixel: UInt = 4
//        let bytesPerRow: UInt = bytesPerPixel * 1
//        let bitsPerComponent: Int = 8
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
//        
////        let context = CGBitmapContextCreate(pixelData, 1, 1, bitsPerComponent ,bytesPerRow, colorSpace, bitmapInfo.rawValue)
//        guard let context = CGContext.init(data: &pixelData, width: 1, height: 1, bitsPerComponent: bitsPerComponent, bytesPerRow: Int(bytesPerRow), space: colorSpace, bitmapInfo: bitmapInfo.rawValue) else {
//            // print
//            return false
//        }
//        context.setBlendMode(.copy)
//        context.transl
//
//        CGContextTranslateCTM(context, -pointX, pointY - height)
//        CGContextDrawImage(context, CGRectMake(0.0, 0.0, width, height), cgImage)
//
//        //        let r = CGFloat(pixelData[0]) / 255.0
//        //        let g = CGFloat(pixelData[1]) / 255.0
//        //        let b = CGFloat(pixelData[2]) / 255.0
//        let a = CGFloat(pixelData[3]) / 255.0
//        
//        return a != 0.0
//    }
    
    
}
