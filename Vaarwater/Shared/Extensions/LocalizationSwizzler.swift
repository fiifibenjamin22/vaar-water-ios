//
//  LocalizationSwizzler.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 12/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

class LocalizationSwizzler: NSObject {

    class func executeSwizzle() {
        
        swizzleClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedString(forKey:value:table:)) )
    }
}

extension Bundle {
    
    @objc func specialLocalizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        
        let currentLanguage = Localize.currentLanguage()
        
        var bundle = Bundle()
        
        if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            bundle = Bundle(path: _path)!
        } else {
            let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
            bundle = Bundle(path: _path)!
        }
        return (bundle.specialLocalizedString(forKey: key, value: value, table: tableName))
    }
}

/// Exchange the implementation of two methods for the same Class
func swizzleClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
    
  let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
  let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector )!;
    if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
        class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    } else {
        method_exchangeImplementations(origMethod, overrideMethod);
    }
}
