//
//  NSString+Extensions.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 03/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import Foundation
import UIKit

extension NSString {
    
    func trimEmptyTagsAndWhiteSpace() -> String {
        let strippedString = self.replacingOccurrences(of: "(?i)<p\\b[^<]*>\\s*</p>", with: "", options: .regularExpression, range: range(of: self as String))
        return strippedString
    }
    
}

extension NSMutableAttributedString {
    
    func trimEmptyTagsAndWhiteSpace() -> NSMutableAttributedString {
        self.mutableString.setString(self.mutableString.trimEmptyTagsAndWhiteSpace())
        return self
    }
    
}

let __firstpart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
let __serverpart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
let __emailRegex = __firstpart + "@" + __serverpart + "[A-Za-z]{2,6}"
let __emailPredicate = NSPredicate(format: "SELF MATCHES %@", __emailRegex)

extension String {
  func isEmail() -> Bool {
    return __emailPredicate.evaluate(with: self)
  }
}
