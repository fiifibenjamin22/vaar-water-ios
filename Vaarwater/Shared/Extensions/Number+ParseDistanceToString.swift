//
//  ParseDistanceToString.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 10/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import Foundation

extension Int {
    
    var parseDistanceToString: String {
        
        var output:String = " - "
        if ( self < 1000 ) {
            output = String(format: "%.0d m", self )
        } else if ( self < 10000 ) {
            output = String(format: "%.1d km", Double(self)/1000 )
        } else {
            output = String(format: "%.0d km", Double(self)/1000 )
        }
        return output
        
    }
    
//    var parseDistanceToString: String {
//        
//        var output:String = " - "
//        if ( self < 1000 ) {
//            output = String(format: "%.0d m", self )
//        } else if ( self < 10000 ) {
//            output = String(format: "%.1d km", round(self/1000) )
//        } else {
//            output = String(format: "%.0d km", round(Double(self/1000)) )
//        }
//        return output
//        
//    }
    
}

extension FloatingPoint {
    
    var parseDistanceToString: String {
        
        var output:String = " - "
        if ( self < 1000 ) {
            output = String(format: "%.0f m", self as! CVarArg)
//            output = String(format: "%.0d m", Double(self) )
        } else if ( self < 10000 ) {
            output = String(format: "%.1f km", (self/1000) as! CVarArg)
        } else {
            output = String(format: "%.0f km", (self/1000) as! CVarArg)
        }
        return output
        
    }
    
//    var parseDistanceToString: String {
//        
//        var output:String = " - "
//        if ( self < 1000 ) {
//            output = String(format: "%.0f m", [self] )
//        } else if ( self < 10000 ) {
//            output = String(format: "%.1f km", [self/1000] )
//        } else {
//            output = String(format: "%.0f km", [self/1000] )
//        }
//        return output
//    }
    
}
