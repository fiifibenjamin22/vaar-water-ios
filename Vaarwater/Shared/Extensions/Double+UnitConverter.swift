//
//  UnitConverter.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 21/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

extension Double {

    var kilometersPerHour: Double { return Double(self) * 3.6 }

}
