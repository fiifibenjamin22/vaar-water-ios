//
//  DegreesToRadians.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 06/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / .pi }
    
    var distanceToString: String {
        
        var output:String = " - "
        if ( self < 1000 ) {
            output = String(format: "%.0d m", self )
        } else if ( self < 10000 ) {
            output = String(format: "%.1d km", Double(self)/1000 )
        } else {
            output = String(format: "%.0d km", Double(self)/1000 )
        }
        return output
        
    }
    
}

extension Double {
    var kilometersPerHour: Double { return Double(self) * 3.6 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
    
    var distanceToString: String {
        
        var output:String = " - "
        if ( self < 1000 ) {
            output = String(format: "%.0f m", self as! CVarArg)
            //            output = String(format: "%.0d m", Double(self) )
        } else if ( self < 10000 ) {
            output = String(format: "%.1f km", (self/1000) as! CVarArg)
        } else {
            output = String(format: "%.0f km", (self/1000) as! CVarArg)
        }
        return output
        
    }
    
    var durationToString: String {
        
        var output:String = " - "
        if ( self < 3600 ) {
            output = String(format: "%.0f min", (self / 60) as! CVarArg )
            //            output = String(format: "%.0d m", Double(self) )
        } else {
            output = String(format: "%.0f uur", (self/3600) as! CVarArg)
        }
        return output
        
    }
  
  var minutesDurationToString: String {
    return (self * 60).durationToString
  }
}
