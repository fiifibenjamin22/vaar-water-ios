//
//  UITextView+Extensions.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 26/05/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setAttributedText( text:String ) {
        
        // Set text
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.attributedText!)
        attributedString.mutableString.setString(text)
        self.attributedText = attributedString
        
    }
    
}


extension UITextView {
    
    func setAttributedText( text:String ) {
        
        // Set text
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(attributedString: self.attributedText!)
        attributedString.mutableString.setString(text)
        self.attributedText = attributedString        
        
    }
    
}
