//
//  MarkerManager.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 16/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

class MarkerManager {
    
    final class func getImageForFamily( forTarget target: String, family: String ) -> UIImage? {
        
        var image:UIImage?
        var imageName:String = ""
        
        switch (family) {
            
        case "mooring":
            imageName = "\(target) Marker Aanmeerplek"
        case "nice-stuff":
            imageName = "\(target) Marker Leuke Dingen"
        case "toilets":
            imageName = "\(target) Marker Toilet"
        case "bridges-and-locks":
            imageName = "\(target) Marker Vaste Brug"
        case "food-and-drinks":
            imageName = "\(target) Marker Supermarkt"
        case "hospitality":
            imageName = "\(target) Marker Horeca"
        case "refueling-and-recharging":
            imageName = "\(target) Marker Tankstation"
        case "emergency":
            imageName = "\(target) Marker EHBO"
        case "boat-repairs":
            imageName = "\(target) Marker Reparatiewerkplaats"
        case "swim-location":
            imageName = "\(target) Marker Zwemlocatie"
        case "boat-rental":
            imageName = "\(target) Marker Bootverhuur"
        case "anchor":
            imageName = "\(target) Marker Anker"
        case "buoy":
            imageName = "\(target) Marker Boei"
            
        case "activities":
            imageName = "\(target) Marker Zwemlocatie"
            
//        case "reports":
//            image = UIImage(named: "Search Marker Nautische Winkel")
        default:
            imageName = "\(target) Marker Empty"
           // print("DEBUG: missing image for family "+family)
        }
        
        image = UIImage(named: imageName)
        return image
        
    }
    
    final class func getImageForCategory( forTarget target:String, category: String, highlighted: Bool = false ) -> UIImage? {
        
        var image:UIImage?
        var imageName: String = ""
        
        switch (category) {
        
        case "landing-stage":
            imageName = "\(target) Marker Aanmeerplek"
        case "mooring":
            imageName = "\(target) Marker Aanmeerplek"
        case "marina":
            imageName = "\(target) Marker Jachthaven"
        case "nice-stuff":
            imageName = "\(target) Marker Leuke Dingen"
        case "hospitality":
            imageName = "\(target) Marker Horeca"
        case "supermarket":
            imageName = "\(target) Marker Supermarkt"
        case "convenience-store":
            imageName = "\(target) Marker Avondwinkel"
        case "toilet":
            imageName = "\(target) Marker Toilet"
        case "toilets":
            imageName = "\(target) Marker Toilet"
        case "urinal":
            imageName = "\(target) Marker Urinoir"
        case "swim-location":
            imageName = "\(target) Marker Zwemlocatie"
        case "gas-station":
            imageName = "\(target) Marker Tankstation"
        case "charging-facility":
            imageName = "\(target) Marker Oplaadpunt"
        case "first-aid":
            imageName = "\(target) Marker EHBO"
        case "emergency":
            imageName = "\(target) Marker EHBO"
        case "water-police":
            imageName = "\(target) Marker Waterpolitie"
        case "fire-department":
            imageName = "\(target) Marker Brandweer"
        case "watertap":
            imageName = "\(target) Marker Watertappunt"
        case "boat-repair":
            imageName = "\(target) Marker Reparatiewerkplaats"
        case "slipway":
            imageName = "\(target) Marker Trailerhelling"
        case "nautical-store":
            imageName = "\(target) Marker Nautische Winkel"
        case "boat-rental":
            imageName = "\(target) Marker Bootverhuur"
        case "set-bridge":
            imageName = "\(target) Marker Vaste Brug"
        case "mobile-bridge":
            imageName = "\(target) Marker Beweegbare Brug"
        case "lock":
            imageName = "\(target) Marker Sluis"
        case "report":
            imageName = "\(target) Marker Melding"
        case "traffic-block":
            imageName = "\(target) Marker Stremming"
        case "impediment":
            imageName = "\(target) Marker Stremming"
        case "delay":
            imageName = "\(target) Marker Melding"
        case "trash-boat":
            imageName = "\(target) Marker Afvalboot"
        case "no-entry":
            imageName = "\(target) Marker Invaarverbod"
        case "anchor":
            imageName = "\(target) Marker Anker"
        case "low-bridge":
            imageName = "\(target) Marker Lage Brug"
        case "buoy":
            imageName = "\(target) Marker Boei"
        case "coffeehouse":
            imageName = "\(target) Marker Koffiehuis"
        case "bakery":
            imageName = "\(target) Marker Bakkerij"
        case "aed-defibrillator":
            imageName = "\(target) Marker AED"
        case "information":
            imageName = "\(target) Marker Informatie"
        case "bar":
            imageName = "\(target) Marker Bar"
        case "icecream-parlor":
            imageName = "\(target) Marker IJssalon"
        case "pharmacy":
            imageName = "\(target) Marker Apotheker"
        case "picnic":
            imageName = "\(target) Marker Picnic"
        case "bbq":
            imageName = "\(target) Marker BBQ"
        case "drinking-water":
            imageName = "\(target) Marker Drinkwater"
        case "bridges-and-locks":
            imageName = "\(target) Marker Vaste Brug"
        case "food-and-drinks":
            imageName = "\(target) Marker Supermarkt"
        case "refueling-and-recharging":
            imageName = "\(target) Marker Tankstation"
        case "boat-repairs":
            imageName = "\(target) Marker Reparatiewerkplaats"
        default:
            imageName = "\(target) Marker Empty"
         //   print("DEBUG: missing image for category: "+category)
        }
        
        if highlighted {
            imageName += " Highlighted"
        }
        image = UIImage(named: imageName )
        
        return image
        
    }

}
