//
//  Globals.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/02/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

struct Globals {
  
  // MARK: - Keys
  static let kPoiLastUpdateKey:String = "POI_LAST_UPDATE_KEY"
  static let kPoiLastFullSyncKey:String = "POI_LAST_FULL_SYNC_KEY"
  static let kPoiUpdatedNotificationKey: String = "POI_UPDATED_NOTIFICATION_KEY"
  static let kPOISelectedKey:String = "POI_SELECTED_KEY"
  static let kPOIDeselectedKey:String = "POI_DESELECTED_KEY"
  static let kPOIPinnedKey:String = "POI_PINNED_KEY"
  static let kPOIUnpinnedKey:String = "POI_UNPINNED_KEY"
  
  static let kAnchorRemovedKey: String = "ANCHOR_REMOVED_KEY"
  
  static let kLocationUpdatedKey: String = "LOCATION_UPDATED_KEY"
  static let kHeadingUpdatedKey: String = "HEADING_UPDATED_KEY"
  static let kShowedEnvironmentalWarning:String = "SHOWED_ENVIRONMENTAL_WARNING_KEY"
  static let kFirstRunVersionKey: String = "FIRST_RUN_VERSION_KEY"
  static let kStoredBuildKey:String = "STORED_BUILD_KEY"
  static let kEventSwitched: String = "EVENT_SWITCHED_KEY"
  static let kSettingsSavedKey: String = "SETTINGS_SAVED_KEY"
  
  static let kSettingsLanguageKey: String = "SETTINGS_LANGUAGE_KEY"
  static let kSettingsBoatDataKey: String = "SETTINGS_BOAT_DATA_KEY"
  static let kLanguageChangeKey: String = "LANGUAGE_CHANGE_KEY"
  static let kSettingsPreferredFamiliesKey: String = "SETTINGS_PREFERRED_FAMILIES_KEY"
  
  static let kNewsUpdatedNotificationKey: String = "NEWS_UPDATED_NOTIFICATION_KEY"
  static let kSailingDirectionChange: String = "SAILING_DIRECTION_CHANGE_KEY"
  static let kActiveRouteChange:String = "ACTIVE_ROUTE_CHANGE_KEY"
  static let kActiveDestinationChange: String = "ACTIVE_DESTINATION_CHANGE_KEY"
  
  static let kNewsItemRead:String = "NEWS_ITEM_READ_CHANGE"
  static let kLaunchCountKey:String = "LAUNCH_COUNT_KEY"
  static let kLastLaunchTodayKey:String = "LAST_LAUNCH_TODAY_KEY"
  
  static let kDataStoreName:String = "Vaarwater"
  
  static let kAppStoreURL_nl:String = "https://itunes.apple.com/nl/app/vaarwater-2/id631323264?mt=8"
  static let kAppStoreURL_en:String = "https://itunes.apple.com/app/vaarwater-2/id631323264?mt=8"
  
  
  static let kCategoriesForReports:Array = ["report", "traffic-block", "delay", "impediment", "no-entry"]
  //    static let kDefaultFamiliesOnCompass:Array = ["family_mooring", "family_nice-stuff", "family_toilets", "family_food-and-drinks", "family_swim-location"]
//  static let kDefaultFamiliesOnCompass:Array = ["family_toilets", "family_food-and-drinks", "family_mooring", "family_refueling-and-recharging", "family_emergency"]
    
    
     static let kDefaultFamiliesOnCompass:Array = ["family_activities", "family_bridges-and-locks", "family_emergency", "family_toilets"]
    
    
    
    
  //    static let categoriesOnMap:Array = ["landing-stage", "marina", "nice-stuff", "toilet", "urinal", "hospitality", "convenience-store", "supermarket", "swim-location", "first-aid"]
  
  // MARK: - Map globals
  static let kAzimuth:Double = -90.0
  static let kMinimumZoom:Float = 10.0
  static let kDefaultZoom:Float = 15.0
  static let kTrackingZoom:Float = 17.0
  static let kMaximumSpeed:Double = 9.0
  
  static let kBuoyDistanceThreshold: Double = 50.0
  
  static let kNavDestinationTreshold = 30.0
  
  // MARK: - Color globals
  static let redColor:UIColor = UIColor(red: 211.0/255.0, green: 12.0/255.0, blue: 24.0/255.0, alpha: 1)
  static let blueColor:UIColor = UIColor(red: 1.0/255.0, green: 165.0/255.0, blue: 240.0/255.0, alpha: 1)
  static let midBlueColor:UIColor = UIColor(red: 0.0/255.0, green: 111.0/255.0, blue: 161.0/255.0, alpha: 1)
  static let darkBlueColor:UIColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 80.0/255.0, alpha: 1)
  static let greenColor: UIColor = UIColor(red: 113.0/255.0, green: 205.0/255.0, blue: 90.0/255.0, alpha: 1)
  static let darkGrayColor:UIColor = UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1)
  static let grayColor:UIColor = UIColor(red: 119.0/255.0, green: 119.0/255.0, blue: 119.0/255.0, alpha: 1)
  static let lightGrayColor:UIColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1)
  
  static let kSpeedingCheckInterval:Double = 5.0
  
  init() {
    
    // Set default styling elements for appearance
    let navigationBarProxy = UINavigationBar.appearance()
    navigationBarProxy.barTintColor = UIColor(red: 0, green: 0/255, blue: 255/255, alpha: 0.98)
    navigationBarProxy.tintColor = UIColor.white
    navigationBarProxy.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue : Globals.darkBlueColor])
    
    // Customize pagecontrol
    let pageControlProxy = UIPageControl.appearance()
    pageControlProxy.pageIndicatorTintColor = UIColor.white
    pageControlProxy.currentPageIndicatorTintColor = UIColor.black
    
    
  }
  
  
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

public func getPageItems(page: UInt, allItems: Array<Dictionary<String, AnyObject>>, maxItemsPerPage: UInt) -> Array<Dictionary<String, AnyObject>> {
    let startIndex = Int(page * maxItemsPerPage)
    var length = max(0, allItems.count - startIndex)
    length = min(Int(maxItemsPerPage), length)

    guard length > 0 else { return [] }

    return Array(allItems[startIndex..<(startIndex + length)])
}

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
