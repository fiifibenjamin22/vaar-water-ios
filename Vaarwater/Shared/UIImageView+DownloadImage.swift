//
//  UIImageView+DownloadImage.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 11/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadImage(url: URL) -> URLSessionDownloadTask {
        let session = URLSession.shared
        let downloadTask = session.downloadTask(with: url, completionHandler: { [weak self] url, response, error in
            if error == nil, let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    if let weakself = self {
                        weakself.image = image
                    }
                }
            }
        })
        downloadTask.resume()
        
        return downloadTask
    }
  
  func rotate(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
    let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
    rotateAnimation.fromValue = 0.0
    rotateAnimation.toValue = CGFloat(Double.pi * 2)
    rotateAnimation.duration = duration
    
    if let delegate: AnyObject = completionDelegate {
      rotateAnimation.delegate = delegate as? CAAnimationDelegate
    }
    
    self.layer.add(rotateAnimation, forKey: nil)
  }
}
