//
//  AppDelegate.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 31/01/17.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import Firebase

// Google Maps
import GoogleMaps

// Crashlytics
import Fabric
import Crashlytics

import Localize_Swift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  var rootViewController:UIViewController?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    
    
    // Enable GoogleMaps API
    //GMSServices.provideAPIKey("AIzaSyDBQ9tIkwkZTwAkjiGIm2RqqSwFhSjI5Ms")
    
    GMSServices.provideAPIKey("AIzaSyDyrt_O8dOICqojtGEV6KgrGMDXOnR5WG0")
    
    //
    
    // enable Crashlytics
    Fabric.with([Crashlytics.self])
    
    FirebaseApp.configure()
       
    LocalizationSwizzler.executeSwizzle()
    
    self.setAppearances()
    self.checkLanguage()
    self.checkEvent()
    self.checkPreferences()
//    self.checkFirstRun()
    self.checkFirstRun2()
    
    self.updateLaunchCount()

    
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    if #available(iOS 10.0, *) {
      self.saveContext()
    } else {
      // Fallback on earlier versions
    }
  }
  
  // MARK: - Core Data stack
  
  @available(iOS 10.0, *)
  lazy var persistentContainer: NSPersistentContainer = {
    /*
     The persistent container for the application. This implementation
     creates and returns a container, having loaded the store for the
     application to it. This property is optional since there are legitimate
     error conditions that could cause the creation of the store to fail.
     */
    let container = NSPersistentContainer(name: "Vaarwater")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      if let error = error as NSError? {
        // Replace this implementation with code to handle the error appropriately.
        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        
        /*
         Typical reasons for an error here include:
         * The parent directory does not exist, cannot be created, or disallows writing.
         * The persistent store is not accessible, due to permissions or data protection when the device is locked.
         * The device is out of space.
         * The store could not be migrated to the current model version.
         Check the error message to determine what the actual problem was.
         */
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    })
    return container
  }()
  
  // MARK: - Core Data Saving support
  
  @available(iOS 10.0, *)
  func saveContext () {
    let context = persistentContainer.viewContext
    if context.hasChanges {
      do {
        try context.save()
      } catch {
        // Replace this implementation with code to handle the error appropriately.
        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
      }
    }
  }
  
  // Mark: - Custom methods
  
  func setAppearances() {
    
    // Set default styling elements for appearance
    let navigationBarProxy = UINavigationBar.appearance()
    navigationBarProxy.isTranslucent = false
    navigationBarProxy.barTintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.98)
    navigationBarProxy.shadowImage = UIImage()
    navigationBarProxy.setBackgroundImage(UIImage(),for:.default)
    navigationBarProxy.titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue : Globals.darkBlueColor])
    
    let windowProxy = UIWindow.appearance()
    windowProxy.backgroundColor = UIColor.white
  }
    
    
    func checkFirstRun() {
      
      let dictionary = Bundle.main.infoDictionary!
      let version = dictionary["CFBundleShortVersionString"] as! String
      let currentBuild = dictionary["CFBundleVersion"] as! String
      
      // Check first run
      let navController = self.window?.rootViewController as! UINavigationController
      navController.navigationBar.isHidden = true
      
      let defaults = UserDefaults.standard
     
      let storedBuild = defaults.object( forKey: Globals.kStoredBuildKey)
      
      print("required build", FirstRunViewController.requiredBuild)
      
      if storedBuild == nil || Int(storedBuild as! String)! < FirstRunViewController.requiredBuild {
        
        let storyboard = UIStoryboard(name: "FirstRun", bundle: nil)
        let firstRunVC = storyboard.instantiateViewController(withIdentifier: "FirstRunViewController") as! FirstRunViewController
        
        navController.setViewControllers([firstRunVC], animated: false)
        
        firstRunVC.onClose = ({
          
          let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
          let mainVC = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
          navController.setViewControllers([mainVC], animated: true)
          
        })
        
        // Update stored versions
        defaults.set( version, forKey:Globals.kFirstRunVersionKey)
        defaults.set( currentBuild, forKey:Globals.kStoredBuildKey)
        
        defaults.synchronize()
        
      } else {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil) //Write your storyboard name
        let mainVC = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        navController.setViewControllers([mainVC], animated: false)
      }
      if #available(iOS 13.0, *) {
                 self.window?.overrideUserInterfaceStyle = .light
             }
      self.window?.makeKeyAndVisible()
      self.rootViewController = self.window?.rootViewController
      
    }
  
  func checkFirstRun2() {
    
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let currentBuild = dictionary["CFBundleVersion"] as! String
    
    // Check first run
    let navController = self.window?.rootViewController as! UINavigationController
    navController.navigationBar.isHidden = true
    
    let defaults = UserDefaults.standard
   
    let storedBuild = defaults.object( forKey: Globals.kStoredBuildKey)
    
    print("required build", FirstRunViewController.requiredBuild)
    
//
//    let storyboard = UIStoryboard(name: "FirstRun", bundle: nil)
//          let firstRunVC = storyboard.instantiateViewController(withIdentifier: "FirstRunViewController") as! FirstRunViewController
//
//          navController.setViewControllers([firstRunVC], animated: false)
//
//          firstRunVC.onClose = ({
//
//            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let mainVC = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//            navController.setViewControllers([mainVC], animated: true)
//
//          })
    
   
    

//    let storyboard = UIStoryboard(name: "Main", bundle: nil) //Write your storyboard name
//         let mainVC = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//         navController.setViewControllers([mainVC], animated: false)
    
   
    
    
    if let _ =  getFirstTime() {

       let storyboard = UIStoryboard(name: "Main", bundle: nil) //Write your storyboard name
            let mainVC = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            navController.setViewControllers([mainVC], animated: false)

    } else {
        
        
        setFirsttime(firsttime: "1")
               
             let storyboard = UIStoryboard(name: "FirstRun", bundle: nil)
             let firstRunVC = storyboard.instantiateViewController(withIdentifier: "FirstRunViewController") as! FirstRunViewController

             navController.setViewControllers([firstRunVC], animated: false)

             firstRunVC.onClose = ({

               let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
               let mainVC = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
               navController.setViewControllers([mainVC], animated: true)

             })

             // Update stored versions
             defaults.set( version, forKey:Globals.kFirstRunVersionKey)
             defaults.set( currentBuild, forKey:Globals.kStoredBuildKey)

             defaults.synchronize()
    }
    
    
    
    
    if #available(iOS 13.0, *) {
               self.window?.overrideUserInterfaceStyle = .light
           }
    self.window?.makeKeyAndVisible()
    self.rootViewController = self.window?.rootViewController
    
  }
  
  func checkEvent() {
    
    // Set koningsdag event
    if let kingsdayStartDate = DateComponents(calendar: .current, year: 2018, month: 4, day: 25).date, let kingsdayEndDate = DateComponents(calendar: .current, year: 2018, month: 4, day: 28).date {
      if Date() >= kingsdayStartDate && Date() <= kingsdayEndDate {
        EventManager.event = KoningsdagEvent()
      }
    }
  }
    
    
 
  
   func checkLanguage() {
     
   
    
     Localize.resetCurrentLanguageToDefault()
     
     let defaults = UserDefaults.standard
     var selectedLanguage:Int = 1 // 1 is for English, default
    
//    defaults.set(["nl"], forKey: "AppleLanguages")
//       Localize.setCurrentLanguage("nl")
    
  
     
     if defaults.object(forKey: Globals.kSettingsLanguageKey) == nil {
       
        print("CURRENT_LANGUAGE: \(defaults.object(forKey: Globals.kSettingsLanguageKey))")
        
       if let systemLanguage = Locale.preferredLanguages[0] as String? {
         // consider, "fr-CA", "fr-FR", "fr-CH" et cetera
         if systemLanguage.contains("nl") {
           selectedLanguage = 0
         }
       }
       
       // Register defaults
       let defaultsDict:NSDictionary = [
         Globals.kSettingsLanguageKey: selectedLanguage as Int
       ]
       defaults.register(defaults: defaultsDict as! [String : AnyObject])
       
     } else {
       
       if let storedValue = defaults.object(forKey: Globals.kSettingsLanguageKey) as? Int,
         storedValue == 0 {
         
         selectedLanguage = 0
       }
       
     }
     
   }

  
  func checkPreferences() {
    
    // Check default array storage
    let defaults = UserDefaults.standard
    let preferredFamilies = defaults.object(forKey: Globals.kSettingsPreferredFamiliesKey) as? Array<String>
    if preferredFamilies == nil || preferredFamilies?.count == 0 {            
      defaults.set(Globals.kDefaultFamiliesOnCompass as Array<String>, forKey: Globals.kSettingsPreferredFamiliesKey)
      defaults.synchronize()
    }
    
  }
    
     func setFirsttime(firsttime: String){
        let defaults = UserDefaults.standard
        
        defaults.set(firsttime, forKey: "first")
        defaults.synchronize()
    }
    
     func getFirstTime() -> String?{
        let defaults = UserDefaults.standard
        return  defaults.string(forKey: "first")
    }

  
  func updateLaunchCount() {
    
    // Check default array storage
    let defaults = UserDefaults.standard
    var launchCount = defaults.integer(forKey: Globals.kLaunchCountKey)
    
    // Update launch count
    launchCount += 1
    
    defaults.set(launchCount as Int, forKey: Globals.kLaunchCountKey)
    defaults.synchronize()
    
  }
  
  func reloadRootViewController() {
    
    if let window = self.window {
        
       
      
      UIView.animate(
        withDuration: 0.04,
        animations: {
          //                    window.alpha = 0
      },
        completion: { (finished) in
          
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          
          // Reload root nav controller for localization switch
          let navController = storyboard.instantiateViewController(withIdentifier: "RootNavigationController") as! UINavigationController
          navController.navigationBar.isHidden = true
          
          window.rootViewController = navController
            
           
          
          self.checkFirstRun()
          
          UIView.transition(with: window,
                            duration: 0.5,
                            options: .transitionFlipFromLeft,
                            animations: { () -> Void in
                              
          }) { (finished) in
            
            //                        UIView.animate(withDuration: 0.5, animations: { 
            //                            window.alpha = 1
            //                        })
            
            //                        navController.viewControllers[0].performSegue(withIdentifier: "showSettingsSegue", sender: nil)
            
          }
          
          
      }
      )
      
    }
    
  }
  
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
  guard let input = input else { return nil }
  return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}


extension UIApplication {

    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }

}

