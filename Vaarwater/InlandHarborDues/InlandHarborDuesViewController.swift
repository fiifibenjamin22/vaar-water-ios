//
//  InlandHarborDuesViewController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 13/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift
import SafariServices

class InlandHarborDuesViewController: UIViewController {
  
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var secondaryText: UILabel!
    
    @IBOutlet weak var txtContent: UILabel!
    
    
    @IBOutlet weak var applyForVignetOutlet: UIButton!
    @IBOutlet weak var moreOneDayOutlet: UIButton!
    
    
    
    
//  @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
//
//    @IBOutlet weak var btnMoor: UIButton!
//
//    @IBOutlet weak var lblPlan: UILabel!
//
//    @IBOutlet weak var textContainer: UIView!
//
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
  override func viewDidLoad() {
    super.viewDidLoad()
    

    
    fillContentTrans()
  }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
                   let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                   let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                   statusBar.backgroundColor = .red
                   window?.addSubview(statusBar)
            } else {
        //           UIApplication.shared.statusBarView?.backgroundColor = .white
        //           UIApplication.shared.statusBarStyle = .lightContent
            }
    }
    


  
//  func fillContent() {
    
//
//    var text: String
//
//    if Localize.currentLanguage() == "nl" {
//      headerLabel.text = "Wilt u aanmeren in Amsterdam? Hiervoor betaalt u binnenhavengeld."
//      text = """
//
//      Vaar je door Amsterdam zonder aan te meren? Dan kost dit niets.
//
//      Ga je varen en aanmeren in Amsterdam? Vergeet dan niet om binnenhavengeld te betalen en een vignet aan te vragen. Een jaarvignet is een sticker die je achter op je boot plakt.
//
//      Eén dagje varen en afmeren in Amsterdam? Koop dan een dagvignet.
//
//      Nog geen vignet? Registreer snel je boot en betaal online
//
//"""
//    } else {
//      headerLabel.text = "Planning to moor in Amsterdam? You have to pay harbour fees."
//      text = """
//
//      Planning a boat trip in Amsterdam without mooring? No fee is required.
//
//      Planning a boat trip including mooring in Amsterdam? Do not forget to pay your inland port dues and apply for a vignette. Stick your annual vignette to the back of your boat.
//
//      Planning a day trip boating and mooring around Amsterdam? Buy a one-day vignette.
//
//      Need a vignette? Register your boat quickly and pay online.
//
//"""
//    }
//
//    do {
//      let htmlString = try NSMutableAttributedString(data: text.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: convertToNSAttributedStringDocumentReadingOptionKeyDictionary([convertFromNSAttributedStringDocumentAttributeKey(NSAttributedString.DocumentAttributeKey.documentType) : convertFromNSAttributedStringDocumentType(NSAttributedString.DocumentType.html)]), documentAttributes: nil)
//
//      if htmlString.length > 0 {
//        let attributes = convertFromNSAttributedStringKeyDictionary(self.textView.attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: self.textView.attributedText.length - 1)))
//
//        htmlString.addAttributes(convertToNSAttributedStringKeyDictionary(attributes), range: NSMakeRange(0, htmlString.length - 1))
//        self.textView?.attributedText = htmlString.trimEmptyTagsAndWhiteSpace()
//    }
//    } catch {
//      print(error)
//    }
//
//    let sizeThatFits = textView.sizeThatFits(CGSize(width: self.view.frame.size.width - 70, height: CGFloat(MAXFLOAT)))
//    print(self.view.frame.size.width - 70)
//    textViewHeightConstraint.constant = sizeThatFits.height - 10
//  }
  
  @IBAction func applyForVignet(_ sender: Any) {

    let svc = SFSafariViewController(url: URL(string: "https://www.digitaalvignet.nl/buy/vignette/annual")!)
    
    UIApplication.topViewController()?.present(svc, animated: true, completion: nil)

     print("Link working....")
  }


    @IBAction func btnMoorOneDay(_ sender: Any) {

        print("Link working....")
        let svc = SFSafariViewController(url: URL(string: "https://www.digitaalvignet.nl/buy/vignette/one-day/")!)
        UIApplication.topViewController()?.present(svc, animated: true, completion: nil)

    }
    
    @IBAction func handleCloseTap(_ sender: Any) {

        let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
        NotificationCenter.default.post(name: name, object: nil)

//        self.dismiss( animated: true, completion: nil)
    }
    
    
//
//  @IBAction func handleCloseTap(_ sender: Any) {
//    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
//    NotificationCenter.default.post(name: name, object: nil)
//  }
//  @IBAction func handleBackTap(_ sender: Any) {
//    self.dismiss( animated: true, completion: nil)
//  }
    
    
    @IBAction func clse(_ sender: Any) {
        
        self.dismiss(animated: true)
    }
    
    
      func fillContentTrans() {
    
        var text: String
        
       
    
        if Localize.currentLanguage() == "nl" {
            
            
            applyForVignetOutlet.setTitle("Jaarvignet aanvragen", for: .normal)
            moreOneDayOutlet.setTitle("Dagvignet aanvragen", for: .normal)
            
          headerLabel.text = "Wilt u aanmeren in Amsterdam? Hiervoor betaalt u Binnenhavengeld (BHG)."
          text = """
    
          Vaart u door Amsterdam zonder aan te meren? Dan kost dit niets. Gaat u varen en aanmeren in Amsterdam? Vergeet dan niet om binnenhavengeld te betalen en een vignet aan te vragen. Een jaarvignet is een unieke sticker die u achter op uw boot plakt.
          Eén dagje varen en afmeren in Amsterdam? Koop dan een dagvignet.
          Nog geen vignet? Registreer snel uw boot en betaal online.
    
    """
            
            txtContent.text = text
            
            secondaryText.text = """

            Wilt u slechts één dag aanmeren? Koop dan een digitaal dagvignet.
            
            """
            
            
            
        } else {
            
       applyForVignetOutlet.setTitle("Apply for an annual vignette", for: .normal)
       moreOneDayOutlet.setTitle("Request day vignette", for: .normal)
          headerLabel.text = "Do you wish to moor in Amsterdam? You need to pay Binnenhavengeld BHG"
            
        text = """
            Do you want to go boating and want to moor, you need to pay BHG. You receive a vignette upon payment. This vignette is valid for one year Jan 1st - Dec 31st and needs to be stuck on the boat.
            
            Order an annual vignette
           

           """
            
            txtContent.text = text
            
            secondaryText.text = """
             When you go boating in Amsterdam and do not need to moor you are not required to pay BHG, but please note that your boat will need a special Doorvaartvignet for passing through as per 2021.

            """
            
            
        }
        
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringDocumentReadingOptionKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.DocumentReadingOptionKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.DocumentReadingOptionKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentAttributeKey(_ input: NSAttributedString.DocumentAttributeKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentType(_ input: NSAttributedString.DocumentType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKeyDictionary(_ input: [NSAttributedString.Key: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
