////
////  TodayPoiViewController.swift
////  Vaarwater
////
////  Created by Carin Zevenbergen on 14/05/2018.
////  Copyright © 2018 Uselab. All rights reserved.
////
//
//import UIKit
//
//protocol TodayPoiViewControllerDelegate: SlidingViewControllerDelegate {
//
//}
//
//class TodayPoiViewController: SlidingViewController {
//
//  var poi: POI?
//  var todayPoiDetailVC: POIDetailViewController?
//
//
//  @IBOutlet weak var containerView: UIView!
//
//  weak var delegate: TodayPoiViewControllerDelegate?
//
//  override func viewDidLoad() {
//    super.viewDidLoad()
//
//    todayPoiDetailVC?.slidingViewDelegate = delegate
//
//    // Do any additional setup after loading the view.
//  }
//
////  override func didReceiveMemoryWarning() {
////    super.didReceiveMemoryWarning()
////    // Dispose of any resources that can be recreated.
////  }
////
//  @IBAction func close(_ sender: Any) {
//    dismiss(animated: true)
//  }
//
//  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//    if (segue.identifier == "ShowPoiDetail") {
//
//    }
//    segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
//
//    if let containedVC = segue.destination as? POIDetailViewController {
//      containedVC.poi = self.poi
//      self.todayPoiDetailVC = containedVC
//    }
//  }
//
//  /*
//   // MARK: - Navigation
//
//   // In a storyboard-based application, you will often want to do a little preparation before navigation
//   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//   // Get the new view controller using segue.destinationViewController.
//   // Pass the selected object to the new view controller.
//   }
//   */
//}
//
