//
//  TodayViewController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 05/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Localize_Swift
import RxSwift
import RxCocoa

protocol TodayViewControllerDelegate {
  func spotlightPoiSelected(sender: Any?)
  func spotlightRouteSelected(sender: Any?)
}

class TodayViewController: SlidingViewController {
  
  //  fileprivate let spotlightManager = SpotlightManager()
  
  fileprivate let formatter = DateFormatter()
  fileprivate let newsDateFormatter = DateFormatter()
  fileprivate let timeFormatter = DateFormatter()
  
  fileprivate let contentSectionInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
  
  private let locationManager = CLLocationManager()
  private let bag = DisposeBag()
  
    let spotlights = BehaviorRelay<[Spotlight]>(value: [])
  
  var spotlightHeight: CGFloat = 100
  
  var todayViewControllerDelegate: TodayViewControllerDelegate?
  
  lazy var fetchedResultsController: NSFetchedResultsController<News> = {
    let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
    
    let sortDescriptor = NSSortDescriptor(key: "createdDate", ascending: false)
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    // Initialize Fetched Results Controller
    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext, sectionNameKeyPath: nil, cacheName: nil)
    
    fetchedResultsController.delegate = self
    
    return fetchedResultsController
  }()
  
  lazy var fetchedPoiResultsController: NSFetchedResultsController<POI> = {
    let fetchRequest:NSFetchRequest<POI> = POI.fetchRequest()
    
    let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    // Initialize Fetched Results Controller
    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext, sectionNameKeyPath: nil, cacheName: nil)
    
    fetchedResultsController.delegate = self
    
    return fetchedResultsController
  }()
  
  lazy var fetchedRouteResultsController: NSFetchedResultsController<Route> = {
    let fetchRequest:NSFetchRequest<Route> = Route.fetchRequest()
    
    let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    // Initialize Fetched Results Controller
    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStackManager.sharedInstance.mainQueueContext, sectionNameKeyPath: nil, cacheName: nil)
    
    fetchedResultsController.delegate = self
    
    return fetchedResultsController
  }()
  
  
  @IBOutlet weak var dateTodayLabel: UILabel!
  @IBOutlet weak var taglineTodayLabel: UILabel!
  
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var spotlightsView: UICollectionView!
  @IBOutlet weak var messagesView: UITableView!
  @IBOutlet weak var collectionViewVerticalConstraint: NSLayoutConstraint!
  @IBOutlet weak var uvValue: UILabel!
  @IBOutlet weak var uvText: UILabel!
  @IBOutlet weak var windValue: UILabel!
  @IBOutlet weak var windText: UILabel!
  @IBOutlet weak var tempValue: UILabel!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    var cellNib = UINib(nibName: "SpotlightsCell", bundle: nil)
    spotlightsView.register(cellNib, forCellWithReuseIdentifier: "spotlightsCell")
    
    let backgroundView = UIImageView(frame: spotlightsView.frame)
    backgroundView.image = UIImage(named: "Today Background")
    backgroundView.backgroundColor = UIColor.clear
    backgroundView.isOpaque = false
    spotlightsView.backgroundView = backgroundView
    
    let contentTopBorder = CALayer()
    contentTopBorder.frame = CGRect(x: 0, y: 0, width: contentView.frame.width, height: CGFloat(1))
    contentTopBorder.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1).cgColor
    contentView.layer.addSublayer(contentTopBorder)
    
    let availableWidth = view.frame.width - contentSectionInsets.left - contentSectionInsets.right
    spotlightHeight = availableWidth * 0.9 / 2
    collectionViewVerticalConstraint.constant = spotlightHeight
    
    cellNib = UINib(nibName: "MessageCell", bundle: nil)
    messagesView.register(cellNib, forCellReuseIdentifier: "messageCell")
    
    cellNib = UINib(nibName: "NoMessagesCell", bundle: nil)
    messagesView.register(cellNib, forCellReuseIdentifier: "noMessagesCell")
    
    timeFormatter.dateStyle = .none
    timeFormatter.timeStyle = .short
    timeFormatter.timeZone = TimeZone.current
    if Localize.currentLanguage() == "nl" {
      timeFormatter.locale = Locale(identifier: "nl_NL")
    } else {
      timeFormatter.locale = Locale(identifier: "en_US")
    }
    
    newsDateFormatter.dateStyle = .long
    if Localize.currentLanguage() == "nl" {
      newsDateFormatter.locale = Locale(identifier: "nl_NL")
    } else {
      newsDateFormatter.locale = Locale(identifier: "en_US")
    }
    
    fillTodayWeather()
    fillDate()
    
    spotlights
      .asObservable()
      .subscribe(onNext: { [weak self] _ in
        DispatchQueue.main.async {
          self?.spotlightsView?.reloadData()
        }
      })
      .disposed(by: bag)
    
    getSpotlights()
    
    var predicate: NSPredicate
    if Localize.currentLanguage() == "nl" {
      predicate = NSPredicate(format: "(read == false) AND (lang == %@)", "nl")
    } else {
      predicate = NSPredicate(format: "(read == false) AND (lang == %@)", "en")
    }
    fetchedResultsController.fetchRequest.predicate = predicate
    
    do {
      try fetchedResultsController.performFetch()
    } catch let error as NSError {
      print("Fetching error: \(error), \(error.userInfo)")
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    trackScreenView()
    super.viewDidAppear(animated)
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    if segue.identifier == "ShowAllNews" {
      return
    }
    
    if let controller = segue.destination as? SlidingViewController {
      
      controller.modalPresentationStyle = .custom
      controller.slidingViewDelegate = self.presentingViewController as? SlidingViewControllerDelegate
      controller.transitioningDelegate = controller
      controller.visibleTapView = false
      
      if segue.identifier == "ShowNews", let showNewsController = controller as? TodayNewsViewController, let indexPath = sender as? IndexPath {
        showNewsController.isExpandable = false
        showNewsController.news = self.fetchedResultsController.object(at: indexPath)
        showNewsController.delegate = self
      }
      
      if segue.identifier == "ShowNews", let showNewsController = controller as? TodayNewsViewController, let news = sender as? News {
        showNewsController.isExpandable = false
        showNewsController.news = news
        showNewsController.delegate = self
      }
//
//      if segue.identifier == "ShowPoi", let showPoiController = controller as? TodayPoiViewController, let poi = sender as? POI {
//        showPoiController.isExpandable = false
//        showPoiController.poi = poi
//        showPoiController.delegate = self
//      }
//
//      if segue.identifier == "ShowRoute", let showRouteController = controller as? TodayRouteViewController, let route = sender as? Route {
//        showRouteController.isExpandable = false
//        showRouteController.route = route
//      }
    }
  }
  
  func getSpotlights() {
    let apiSpotlights = SpotlightManager.spotlights
    apiSpotlights
      .bind(to: spotlights)
      .disposed(by: bag)
  }
  
  func fillSpotlights() {
    spotlights
      .asObservable()
      .subscribe(onNext: { [weak self] _ in
        DispatchQueue.main.async {
          self?.spotlightsView?.reloadData()
        }
      })
      .disposed(by: bag)
    
    getSpotlights()
  }
  
  func fillTodayWeather() {
    let uv = OpenWeatherManager.service.getUV()
      .catchErrorJustReturn(OpenWeatherManager.UV())
      .asDriver(onErrorJustReturn: OpenWeatherManager.UV())
    
    let weatherInfo = OpenWeatherManager.service.getWeatherInfo()
      .catchErrorJustReturn(OpenWeatherManager.WeatherInfo())
      .asDriver(onErrorJustReturn: OpenWeatherManager.WeatherInfo())
    
    uv.map { $0.valueText }
      .drive(uvValue.rx.text)
      .disposed(by: bag)
    
    uv.map { $0.valueDescription.localized() }
      .drive(uvText.rx.text)
      .disposed(by: bag)
    
    weatherInfo
      .map { if let beaufort = $0.wind?.beaufort { return String(beaufort)} else { return "" }}
      .drive(windValue.rx.text)
      .disposed(by: bag)
    
    weatherInfo.map { $0.wind?.valueDescription.localized() ?? "weather.wind.none".localized() }
      .drive(windText.rx.text)
      .disposed(by: bag)
    
    weatherInfo
      .map { if let sunset = $0.sunset { return self.timeFormatter.string(from: sunset) } else { return "" } }
      .drive(tempValue.rx.text)
      .disposed(by: bag)
    
    
    weatherInfo.map { TaglineManager(beaufort: $0.wind?.beaufort, temp: $0.temperature, rain: $0.rain).tagline }
      .drive(taglineTodayLabel.rx.text)
      .disposed(by: bag)
  }
  
  func fillDate() {
    if Localize.currentLanguage() == "nl" {
      formatter.dateFormat = "d MMMM"
      formatter.locale = Locale(identifier: "nl_NL")
    } else {
      formatter.dateFormat = "MMMM d"
      formatter.locale = Locale(identifier: "en_US")
    }
    
    self.dateTodayLabel.text = formatter.string(from: Date())
  }
}

extension TodayViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return spotlights.value.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let spotlight = spotlights.value[indexPath.row]
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "spotlightsCell", for: indexPath) as! SpotlightsCell
    cell.configure(for: spotlight)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
    if let target = spotlights.value[indexPath.row].target, let id = target.id {
      if target.kind == "vaartips" {
        let predicate = NSPredicate(format: "id == %d", id)
        fetchedRouteResultsController.fetchRequest.predicate = predicate
        do {
          try fetchedRouteResultsController.performFetch()
          todayViewControllerDelegate?.spotlightRouteSelected(sender: fetchedRouteResultsController.fetchedObjects?.first)
        } catch let error as NSError {
          print(error)
        }
        
      } else if target.kind == "locaties" {
        
        let predicate = NSPredicate(format: "id == %d", id)
        fetchedPoiResultsController.fetchRequest.predicate = predicate
        
        do {
          try fetchedPoiResultsController.performFetch()
          todayViewControllerDelegate?.spotlightPoiSelected(sender: fetchedPoiResultsController.fetchedObjects?.first)
        } catch let error as NSError {
          print(error)
        }
      } else if target.kind == "nieuws" {
        
        let predicate = NSPredicate(format: "id == %d", id)
        fetchedResultsController.fetchRequest.predicate = predicate
        
        do {
          try fetchedResultsController.performFetch()
          performSegue(withIdentifier: "ShowNews", sender: fetchedResultsController.fetchedObjects?.first)
        } catch let error as NSError {
          print(error)
        }
      }
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: spotlightHeight * 2, height: spotlightHeight)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return contentSectionInsets
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return contentSectionInsets.left
  }
}

extension TodayViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if fetchedResultsController.fetchedObjects?.count ?? 0 > 0 {
      return Bundle.main.loadNibNamed("MessageHeaderCell", owner: messagesView, options: nil)?[0] as? UIView
    }
    return nil
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if fetchedResultsController.fetchedObjects?.count ?? 0 > 0 {
      return 60
    } else {
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let count = fetchedResultsController.fetchedObjects?.count {
      if count > 0 {
        return count
      }
    }
    return 1
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if fetchedResultsController.fetchedObjects?.count ?? 0 > 0 {
      let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! MessageCell
      let message = fetchedResultsController.object(at: indexPath)
      cell.configure(for: message, using: newsDateFormatter)
      cell.selectionStyle = .none
      return cell
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "noMessagesCell")!
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    if fetchedResultsController.fetchedObjects?.count ?? 0 > 0 {
      
      performSegue(withIdentifier: "ShowNews", sender: indexPath)
    } else {
      performSegue(withIdentifier: "ShowAllNews", sender: nil)
    }
  }
  
  func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    return indexPath
  }
  
}

extension TodayViewController: TodayNewsViewControllerDelegate {
  func newsRead() {
    
    var predicate: NSPredicate
    if Localize.currentLanguage() == "nl" {
      predicate = NSPredicate(format: "(read == false) AND (lang == %@)", "nl")
    } else {
      predicate = NSPredicate(format: "(read == false) AND (lang == %@)", "en")
    }
    fetchedResultsController.fetchRequest.predicate = predicate
    do {
      try fetchedResultsController.performFetch()
    } catch let error as NSError {
      print("Fetching error: \(error), \(error.userInfo)")
    }
    self.messagesView.reloadData()
  }
}

extension TodayViewController: NSFetchedResultsControllerDelegate {
  
}




