////
////  TodayRouteViewController.swift
////  Vaarwater
////
////  Created by Carin Zevenbergen on 14/05/2018.
////  Copyright © 2018 Uselab. All rights reserved.
////
//
//import UIKit
//
//class TodayRouteViewController: SlidingViewController {
//  
//  var route: Route?
//  var todayRouteDetailVC: RouteDetailViewController?
//  
//  @IBOutlet weak var closeButton: UIButton!
//  @IBOutlet weak var containerView: UIView!
//  
//  
//  override func viewDidLoad() {
//    super.viewDidLoad()
//    
//    // Do any additional setup after loading the view.
//  }
//  
//  override func didReceiveMemoryWarning() {
//    super.didReceiveMemoryWarning()
//    // Dispose of any resources that can be recreated.
//  }
//  
//  @IBAction func close(_ sender: Any) {
//    dismiss(animated: true)
//  }
//  
//  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    
//    if (segue.identifier == "ShowRouteDetail") {
//      
//    }
//    segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
//    
//    if let containedVC = segue.destination as? RouteDetailViewController {
//      containedVC.route = self.route
//      self.todayRouteDetailVC = containedVC
//    }
//  }
//  
//  /*
//   // MARK: - Navigation
//   
//   // In a storyboard-based application, you will often want to do a little preparation before navigation
//   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//   // Get the new view controller using segue.destinationViewController.
//   // Pass the selected object to the new view controller.
//   }
//   */
//}
