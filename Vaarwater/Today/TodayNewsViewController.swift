//
//  TodayNewsViewController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 25/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

protocol TodayNewsViewControllerDelegate: class {
    func newsRead()
}

class TodayNewsViewController: SlidingViewController {
    
    var news: News?
    var todayNewsDetailVC: NewsDetailViewController?

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    weak var delegate: TodayNewsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close(_ sender: Any) {
        delegate?.newsRead()
        dismiss(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "ShowNewsDetail") {

        }
        segue.destination.view.translatesAutoresizingMaskIntoConstraints = false
        
        if let containedVC = segue.destination as? NewsDetailViewController {
            containedVC.news = self.news
            containedVC.fillContentWithNewsData()
            self.todayNewsDetailVC = containedVC
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
