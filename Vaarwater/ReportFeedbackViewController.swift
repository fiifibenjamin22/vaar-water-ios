//
//  ReportFeedbackViewController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 15/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import RxSwift
import Localize_Swift

class ReportFeedbackViewController: UIViewController, CAAnimationDelegate {
  
  private let bag = DisposeBag()
  var data: Data?
  var parameters: [String: String] = [:]
  var stopAnimation = false
  
  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var statusImage: UIImageView!
  @IBOutlet weak var statusImageHeight: NSLayoutConstraint!
  @IBOutlet weak var statusImageWidth: NSLayoutConstraint!
  @IBOutlet weak var headerText: UILabel!
  @IBOutlet weak var tryAgainButton: VWButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    headerText.isHidden = true
    tryAgainButton.isHidden = true
    statusImage.rotate(completionDelegate: self)
    
    ReportManager.submit(data: data, parameters: parameters)
      .subscribe(onSuccess: { [weak self] text in
        self?.stopAnimation = true
        self?.statusImage.layer.removeAllAnimations()
        self?.messageLabel.text = "report.feedback.body.success".localized()
        self?.statusImageWidth.constant = 50
        self?.statusImageHeight.constant = 50
        self?.statusImage.image = UIImage(named: "Icon Header Melding Verstuurd")
        self?.headerText.isHidden = false
        self?.headerText.text = "report.feedback.header.success".localized()

        }, onError: { [weak self] error in
          self?.stopAnimation = true
          self?.statusImage.layer.removeAllAnimations()
          self?.messageLabel.text = "report.feedback.body.error".localized()
          self?.statusImageWidth.constant = 50
          self?.statusImageHeight.constant = 50
          self?.statusImage.image = UIImage(named: "Icon Header Melding Error")
          self?.headerText.isHidden = false
          self?.headerText.text = "report.feedback.header.error".localized()
          self?.tryAgainButton.isHidden = false

      })
      .disposed(by: bag)
    
    // Do any additional setup after loading the view.
  }
  
  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    if !stopAnimation {
      statusImage.rotate(completionDelegate: self)
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func handleTryAgainTap(_ sender: Any) {
    
  }
  
  @IBAction func handleBackTap(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
  
  @IBAction func handleCloseTap(_ sender: Any) {
    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
    NotificationCenter.default.post(name: name, object: nil)
  }
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
