//
//  Message.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 11/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation

class Message: NSObject, Codable {
    var id = 0
    var created_at: Date?
    var name = ""
    var text = ""
}

class MessagesWrapper: NSObject, Codable {
    var count = 0
    var messages = [Message]()
    
    enum CodingKeys: String, CodingKey {
        case messages = "data"
        case count = "total"
    }
}
