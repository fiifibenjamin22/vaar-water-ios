//
//  Event.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 04/04/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit

struct EventPage {
    var name_nl:String
    var name_en:String
    var text_nl:String
    var text_en:String
}

struct EventImage {
    var small:String
    var medium:String
    var large:String
    var xlarge:String
}

protocol Event {
    
    var name_en:String {get}
    var name_nl:String {get}
    
    var intro_en:String {get}
    var intro_nl:String {get}
    
    var slug:String {get}
    
    var eventColor:UIColor {get}
    
    var eventIcon:EventImage {get}
    var eventScreenIcon:EventImage {get}
    
    var eventImage:EventImage {get}
    var compassBackground:EventImage {get}
    
    var startDate:Date {get}
    var endDate:Date {get}
    
    var pages:[EventPage] {get}
}
