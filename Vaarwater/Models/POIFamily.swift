//
//  POIFamily.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

extension POIFamily {
    
    func updateFromDictionary( dictionary: Dictionary<String, AnyObject>) throws {
        
        // Only update the POIFamily if all the relevant properties can be checked
        guard let slug = dictionary["slug"] as? String,
            let nameNL = dictionary["name_nl"] as? String,
            let nameEN = dictionary["name_en"] as? String,
            let sortOrder = dictionary["sortorder"] as? Int else {
            
            let localizedDescription = NSLocalizedString("Could not interpret POIFamily data from the server.", comment: "")
        
        throw NSError(domain: "CoreData+POIFamily", code: 999, userInfo: [
            NSLocalizedDescriptionKey: localizedDescription])
        
        
        }
        
        self.setValue(slug, forKey: "slug")
        self.setValue(nameNL, forKey: "name_nl")
        self.setValue(nameEN, forKey: "name_en")
        self.setValue(sortOrder, forKey: "sortorder")
            
        if let searchDefault = dictionary["search_default"] {
            self.setValue(searchDefault, forKey: "search_default")
        }        
    
    }


}
