//
//  POICategory.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 02/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

extension POICategory {
    
    func updateFromDictionary( dictionary: Dictionary<String, AnyObject>) throws {
        
        // Only update the POICategory if all the relevant properties can be checked
        guard let id = dictionary["id"] as? Int,
            let slug = dictionary["slug"] as? String,
            let nameNL = dictionary["name_nl"] as? String,
            let nameEN = dictionary["name_en"] as? String else {
            
                let localizedDescription = NSLocalizedString("Could not interpret POICategory data from the server.", comment: "")
                
                throw NSError(domain: "CoreData+POICategory", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
                
                
        }
        
        self.setValue(id, forKey: "id")
        self.setValue(slug, forKey: "slug")
        self.setValue(nameNL, forKey: "name_nl")
        self.setValue(nameEN, forKey: "name_en")
        
        if let mapDefault = dictionary["map_default"] {
            self.setValue(mapDefault, forKey: "map_default")
        }    
        
    }
    
    
}
