//
//  GeoPosition+CoreDataProperties.swift
//  
//
//  Created by Wes Saalmink on 23/03/2017.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension GeoPosition {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GeoPosition> {
        return NSFetchRequest<GeoPosition>(entityName: "GeoPosition")
    }

    @NSManaged public var id: Int64
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var poi: POI?

}
