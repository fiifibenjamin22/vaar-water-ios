//
//  Anchor+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension Anchor {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Anchor> {
        return NSFetchRequest<Anchor>(entityName: "Anchor")
    }

    @NSManaged public var distanceToUser: Double
    @NSManaged public var id: Int64
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double

}
