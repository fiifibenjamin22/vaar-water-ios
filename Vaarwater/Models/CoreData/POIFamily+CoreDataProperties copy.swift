//
//  POIFamily+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension POIFamily {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<POIFamily> {
        return NSFetchRequest<POIFamily>(entityName: "POIFamily")
    }

    @NSManaged public var name_en: String?
    @NSManaged public var name_nl: String?
    @NSManaged public var search_default: Bool
    @NSManaged public var slug: String?
    @NSManaged public var sortorder: Int16
    @NSManaged public var categories: NSSet?

}

// MARK: Generated accessors for categories
extension POIFamily {

    @objc(addCategoriesObject:)
    @NSManaged public func addToCategories(_ value: POICategory)

    @objc(removeCategoriesObject:)
    @NSManaged public func removeFromCategories(_ value: POICategory)

    @objc(addCategories:)
    @NSManaged public func addToCategories(_ values: NSSet)

    @objc(removeCategories:)
    @NSManaged public func removeFromCategories(_ values: NSSet)

}
