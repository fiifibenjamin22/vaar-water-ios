//
//  Route+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension Route {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Route> {
        return NSFetchRequest<Route>(entityName: "Route")
    }

    @NSManaged public var createdDate: NSDate?
    @NSManaged public var distance: NSNumber?
    @NSManaged public var duration: NSNumber?
    @NSManaged public var id: NSNumber?
    @NSManaged public var mediaUrl: String?
    @NSManaged public var name: String?
    @NSManaged public var name_en: String?
    @NSManaged public var name_nl: String?
    @NSManaged public var slug: String?
    @NSManaged public var stoppedDate: NSDate?
    @NSManaged public var tagline_en: String?
    @NSManaged public var tagline_nl: String?
    @NSManaged public var text: String?
    @NSManaged public var text_en: String?
    @NSManaged public var text_nl: String?
    @NSManaged public var updatedDate: NSDate?
    @NSManaged public var positions: NSSet?

}

// MARK: Generated accessors for positions
extension Route {

    @objc(addPositionsObject:)
    @NSManaged public func addToPositions(_ value: GeoPosition)

    @objc(removePositionsObject:)
    @NSManaged public func removeFromPositions(_ value: GeoPosition)

    @objc(addPositions:)
    @NSManaged public func addToPositions(_ values: NSSet)

    @objc(removePositions:)
    @NSManaged public func removeFromPositions(_ values: NSSet)

}
