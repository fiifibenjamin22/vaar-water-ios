//
//  POICategory+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension POICategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<POICategory> {
        return NSFetchRequest<POICategory>(entityName: "POICategory")
    }

    @NSManaged public var id: Int64
    @NSManaged public var map_default: Bool
    @NSManaged public var name_en: String?
    @NSManaged public var name_nl: String?
    @NSManaged public var slug: String?
    @NSManaged public var family: POIFamily?
    @NSManaged public var items: NSSet?

}

// MARK: Generated accessors for items
extension POICategory {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: POI)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: POI)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}
