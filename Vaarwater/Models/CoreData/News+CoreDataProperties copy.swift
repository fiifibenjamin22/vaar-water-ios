//
//  News+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension News {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<News> {
        return NSFetchRequest<News>(entityName: "News")
    }

    @NSManaged public var createdDate: NSDate?
    @NSManaged public var html: String?
    @NSManaged public var id: NSNumber?
    @NSManaged public var lang: String?
    @NSManaged public var mediaTag: String?
    @NSManaged public var mediaUrl: String?
    @NSManaged public var read: Bool
    @NSManaged public var text: String?
    @NSManaged public var thumbLarge: String?
    @NSManaged public var thumbMedium: String?
    @NSManaged public var thumbSmall: String?
    @NSManaged public var title: String?
    @NSManaged public var updatedDate: NSDate?

}
