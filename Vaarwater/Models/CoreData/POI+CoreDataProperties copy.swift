//
//  POI+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension POI {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<POI> {
        return NSFetchRequest<POI>(entityName: "POI")
    }

    @NSManaged public var address: String?
    @NSManaged public var anchor: Bool
    @NSManaged public var city: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var depth: NSNumber?
    @NSManaged public var distanceToUser: Double
    @NSManaged public var email: String?
    @NSManaged public var endDate: NSDate?
    @NSManaged public var eventId: Int16
    @NSManaged public var geotype: NSNumber?
    @NSManaged public var height: NSNumber?
    @NSManaged public var html: String?
    @NSManaged public var html_en: String?
    @NSManaged public var html_nl: String?
    @NSManaged public var id: NSNumber?
    @NSManaged public var latitude: NSNumber?
    @NSManaged public var length: NSNumber?
    @NSManaged public var longitude: NSNumber?
    @NSManaged public var name: String?
    @NSManaged public var permalink: String?
    @NSManaged public var phone: String?
    @NSManaged public var pinned: Bool
    @NSManaged public var postalCode: String?
    @NSManaged public var slug: String?
    @NSManaged public var startDate: NSDate?
    @NSManaged public var text: String?
    @NSManaged public var text_en: String?
    @NSManaged public var text_nl: String?
    @NSManaged public var thumbLarge: String?
    @NSManaged public var thumbMedium: String?
    @NSManaged public var thumbSmall: String?
    @NSManaged public var updatedDate: NSDate?
    @NSManaged public var views: NSNumber?
    @NSManaged public var website: String?
    @NSManaged public var width: NSNumber?
    @NSManaged public var categories: NSSet?
    @NSManaged public var positions: NSSet?

}

// MARK: Generated accessors for categories
extension POI {

    @objc(addCategoriesObject:)
    @NSManaged public func addToCategories(_ value: POICategory)

    @objc(removeCategoriesObject:)
    @NSManaged public func removeFromCategories(_ value: POICategory)

    @objc(addCategories:)
    @NSManaged public func addToCategories(_ values: NSSet)

    @objc(removeCategories:)
    @NSManaged public func removeFromCategories(_ values: NSSet)

}

// MARK: Generated accessors for positions
extension POI {

    @objc(addPositionsObject:)
    @NSManaged public func addToPositions(_ value: GeoPosition)

    @objc(removePositionsObject:)
    @NSManaged public func removeFromPositions(_ value: GeoPosition)

    @objc(addPositions:)
    @NSManaged public func addToPositions(_ values: NSSet)

    @objc(removePositions:)
    @NSManaged public func removeFromPositions(_ values: NSSet)

}
