//
//  GeoPosition+CoreDataProperties.swift
//  
//
//  Created by Carin Zevenbergen on 23/04/2018.
//
//

import Foundation
import CoreData


extension GeoPosition {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GeoPosition> {
        return NSFetchRequest<GeoPosition>(entityName: "GeoPosition")
    }

    @NSManaged public var id: Int64
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var poi: POI?
    @NSManaged public var route: Route?

}
