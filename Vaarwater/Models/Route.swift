//
//  Route.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

extension Route {
    
    // MARK: Custom optional variables
    
    
    
    private func printAndFail<T>(message: String) -> T? {
        print(message)
        return nil
    }
    
    public class func createRouteEntity( withDictionary dictionary: Dictionary<String, AnyObject>, inContext context:NSManagedObjectContext) throws {
        
        let route = NSEntityDescription.insertNewObject(forEntityName: "Route", into: context) as! Route
        do {
            try route.updateFromDictionary(dictionary: dictionary, inContext: context)
        }
        catch {
            let localizedDescription = NSLocalizedString("Couldn't update Route data with dictionary: \(dictionary)", comment: "")
            context.delete(route)
            throw NSError(domain: "CoreData+Route", code: 999, userInfo: [
                NSLocalizedDescriptionKey: localizedDescription])
            
        }
        
    }
    
    public func updateFromDictionary( dictionary: Dictionary<String, AnyObject>, inContext context:NSManagedObjectContext) throws {
        
        // Only update the POI if all the necessary properties as met
        guard let id = dictionary["id"] as? Int,
            let name = dictionary["name"] as? String,
            let slug = dictionary["slug"] as? String,
            let text = dictionary["text"] as? String else {
                
                let localizedDescription = NSLocalizedString("Could not interpret Route data from the server.", comment: "")
                
                throw NSError(domain: "CoreData+Route", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
                
        }
        
        self.setValue(id, forKey: "id")
        self.setValue(slug, forKey: "slug")
        self.setValue(name, forKey: "name")
        self.setValue(text, forKey: "text")
        
        if let nameNL = dictionary["name_nl"] as? String {
            self.setValue(nameNL, forKey: "name_nl")
        }
        if let nameEN = dictionary["name_en"] as? String {
            self.setValue(nameEN, forKey: "name_en")
        }
        
        if let textNL = dictionary["text_nl"] as? String {
            self.setValue(textNL, forKey: "text_nl")
        }
        if let textEN = dictionary["text_en"] as? String {
            self.setValue(textEN, forKey: "text_en")
        }
        
        if let taglineNL = dictionary["tagline_nl"] as? String {
            self.setValue(taglineNL, forKey: "tagline_nl")
        }
        if let taglineEN = dictionary["tagline_en"] as? String {
            self.setValue(taglineEN, forKey: "tagline_en")
        }
        
        
        if let mediaUrl = dictionary["media_url"] as? String {
            self.setValue(mediaUrl, forKey: "mediaUrl")
        }
        
        if let duration = dictionary["duration"] as? Double {
            self.setValue(duration, forKey: "duration")
        }
        
        if let distance = dictionary["distance"] as? Double {
            self.setValue(distance, forKey: "distance")
        }
        
        if let createdDate = dictionary["created_at"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "nl_NL")
            dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            self.setValue( dateFormatter.date(from: createdDate) , forKey: "createdDate")
        }
        
        if let stoppedDate = dictionary["stopped_at"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "nl_NL")
            dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            self.setValue( dateFormatter.date(from: stoppedDate) , forKey: "stoppedDate")
        }
        
        // if positions are set, loop through and create entities. If data not valid, delete and continue
        if let positions = dictionary["positions"] as? Array<Dictionary<String, AnyObject>> {
            
            for positionDict in positions {
                
                let geoPosition = NSEntityDescription.insertNewObject(forEntityName: "GeoPosition", into: context) as! GeoPosition
                do {
                    try geoPosition.updateFromDictionary(dictionary: positionDict)
                }
                catch {
                    context.delete(geoPosition)
                }
                self.addToPositions(geoPosition)
                
            }
        }
        
    }
    
    
}
