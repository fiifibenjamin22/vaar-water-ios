//
//  BoatData.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 31/03/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import Localize_Swift

enum EngineType: Int {
    case unknown = 0
    case manual = 1
    case electric = 2
    case twoStrokeOutboardBefore2007 = 3
    case twoStrokeOutboardAfter2007 = 4
    case twoStrokeInboard = 5
    case fourStroke = 6
    case diesel = 7
    case steam = 8
    
    static var count: Int { return EngineType.steam.rawValue + 1 }
    
    var description: String {
        switch self {
        case .unknown:
            return "engineType.unknown".localized()
        case .manual:
            return "engineType.manual".localized()
        case .electric:
            return "engineType.electricEngine".localized()
        case .twoStrokeOutboardBefore2007:
            return "engineType.twoStrokeEngineOutboardBefore2007".localized()
        case .twoStrokeOutboardAfter2007:
            return "engineType.twoStrokeEngineOutboardAfter2007".localized()
        case .twoStrokeInboard:
            return "engineType.twoStrokeEngineInboard".localized()
        case .fourStroke:
            return "engineType.fourStrokeEngine".localized()
        case .diesel:
            return "engineType.diesel".localized()
        case .steam:
            return "engineType.steam".localized()
        }
    }
    
    init?(value: Int) {
        switch value {
        case 0 : self = .unknown
        case 1 : self = .manual
        case 2 : self = .electric
        case 3 : self = .twoStrokeOutboardBefore2007
        case 4 : self = .twoStrokeOutboardAfter2007
        case 5 : self = .twoStrokeInboard
        case 6 : self = .fourStroke
        case 7 : self = .diesel
        case 8 : self = .steam
        default : return nil
        }
    }

}

struct BoatData {
    
    var name:String = ""
    var engine:EngineType = EngineType.unknown
    var width:Int = 0
    var height:Int = 0
    var length:Int = 0
    
    init() {
    }
    
    init(fromDict dict: Dictionary<String, AnyObject>) {
        self.name = dict["name"] as? String ?? ""
        self.engine = EngineType(rawValue: dict["engine"] as? Int ?? 0) ?? EngineType.unknown
        self.width = dict["width"] as? Int ?? 0
        self.height = dict["height"] as? Int ?? 0
        self.length = dict["length"] as? Int ?? 0
        
    }
    
    // Encode
    public func encode() -> Dictionary<String, AnyObject> {
        
        var dictionary : Dictionary = Dictionary<String, AnyObject>()
        dictionary["name"] = name as AnyObject?
        dictionary["engine"] = engine.rawValue as AnyObject?
        dictionary["width"] = width as AnyObject?
        dictionary["height"] = height as AnyObject?
        dictionary["length"] = length as AnyObject?
        return dictionary
    }
        
}
