//
//  Anchor.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

extension Anchor {
    
    // MARK: Custom optional variables
    
    public var location:CLLocation? {
        get {
            return self.getLocation()
        }
    }
    
    private func getLocation() -> CLLocation? {
        
        // check values before returning
        if latitude > 0 && longitude > 0 {
            return CLLocation(latitude: latitude, longitude: longitude)
        }
        return nil
        
    }
    
    public func updateDistanceToUser( userLocation: CLLocation ) {
        
        guard let location = self.location else {
            return
        }
        
        self.managedObjectContext?.performAndWait({
            let calculatedDistance:Double = location.distance(from: userLocation )
            self.distanceToUser = calculatedDistance
        })
        
    }
    
    public func updateFromDictionary( dictionary: Dictionary<String, AnyObject>) throws {
        
        // Only update the POI if all the necessary properties as met
        guard let id = dictionary["id"] as? Int,
              let latitude = dictionary["latitude"] as? Double,
              let longitude = dictionary["longitude"] as? Double else {
                
                let localizedDescription = NSLocalizedString("Could not interpret Anchor data.", comment: "")
                
                throw NSError(domain: "CoreData+Anchor", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
                
        }
        
        self.setValue(id, forKey: "id")
        
        if latitude == 0 && longitude == 0 {
            
            let localizedDescription = NSLocalizedString("Location latitude/longitude is 0", comment: "")
            
            throw NSError(domain: "CoreData+Anchor", code: 999, userInfo: [
                NSLocalizedDescriptionKey: localizedDescription])
        }
        
        self.setValue(latitude, forKey: "latitude")
        self.setValue(longitude, forKey: "longitude")
        
    }
    
    
}
