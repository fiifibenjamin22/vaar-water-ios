//
//  News+CoreDataProperties.swift
//  
//
//  Created by Wes Saalmink on 28/03/2017.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension News {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<News> {
        return NSFetchRequest<News>(entityName: "News")
    }

    @NSManaged public var updatedDate: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var thumbSmall: String?
    @NSManaged public var thumbMedium: String?
    @NSManaged public var thumbLarge: String?
    @NSManaged public var text: String?
    @NSManaged public var read: Bool
    @NSManaged public var mediaUrl: String?
    @NSManaged public var id: NSNumber?
    @NSManaged public var html: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var lang: String?
    @NSManaged public var mediaTag: String?

}
