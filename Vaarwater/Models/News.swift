//
//  POI.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData

extension News {
    
    // MARK: Custom optional variables
    
    public func updateRead( _ hasRead: Bool ) {
        self.managedObjectContext?.performAndWait({
            self.read = hasRead
            do {
                try self.managedObjectContext?.save()
            }
            catch {}
        })
    }
    
    private func printAndFail<T>(message: String) -> T? {
        print(message)
        return nil
    }
    
    public func updateFromDictionary( dictionary: Dictionary<String, AnyObject>) throws {
        
        // Only update the POI if all the necessary properties as met
        guard let id = dictionary["id"] as? Int,
            let name = dictionary["name"] as? String,
            let createdDate = dictionary["created_at"] as? String,
            let text = dictionary["text"] as? String,
            let html = dictionary["html"] as? String,
            let lang = dictionary["lang"] as? String else {
                
                let localizedDescription = NSLocalizedString("Could not interpret News data from the server.", comment: "")
                
                throw NSError(domain: "CoreData+News", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
                
        }
        
        self.setValue(id, forKey: "id")
        self.setValue(name, forKey: "title")
        self.setValue(text, forKey: "text")
        self.setValue(lang, forKey: "lang")
        self.setValue(html, forKey: "html")
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "nl_NL")
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        self.setValue( dateFormatter.date(from: createdDate) , forKey: "createdDate")
        
        // Set metadata is available
        if let mediaUrl = dictionary["media_url"] {
            self.setValue(mediaUrl, forKey: "mediaUrl")
        }
        if let mediaTag = dictionary["mediaTag"] {
            self.setValue(mediaTag, forKey: "mediaTag")
        }
        if let thumbSmall = dictionary["thumbSmall"] {
            self.setValue(thumbSmall, forKey: "thumbSmall")
        }
        if let thumbMedium = dictionary["thumbMedium"] {
            self.setValue(thumbMedium, forKey: "thumbMedium")
        }
        if let thumbLarge = dictionary["thumbLarge"] {
            self.setValue(thumbLarge, forKey: "thumbLarge")
        }
        
        // Default set read
//        if self.read {
//            self.setValue(false, forKey: "read")
//        }
        
    }
    
    
}
