//
//  POIFamily+CoreDataProperties.swift
//  
//
//  Created by Wes Saalmink on 16/03/2017.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension POIFamily {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<POIFamily> {
        return NSFetchRequest<POIFamily>(entityName: "POIFamily");
    }

    @NSManaged public var name_nl: String?
    @NSManaged public var name_en: String?
    @NSManaged public var slug: String?
    @NSManaged public var sortorder: Int16
    @NSManaged public var search_default: Bool

}
