//
//  Spotlight.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 10/04/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

//protocol Spotlight {
//    var id: Int {get}
//    var createdDate: String {get}
//    var name_nl: String {get}
//    var name_en: String {get}
//    var text_nl: String {get}
//    var text_en: String {get}
//    var media: String {get}
//    var link: String {get}
//    var activeAt: String {get}
//    var activeTill: String {get}
//    var mediaUrl: String {get}
//    var thumb_small: String {get}
//    var thumb_medium: String {get}
//    var thumb_large: String {get}
//    var media_tag: String {get}
//}

class Spotlight: NSObject, Codable {
  var id = 0
  //var created_at = ""
  var name_nl = ""
  var name_en = ""
  var thumb_medium: String? = ""
  var link_nl = ""
  var link_en = ""
  
  var target: SpotlightTarget? {
    let pathComponents = link_nl.components(separatedBy: "/")
    let index = pathComponents.count
    
    var kind = ""
    var id: Int? = 0
    
    if index > 3 {
    
      kind = pathComponents[index - 3]
      id = Int(pathComponents[index - 1])
    } else if index == 3 {
      kind = pathComponents[index - 2]
      id = Int(pathComponents[index - 1])
    }
    
    return SpotlightTarget(kind: kind, id: id)

  }
}

class SpotlightsWrapper: NSObject, Codable {
  var count = 0
  var spotlights = [Spotlight]()
  
  enum CodingKeys: String, CodingKey {
    case spotlights = "data"
    case count = "total"
  }
}

struct SpotlightTarget {
  var kind: String
  var id: Int?
}

