//
//  POI.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

extension POI {
    
    // MARK: Custom optional variables     

    public var location:CLLocation? {
        get {
            return self.getLocation()
        }
    }
    
    private func getLocation() -> CLLocation? {
        
        // check values before returning
        if let latitude = self.latitude as? Double,
           let longitude = self.longitude as? Double {
            
            if latitude > 0 && longitude > 0 {
                return CLLocation(latitude: latitude, longitude: longitude)
            }
            
        }        
        return nil
        
    }
    
    public func updateLocation( forCoordinate coordinate: CLLocationCoordinate2D ) {
        
        if self.managedObjectContext != nil {
            self.managedObjectContext?.performAndWait({
                self.latitude = coordinate.latitude as NSNumber?
                self.longitude = coordinate.longitude as NSNumber?
            })
        } else {
            self.latitude = coordinate.latitude as NSNumber?
            self.longitude = coordinate.longitude as NSNumber?
        }
        
    }
    
    public func updateDistanceToUser( userLocation: CLLocation ) {
        
        guard let location = self.location else {
            return
        }
        
        self.managedObjectContext?.performAndWait({
            let calculatedDistance:Double = location.distance(from: userLocation )
            self.distanceToUser = calculatedDistance
        })
        
    }
    
    public func updatePinToCompass( _ pinned:Bool ) {
        
        self.managedObjectContext?.performAndWait({
            self.pinned = pinned
        })
        
    }
    
    public var fitsThrough:Bool {
        get {
            
            // Get boat data, or if it doesn't exist, return true
            let defaults = UserDefaults.standard
            guard let storedBoatDataDict = defaults.object(forKey: Globals.kSettingsBoatDataKey) as? Dictionary<String, AnyObject> else {
                return true
            }
            
            let boatData:BoatData = BoatData(fromDict: storedBoatDataDict)
            if let height = self.height as? Int, height > 0, boatData.height > 0, boatData.height > height {
                return false
            }
            if let width = self.width as? Int, width > 0, boatData.width > 0, boatData.width > width {
                return false
            }
            
            // else just return true
            return true
            
        }
    }
    
    private func printAndFail<T>(message: String) -> T? {
        //print(message)
        return nil
    }
    
    public class func createPOIEntity( withDictionary dictionary: Dictionary<String, AnyObject>, inContext context:NSManagedObjectContext) throws -> POI {
        
        let poi = NSEntityDescription.insertNewObject(forEntityName: "POI", into: context) as! POI
        do {
            try poi.updateFromDictionary(dictionary: dictionary, inContext: context)
        }
        catch {
            let localizedDescription = NSLocalizedString("Couldn't update Route data with dictionary: \(dictionary)", comment: "")
            context.delete(poi)
            throw NSError(domain: "CoreData+Route", code: 999, userInfo: [
                NSLocalizedDescriptionKey: localizedDescription])
        }
        
        //try context.save()
        
        return poi
        
    }
   
    public func updateFromDictionary( dictionary: Dictionary<String, AnyObject>, inContext context:NSManagedObjectContext) throws {
        
        // Only update the POI if the necessary properties are met
        guard let id = dictionary["id"] as? Int,
            let name = dictionary["name"] as? String,
            let slug = dictionary["slug"] as? String,
            let location = dictionary["location"] else {
            
                let localizedDescription = NSLocalizedString("Could not interpret POI data from the server.", comment: "")
                throw NSError(domain: "CoreData+POI", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
                
        }
        
        self.setValue(id, forKey: "id")
        self.setValue(slug, forKey: "slug")
        self.setValue(name, forKey: "name")
        self.setValue(text, forKey: "text")
        self.setValue(html, forKey: "html")
        
        if let textNL = dictionary["text_nl"] as? String {
            self.setValue(textNL, forKey: "text_nl")
        }
        if let textEN = dictionary["text_en"] as? String {
            self.setValue(textEN, forKey: "text_en")
        }
        if let htmlNL = dictionary["html_nl"] as? String {
            self.setValue(htmlNL, forKey: "html_nl")
        }
        if let htmlEN = dictionary["html_en"] as? String {
            self.setValue(htmlEN, forKey: "html_en")
        }
        
            
        self.setValue(views, forKey: "views")
        
        if let createdDate = dictionary["created_at"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "nl_NL")
            dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            self.setValue( dateFormatter.date(from: createdDate) , forKey: "createdDate")
        }

        if let latitude = location["latitude"] as? Double,
            let longitude = location["longitude"] as? Double {
            
            if latitude == 0 && longitude == 0 && id != 999998{
                
                let localizedDescription = NSLocalizedString("Location latitude/longitude is 0", comment: "")
                
                throw NSError(domain: "CoreData+POI", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
            }
            
            self.setValue(latitude, forKey: "latitude")
            self.setValue(longitude, forKey: "longitude")
            
        }
        
        // Set metadata is available
        if let metadata = dictionary["metadata"] {
            if let address = metadata["adres"] as? String {
                self.setValue( address, forKey: "address")
            }
            if let postalCode = metadata["postcode"] as? String {
                self.setValue( postalCode, forKey: "postalCode")
            }
            if let city = metadata["plaats"] as? String {
                self.setValue( city, forKey: "city")
            }
            if let email = metadata["e-mail"] as? String {
                self.setValue( email, forKey: "email")
            }
            if let phone = metadata["telefoonnummer"] as? String {
                self.setValue( phone, forKey: "phone")
            }
            if let website = metadata["website"] as? String {
                self.setValue( website, forKey: "website")
            }
            
        }
        
        // Set measurements if they exist
        if let height = dictionary["height"] as? Double {
            self.setValue(height, forKey:"height")
        }
        if let width = dictionary["width"] as? Double {
            self.setValue(width, forKey:"width")
        }
        if let length = dictionary["length"] as? Double {
            self.setValue(length, forKey:"length")
        }
        
        
        // Default set pinned
        self.setValue(false, forKey: "pinned")
        
        // Set event ID if given
        if let eventId = dictionary["event_id"] as? Int {
            self.setValue(eventId, forKey: "eventId")
        }
        
        if slug == "anchor" {
            self.setValue(true, forKey: "anchor")
        }
        
        // if a category is set, loop through and create entities. If data not valid, delete and continue
        if let categories = dictionary["categories"] as? Array<Dictionary<String, AnyObject>> {
            
            for categoryDict in categories {
                let category = NSEntityDescription.insertNewObject(forEntityName: "POICategory", into: context) as! POICategory
                do {
                    try category.updateFromDictionary(dictionary: categoryDict)
                }
                catch {
                    context.delete(category)
                }
                
                // if a category is set, loop through and create entities. If data not valid, delete and continue
                if let familyDict = categoryDict["family"] as? Dictionary<String, AnyObject> {
                    
                    let family = NSEntityDescription.insertNewObject(forEntityName: "POIFamily", into: context) as! POIFamily
                    do {
                        try family.updateFromDictionary(dictionary: familyDict)
                    }
                    catch {
                        context.delete(family)
                    }
                    
                    category.family = family
                    family.addToCategories(category)
                }
                
                self.addToCategories( category )
            }
        }
        
        // if positions are set, loop through and create entities. If data not valid, delete and continue
        if let positions = dictionary["positions"] as? Array<Dictionary<String, AnyObject>> {
            
            for positionDict in positions {
                
                let geoPosition = NSEntityDescription.insertNewObject(forEntityName: "GeoPosition", into: context) as! GeoPosition
                do {
                    try geoPosition.updateFromDictionary(dictionary: positionDict)
                }
                catch {
                    context.delete(geoPosition)
                }
                
                self.addToPositions(geoPosition)
                
            }
        }
    
    }

    
}
