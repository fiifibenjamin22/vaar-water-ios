//
//  GeoPosition.swift
//  Vaarwater
//
//  Created by Wes Saalmink on 23/02/2017.
//  Copyright © 2017 Uselab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

extension GeoPosition {
    
    // MARK: Custom optional variables
    
    public var location:CLLocation {
        get {
            return self.getLocation()
        }
    }
    
    private func getLocation() -> CLLocation {
        
        // check values before returning
        return CLLocation(latitude: latitude, longitude: longitude)
        
    }
    
    private func printAndFail<T>(message: String) -> T? {
        print(message)
        return nil
    }
    
    public func updateFromDictionary( dictionary: Dictionary<String, AnyObject>) throws {
        
        // Only update the POI if all the necessary properties as met
        guard let id = dictionary["id"] as? Int,
            let latitude = dictionary["geo_lat"] as? String,
            let latitudeDouble = Double(latitude),
            let longitude = dictionary["geo_lng"] as? String,
            let longitudeDouble = Double(longitude) else {
                
                let localizedDescription = NSLocalizedString("Could not interpret GeoPosition data from the server.", comment: "")
                
                throw NSError(domain: "CoreData+POI", code: 999, userInfo: [
                    NSLocalizedDescriptionKey: localizedDescription])
                
        }
        
        self.setValue(id, forKey: "id")
        
        self.setValue(latitudeDouble, forKey: "latitude")
        self.setValue(longitudeDouble, forKey: "longitude")
        
    }
    
    
}
