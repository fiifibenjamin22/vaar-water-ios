//
//  Itinerary.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 06/06/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import Foundation
import CoreLocation

struct Itinerary: Codable {
  var summary: Summary?
  var geometry: Geometry?

  var distance: Double? {
    return summary?.distance
  }
  var duration: Double? {
    return summary?.duration
  }

  var waypoints: [Waypoint] {
    if let coordinates = geometry?.coordinates {
      return coordinates.map {
        Waypoint(latitude: ($0[1]), longitude: $0[0])
      }
    } else {
      return [Waypoint]()
    }
  }

  static func empty() -> Itinerary {
    return Itinerary(summary: nil, geometry: nil)
  }

  enum CodingKeys: String, CodingKey {
    case summary = "properties"
    case geometry = "geometry"
  }
}

struct Geometry: Codable {
  let type: String?
  let coordinates: [[Double]]
}

struct Waypoint: Codable {
  var latitude: Double?
  var longitude: Double?

//  var location: CLLocation? {
//    if let latitude = latitude, let longitude = longitude {
//      return CLLocation(latitude: latitude, longitude: longitude)
//    } else {
//      return nil
//    }
//  }

  var location: CLLocationCoordinate2D? {
    if let latitude = latitude, let longitude = longitude {
      return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    } else {
      return nil
    }
  }
}

struct Summary: Codable {
  let duration: Double
  let distance: Double
}

