//
//  ReportCollectionViewFlowLayout.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 14/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit

class ReportCollectionViewFlowLayout: UICollectionViewFlowLayout {
  
  override init() {
    super.init()
    setupLayout()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupLayout()
  }
  
  func setupLayout() {
    minimumLineSpacing = 1
    minimumInteritemSpacing = 1
    scrollDirection = .horizontal
  }
}
