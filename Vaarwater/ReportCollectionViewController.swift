//
//  ReportCollectionViewController.swift
//  Vaarwater
//
//  Created by Carin Zevenbergen on 14/05/2018.
//  Copyright © 2018 Uselab. All rights reserved.
//

import UIKit
import SafariServices
import Localize_Swift
import Firebase

class ReportCollectionViewController: UIViewController {
  
//  fileprivate let kMeldingGemeenteURL: String = "https://www.amsterdam.nl/wonen-leefomgeving/melding-openbare/overlast-boten/"
    
  fileprivate let kMeldingGemeenteURL: String = "https://www.amsterdam.nl/wonen-leefomgeving/melding-openbare/"
  fileprivate let kMeldingOpenbareRuimteURL: String = "https://formulieren.amsterdam.nl/TriplEforms/DirectRegelen/formulier/nl-NL/evAmsterdam/scmor.aspx"
  fileprivate let kFeedbackURL_en: String = "https://www.kcmsurvey.com/qSwubc095e90e7663c4179794a3fMaSd"
  fileprivate let kFeedbackURL_nl: String = "https://www.kcmsurvey.com/qSwubc095e90e7663c4179794a3fMaSd"
  
  @IBOutlet weak var reportCollectionView: UICollectionView!
  @IBOutlet weak var collectionViewVerticalConstraint: NSLayoutConstraint!
 
  @IBAction func handleFeedbackTap(_ sender: Any) {
    let feedbackURL = (Localize.currentLanguage() == "nl") ? kFeedbackURL_nl : kFeedbackURL_en
    
    let svc = SFSafariViewController(url: URL(string: feedbackURL)!)
    UIApplication.topViewController()?.present(svc, animated: true, completion: nil)
  }
  
  @IBAction func handleBackTap(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func handleCloseTap(_ sender: Any) {
    let name:NSNotification.Name = NSNotification.Name("modallyDismissNavigationController")
    NotificationCenter.default.post(name: name, object: nil)
  }  
  
  var collectionHeight: CGFloat = 300
    
  
  var reportTypes: [ReportType] {
    return [
      ReportType(imageName: "Icon Report Te hard varen", name_nl: "Te hard varen", name_en: "Speeding", reportDestination: .gemeente),
      
     
      
      
      
//    ReportType(imageName: "Icon Report Iets anders", name_nl: "Iets anders ...", name_en: "Something else...", reportDestination: .waternet),
      ReportType(imageName: "Icon Report Misstanden", name_nl: "Gebreken borden en markeringen", name_en: "Unclear signs/markings", reportDestination: .gemeente),
      ReportType(imageName: "Icon Report Geluidsoverlast", name_nl: "Geluidsoverlast", name_en: "Excessive noise", reportDestination: .gemeente),
      ReportType(imageName: "Icon Report Gezonken boot", name_nl: "Gezonken/verkeerd afgemeerde boot", name_en: "Sunken/not properly moored boat", reportDestination: .gemeente)
    ]
  }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
//     Crashlytics.sharedInstance().crash()
    
    let cellNib = UINib(nibName: "ReportCollectionViewCell", bundle: nil)
    reportCollectionView.register(cellNib, forCellWithReuseIdentifier: "reportCollectionViewCell")
    
    collectionViewVerticalConstraint.constant = collectionHeight
  }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
                   let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                   let statusBar = UIView(frame: window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                   statusBar.backgroundColor = .red
                   window?.addSubview(statusBar)
            } else {
        //           UIApplication.shared.statusBarView?.backgroundColor = .white
        //           UIApplication.shared.statusBarStyle = .lightContent
            }
    }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showReportForm" {
      if let controller = segue.destination as? ReportFormTableViewController, let indexPath = sender as? IndexPath {
        controller.reportType = self.reportTypes[indexPath.row].name_nl
      }
    }
  }
}



extension ReportCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 4
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reportCollectionViewCell", for: indexPath) as! ReportCollectionViewCell
    let reportType = reportTypes[indexPath.row]
    cell.configure(for: reportType)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    collectionView.deselectItem(at: indexPath, animated: true)
    let destination = reportTypes[indexPath.row].reportDestination
    if destination == .gemeente {
      let svc = SFSafariViewController(url: URL(string: kMeldingGemeenteURL)!)
      UIApplication.topViewController()?.present(svc, animated: true, completion: nil)
    } else if destination == .openbareruimte {
      let svc = SFSafariViewController(url: URL(string: kMeldingOpenbareRuimteURL)!)
      UIApplication.topViewController()?.present(svc, animated: true, completion: nil)
    } else {
      performSegue(withIdentifier: "showReportForm", sender: indexPath)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let itemWidth = (self.view.frame.width - 2) / 2
    return CGSize(width: itemWidth, height: 120)
  }
}

struct ReportType {
  var imageName = ""
  var name_nl = ""
  var name_en = ""
  var reportDestination: ReportDestination = .waternet
  
  enum ReportDestination {
    case waternet
    case gemeente
    case openbareruimte
  }
}



